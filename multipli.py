"""module multipli contenant la fonction table"""

import os, sys

def table(nb, max=10):
    """Fonction affichant la table de multiplication par nb de
    1 * nb jusqu'à max * nb"""
    i = 0
    while i < max:
        print(i + 1, "*", nb, "=", (i + 1) * nb)
        i += 1

# test de la fonction table
if __name__ == "__main__":
    x = len(sys.argv)
    if x == 1:
        print("Veuillez passer une variable au script")
        os.system("pause")
    else:
        y = sys.argv[1]
        try:
            y = int(y)
        except ValueError:
            pass
        p = isinstance(y,int)
        if str(p) == "False":
            print("La variable passée n'est pas un integer")
            os.system("pause")
        else:
            table(y)
            os.system("pause")