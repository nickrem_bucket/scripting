import os
import sys
import time

from twisted.internet import defer, reactor

from stompest.config import StompConfig
from stompest.async import Stomp
from stompest.async.listener import SubscriptionListener

user = os.getenv('ACTIVEMQ_USER') or 'admin'
password = os.getenv('ACTIVEMQ_PASSWORD') or 'password'
host = os.getenv('ACTIVEMQ_HOST') or 'localhost'
port = int(os.getenv('ACTIVEMQ_PORT') or 61613)
destination = sys.argv[1:2] or ['/topic/event']
destination = destination[0]

messages = 10000

class Listener(object):
    @defer.inlineCallbacks
    def run(self):
        config = StompConfig('tcp://%s:%d' % (host, port), login=user, passcode=password, version='1.1')
        client = Stomp(config)
        yield client.connect(host='mybroker')
        
        self.count = 0
        self.start = time.time()
        client.subscribe(destination, listener=SubscriptionListener(self.handleFrame), headers={'ack': 'auto', 'id': 'required-for-STOMP-1.1'})
        
    @defer.inlineCallbacks
    def handleFrame(self, client, frame):
        self.count += 1
        if self.count == messages:
            self.stop(client)
    
    @defer.inlineCallbacks
    def stop(self, client):
        print('Disconnecting. Waiting for RECEIPT frame ...'),
        yield client.disconnect(receipt='bye')
        print('ok')
        
        diff = time.time() - self.start
        print('Received %s frames in %f seconds' % (self.count, diff))
        reactor.stop()

if __name__ == '__main__':
    Listener().run()
    reactor.run()