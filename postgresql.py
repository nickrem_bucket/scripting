import sys, psycopg2, pgpasslib, keyring, subprocess

sys.path.insert(0, 'd:\\Python\\params')
import conn_postgre
import nostri
from conn_postgre import var_database, var_user_own, var_user, var_host, var_port

from nostri import DIRECTORY
fichier = "student.csv"
fichier_absolu = DIRECTORY + "\\student.csv"

#var_password = pgpasslib.getpass(var_host, var_port, var_database, var_user)
#if not var_password:
#    raise ValueError('Did not find a password in the .pgpass file')

keyring.get_keyring()
var_password = keyring.get_password(var_database, var_user)


conn = None
try:

    conn = psycopg2.connect(database=var_database, user=var_user, password=var_password, host=var_host, port=var_port)
    print("Database opened successfully")


    cur = conn.cursor()

    #cur.execute(""" insert into STUDENT (ADMISSION, NAME, AGE, COURSE, DEPARTMENT) values (3420, 'John', 18, 1, 1)");
    #cur.execute(""" insert into STUDENT (ADMISSION, NAME, AGE, COURSE, DEPARTMENT) values (3419, 'Abel', 17, 1, 1)");
    #cur.execute(""" insert into STUDENT (ADMISSION, NAME, AGE, COURSE, DEPARTMENT) values (3421, 'Joel', 17, 1, 1)");
    #cur.execute(""" insert into STUDENT (ADMISSION, NAME, AGE, COURSE, DEPARTMENT) values (3422, 'Antony', 19, 2, 2)");
    #cur.execute(""" insert into STUDENT (ADMISSION, NAME, AGE, COURSE, DEPARTMENT) values (3423, 'Alice', 18, 3, 1)");
    #cur.execute(""" insert into STUDENT (ADMISSION, NAME, AGE, COURSE, DEPARTMENT) values (3424, 'Tom', 20, 3, 1)");
    #conn.commit()
    #conn.close()
    #exit()

    #conn.commit()
    #print("Records inserted successfully")

    #cur.execute("update STUDENT set AGE = 19 where ADMISSION = 3424")
    #conn.commit()
    #print("Total updated rows:", cur.rowcount)

    #cur.execute("delete from STUDENT;")
    #conn.commit()
    #print("Total deleted rows:", cur.rowcount)
    #exit()

    #cur.execute("insert into COURSE (NAME_COURSE) values ('test') returning id_course;");
    #id = cur.fetchone()[0]
    #print(id)
    
    cur.execute("""
        select    stu.ADMISSION
                , trim(trailing from stu.NAME)
                , stu.AGE
                , trim(trailing from cou.NAME)
                , trim(trailing from dep.NAME)
          from    STUDENT stu
                , COURSE cou
                , DEPARTMENT dep
         where  stu.COURSE     = cou.ID
           and  stu.DEPARTMENT = dep.ID
         order by stu.ADMISSION;
        """)
    rows = cur.fetchall()

    cur.close()

except psycopg2.DatabaseError as error:
    print(error)
    print("")

finally:
    if conn is not None:
        conn.close()

list_student = []
for row in rows:
    infos = [row[0], row[1], row[2], row[3], row[4]]
    list_student.append(infos)


#sys.path.insert(0, 'd:\\Python')
t = os.path.isfile(fichier_absolu)
if str(t) == "True":
    subprocess.call("del {}".format(fichier_absolu), shell=True)

filwri = None
try:
    filwri = open(fichier_absolu, "a")

    i = 0
    while i < len(list_student):
        j = 0
        while j < len(list_student[i]):
            if j == 0:
                output = str(list_student[i][j])
            else:
                output = output + ";" + str(list_student[i][j])
            j += 1
    
        output = output + "\n"
        filwri.write(output)
        i += 1

except IOError as err:
    errno, strerror = err.args
    print("Error on file {}".format(fichier_absolu))
    print("I/O error({}): {}".format(errno, strerror))
    exit()

except:
   print("Unexpected error : ", sys.exc_info()[0])
   exit()

finally:
    if filwri is not None:
        filwri.close()

print("")
print(" ADMISSION  NAME     AGE  COURSE                  DEPARTEMENT")
print(" ---------  -------  ---  ----------------------  -----------")

i = 0
while i < len(list_student):
    var_1 = str(list_student[i][0]).rjust(9)
    var_2 = str(list_student[i][1]).rjust(7)
    var_3 = str(list_student[i][2]).rjust(3)
    var_4 = str(list_student[i][3]).rjust(23)
    var_5 = str(list_student[i][4]).rjust(12)
    print(" {}  {}  {} {} {} ".format(var_1, var_2, var_3, var_4, var_5))
    i = i + 1
    
print("")


print("Operation done successfully")

conn.close()