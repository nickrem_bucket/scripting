# -*- coding: utf-8 -*-
# pylint: disable=C0111,C0103,R0205

import functools
import logging
import json
import pika
import os
import sys
import importlib

LOG_FORMAT = ('%(levelname) -10s %(asctime)s %(name) -30s %(funcName) '
              '-35s %(lineno) -5d: %(message)s')
LOGGER = logging.getLogger(__name__)

nb_params = len(sys.argv)
if nb_params == 1:
    print("Please set a variable to this script")
    exit()
elif nb_params == 2:
    params = sys.argv[1]
    file = params + ".py"
    file_absolu = "d:\\data\\module\\params\\" + file
    if not os.path.isfile(file_absolu):
        print("")
        print("The file {} is not found on the params directory".format(file))
        exit()
else:
    print("")
    print("There is too many variables set to this script")
    print("    --> Please set only one variable !")
    exit()


class RabbitmqPublisher(object):
    """
    Définition de la classe du publisher qui gère les interractions avec RabbitMQ,
    telles que les fermeture de canaux, ou encore la connexion.
    Si RabbitMQ ferme la connexion, il faut contrôle l'output générée, pour en
    comprendre la raison. C'est principalement lié à des droits/permissions, ou 
    à un timeout du socket.
    """
    #from rabbitmq_config import name_exchange, name_exchange_type, name_queue
    #from rabbitmq_config import value_publish_interval, value_routing_key
    module = importlib.import_module(params)
    locals().update(module.__dict__)
    EXCHANGE = name_exchange
    EXCHANGE_TYPE = name_exchange_type
    PUBLISH_INTERVAL = value_publish_interval
    QUEUE = name_queue
    ROUTING_KEY = value_routing_key

    def __init__(self, amqp_url):
        """
        Cette méthode configure la connexion à RabbitMQ
        """
        self._connection = None
        self._channel = None

        self._deliveries = None
        self._acked = None
        self._nacked = None
        self._message_number = None

        self._stopping = False
        self._url = amqp_url

    def connect(self):
        """
        Cette méthode exécute la connection à RabbitMQ, et renvoie le descripteur de
        connexion. Lorsque la connexion est établie, la méthode on_connection_open
        est invoquée par PIKA
        """
        label = "Connecting to {}".format(self._url)
        LOGGER.info(label)
        return pika.SelectConnection(
                                       pika.URLParameters(self._url)
                                     , on_open_callback=self.on_connection_open
                                     , on_open_error_callback=self.on_connection_open_error
                                     , on_close_callback=self.on_connection_closed
                                    )

    def on_connection_open(self, _unused_connection):
        """
        Cette méthode est appelée par PIKA une fois la connexion établie.
        La paramètre _unused_connection que la connexion est inutilisée à ce stade.
        """
        label = "Connection opened"
        LOGGER.info(label)
        self.open_channel()

    def on_connection_open_error(self, _unused_connection, err):
        """
        Cette méthode est appelée par PIKA, si la connexion à RabbitMQ ne peut pas être faite
        """
        label = "Connection open failed, reopening in 5 seconds: {}".format(err)
        LOGGER.error(label)
        self._connection.ioloop.call_later(5, self._connection.ioloop.stop)

    def on_connection_closed(self, _unused_connection, reason):
        """
        Cette méthode est appelée par PIKA, lorsque la connexion à RabbitMQ est fermée
        inopinément. Comme ce n'est pas un arrêt "normal", la méthode demande à
        RabbitMQ de se reconnecter.
        """
        self._channel = None
        if self._stopping:
            self._connection.ioloop.stop()
        else:
            label = "Connection closed, reopening in 5 seconds: {}".format(reason)
            LOGGER.warning(label)
            self._connection.ioloop.call_later(5, self._connection.ioloop.stop)

    def open_channel(self):
        """
        Cette méthode va créé un nouveau canal avec RabbitMQ, via la commande RPC
        channel.open. Dès que RabbitMQ confirme via la réponse RPC channel.openok,
        la méthode on_channel_open est invoquée.
        """
        label = "Creating a new channel"
        LOGGER.info(label)
        self._connection.channel(on_open_callback=self.on_channel_open)

    def on_channel_open(self, channel):
        """
        Cette méthode est appelée par PIKA, lorsque le channel a été ouvert.
        L'objet canal est passé en paramètre. Maintenant qu'il est ouvert, il faut
        déclarer l'EXCHANGE a utilisé.
        """
        label = "Channel opened"
        LOGGER.info(label)
        self._channel = channel
        self.add_on_channel_close_callback()
        self.setup_exchange(self.EXCHANGE)

    def add_on_channel_close_callback(self):
        """
        Cette méthode dit à PIKA d'invoquée la méthode on_channel_closed, si RabbitMQ
        ferme inopinément le canal.
        """
        label = "Adding channel close callback"
        LOGGER.info(label)
        self._channel.add_on_close_callback(self.on_channel_closed)

    def on_channel_closed(self, channel, reason):
        """
        Cette méthode est appelée par PIKA, lorsque RabbitMQ ferme inopinément le canal.
        Invoked by pika when RabbitMQ unexpectedly closes the channel.
        Les canaux sont généralement fermés si une commande viole le protocol de communication,
        tel que re-déclarer un EXCHANGE ou une QUEUE d'attente avec différents paramètres.
        Dans ce cas, la connexion est arrêtée par destruction de l'objet.
        """
        label = "Channel {} was closed: {}".format(channel, reason)
        LOGGER.warning(label)
        self._channel = None
        if not self._stopping:
            self._connection.close()

    def setup_exchange(self, exchange_name):
        """
        Cette méthode configure l'EXCHANGE dans RabbitMQ, par invocation de la commande RPC
        exchange.declare. Une fois que la commande est exécutée, la méthode on_exchange_
        declareok est exécutée par PIKA
        """
        label = "Declaring exchange {}".format(exchange_name)
        LOGGER.info(label)

        cb = functools.partial(
                                 self.on_exchange_declareok
                               , userdata=exchange_name
                              )
        self._channel.exchange_declare(
                                         exchange=exchange_name
                                       , exchange_type=self.EXCHANGE_TYPE
                                       , callback=cb
                                      )

    def on_exchange_declareok(self, _unused_frame, userdata):
        """
        Cette méthode est appelée par PIKA, lorsque RabbitMQ exécutée et terminée la commande
        RPC exchange.declare
        """
        label = "Exchange declared: {}".format(userdata)
        LOGGER.info(label)
        self.setup_queue(self.QUEUE)

    def setup_queue(self, queue_name):
        """
        Cette méthode configure la QUEUE dans RabbitMQ par invocation de la commande RPC
        queue.declare. A la fin, la méthode on_queue_declareok est appelée par PIKA.
        """
        label = "Declaring queue {}".format(queue_name)
        LOGGER.info(label)
        self._channel.queue_declare(
                                      queue=queue_name
                                    , callback=self.on_queue_declareok
                                   )

    def on_queue_declareok(self, _unused_frame):
        """
        Cette méthode est appelée par PIKA, lorsque la commande RPC queue.declare est 
        exécutée une fois le setup_queue terminée. Elle lie la QUEUE et l'EXCHANGE ensemble
        avec la ROUTING_KEY par invocation de la RPC commande queue.bind.
        Une fois que cette commande est terminée, la méthode on_bindok est appelée par PIKA.
        """
        label = "Binding {} to {} with {}".format(self.EXCHANGE, self.QUEUE, self.ROUTING_KEY)
        LOGGER.info(label)
        self._channel.queue_bind(
                                   self.QUEUE
                                 , self.EXCHANGE
                                 , routing_key=self.ROUTING_KEY
                                 , callback=self.on_bindok
                                )

    def on_bindok(self, _unused_frame):
        """
        Cette méthode est appelée par PIKA, lorsque la réponse de la commande RPC queue.bindok
        est reçue. Ainsi, il est temps de publier des messages dans RabbitMQ
        time to start publishing."""
        label = "Queue bound"
        LOGGER.info(label)
        self.start_publishing()

    def start_publishing(self):
        """
        Cette méthode active la "delivery confirmation" et planifie le premier message a envoyer
        à RabbitMQ
        """
        label = "Issuing consumer related RPC commands"
        LOGGER.info(label)
        self.enable_delivery_confirmations()
        self.schedule_next_message()

    def enable_delivery_confirmations(self):
        """
        Cette méthode exécute la commande RPC à RabbitMQ pour activer la "delivery confirmation"
        sur le canal. Le seul moyen de la désactive est de fermer le canal et d'en recréer un
        nouveau.
        Send the Confirm.Select RPC method to RabbitMQ to enable delivery
        confirmations on the channel. The only way to turn this off is to close
        the channel and create a new one.
        Lorsque le message est confirmé par RabbitMQ, la méthode on_delivery_confirmation est
        invoquée par PIKA, en retournant un basic.ack ou un basic.nack à RabbitMQ, pour lui 
        indiquer les messages OK et les messages rejetés
        """
        label = "Issuing Confirm.Select RPC command"
        LOGGER.info(label)
        self._channel.confirm_delivery(self.on_delivery_confirmation)

    def on_delivery_confirmation(self, method_frame):
        """
        Cette méthode est appelée par PIKA, lorsque RabbitMQ réponds à la commande RPC basic.publish,
        en retournant le "delivery_tag" sur le basic.ack ou le basic.nack du message publié.
        Le "delivery_tag" est un compteur indiquant le numéro de message publié sue le canal.
        Il va servir a faire le ménage dans les messages qui attendent une confirmation de livraison,
        messages stockés dans une liste utilisée pour le tracage
        """
        confirmation_type = method_frame.method.NAME.split('.')[1].lower()
        label = "Received {} for delivery tag: {}".format(confirmation_type, method_frame.method.delivery_tag)
        LOGGER.info(label)
        if confirmation_type == 'ack':
            self._acked += 1
        elif confirmation_type == 'nack':
            self._nacked += 1
        self._deliveries.remove(method_frame.method.delivery_tag)
        label = "Published {} messages, {} have yet to be confirmed, {} were acked and {} were nacked".format(self._message_number, len(self._deliveries), self._acked, self._nacked)
        LOGGER.info(label)

    def schedule_next_message(self):
        """
        Cette méthode est appelée par PIKA, tant que la connexion à RabbitMQ n'est pas fermée.
        Elle va planifier chaque nouveau message a envoyé sur RabbitMQ, toutes les xxx secondes,
        xxx prenant la valeur de PUBLISH_INTERVAL.
        """
        label = "Scheduling next message for {}.1f seconds".format(self.PUBLISH_INTERVAL)
        LOGGER.info(label)
        self._connection.ioloop.call_later(
                                             self.PUBLISH_INTERVAL
                                           , self.publish_message
                                          )

    def publish_message(self):
        """
        Tant que le classe n'est pas arrêtée, chaque message publié sur RabbitMQ est ajouter
        à une liste indexé vie le numéro du message envoyé.
        Cette liste est utilisée pour vérifier les "delivery confirmations" reçu via la 
        méthode on_delivery_confirmations.
        If the class is not stopping, publish a message to RabbitMQ,
        appending a list of deliveries with the message number that was sent.
        This list will be used to check for delivery confirmations in the
        on_delivery_confirmations method.
        Une fois le message envoyé, un nouveau message est planifié pour l'être à son tour.
        """
        if self._channel is None or not self._channel.is_open:
            return

        hdrs = {u'مفتاح': u' قيمة', u'键': u'值', u'キー': u'値'}
        module = importlib.import_module(params)
        locals().update(module.__dict__)
        #from rabbitmq_config import value_app_id, value_content_type
        properties = pika.BasicProperties(
                                            app_id=value_app_id
                                          , content_type=value_content_type
                                          , headers=hdrs
                                         )

        message = u'مفتاح قيمة 键 值 キー 値'
        self._channel.basic_publish(
                                      self.EXCHANGE
                                    , self.ROUTING_KEY
                                    , json.dumps(message, ensure_ascii=False)
                                    , properties
                                   )
        self._message_number += 1
        self._deliveries.append(self._message_number)
        label = "Published message # {}".format(self._message_number)
        LOGGER.info(label)
        self.schedule_next_message()

    def run(self):
        """
        Exécution du code de connexion à RabbitMQ, et envoi en boucle (IOLoop) des messages
        """
        while not self._stopping:
            self._connection = None
            self._deliveries = []
            self._acked = 0
            self._nacked = 0
            self._message_number = 0

            try:
                self._connection = self.connect()
                self._connection.ioloop.start()
            except KeyboardInterrupt:
                self.stop()
                if (self._connection is not None and not self._connection.is_closed):
                    # Terminer la fermeture
                    self._connection.ioloop.start()

        label = "Stopped"
        LOGGER.info(label)

    def stop(self):
        """
        Arrêt de l'exécution par fermeture du canal et de la connexion.
        Stop the example by closing the channel and connection.
        Le flag "stopping" est assigné pour indiqué à la boucle IOLoop de s'arrêtée.
        De plus, si un CTRL-C est fait, il est intercepté par cette méthode pour arrêter
        proprement canal et connexions à RabbitMQ et la boucle d'envoi IOLoop.
        """
        label= "Stopping"
        LOGGER.info(label)
        self._stopping = True
        self.close_channel()
        self.close_connection()

    def close_channel(self):
        """
        Cette méthode exécute la RPC commande channel.close, indiquant à RabbitMQ la fermeture
        du canal.
        """
        if self._channel is not None:
            label = "Closing the channel"
            LOGGER.info(label)
            self._channel.close()

    def close_connection(self):
        """
        Cette méthode exécute la RPC commande connection.close, indiquant à RabbitMQ la fermeture
        de la connexion
        """
        if self._connection is not None:
            label = "Closing connection"
            LOGGER.info(label)
            self._connection.close()


def main():
    module = importlib.import_module(params)
    globals().update(module.__dict__)
    #from rabbitmq_config import logger_level_producer
    #from rabbitmq_config import port_rabbitmq, address_rabbitmq
    #from rabbitmq_config import user_login, passwd_login
    #from rabbitmq_config import conn_attempts, conn_heartbeat
    if logger_level_producer.upper() == "DEBUG":
        logging.basicConfig(level=logging.DEBUG, format=LOG_FORMAT)
    elif logger_level_producer.upper() == "INFO":
        logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)
    elif logger_level_producer.upper() == "WARN":
        logging.basicConfig(level=logging.WARN, format=LOG_FORMAT)
    elif logger_level_producer.upper() == "ERROR":
        logging.basicConfig(level=logging.ERROR, format=LOG_FORMAT)
    elif logger_level_producer.upper() == "CRITICAL":
        logging.basicConfig(level=logging.CRITICAL, format=LOG_FORMAT)
    else:
        logging.basicConfig(level=logging.NOTSET, format=LOG_FORMAT)

    # Connect to localhost:5672 as guest with the password guest and virtual host "/" (%2F)
    amqp_url = "amqp://" + user_login + ":" + passwd_login + "@" + address_rabbitmq + ":" + port_rabbitmq + "/%2F?connection_attempts=" + str(conn_attempts) + "&heartbeat=" + str(conn_heartbeat)
    async_publisher = RabbitmqPublisher(amqp_url)
    async_publisher.run()


if __name__ == '__main__':
    main()