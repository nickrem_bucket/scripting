import os, sys
from mariadb_config import exec_sql
from nostri import DIRECTORY
from datetime import datetime

file_none = "None"
date = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
file_cmds_sql = DIRECTORY + "\\mariadb_showtables_" + date + ".sql"
file_result_sql = DIRECTORY + "\\mariadb_sql_output_" + date + ".txt"
with open(file_cmds_sql, "w") as file:
    file.write("shows tables;\n")
    
params = exec_sql("own", file_cmds_sql, file_result_sql, file_none)

if os.stat(file_result_sql).st_size == 0:
    print("\nAll SQL commands on MariaDB are OK\n")
else:
    if 'ERROR' in open(file_result_sql).read():
        error_sql = True
    else:
        error_sql = False

if error_sql == True:
    list_lines = []
    First_Error = False
    with open(file_result_sql, "r") as file:
        for line in file:
            list_lines.append(line)
    i = 0
    while i < len(list_lines):
        a = list_lines[i].rstrip()
        b = a.lstrip()
        if b.lower().startswith("erreur") or b.lower().startswith("error"):
            print(3)
            indice = i
            break
        i += 1
    a = list_lines[indice].rstrip()
    b = a.lstrip()
    temp_msg = a
    raise ValueError(temp_msg)

else:
    print("\nAll SQL commands on PostgreSQL are OK\n")