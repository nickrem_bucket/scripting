import keyring
import os
import sys
#import importlib
from cryptography.fernet import Fernet

#x = len(sys.argv)
#if x == 1:
#    print("Veuillez passer une variable au script")
#    exit()
#parameter = sys.argv[1]


def set_password_into_keying(software, type_data, type_user, type_exec):

    from cipher import get_usr_or_passwd
    from cipher import deroule_actions
    
    dict_user = {  'mariadb_root':'root', 'mariadb_own':'scott', 'mariadb_app':'scott2'
                 , 'mysql_root':'root', 'mysql_own':'scott', 'mysql_app':'scott2'
                 , 'oracle_root':'system', 'oracle_own':'scott', 'oracle_app':'scott2'
                 , 'postgresql_root':'postgres', 'postgresql_own':'postgres', 'postgresql_app':'postgres_app'
                 , 'firebird_root':'sysdba', 'firebird_own':'scott', 'firebird_app':'scott2'
                 , 'cubrid_root':'root', 'cubrid_own':'scott', 'cubrid_app':'scott2'
                }
    dict_user_mdp = {  'mariadb_root':'nicolas', 'mariadb_own':'tiger', 'mariadb_app':'tiger'
                     , 'mysql_root':'nicolas', 'mysql_own':'tiger', 'mysql_app':'tiger'
                     , 'oracle_root':'nicolas', 'oracle_own':'tiger', 'oracle_app':'tiger'
                     , 'postgresql_root':'nicolas', 'postgresql_own':'nicolas', 'postgresql_app':'nicolas'
                     , 'firebird_root':'nicolas', 'firebird_own':'tiger', 'firebird_app':'tiger'
                     , 'cubrid_root':'nicolas', 'cubrid_own':'tiger', 'cubrid_app':'tiger'
                    }
    dict_wallet = { 'wallet_own':'XE_scott', 'wallet_app':'XE_scott2' }
    
    dict_ip = { 'cubrid':'localhost:30000', 'firebird':'localhost:3050', 'mariadb':'localhost:3306', 'mysql':'localhost:3307', 'oracle':'localhost:1521', 'postgresql':'localhost:5432' }
    dict_database = { 'cubrid':'testdb', 'firebird':'test.fdb', 'mariadb':'test', 'mysql':'demo', 'oracle':'XE', 'postgresql':'postgres' } 
    

    key_search = software + "_" + type_user
    name_user = dict_user[key_search]
    passwd_user = dict_user_mdp[key_search]
    #from db_access import get_param_database
    #param = get_param_database('json', software)
    #database = param['database']
    database = dict_database[software]
    address = dict_ip[software]

    if flag_address_ip == True:
        label_software = software
        label_type = 'address'
        value = address
        if type_exec == "get":
            a = "resultat = get_usr_or_passwd({}, {})".format(label_software, label_type)
            print(a)
            resultat = get_usr_or_passwd(label_software, label_type)
            print(resultat)
        else:
            a = "deroule_actions({}, {}, {})".format(label_software, label_type, value)
            print(a)
            save_result = deroule_actions(label_software, label_type, value)
        label_type = 'database'
        value = database
        if type_exec == "get":
            a = "resultat = get_usr_or_passwd({}, {})".format(label_software, label_type)
            print(a)
            resultat = get_usr_or_passwd(label_software, label_type)
            print(resultat)
        else:
            a = "deroule_actions({}, {}, {})".format(label_software, label_type, value)
            print(a)
            save_result = deroule_actions(label_software, label_type, value)
    else:
        if type_data == "wallet":
            if software == "mariadb" or software == "mysql" or software == "postgresql":
                print("The software {} doesn't have any key wallet".format(software))
                print("    --> Exit of the program ...")
                exit()
            else:
                if type_user == "root":
                    print("The user root of database doesn't have any key wallet")
                    print("    --> Exit of the program ...")
                    exit()
                elif type_user == "own":
                    label_user_type = "wallet_own"
                else:
                    label_user_type = "wallet_app"
                label_wallet = database + "_" + name_user
                label_software = software + "_" + database
                if type_exec == "get":
                    a = "resultat = get_usr_or_passwd({}, {})".format(label_software, label_user_type)
                    print(a)
                    #resultat = get_usr_or_passwd(label_software, label_user_type)
                    #print(resultat)
                else:
                    a = "deroule_actions({}, {}, {})".format(label_software, label_user_type, label_wallet)
                    print(a)
                    #save_result = deroule_actions(label_software, label_user_type, label_wallet)
        elif type_data == "user":
            if type_user == "root":
                label_user_type = "user_root"
            elif type_user == "own":
                label_user_type = "user_own"
            else:
                label_user_type = "user_app"
            label_software = software + "_" + database
            if type_exec == "get":
                a = "resultat = get_usr_or_passwd({}, {})".format(label_software, label_user_type)
                print(a)
                resultat = get_usr_or_passwd(label_software, label_user_type)
                print(resultat)
            else:
                a = "deroule_actions({}, {}, {})".format(label_software, label_user_type, name_user)
                print(a)
                save_result = deroule_actions(label_software, label_user_type, name_user)
        else:
            if type_user == "root":
                label_passwd_type = "passwd_root"
            elif type_user == "own":
                label_passwd_type = "passwd_own"
            else:
                label_passwd_type = "passwd_app"
            label_software = software + "_" + database
            if type_exec == "get":
                a = "resultat = get_usr_or_passwd({}, {})".format(label_software, label_passwd_type)
                print(a)
                resultat = get_usr_or_passwd(label_software, label_passwd_type)
                print(resultat)
            else:
                a = "deroule_actions({}, {}, {})".format(label_software, label_passwd_type, passwd_user)
                print(a)
                save_result = deroule_actions(label_software, label_passwd_type, passwd_user)



    exit()
    #return resultat

#module = importlib.import_module(parameter)
#globals().update(module.__dict__)
#__main__()
#from cipher import deroule_actions
#from cipher import get_password


list_software = ['cubrid', 'firebird', 'mariadb', 'mysql', 'oracle', 'postgresql']
list_type_data = ['user', 'pass', 'wallet']
list_type_user = ['root', 'own', 'app']
list_type_exec = ['get', 'hash']


branche = "dynamic"
type_data = 'pass'
type_user = 'app'
type_exec = 'hash'
software = 'cubrid'
flag_address_ip = False

if branche == "dynamic":
    if software not in list_software:
        print("The Value of Software \"{}\" is not accepted".format(software))
        print("    Accpeted Values : {}".format(list_software))
        print("    --> Exit of the program ...")
        exit()
        
    if type_user not in list_type_user:
        print("The Value of Type_User \"{}\" is not accepted".format(type_user))
        print("    Accepted Values : {}".format(list_type_user))
        print("    --> Exit of the program ...")
        exit()

    if type_exec not in list_type_exec:
        print("The Value of Type_Exec \"{}\" is not accepted".format(type_exec))
        print("    Accedpted Values : {}".format(list_type_exec))
        print("    --> Exit of the program ...")
        exit()

    if type_data not in list_type_data:
        print("The Value of Type_Data \"{}\" is not accepted".format(type_data))
        print("    Accedpted Values : {}".format(list_type_data))
        print("    --> Exit of the program ...")
        exit()

    resultat = set_password_into_keying(software, type_data, type_user, type_exec)
else:
    print(1)
    exit()
    if soft == 'mariadb':
        alert, resultat = calculate_mariadb(execution_type, user_type, get_or_hash)
    elif soft == 'mysql':
        alert, resultat = calculate_mysql(execution_type, user_type, get_or_hash)
    elif soft == 'oracle':
        alert, resultat = calculate_oracle(execution_type, user_type, get_or_hash)
        if user_type == 'root':
            calculate_wallet_oracle()
    elif soft == 'postgresql':
        alert, resultat = calculate_postgresql(execution_type, user_type, get_or_hash)
    else:
        alert = "KO"
        resultat = "Wrong soft"

a = " - Software       : " + soft
print(a)
a = " - Execution Type : " + execution_type
print(a)
a = " - User Type      : " + user_type
print(a)
a = " - Get or Hash    : " + get_or_hash
print(a)
if alert == 'KO':
    print("")
    print(" ***** ERROR ***** ")
    valeur = "    ---> " + resultat
    print(valeur)
    exit()
else:
    print("")
    print(" ***** EXECUTION OK ***** ")
    valeur = "    ---> " + resultat
    print(valeur)
    exit()


exit()
        

# mot de passe des users sauvegarder dans keyring
#software = 'postgresql'
#type = 'user'
#user = 'postgres_app'
#plain_text = 'nicolas'
#resultat = deroule_actions(software, type, user, plain_text)
#resultat = get_password(software, type, user)
#print(resultat)

#software = 'oracle'
#type = 'user'
#user = 'system'
#user = 'scott'
#user = 'scott2'
#plain_text = 'nicolas'
#plain_text = 'tiger'
#plain_text = 'tiger'
#resultat = deroule_actions(software, type, user, plain_text)
#resultat = get_password(software, type, user)
#print(resultat)

"""software = 'mysql'
type = 'user'
user = 'root'
plain_text = 'nicolas'
resultat = deroule_actions(software, type, user, plain_text)
print(resultat)

software = 'mysql'
type = 'user'
user = 'scott'
plain_text = 'tiger'
resultat = deroule_actions(software, type, user, plain_text)
print(resultat)

software = 'mysql'
type = 'user'
user = 'scott2'
plain_text = 'tiger'
resultat = deroule_actions(software, type, user, plain_text)
print(resultat)

software = 'mariadb'
type = 'user'
user = 'root'
plain_text = 'nicolas'
resultat = deroule_actions(software, type, user, plain_text)
print(resultat)

software = 'mariadb'
type = 'user'
user = 'scott'
plain_text = 'tiger'
resultat = deroule_actions(software, type, user, plain_text)
print(resultat)

software = 'mariadb'
type = 'user'
user = 'scott2'
plain_text = 'tiger'
resultat = deroule_actions(software, type, user, plain_text)
print(resultat)"""


exit()


#keyring.set_password("zip-file", "zip_passwd", "nicolas")
#keyring.set_password('zip-file','zip_passwd','nicolas')
#keyring.set_password('mariadb_test', 'scott2', 'tiger')
#keyring.set_password("gpg-key", "gpg-passwd", "Azerty*6789*")
#keyring.set_password("postgres_postgres", "postgres", "nicolas")
#keyring.set_password("oracle_xe", "scott", "tiger")
#keyring.set_password("oracle_xe", "scott2", "tiger")
#keyring.set_password("mysql_demo", "scott", "tiger")
#keyring.set_password("mysql_demo", "scott2", "tiger")
#keyring.set_password("mysql_demo", "root", "nicolas")
#keyring.set_password("mariadb_test", "root", "nicolas")
#password = keyring.get_password("mysql_demo", "scott")
#print(password)
#keyring.delete_password("postgres_postgres", "postgres")
#password = keyring.get_password("postgres_postgres", "postgres")
#print(password)
exit()


sys.path.insert(0, 'd:\\Python')
try:
    os.remove("notes24.txt")


#except IOError as err:
#    errno, strerror = err.args
#    print("Error on file {}".format(err.filename))
#    print("I/O error({}): {}".format(errno, strerror))
#    exit()

except OSError as err:
    errno, strerror = err.args
    print("Error on file {}".format(err.filename))
    print("OS error({}): {}".format(errno, strerror))
    #print("OS Error: %s - %s." % (err.filename, err.strerror))

except NameError as err:
    print(err)
    exit()

except ValueError as err:
    print(err)
    exit()

except:
   print("Unexpected error : ", sys.exc_info()[0])
   exit()