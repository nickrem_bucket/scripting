import pyAesCrypt
import keyring
import os
from cryptography.fernet import Fernet

message = "This is a secret message"
length = len(message)
#key = os.urandom(length)
#print(key)
#key = Fernet.generate_key()
#key_decode = key.decode()
#key_decode = "S_nI9NPJlSgkdhRsPl-nGNwMhAJXN4FmbIbIIJwswDM="
#print(key_decode)
label1 = "secure_parameter_file"
label2 = "key_secure_file"
#keyring.set_password(label1, label2, key_decode)
#passw = keyring.get_password(label1, label2)
#print(passw)


# Encrypt/Decrypt buffer size - 64K
bufferSize = 64 * 1024
#password = "KHFkjsqlkJDLKQfbekgjbdsrlgkjbfQ>dvsgsbjfgsekbfwdgbHGJHfkzrwmlfgiàrruà-tlmfg!g"
password = keyring.get_password(label1, label2)
print(password)

file_input = "d:\\data\\module\\params\\database.yaml"
file_output = "d:\\data\\module\\params\\database.aes.yaml"
new_file = "d:\\Python\database2.ini"

#if os.path.isfile(file_output):
#    os.remove(file_output)
#if os.path.isfile(new_file):
#    os.remove(new_file)

function = "encrypt"

if function == "encrypt":
    with open(file_input, "rb") as fIn:
        with open(file_output, "wb") as fOut:
            pyAesCrypt.encryptStream(fIn, fOut, password, bufferSize)
elif function == "decrypt":
    # get encrypt file size
    encFileSize = os.stat(file_output).st_size

    # decrypt
    with open(file_output, "rb") as fIn:
        try:
            with open(new_file, "wb") as fOut:
                # decrypt file stream
                pyAesCrypt.decryptStream(fIn, fOut, password, bufferSize, encFileSize)
        except ValueError:
            # remove output file on error
            os.remove(new_file)