select  stu.ADMISSION
        || ';' ||
        trim(stu.NAME)
		|| ';' ||
        stu.AGE
		|| ';' ||
        trim(cou.NAME)
		|| ';' ||
        trim(dep.NAME)
		|| ';' ||
        trim(to_char((cou.AMOUNT/100),'9999999999D00'))
  from    STUDENT stu
        , COURSE cou
        , DEPARTMENT dep
 where
         (     stu.COURSE     = cou.ID
          and  stu.DEPARTMENT = dep.ID)
 order by stu.ADMISSION;