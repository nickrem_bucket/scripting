import time

print("Pensez à un nombre entre 1 et 100.")
time.sleep(2)


fin = 0
nb = 50
i = 0
borne_min = 1
borne_max = 100
while fin == 0:
    question = input("Est-ce que votre nombre est plus grand, plus petit ou égal à {} ? [+/-/=] ".format(nb))
    if question == "+":
        new_nb = int(nb) + (int(borne_max) - int(nb))/2
        nb = int(new_nb)
        i += 1
    elif question == "-":
        new_borne_max = int(nb) + (int(borne_max) - int(nb))/2
        new_nb = int(nb) - (int(borne_max) - int(nb))/2
        nb = int(new_nb)
        borne_max = int(new_borne_max)
        i = i + 1
    elif question == "=":
        if i == 1:
            print("J'ai trouvé en 1 question !")
        else:
            print("J'ai trouvé en {} questions !".format(i))
        fin = 1

