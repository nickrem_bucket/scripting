import os
import json
import time
from cassandra.cluster import Cluster, ExecutionProfile

# Create the cluster connection.
cluster = Cluster(['127.0.0.1'])

profile = ExecutionProfile()
profile.request_timeout = 60
cluster.add_execution_profile('standard', profile)
session = cluster.connect()

# Create the keyspace.
session.execute("""
    create keyspace if not exists TEST;
""")
print("Created keyspace Test")

# Create the table.
session.execute("""
    create table if not exists TEST.TEST_TABLE (
              id int,
            , data jsonb
            , primary key (ID)
    );
  """)
print("Created table employee")


dirname = '/home/ajung/content_plone_portal_2019-07-19-13-10-34'

i = 0
for dirname, dirnames, filenames in os.walk(dirname):

    for filename in filenames:
        fn = os.path.join(dirname, filename)
        if not fn.endswith('.json'):
            continue
        with open(fn) as fp:
            data = fp.read()

        print(fn, len(data))
        i = i + 1

        data = data.replace("'", "")
        data = json.loads(data)
        data = json.dumps(data, sort_keys=True)
        try:
            ts = time.time()
            s = f"INSERT INTO ybdemo.ugent(id, data) VALUES ({i}, '{data}'); """
            session.execute(s, execution_profile=profile)
        except Exception as e:
            raise
        finally:
            print(time.time() - ts)

# Close the connection.
cluster.shutdown()