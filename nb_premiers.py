import sys

x = len(sys.argv)
if x == 1:
    print("Veuillez passer une variable au script")
    exit()

y = sys.argv[1]
try:
    y = int(y)
except ValueError:
    pass
p = isinstance(y,int)
if str(p) == "False":
    print("La variable passée n'est pas un integer")
    exit()

liste = []
for i in range(int(y)):
    if i != 0:
        i2 = i + 1
        nbpremier = 0
        for j in range(12):
            if j != 0:
                b = i % j
                if b == 0:
                    nbpremier += 1
        if nbpremier == 2:
            liste.append(i)
				
i = 0
while i < len(liste):
    if i == 0:
        list_disp = str(liste[i])
    else:
        list_disp = list_disp + ", " + str(liste[i])
    i += 1
	
print("")
print("Liste des nombres premiers inférieur : ")
print("     - Nombre parametre : {}".format(y))
print("     - Liste : {}".format(list_disp))