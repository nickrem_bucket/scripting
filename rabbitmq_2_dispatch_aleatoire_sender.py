import pika, sys

# Script de test, envoi du message "Hello World", à la queue Hello via un exchange
# message passé via la ligne de commande

connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.queue_declare(
                        queue='hello'
                     )

message = ' '.join(sys.argv[1:]) or "Hello World!"

channel.basic_publish(
                        exchange=''
                      , routing_key='hello'
                      , body=message
                     )
                      
print(" [x] Sent %r" % message)

connection.close()