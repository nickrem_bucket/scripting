import pika, os, time

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='hello')

# Avec le time.sleep, on joue sur le temps avant d'envoyer l'acquitement du messages
# Si un des receiver est kill, celui qui est actif prend en charge
# Si tous les receivers sont off, au redémarrage, c'est le premier actif qui retraite tout
def callback(ch, method, properties, body):
    print(" [x] Received %r" % body)
    time.sleep(body.count(b'.'))
    #time.sleep(10)
    print(" [x] Done")
    ch.basic_ack(delivery_tag = method.delivery_tag)


channel.basic_consume(  queue='hello'
                      , on_message_callback=callback
                      #, auto_ack=True
                     )

print(' [*] Waiting for messages. To exit press CTRL+C')


channel.start_consuming()
