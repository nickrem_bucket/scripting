inventaire = [ ("pommes", 22), ("melons", 4), ("poires", 4), ("fraises", 76), ("prunes", 51),]

# changement de l'ordre de la liste pour trié sur la quantité
inventaire_inverse = [(quantite, nom_fruit) for nom_fruit, quantite in inventaire]

print(inventaire)
print(inventaire_inverse)

# tri de la liste inventaire sur la quantité
inventaire_bis = [(nom_fruit, quantite) for quantite, nom_fruit in sorted(inventaire_inverse, reverse=True)]

print(inventaire_bis)