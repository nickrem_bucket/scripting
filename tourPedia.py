import pymongo
import re

from pymongo import MongoClient, DESCENDING
from pprint import pprint
from bson import ObjectId
from bson import Code
from datetime import datetime

client = MongoClient('mongodb://localhost:27017/')

with client:

    #fichier = "resultat_mongodb.txt"
    #filwri = open(fichier, "w")
    #db = client.new_york
    db = client.tourPedia
    # Premier test, operation find / find_one
    docs = db.paris.find( { "category" : "accommodation" , "services":"blanchisserie" }, { "location.address":1} ).limit(200)
    #docs = db.paris.distinct("location.address")
    #print(docs.next())
    for doc in docs:
        #filwri.write(str(doc))
        print(doc)
        #print(doc['location'])
    #filwri.close()
    
    """docs = db.restaurants.find_one()
    print(docs)"""
    
    #    D:\Python>python3 restaurants.py
    #    {'_id': ObjectId('5dceba07f5a868598a76ef47'), 'address': {'building': '351', 'coord': {'type': 'Point', 'coordinates': [-73.98513559999999, 40.7676919]}, 'street': 'West   57 Street', 'zipcode': '10019'}, 'borough': 'Manhattan', 'cuisine': 'Irish', 'grades': [{'date': datetime.datetime(2014, 9, 6, 0, 0), 'grade': 'A', 'score': 2}, {'date': datetime.datetime(2013, 7, 22, 0, 0), 'grade': 'A', 'score': 11}, {'date': datetime.datetime(2012, 7, 31, 0, 0), 'grade': 'A', 'score': 12}, {'date': datetime.datetime(2011, 12, 29, 0, 0), 'grade': 'A', 'score': 12}], 'name': 'Dj Reynolds Pub And Restaurant', 'restaurant_id': '30191841', 'note': 12}


    # 2eme test, utilisation find avec filtre
    """docs = db.restaurants.find( { "borough" : "Brooklyn" } )
    for doc in docs:
        print(doc)"""

    #    D:\Python>python3 restaurants.py
    #    {'_id': ObjectId('5dceba07f5a868598a76f151'), 'address': {'building': '854', 'coord': {'type': 'Point', 'coordinates': [-73.94994129999999, 40.67140699999999]}, 'street': 'St Johns Place', 'zipcode': '11216'}, 'borough': 'Brooklyn', 'cuisine': 'Caribbean', 'grades': [{'date': datetime.datetime(2014, 10, 11, 0, 0), 'grade': 'A', 'score': 13}, {'date': datetime.datetime(2014, 3, 5, 0, 0), 'grade': 'B', 'score': 14}, {'date': datetime.datetime(2013, 7, 30, 0, 0), 'grade': 'B', 'score': 16}, {'date': datetime.datetime(2012, 6, 12, 0, 0), 'grade': 'A', 'score': 11}, {'date': datetime.datetime(2012, 1, 27, 0, 0), 'grade': 'A', 'score': 12}], 'name': "Glenda'S Restaurant", 'restaurant_id': '40382147', 'comment': 'acceptable'}
    #    {'_id': ObjectId('5dceba07f5a868598a76f152'), 'address': {'building': '62', 'coord': {'type': 'Point', 'coordinates': [-73.974896, 40.675678]}, 'street': '7 Avenue', 'zipcode': '11217'}, 'borough': 'Brooklyn', 'cuisine': 'Tex-Mex', 'grades': [{'date': datetime.datetime(2014, 9, 26, 0, 0), 'grade': 'A', 'score': 13}, {'date': datetime.datetime(2014, 3, 13, 0, 0), 'grade': 'A', 'score': 8}, {'date': datetime.datetime(2013, 6, 12, 0, 0), 'grade': 'A', 'score': 12}, {'date': datetime.datetime(2012, 12, 3, 0, 0), 'grade': 'A', 'score': 9}, {'date': datetime.datetime(2012, 2, 13, 0, 0), 'grade': 'B', 'score': 21}], 'name': 'Santa Fe Grill & Bar', 'restaurant_id': '40382302', 'comment': 'acceptable'}
    #    ...

    """docs = db.restaurants.find( { "borough" : "Brooklyn" } ).limit(100)
    for doc in docs:
        #print(doc)
        print("Name    : {}".format(doc['name']))
        print("Cuisine : {}".format(doc['cuisine']))
        print("Address : Building {} , Street {} , zipcode {}".format(doc['address']["building"], doc["address"]["street"], doc["address"]["zipcode"]))
        i = 0
        while i < len(doc["grades"]):
            a = doc["grades"][i]['grade']
            b = doc["grades"][i]['date'].strftime('%Y-%m-%d')
            print("Grade   : {} (date : {})".format(a, b))
            i +=1
        print("")"""

    """docs = db.restaurants.find( { "borough" : "Brooklyn" } ).count()
    print(docs)"""


    # 3eme test, utilisation find avec multiplication des filtres
    """docs = db.restaurants.find(
        {
              "borough" : "Brooklyn"
            , "cuisine" : "Italian"
        }
    )
    for doc in docs:
        print(doc)"""

    #    D:\Python>python3 restaurants.py
    #    {'_id': ObjectId('5dceba07f5a868598a76ef83'), 'address': {'building': '10004', 'coord': {'type': 'Point', 'coordinates': [-74.03400479999999, 40.6127077]}, 'street': '4 Avenue', 'zipcode': '11209'}, 'borough': 'Brooklyn', 'cuisine': 'Italian', 'grades': [{'date': datetime.datetime(2014, 2, 25, 0, 0), 'grade': 'A', 'score': 12}, {'date': datetime.datetime(2013, 6, 27, 0, 0), 'grade': 'A', 'score': 7}, {'date': datetime.datetime(2012, 12, 3, 0, 0), 'grade': 'A', 'score': 10}, {'date': datetime.datetime(2011, 11, 9, 0, 0), 'grade': 'A', 'score': 12}], 'name': 'Philadelhia Grille Express', 'restaurant_id': '40364305', 'comment': 'acceptable'}
    #    {'_id': ObjectId('5dceba07f5a868598a76efad'), 'address': {'building': '7201', 'coord': {'type': 'Point', 'coordinates': [-74.0166091, 40.6284767]}, 'street': '8 Avenue', 'zipcode': '11228'}, 'borough': 'Brooklyn', 'cuisine': 'Italian', 'grades': [{'date': datetime.datetime(2014, 12, 4, 0, 0), 'grade': 'A', 'score': 11}, {'date': datetime.datetime(2014, 2, 19, 0, 0), 'grade': 'A', 'score': 10}, {'date': datetime.datetime(2013, 7, 9, 0, 0), 'grade': 'A', 'score': 9}, {'date': datetime.datetime(2012, 6, 6, 0, 0), 'grade': 'A', 'score': 10}, {'date': datetime.datetime(2011, 12, 19, 0, 0), 'grade': 'A', 'score': 12}], 'name': 'New Corner', 'restaurant_id': '40365355', 'comment': 'acceptable'}
    #    ...
    
    # 3eme test, decompte du filtre
    """docs = db.restaurants.find(
        {
              "borough" : "Brooklyn"
            , "cuisine" : "Italian"
        }
    ).count()
    print(docs)"""

    #    D:\Python>python3 restaurants.py
    #    restaurants.py:63: DeprecationWarning: count is deprecated. Use Collection.count_documents instead.
    #    , "cuisine" : "Italian"
    #    192

    """docs = db.restaurants.find(
        {
              "borough" : "Brooklyn"
            , "cuisine" : "Italian"
            , "address.street" : "5 Avenue"
        }
    )
    for doc in docs:
        print(doc)"""

    #    D:\Python>python3 restaurants.py
    #    {'_id': ObjectId('5dceba07f5a868598a76f8ca'), 'address': {'building': '248', 'coord': {'type': 'Point', 'coordinates': [-73.9814311, 40.6753101]}, 'street': '5 Avenue', 'zipcode': '11215'}, 'borough': 'Brooklyn', 'cuisine': 'Italian', 'grades': [{'date': datetime.datetime(2014, 8, 21, 0, 0), 'grade': 'B', 'score': 19}, {'date': datetime.datetime(2014, 1, 16, 0, 0), 'grade': 'A', 'score': 10}, {'date': datetime.datetime(2013, 4, 25, 0, 0), 'grade': 'A', 'score': 11}, {'date': datetime.datetime(2012, 9, 12, 0, 0), 'grade': 'A', 'score': 10}, {'date': datetime.datetime(2012, 2, 23, 0, 0), 'grade': 'B', 'score': 19}], 'name': 'Al Di La', 'restaurant_id': '40661290', 'comment': 'acceptable'}
    #    {'_id': ObjectId('5dceba07f5a868598a76fb59'), 'address': {'building': '68', 'coord': {'type': 'Point', 'coordinates': [-73.9775629, 40.6810713]}, 'street': '5 Avenue', 'zipcode': '11217'}, 'borough': 'Brooklyn', 'cuisine': 'Italian', 'grades': [{'date': datetime.datetime(2015, 1, 14, 0, 0), 'grade': 'A', 'score': 7}, {'date': datetime.datetime(2014, 6, 19, 0, 0), 'grade': 'A', 'score': 4}, {'date': datetime.datetime(2013, 11, 1, 0, 0), 'grade': 'A', 'score': 8}, {'date': datetime.datetime(2013, 4, 17, 0, 0), 'grade': 'A', 'score': 2}, {'date': datetime.datetime(2012, 7, 27, 0, 0), 'grade': 'A', 'score': 10}], 'name': 'Convivium Osteria', 'restaurant_id': '40783014', 'comment': 'acceptable'}
    #    ...
    
    # Recherche avec le mot pizza dans la nom
    filtre = re.compile(".*pizza.*", re.IGNORECASE) # note : sur la base direct --> "name": /pizza/i
    """docs = db.restaurants.find(
        {
              "borough" : "Brooklyn"
            , "cuisine" : "Italian"
            , "address.street" : "5 Avenue"
            , "name" : {'$regex': filtre}
        }
    ).count()
    print(doc)
    docs = db.restaurants.find(
        {
              "borough" : "Brooklyn"
            , "cuisine" : "Italian"
            , "address.street" : "5 Avenue"
            , "name" : {'$regex': filtre}
        }
    )
    for doc in docs:
        print(doc)"""

    #    D:\Python>python3 restaurants.py
    #    restaurants.py:95: DeprecationWarning: count is deprecated. Use Collection.count_documents instead.
    #    , "name" : {'$regex': filtre}
    #    2
    #    {'_id': ObjectId('5dceba07f5a868598a771fff'), 'address': {'building': '349', 'coord': {'type': 'Point', 'coordinates': [-73.98379, 40.6717439]}, 'street': '5 Avenue', 'zipcode': '11215'}, 'borough': 'Brooklyn', 'cuisine': 'Italian', 'grades': [{'date': datetime.datetime(2014, 6, 4, 0, 0), 'grade': 'A', 'score': 12}, {'date': datetime.datetime(2014, 1, 23, 0, 0), 'grade': 'A', 'score': 13}, {'date': datetime.datetime(2013, 7, 24, 0, 0), 'grade': 'C', 'score': 42}, {'date': datetime.datetime(2012, 12, 17, 0, 0), 'grade': 'B', 'score': 25}, {'date': datetime.datetime(2012, 5, 9, 0, 0), 'grade': 'B', 'score': 19}, {'date': datetime.datetime(2011, 12, 13, 0, 0), 'grade': 'C', 'score': 40}, {'date': datetime.datetime(2011, 6, 28, 0, 0), 'grade': 'B', 'score': 22}], 'name': "Joe'S Pizza", 'restaurant_id': '41540476'}
    #    {'_id': ObjectId('5dceba07f5a868598a7734cc'), 'address': {'building': '3905', 'coord': {'type': 'Point', 'coordinates': [-74.0039304, 40.65098]}, 'street': '5 Avenue', 'zipcode': '11232'}, 'borough': 'Brooklyn', 'cuisine': 'Italian', 'grades': [{'date': datetime.datetime(2014, 8, 28, 0, 0), 'grade': 'A', 'score': 12}, {'date': datetime.datetime(2014, 1, 8, 0, 0), 'grade': 'B', 'score': 23}, {'date': datetime.datetime(2013, 5, 14, 0, 0), 'grade': 'A', 'score': 13}], 'name': "Gina'S Pizzaeria/ Deli", 'restaurant_id': '41706533', 'comment': 'acceptable'}

    # Affichage de projections spécifiques (valeurs du document)
    """filtre = re.compile(".*pizza.*", re.IGNORECASE) # note : sur la base direct --> "name": /pizza/i
    docs = db.restaurants.find(
        {
              "borough" : "Brooklyn"
            , "cuisine" : "Italian"
            , "address.street" : "5 Avenue"
            , "name" : {'$regex': filtre}
        }
        ,
        {
              "name":1
            , "grades.score":1
        }
    )
    for doc in docs:
        print(doc)"""

    #    D:\Python>python3 restaurants.py
    #    {'_id': ObjectId('5dceba07f5a868598a771fff'), 'grades': [{'score': 12}, {'score': 13}, {'score': 42}, {'score': 25}, {'score': 19}, {'score': 40}, {'score': 22}], 'name': "Joe'S Pizza"}
    #    {'_id': ObjectId('5dceba07f5a868598a7734cc'), 'grades': [{'score': 12}, {'score': 23}, {'score': 13}], 'name': "Gina'S Pizzaeria/ Deli"}

    # Affichage de projections spécifiques (valeurs du document), sans l'id qui est mis par défaut
    """filtre = re.compile(".*pizza.*", re.IGNORECASE) # note : sur la base direct --> "name": /pizza/i
    docs = db.restaurants.find(
        {
              "borough" : "Brooklyn"
            , "cuisine" : "Italian"
            , "address.street" : "5 Avenue"
            , "name" : {'$regex': filtre}
        }
        ,
        {
              "name":1
            , "grades.score":1
            , "_id":0
        }
    )
    for doc in docs:
        print(doc)"""

    #    D:\Python>python3 restaurants.py
    #    {'grades': [{'score': 12}, {'score': 13}, {'score': 42}, {'score': 25}, {'score': 19}, {'score': 40}, {'score': 22}], 'name': "Joe'S Pizza"}
    #    {'grades': [{'score': 12}, {'score': 23}, {'score': 13}], 'name': "Gina'S Pizzaeria/ Deli"}

    # Affichage de projections spécifiques, avec critère approximatif (plus grand, plus petit ...)
    """score = { '$lt': 10 }
    docs = db.restaurants.find(
        {
              "borough" : "Manhattan"
            , "grades.score" : score
        }
        ,
        {
              "name":1
            , "grades.score":1
            , "_id":0
        }
    )
    for doc in docs:
        print(doc)"""

    #    D:\Python>python3 restaurants.py
    #    {'grades': [{'score': 2}, {'score': 11}, {'score': 12}, {'score': 12}], 'name': 'Dj Reynolds Pub And Restaurant'}
    #    {'grades': [{'score': 3}, {'score': 4}, {'score': 6}, {'score': 0}], 'name': '1 East 66Th Street Kitchen'}
    #    {'grades': [{'score': 12}, {'score': 16}, {'score': 9}, {'score': 13}, {'score': 11}], 'name': 'Glorious Food'}
    #    {'grades': [{'score': 12}, {'score': 11}, {'score': 6}, {'score': 8}], 'name': "Bully'S Deli"}
    #      --> ca affiche des scores supérieurs à 10
    #      '$lt':10 signifie "est-ce que a liste "grades" contient au moins une valeur inférieur
    #             Il faut combiner le lower avec le not (greater or equal) pour n'avoir que les < 10
    """score = { '$lt': 10, '$not': { '$gte':10 } }
    docs = db.restaurants.find(
        {
              "borough" : "Manhattan"
            , "grades.score" : score
        }
        ,
        {
              "name":1
            , "grades.score":1
            , "_id":0
        }
    )
    for doc in docs:
        print(doc)"""

    #    D:\Python>python3 restaurants.py
    #    {'grades': [{'score': 3}, {'score': 4}, {'score': 6}, {'score': 0}], 'name': '1 East 66Th Street Kitchen'}
    #    {'grades': [{'score': 2}, {'score': 7}, {'score': 2}, {'score': 6}, {'score': 5}], 'name': 'White Horse Tavern'}
    #    {'grades': [{'score': 2}, {'score': 7}, {'score': 7}, {'score': 9}], 'name': "Dorrian'S Red Hand Restaurant"}
    #    {'grades': [{'score': 9}, {'score': 2}, {'score': 8}], 'name': 'Pergola Des Artistes'}

    # Recherche d'un élément qui match sur deux éléments d'une même liste
    """grade_score = { '$elemMatch': {
                          "grade":"C"
                        , "score": { '$lt':40 }
                                 }
                  }
    docs = db.restaurants.find(
        {
            "grades" : grade_score
        }
        ,
        {
              "grades.grade":1
            , "grades.score":1
            , "_id":0
        }
    )
    for doc in docs:
        print(doc)"""

    #    D:\Python>python3 restaurants.py
    #    {'grades': [{'grade': 'A', 'score': 9}, {'grade': 'A', 'score': 10}, {'grade': 'A', 'score': 9}, {'grade': 'C', 'score': 32}]}
    #    {'grades': [{'grade': 'A', 'score': 4}, {'grade': 'C', 'score': 39}, {'grade': 'A', 'score': 12}, {'grade': 'A', 'score': 13}, {'grade': 'A', 'score': 10}]}
    #    {'grades': [{'grade': 'A', 'score': 10}, {'grade': 'A', 'score': 9}, {'grade': 'C', 'score': 36}, {'grade': 'A', 'score': 9}]}
    #    {'grades': [{'grade': 'A', 'score': 12}, {'grade': 'C', 'score': 40}, {'grade': 'A', 'score': 2}, {'grade': 'A', 'score': 11}, {'grade': 'C', 'score': 4}, {'grade': 'A', 'score': 2}]}
    #    {'grades': [{'grade': 'B', 'score': 15}, {'grade': 'A', 'score': 13}, {'grade': 'C', 'score': 36}, {'grade': 'B', 'score': 22}, {'grade': 'C', 'score': 36}, {'grade': 'C', 'score': 7}]}

    # recherche des noms et quartiers des restaurants dont la dernière inspection
    # (la plus récente, donc la première de la liste, indice 0 de la liste grade) a donné un grade ‘C’
    """docs = db.restaurants.find(
        {
            "grades.0.grade" : "C"
        }
        ,
        {
              "name":1
            , "borough":1
            , "grades.grade":1
            , "_id":0
        }
    )
    for doc in docs:
        print(doc)"""

    #   D:\Python>python3 restaurants.py
    #    {'borough': 'Queens', 'grades': [{'grade': 'C'}, {'grade': 'A'}, {'grade': 'A'}, {'grade': 'A'}, {'grade': 'A'}, {'grade': 'A'}], 'name': "Mcdonald'S"}
    #    {'borough': 'Queens', 'grades': [{'grade': 'C'}, {'grade': 'B'}, {'grade': 'C'}, {'grade': 'A'}, {'grade': 'A'}], 'name': 'Nueva Villa China Restaurant'}
    #    {'borough': 'Queens', 'grades': [{'grade': 'C'}, {'grade': 'A'}, {'grade': 'B'}, {'grade': 'B'}, {'grade': 'P'}, {'grade': 'A'}], 'name': 'Tequilla Sunrise'}
    #    {'borough': 'Manhattan', 'grades': [{'grade': 'C'}, {'grade': 'A'}, {'grade': 'B'}, {'grade': 'A'}, {'grade': 'A'}, {'grade': 'C'}], 'name': 'Dinastia China'}

    # Selection distincte
    """docs = db.restaurants.distinct("borough")
    for doc in docs:
        print(doc)"""

    #    D:\Python>python3 restaurants.py
    #    Manhattan
    #    Bronx
    #    Brooklyn
    #    Staten Island
    #    Queens
    #    Missing

    """docs = db.restaurants.distinct("grades.grade")
    for doc in docs:
        print(doc)"""

    #    D:\Python>python3 restaurants.py
    #    A
    #    B
    #    Z
    #    P
    #    C
    #    Not Yet Graded

    # find & aggregate
    """docs = db.restaurants.aggregate(
        [
              { '$match' : { "grades.0.grade":"C"} }
            , { '$project' : { "name":1 , "borough":1 , "_id":0 } }
        ]
    )
    for doc in docs:
        print(doc)"""

    # ou :
    """varMatch = { '$match' : { "grades.0.grade":"C" } }
    varProject = { '$project' : { "name":1, "borough":1, "_id":0 } }
    docs = db.restaurants.aggregate( [ varMatch, varProject ] )
    for doc in docs:
        print(doc)"""

    #    D:\Python>python3 restaurants.py
    #    {'borough': 'Queens', 'name': "Mcdonald'S"}
    #    {'borough': 'Queens', 'name': 'Nueva Villa China Restaurant'}
    #    {'borough': 'Queens', 'name': 'Tequilla Sunrise'}
    #    {'borough': 'Manhattan', 'name': 'Dinastia China'}
    #    {'borough': 'Brooklyn', 'name': 'Quick Stop Restaurant'}

    # tri
    """varSort = { '$sort' : { "name":1 } }
    docs = db.restaurants.aggregate( [ varMatch, varProject, varSort ] )
    for doc in docs:
        print(doc)"""

    #    D:\Python>python3 restaurants.py
    #    {'borough': 'Manhattan', 'name': '121 Fulton Street'}
    #    {'borough': 'Manhattan', 'name': '508 Restaurant And Bar'}
    #    {'borough': 'Manhattan', 'name': '525 Lex Restaurant & Bar'}
    #    {'borough': 'Manhattan', 'name': 'Abottega'}
    #    {'borough': 'Queens', 'name': "Acey Ducey'S"}
    #    {'borough': 'Manhattan', 'name': 'Adam Chinese Cottage'}
    #    {'borough': 'Manhattan', 'name': 'Agoda Asian Cuisine'}
    #    {'borough': 'Manhattan', 'name': 'Aki Sushi'}
    #    {'borough': 'Manhattan', 'name': 'Aki Sushi & Grill'}
    #    {'borough': 'Bronx', 'name': "Antonio'S"}
    #    {'borough': 'Manhattan', 'name': 'Arte Around The Corner'}
    #    {'borough': 'Brooklyn', 'name': 'Bahar Masala'}
    #    {'borough': 'Manhattan', 'name': 'Bamboo Restaurant'}
    #    {'borough': 'Queens', 'name': 'Bar & Grill 43'}
    #    {'borough': 'Manhattan', 'name': 'Bello Giardino'}

    # Groupement de donnees
    """varGroup = { '$group' : { "_id":"null", "total": { '$sum':1 } } }
    docs = db.restaurants.aggregate( [ varMatch, varGroup ] )
    for doc in docs:
        print(doc)"""

    #    D:\Python>python3 restaurants.py
    #    {'_id': 'null', 'total': 220}
    
    # Groupement par valeur
    """varGroup = { '$group' : { "_id":"borough", "total": { '$sum':1 } } }
    docs = db.restaurants.aggregate( [ varMatch, varGroup ] )
    for doc in docs:
        print(doc)"""

    #    D:\Python>python3 restaurants.py
    #    {'_id': 'borough', 'total': 220}
 
    # Groupement par valeur, pour toutes les valeurs d'une clé
    """varGroup = { '$group' : { "_id":"$borough", "total": { '$sum':1 } } }
    docs = db.restaurants.aggregate( [ varMatch, varGroup ] )
    for doc in docs:
        print(doc)"""

    #    D:\Python>python3 restaurants.py
    #    {'_id': 'Manhattan', 'total': 83}
    #    {'_id': 'Bronx', 'total': 27}
    #    {'_id': 'Staten Island', 'total': 7}
    #    {'_id': 'Queens', 'total': 47}
    #    {'_id': 'Brooklyn', 'total': 56}



    # Calcul du score moyen par quartiers, et en ordre décroissant
    """varUnwind = { '$unwind' : '$grades' }
    varGroup = { '$group' : { "_id" : "$borough", "moyenne" : { '$avg' : "$grades.score" } } }
    varSort = { '$sort' : { "moyenne" : -1 } }
    docs = db.restaurants.aggregate( [ varUnwind, varGroup, varSort ] )
    for doc in docs:
        print(doc)"""

    #    D:\Python>python3 restaurants.py
    #    {'_id': 'Queens', 'moyenne': 11.634865110930088}
    #    {'_id': 'Brooklyn', 'moyenne': 11.447723132969035}
    #    {'_id': 'Manhattan', 'moyenne': 11.41823125728344}
    #    {'_id': 'Staten Island', 'moyenne': 11.370957711442786}
    #    {'_id': 'Bronx', 'moyenne': 11.036186099942562}
    #    {'_id': 'Missing', 'moyenne': 9.632911392405063}

    # Ajout d'un nouveau commentaire à un enregistrement
    """docs = db.restaurants.update(
                                      { "_id" : ObjectId("5dceba08f5a868598a774f8c") }
                                    , { '$set' : {"comment" : "My new comment"} }
                                )
    for doc in docs:
        print(doc)"""

    """docs = db.restaurants.update_one(
                                      { "_id" : ObjectId("5dceba08f5a868598a774f8c") }
                                    , { '$set' : {"comment" : "My new comment"} }
                                )

    docs = db.restaurants.find(
                                      { "_id": ObjectId("5dceba08f5a868598a774f8c") }
                                    , { "_id":1 , "comment":1 }
                              )
    for doc in docs:
        print(doc)"""

    #    D:\Python>python3 restaurants.py
    #    restaurants.py:388: DeprecationWarning: update is deprecated. Use replace_one, update_one or update_many instead.
    #    , { '$set' : {"comment" : "My new comment"} }
    #    n
    #    nModified
    #    ok
    #    updatedExisting
    #    {'_id': ObjectId('5dceba08f5a868598a774f8c'), 'comment': 'My new comment'}

    # Pour supprimer la clé "comment", c'est la commande unset
    """docs = db.restaurants.update(
                                      { "_id" : ObjectId("5dceba08f5a868598a774f8c") }
                                    , { '$unset' : {"comment" : 1} }
                                )
    for doc in docs:
        print(doc)

    docs = db.restaurants.update_one(
                                      { "_id" : ObjectId("5dceba08f5a868598a774f8c") }
                                    , { '$unset' : {"comment" : 1} }
                                )
    docs = db.restaurants.find(
                                      { "_id": ObjectId("5dceba08f5a868598a774f8c") }
                                    , { "_id":1 , "comment":1 }
                              )
    for doc in docs:
        print(doc)"""

    #    D:\Python>python3 restaurants.py
    #    restaurants.py:412: DeprecationWarning: update is deprecated. Use replace_one, update_one or update_many instead.
    #    , { '$unset' : {"comment" : 1} }
    #    n
    #    nModified
    #    ok
    #    updatedExisting
    #    {'_id': ObjectId('5dceba08f5a868598a774f8c')}

    # update multiples enregistrements
    """docs = db.restaurants.update(
                                      { "grades.grade" : { '$not' : {'$eq' : "C"} } }
                                    , { '$set' : {"comment" : "acceptable"} }
                                    , multi=True
                                )"""

    """docs = db.restaurants.update_many(
                                          { "grades.grade" : { '$not' : {'$eq' : "C"} } }
                                        , { '$set' : {"comment" : "acceptable"} }
                                     )"""
    #for doc in docs:
    #    print(doc)

    """varFiltre = { "comment": "acceptable" }
    docs = db.restaurants.find( varFiltre ).count()
    print(docs)"""

    #    D:\Python>python3 restaurants.py
    #    restaurants.py:457: DeprecationWarning: count is deprecated. Use Collection.count_documents instead.
    #    docs = db.restaurants.find( varFiltre ).count()
    #    22649

    """docs = db.restaurants.update(
                                      { "grades.grade" : { '$not' : {'$eq' : "C"} } }
                                    , { '$unset' : {"comment" : 1} }
                                    , multi=True
                                )"""

    """docs = db.restaurants.update_many(
                                          { "grades.grade" : { '$not' : {'$eq' : "C"} } }
                                        , { '$unset' : {"comment" : 1} }
                                     )"""
    #for doc in docs:
    #    print(doc)

    """varFiltre = { "comment": "acceptable" }
    docs = db.restaurants.find( varFiltre ).count()
    print(docs)"""

    #    D:\Python>python3 restaurants.py
    #    restaurants.py:457: DeprecationWarning: count is deprecated. Use Collection.count_documents instead.
    #    docs = db.restaurants.find( varFiltre ).count()
    #    0

    # Mise à jour avec javascript
    # Consignes : 3 points pour un rang A
    #             1 point pour un rang B
    #            -1 point pour un rang C
    """docs = db.restaurants.find(
                { "grades.grade" : { '$not' : { '$eq' : "C" } } }.forEach(bson.Code( '''
                    function(restaurant){
                        total = 0;
                        for(i=0; i<restaurant.grades.length ;i++){
                            if(restaurant.grades[i].grade == "A")       total += 3;
                            else if(restaurant.grades[i].grade == "B")  total += 1;
                            else                                        total -= 1;
                        }
                        restaurant.note = total;
                        db.restaurants.save(restaurant);
                    }
                ''') ) }
           )"""

    """docs = db.restaurants.find(  { "grades.grade" :  {'$in' : ["A", "B", "C"] } }  , { "grades.grade":1, "_id":1} )
    for doc in docs:
        # la valeur du grade est le 1er élément de la liste grades[i].grade
        #print(doc)
        ident = doc['_id']
        longueur = len(doc['grades'])
        i = 0
        total = 0
        while i < longueur:
            grade = doc['grades'][i]['grade']
            if grade == "A":
                total += 3
            elif grade == "B":
                total += 1
            else:
                total -= 1
            i += 1
        #print("{} {}".format(i, total))

        db.restaurants.update_one(
                                      { "_id" : ObjectId(ident) }
                                    , { '$set' : {"note" : total} }
                                 )"""
        
    """varMatch = { '$match' : { "grades.grade" :  {'$in' : ["A", "B", "C"] } } }
    varProject = { '$project' : { "name":1 , "_id":0 , "note":1 , "borough":1 } }
    varSort = { '$sort' : { "note":-1 } }
    docs = db.restaurants.aggregate( [ varMatch, varProject, varSort ] )
    for doc in docs:
        print(doc)"""

    #    D:\Python>python3 restaurants.py
    #    {'borough': 'Queens', 'name': 'Taco Veloz', 'note': 24}
    #    {'borough': 'Manhattan', 'name': 'Gemini Diner', 'note': 22}
    #    {'borough': 'Manhattan', 'name': "Au Za'Atar", 'note': 22}
    #    {'borough': 'Brooklyn', 'name': 'Lucky 11 Bakery', 'note': 22}
    #    {'borough': 'Queens', 'name': "Mcdonald'S", 'note': 21}
    #    {'borough': 'Queens', 'name': 'Rincon Salvadoreno Restaurant', 'note': 21}
    #    {'borough': 'Manhattan', 'name': 'Kelley & Ping', 'note': 21}

    # Insertion d'un enregistrement
    """db.restaurants.save( { "_id" : 1 , "test":1 } )
    #db.restaurants.insert_one( { "_id" : 1 , "test":1 } )
    #db.restaurants.remove( { "_id" : 1 } )
    #db.restaurants.delete_one( { "_id" : 1 } )

    #docs = db.restaurants.find(
    #                                  { "_id":1 }
    #                                #, { "_id":1 , "test":1 }
    #                          )
    #for doc in docs:
    #    print(doc)"""
    
    
    """docs = db.restaurants.find().count()
    print("Avant delete : {}".format(docs))

    db.restaurants.delete_many(  { "note":0 })
    #db.restaurants.remove( { "note":0 } , multi=True )

    docs = db.restaurants.find().count()
    print("Apres delete : {}".format(docs))"""