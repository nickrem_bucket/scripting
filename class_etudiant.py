from operator import attrgetter

class Etudiant:

    """Classe représentant un étudiant.

    On représente un étudiant par son prénom (attribut prenom), son âge
    (attribut age) et sa note moyenne (attribut moyenne, entre 0 et 20).

    Paramètres du constructeur :
        prenom -- le prénom de l'étudiant
        age -- l'âge de l'étudiant
        moyenne -- la moyenne de l'étudiant

    """

    def __init__(self, prenom, age, moyenne):
        self.prenom = prenom
        self.age = age
        self.moyenne = moyenne

    def __repr__(self):
        return "<Étudiant {} (âge={}, moyenne={})>".format(
                self.prenom, self.age, self.moyenne)

etudiants = [
    Etudiant("Clément", 14, 16),
    Etudiant("Charles", 12, 15),
    Etudiant("Oriane", 14, 18),
    Etudiant("Thomas", 11, 12),
    Etudiant("Damien", 10, 15),
]

print("Liste de départ")
print(etudiants)

#sorted(etudiants n'est pas suffisante pour pyhton, il faut préciser une key lambda
#print("")
#print("Tri sur le prénom de l'étudiant")
#print(sorted(etudiants, key=lambda etudiant: etudiant.prenom))

#print("")
#print("Tri sur l'age de l'étudiant")
#print(sorted(etudiants, key=lambda etudiant: etudiant.age))

#print("")
#print("Tri dans l'ordre inverse de l'age de l'étudiant")
#print(sorted(etudiants, key=lambda etudiant: etudiant.age, reverse=True))

print("")
print("Tri sur la moyenne de l'étudiant")
#print(sorted(etudiants, key=lambda etudiant: etudiant.moyenne))
print(sorted(etudiants, key=attrgetter("moyenne")))

print("")
print("Tri sur l'age et la moyenne de l'étudiant")
print(sorted(etudiants, key=attrgetter("age","moyenne")))