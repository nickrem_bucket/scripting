date = "2019-12-04"
station_number = 15
station_address = "5 rue de la gare, Amiens"
station_contract = "Amiens"
routing_key = "France"
cpt_temp = 1
cpt_empty = 10
cpt_notempty = 8

text1 = " [x] Message \"not empty\" Sent :"
text2 = "\n     {} --- Station : {} ({} {})".format(date, station_number, station_address, station_contract)
text3 = "\n            Routing Key : {}".format(routing_key)
text4 = "\n            Nb Empty Station in {} : {}".format(station_contract, cpt_temp)
text5 = "\n"
text6 = "\n            Nb Msg \"Empty\" Sent by This Producer     : {}".format(str(cpt_empty))
text7 = "\n            Nb Msg \"Not Empty\" Sent by This Producer : {}".format(str(cpt_notempty))
text8 = "\n"
text = "{}{}{}{}{}{}{}{}".format(text1, text2, text3, text4, text5, text6, text7, text8)

print(text)