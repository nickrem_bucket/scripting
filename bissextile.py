import sys

x = len(sys.argv)
if x == 1:
    print("Veuillez passer une variable au script")
    exit()

annee = sys.argv[1]
try:
    annee = int(annee)
except ValueError:
    pass
p = isinstance(annee,int)
if str(p) == "False":
    print("La variable passée n'est pas un integer")
    exit()

try:
    if annee <= 0:
        if annee == 0:
            ErrorMsg = "nulle"
        else:
            ErrorMsg = "négative"
        raise ValueError("L'année passée en paramètre est {}".format(ErrorMsg))
except ValueError as err:
    print("La variable passée est invalide")
    print("Erreur : {}".format(err))
    exit()

# Multiple de 4 ?
operation_4 = annee % 4
operation_100 = annee % 100
operation_400 = annee % 400

if (operation_400 == 0) or (operation_4 == 0 and operation_100 !=0):
    annee_bissextile = "True"
    
else:
    annee_bissextile = "False"
    
    
print("")
print("Détermination si une année est bissextile ou non : ")
print("")
print("     - Année paramètre : {}".format(annee))
if annee_bissextile == "True":
    print("     ---> l'année est bien bissextile")
else:
    print("     ---> l'année n'est pas bissextile")

print("")