def afficher(*parametres, separateur=' ', fin='\n'):
    """Fonction chargée de reproduire le comportement de print.
    
    Elle doit finir par faire appel à print pour afficher le résultat.
    Mais les paramètres devront déjà avoir été formatés. 
    On doit passer à print une unique chaîne, en lui spécifiant de ne rien mettre à la fin :

    print(chaine, end='')"""
    
    # convertir le tuple en liste
    parametres = list(parametres)
    
    # Convertir les valeurs en chaine, nécessaire au join
    for i, parametre in enumerate(parametres):
        parametres[i] = str(parametre)

    # À présent on va constituer la chaîne finale
    chaine = separateur.join(parametres)

    # Ajout du paramètre fin à la fin de la chaîne
    chaine += fin

    # déclenchement du print
    print(chaine, end='')

afficher('a', 'e', 'f', separateur=' ', fin='\n')