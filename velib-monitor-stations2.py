import json, time, os, hashlib, copy
from kafka import KafkaConsumer
from datetime import datetime

stations = {}
first = True

consumer = KafkaConsumer(  "empty-stations"
                         #, bootstrap_servers='localhost:9092'
                         , bootstrap_servers=['localhost:9092', 'localhost:9093']
                         , group_id="velib-monitor-stations2"
                        )
i = 0
for message in consumer:

    station = json.loads(message.value.decode())
    station2 = copy.deepcopy(station)
    sha = hashlib.sha256(str(station2).encode("utf-8")).hexdigest()
    mdpass = hashlib.md5(str(station2).encode("utf-8")).hexdigest()
    print(mdpass)
    station_number = station["number"]
    contract = station["contract_name"].capitalize()
    available_bikes = station["available_bikes"]
    address = station["address"]
    nb_empty = station["number_stations_empty"]

    fichier = 'test.toto2.nico' + str(i)
    i += 1
    if not os.path.isfile(fichier):
        with open(fichier, 'w') as f:
            f.write("2")
    
    #time.sleep(0.005)
    date = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')
    if available_bikes == 0:
        print("{} --- The station number {} ({} , {}), just passed empty !!!".format(date, station_number, address, contract))
        print("       Number of Empty Station in {} : {} ".format(contract, nb_empty))
        print("")
    else:
        print("{} --- The station number {} ({} , {}), is ok now".format(date, station_number, address, contract))
        print("       Number of Empty Station in {} : {} ".format(contract, nb_empty))
        print("")

    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()

    channel.exchange_declare(
                               exchange='direct_logs'
                             , exchange_type='direct'
                            )

    routing_key = "toto"
    channel.basic_publish(
                            exchange='direct_logs'
                          , routing_key=routing_key
                          , body=message
                         )
    print(" [x] Sent %r:%r" % (routing_key, message[0:15]))
    connection.close()