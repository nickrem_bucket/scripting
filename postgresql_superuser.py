# -*- coding: utf-8 -*-

import os
import sys
import importlib

#x = len(sys.argv)
#if x == 1:
#    print("Veuillez passer une variable au script")
#    exit()
#parameter = sys.argv[1]

#module = importlib.import_module(parameter)
#globals().update(module.__dict__)
#__main__()
type_user = "own"
software = "postgresql"
replace_user_app = False
from datetime import datetime
from db_access import connect
from db_access import exec_sql
from db_access import get_user_app
from nostri import DIRECTORY
from nostri import DIRECTORYS
date = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
file_cmds_sql = DIRECTORY + "\\psql_create_" + date + ".sql"
file_output_sql = DIRECTORY + "\\psql_resultat_" + date + ".txt"
file_result_sql = DIRECTORY + "\\psql_sql_output_" + date + ".txt"


try:

    date = datetime.now().strftime('%H:%M:%S')
    print("")
    print("")
    print("      ... Execution Started : {}".format(date))
    print(" ------------------------------------------- ")
    print("")

    with open(file_cmds_sql, "w") as file:
        file.write("drop table if exists STUDENT cascade;\n")
        file.write("drop table if exists COURSE cascade;\n")
        file.write("drop table if exists DEPARTMENT cascade;\n")
        file.write("drop sequence if exists seq_course_id;\n")
        file.write("drop sequence if exists seq_department_id;\n")
        file.write("drop function if exists function_course_id;\n")
        file.write("drop function if exists function_department_id;\n")
        file.write("\n")
        file.write("create table  COURSE\n")
        file.write("    (\n")
        file.write("       ID                       serial         not null\n")
        file.write("     , NAME                     char(50)\n")
        file.write("     , AMOUNT                   int\n")
        file.write("     , constraint PK_COURSE     primary key (ID)\n")
        file.write("    );\n")
        file.write("\n")
        list_course = [('Computer Science', 500042), ('Electrical Engineering', 450000), ('Information Technology', 420000)]
        i = 0
        while i < len(list_course):
            sql_statement = ("insert into COURSE (NAME, AMOUNT) values ('{}', {});\n").format(list_course[i][0], list_course[i][1])
            file.write(sql_statement)
            i += 1
        file.write("\n")
        file.write("create table  DEPARTMENT\n")
        file.write("    (\n")
        file.write("       ID                       serial         not null\n")
        file.write("     , NAME                     char(50)\n")
        file.write("     , constraint PK_DEPARTMENT primary key (ID)\n")
        file.write("    );\n")
        file.write("\n")
        list_department = ["ICT", "Engineering"]
        i = 0
        while i < len(list_department):
            sql_statement = ("insert into DEPARTMENT (NAME) values ('{}');\n").format(list_department[i])
            file.write(sql_statement)
            i += 1
        file.write("\n")
        file.write("create table  STUDENT\n")
        file.write("    (\n")
        file.write("       ADMISSION               int           not null\n")
        file.write("     , NAME                    text          not null\n")
        file.write("     , AGE                     int           not null\n")
        file.write("     , COURSE                  int\n")
        file.write("     , DEPARTMENT              int\n")
        file.write("     , constraint PK_STUDENT   primary key (ADMISSION)\n")
        file.write("     , constraint FK01_STUDENT foreign key (COURSE)     references COURSE(ID)\n")
        file.write("     , constraint FK02_STUDENT foreign key (DEPARTMENT) references DEPARTMENT(ID)\n")
        file.write("    );\n")
        file.write("\n")
        list_student = [[3420, "John", 18, "Computer Science", "ICT"],
                        [3419, "Abel", 17, "Computer Science", "ICT"],
                        [3421, "Joel", 17, "Computer Science", "ICT"],
                        [3422, "Anthony", 19, "Electrical Engineering", "Engineering"],
                        [3423, "Alice", 18, "Information Technology", "ICT"],
                        [3424, "Tom", 20, "Information Technology", "ICT"]]
        i = 0
        while i < len(list_student):
            id_adm      = list_student[i][0]
            nam_student = list_student[i][1]
            age_student = list_student[i][2]
            cou_student = list_student[i][3]
            dep_student = list_student[i][4]
            sel_cou_student = ("(select ID from COURSE where NAME = '{}')").format(cou_student)
            sel_dep_student = ("(select ID from DEPARTMENT where NAME = '{}')").format(dep_student)
            sql_statement = ("insert into STUDENT (ADMISSION, NAME, AGE, COURSE, DEPARTMENT)  values ({}, '{}', {}, {}, {});\n").format(id_adm, nam_student, age_student, sel_cou_student, sel_dep_student);
            file.write(sql_statement)
            i += 1
        file.write("\n")
        replace_user_app = True
        name_user_app = '%user_app%'
        list_table = ['COURSE', 'DEPARTMENT', 'STUDENT']
        i = 0
        while i < len(list_table):
            sql_statement = ("grant select, insert, update, delete on table {} to {};\n").format(list_table[i], name_user_app)
            file.write(sql_statement)
            i += 1

    #exit()

    params = exec_sql(type_user, file_cmds_sql, file_result_sql, file_output_sql, replace_user_app, DIRECTORYS, software)
    exit()

    if os.stat(file_result_sql).st_size == 0:
        print("\nAll SQL commands on PostgreSQL are OK\n")
    else:
        if 'ERREUR' in open(file_result_sql).read():
            error_sql = True
        else:
            error_sql = False

    if error_sql == True:
        list_lines = []
        First_Error = False
        with open(file_result_sql, "r") as file:
            for line in file:
                list_lines.append(line)
        i = 0
        while i < len(list_lines):
            a = list_lines[i].rstrip()
            b = a.lstrip()
            if b.lower().startswith("erreur") or b.lower().startswith("error"):
                indice = i
                indice2 = indice + 1
                break
            i += 1
        a = list_lines[indice].rstrip()
        b = a.lstrip()
        c = list_lines[indice2].rstrip()
        d = c.lstrip()
        if d.lower().startswith("ligne") or d.lower().startswith("line"):
            temp_msg = list_lines[indice] + list_lines[indice2]
        else:
            temp_msg = list_lines[indice]
        raise ValueError(temp_msg)
        
    else:
        print("\nAll SQL commands on PostgreSQL are OK\n")


except NameError as err:
    print(err)

except OSError as err:
    print(err)

except ValueError as err:
    print(err)

except TypeError as err:
    print(err)

except KeyError as err:
    print(err)

except Exception as err:
    print(err)

except:
   print("Unexpected error : ", sys.exc_info()[0])
   
finally:
    if os.path.isfile(file_cmds_sql):
        os.remove(file_cmds_sql)
    if os.path.isfile(file_result_sql):
        os.remove(file_result_sql)
    if os.path.isfile(file_output_sql):
        os.remove(file_output_sql)
    print("")
    print("")
    from datetime import datetime
    date = datetime.now().strftime('%H:%M:%S')
    print(" ------------------------------------------- ")
    print("      ... Execution Ended : {}".format(date))

