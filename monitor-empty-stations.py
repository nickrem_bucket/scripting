import json
from kafka import KafkaConsumer
from datetime import datetime

stations = {}

consumer = KafkaConsumer("empty-stations", bootstrap_servers='localhost:9092',
                         group_id="monitor-empty-stations")

for message in consumer:
  
    station = json.loads(message.value.decode())
    station_number = station["number"]
    contract = station["contract_name"].capitalize()
    available_bikes = station["available_bikes"]
    address = station["address"]
    nb_empty = station["number_stations_empty"]

    date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    if available_bikes == 0:
        print("{} --- The station number {} ({} , {}), just passed empty !!!".format(date, station_number, address, contract))
        print("       Number of Empty Station in {} : {} ".format(contract, nb_empty))
        print("")
    else:
        print("{} --- The station number {} ({} , {}), is ok now".format(date, station_number, address, contract))
        print("       Number of Empty Station in {} : {} ".format(contract, nb_empty))
        print("")