import sqlite3

conn = sqlite3.connect('ma_base.db')

cursor = conn.cursor()
#cursor.execute("""
#create table if not exists USERS(
#    ID integer primary key autoincrement unique,
#    name TEXT,
#    age INTEGER
#)
#""")

#cursor.execute("""
#drop table USERS
#""")

#data = {"name" : "jean-louis", "age" : 90}

#cursor.execute("""
#insert into USERS(name, age) values(:name, :age)""", data)

#conn.commit()

#id = cursor.lastrowid
#print(id)

#cursor.execute("""select NAME, AGE from USERS""")

#user1 = cursor.fetchone()
#print(user1)

cursor.execute("""update USERS set AGE = ? where ID = 2""", (31,))
conn.commit()

cursor.execute("""select ID, NAME, AGE from USERS""")
for row in cursor:
    print('{0} : {1} - {2}'.format(row[0], row[1], row[2]))


conn.close()