# coding: latin1

#@(#) ========================================================================
#@(#) SCRIPT : secure_string.py
#@(#) OBJET  : Ce script sécurise une chaîne de caractère à partir d'un
#@(#)          libellé sauvegarde dans un fichier chiffrer AES
#@(#)
#@(#) AUTEUR : NRE
#@(#) ------------------------------------------------------------------------
#@(#) PARAMETRES :
#@(#)
#@(#)     (1) : libellé de la chaine de caractère a récupérer
#@(#)           exemple : password_crtdba
#@(#)
#@(#)     (2) : la chaîne de cactère a sécuriser
#@(#)
#@(#) ------------------------------------------------------------------------
#@(#) DATE        AUTEUR        MODIFICATION
#@(#) 17/04/2020  NRE           Creation
#@(#)
#@(#) ========================================================================

from SecureString import Class_SecureString

p1 = "I10426CP10_wallt_crtdba"
p2 = "I10426CP1_crtdba"

new_class = Class_SecureString()

new_class.encrypt(p1, p2)

print("Chiffrement Complet!")
