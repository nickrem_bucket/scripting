from cryptography.fernet import Fernet
from cryptography.fernet import MultiFernet

passphrase = "This is a Test!"
passphrase_bytes = passphrase.encode()

gen_key = Fernet.generate_key()
key1 = Fernet(gen_key)
key2 = Fernet(gen_key)
f1 = MultiFernet([key1, key2])

token1 = f1.encrypt(passphrase_bytes)
disp_token1 = token1.decode()
print(disp_token1)

key3 = Fernet(gen_key)
f2 = MultiFernet([key3, key1, key2])

rotated = f2.rotate(token1)
token2 = f2.decrypt(rotated)
disp_token2 = token2.decode()
print(disp_token2)
