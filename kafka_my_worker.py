# my_worker.py

import logging

from kafka import KafkaConsumer
from kq import Worker

# Set up logging.
formatter = logging.Formatter('[%(levelname)s] %(message)s')
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger = logging.getLogger('kq.worker')
logger.setLevel(logging.DEBUG)
logger.addHandler(stream_handler)

# Set up a Kafka consumer.
consumer = KafkaConsumer(
                           #bootstrap_servers='127.0.0.1:9095'
                           bootstrap_servers=['localhost:9095', 'localhost:9096']
                         , group_id='velib-monitor-stations'
                         , auto_offset_reset='latest'
                        )

# Set up a worker.
worker = Worker(topic='empty-stations', consumer=consumer)
worker.start()