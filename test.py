import copy
import gzip

liste_a = [1, 2, [3,5], 4]

liste_b = copy.copy(liste_a)

#liste_b.append(10)

#print(liste_a)
#print(liste_b)

a = 5

print("first  a : {}".format(a))

b = a

print("")
print("Second a : {}".format(a))
print("Second b : {}".format(b))

b = 7

print("")
print("Third  a : {}".format(a))
print("Third  b : {}".format(b))