import json
import time
import urllib.request
import copy

from kafka import KafkaProducer

API_KEY = "7e19ba325ea45f307dc34008f3f97be1ff769274" # FIXME
url = "https://api.jcdecaux.com/vls/v1/stations?apiKey={}".format(API_KEY)

producer = KafkaProducer(bootstrap_servers="localhost:9092")

first_message = True

list_column = ['number', 'contract_name', 'address', 'available_bikes']
list_stations_ref = []
new_dict = {}

dict_nb_empty_tot = {}

while True:
    response = urllib.request.urlopen(url)
    stations = json.loads(response.read().decode())

    print("Received {} station records".format(len(stations)))
    # première éxécution, sauvegarde de la liste comme référence
    # + création dictionnaire pour avoir le nombre de station vide par ville
    if first_message == True:
        i = 0
        while i < len(stations):
            for key in stations[i].keys():
                if key in list_column:
                    new_dict[key] = copy.deepcopy(stations[i][key])

                    if key == "contract_name":
                        a = stations[i][key]
                        b = stations[i]['available_bikes']
                        if b == 0:
                            if a in dict_nb_empty_tot:
                                dict_nb_empty_tot[a] = dict_nb_empty_tot[a] + 1
                            else:
                                dict_nb_empty_tot[a] = 1

            #pour ne pas impacter la list_stations_ref
            dict_temp = copy.deepcopy(new_dict)
            list_stations_ref.append(dict_temp)
            i += 1
        first_message = False

    else:
        for station in stations:
        
            # récupération des information de la station
            station_number = station['number']
            station_contract = station['contract_name']
            station_available_bikes = station['available_bikes']
            station_address = station['address']

            # Vérification si la station (number, contract) n'existe pas, pour l'ajouter à la liste référence
            i = 0
            in_list = False
            while i < len(list_stations_ref):
                if (list_stations_ref[i]['number'] == station_number) and (list_stations_ref[i]['contract_name'] == station_contract):
                    in_list = True
                i += 1
            if in_list == False:
                print("new entry in list_stations_ref")
                new_dict = {}
                new_dict['number'] = station_number
                new_dict['contract_name'] = station_contract
                new_dict['address'] = station_address
                new_dict['available_bikes']= station_address
                dict_temp = copy.deepcopy(new_dict)
                list_stations_ref.append(dict_temp)

            # récupération de l'indice de la liste de référence correspondance à la station en cours
            i = 0
            while i < len(list_stations_ref):
                station_ref_number = list_stations_ref[i]['number']
                station_ref_contract = list_stations_ref[i]['contract_name']
                station_ref_available_bikes = list_stations_ref[i]['available_bikes']
                if (station_ref_number == station_number) and (station_ref_contract == station_contract):
                    break
                i += 1

            # La station était vide
            if station_ref_available_bikes == 0:

                # la station n'est plus vide, envoi d'un message à "empty-stations"
                if station_available_bikes > 0:
                    list_stations_ref[i]['available_bikes'] = station_available_bikes

                    dict_nb_empty_tot[station_contract] = dict_nb_empty_tot[station_contract] - 1
                    
                    station['number_stations_empty'] = dict_nb_empty_tot[station_contract]

                    producer.send("empty-stations", json.dumps(station).encode(),
                            key=str(station["number"]).encode())

                    print("Produced 1 not empty record ({} , {})".format(station_number, station_contract))

            # la station n'était pas vide
            elif station_ref_available_bikes == 1:

                # la station est de nouveau vide, envoi d'un message à "empty-stations"
                if station_available_bikes == 0:
                    list_stations_ref[i]['available_bikes'] = 0

                    dict_nb_empty_tot[station_contract] = dict_nb_empty_tot[station_contract] + 1

                    station['number_stations_empty'] = dict_nb_empty_tot[station_contract]

                    producer.send("empty-stations", json.dumps(station).encode(),
                            key=str(station["number"]).encode())

                    print("Produced 1 empty stations record ({} , {})".format(station_number, station_contract))
    time.sleep(1)

