# -*- coding: utf-8 -*-

type_user = "app"
replace_user_app = False
section = "cubrid"
import os
import sys
import subprocess
import zipfile
import gzip
from shutil import copy2
from shutil import move
from db_access import exec_sql
from format_xlsx import format_xlsx_file
from module_compress_file import compress_file_by_type

from nostri import DIRECTORY, DIRECTORYS, DIRECTORY2
from nostri import dir_sql_file
from datetime import datetime

date = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
file_list_file_to_compress = DIRECTORY + "\\list_file_to_zip_" + date + ".txt"
file_output_csv_direname = DIRECTORY
file_output_csv_basename = "student_" + date + ".csv"
file_output_csv = file_output_csv_direname + "\\" + file_output_csv_basename


date_start = datetime.now().strftime('%H:%M:%S')
print("")
print("")
print("      ... Execution Started : {}".format(date_start))
print(" ------------------------------------------- ")
print("")

try:

    os.chdir(DIRECTORY)

    if section == "cubrid":
        from nostri import DIRECTORY2

        file_commands_sql = DIRECTORY + "\\cubrid_sql_commands_" + date + ".sql"
        file_logs_sql = DIRECTORY + "\\cubrid_sql_logs_" + date + ".log"
        file_output_result = DIRECTORY + "\\cubrid_result_" + date + ".txt"
        file_output_result2 = DIRECTORY + "\\cubrid_result2_" + date + ".txt"

        # fichier sql générique
        file_sql_generic = "select1_cubrid.sql"
        file_sql_dynamic = dir_sql_file + "\\" + file_sql_generic
        if not os.path.isfile(file_sql_dynamic):
                temp_msg = "The file " + file_sql_generic + " is not found on server !"
                raise ValueError(temp_msg)

        copy2(file_sql_dynamic, file_commands_sql)
        params = exec_sql(type_user, file_commands_sql, file_logs_sql, file_output_result, replace_user_app, DIRECTORY, section)

        if os.stat(file_logs_sql).st_size == 0:
            if os.stat(file_output_result).st_size == 0:
                raise ValueError("Cubrid Error : no rows selected")
            else:
                list_lines = []
                with open(file_output_result, "r") as infile, open(file_output_result2, 'w') as outfile:
                    for line in infile:
                        if not line.strip(): continue  # skip the empty line
                        list_lines.append(line)
                        outfile.write(line)
                error_sql = False
                move(file_output_result2, file_output_result)
                if len(list_lines) == 1:
                    print("  ---> Command Executed. {} row selected in file {}".format(len(list_lines), os.path.basename(file_output_result)))
                else:
                    print("  ---> Command Executed. {} rows selected in file {}".format(len(list_lines), os.path.basename(file_output_result)))            
        else:        
            if 'ERROR' in open(file_logs_sql).read() or 'ERREUR' in open(file_logs_sql).read():
                error_sql = True
            else:
                error_sql = False

            if error_sql == True:
                list_lines = []
                with open(file_logs_sql, "r") as file:
                    for line in file:
                        list_lines.append(line)
                if len(list_lines) == 1:
                    temp_msg = "\nCubrid Error on Commands" + list_lines[0]
                else:
                    i = 0
                    while i < len(list_lines):
                        if i == 0:
                            temp_msg = "\nCubrid Error on Commands" + list_lines[i]
                        else:
                            temp_msg = temp_msg + list_lines[i]
                        i += 1
                raise ValueError(temp_msg)
            else:
                list_lines = []
                with open(file_output_result, "r") as infile, open(file_output_result2, 'w') as outfile:
                    for line in infile:
                        if not line.strip(): continue  # skip the empty line
                        list_lines.append(line)
                        outfile.write(line)
                error_sql = False
                move(file_output_result2, file_output_result)
                if len(list_lines) == 1:
                    print("  ---> Command Executed. {} row selected in file {}".format(len(list_lines), os.path.basename(file_output_result)))
                else:
                    print("  ---> Command Executed. {} rows selected in file {}".format(len(list_lines), os.path.basename(file_output_result)))
        
    elif section == "firebird":
        from nostri import DIRECTORY2

        file_commands_sql = DIRECTORY + "\\firebird_sql_commands_" + date + ".sql"
        file_logs_sql = DIRECTORY + "\\firebird_sql_logs_" + date + ".log"
        file_output_result = DIRECTORY + "\\firebird_result_" + date + ".txt"
        file_output_result2 = DIRECTORY + "\\firebird_result2_" + date + ".txt"

        # fichier sql générique
        file_sql_generic = "select1_firebird.sql"
        file_sql_dynamic = dir_sql_file + "\\" + file_sql_generic
        if not os.path.isfile(file_sql_dynamic):
                temp_msg = "The file " + file_sql_generic + " is not found on server !"
                raise ValueError(temp_msg)

        copy2(file_sql_dynamic, file_commands_sql)
        params = exec_sql(type_user, file_commands_sql, file_logs_sql, file_output_result, replace_user_app, DIRECTORY, section)

        if os.stat(file_output_result).st_size == 0:
            raise ValueError("Firebird Error : no rows selected")
        else:
            if 'Statement failed' in open(file_output_result).read():
                error_sql = True
            else:
                error_sql = False

            if error_sql == True:
                list_lines = []
                with open(file_output_result, "r") as file:
                    for line in file:
                        list_lines.append(line)
                i = 0
                while i < len(list_lines):
                    a = list_lines[i].rstrip()
                    b = a.lstrip()
                    if b.lower().startswith("statement failed"):
                        indice = i
                        break
                    i += 1
                first = True
                while i <= (indice + 4):
                    a = list_lines[i].rstrip()
                    if first == True:
                        first = False
                        temp_msg = a.lstrip()
                    else:
                        temp_msg = temp_msg + "\n" + a.lstrip()
                    i += 1
                raise ValueError(temp_msg)
            else:
                list_lines = []
                # remove blanc line, "CONCATENATION" line and "========" line
                with open(file_output_result) as infile, open(file_output_result2, 'w') as outfile:
                    for line in infile:
                        if not line.strip(): continue  # skip the empty line
                        if line.lower().startswith("concatenation"): continue
                        if line.lower().startswith("============="): continue
                        list_lines.append(line)
                        outfile.write(line)  #

                move(file_output_result2, file_output_result)
                if len(list_lines) == 1:
                    print("  ---> Command Executed. {} row selected in file {}".format(len(list_lines), os.path.basename(file_output_result)))
                else:
                    print("  ---> Command Executed. {} rows selected in file {}".format(len(list_lines), os.path.basename(file_output_result)))    

    elif section == "mariadb":
        from nostri import DIRECTORY2
        from shutil import copy2
        from shutil import move

        file_commands_sql = DIRECTORY + "\\mariadb_sql_commands_" + date + ".sql"
        file_logs_sql = DIRECTORY + "\\mariadb_sql_logs_" + date + ".log"
        file_output_result = DIRECTORY + "\\mariadb_result_" + date + ".txt"

        # fichier sql générique
        file_sql_generic = "select1_mariadb.sql"
        file_sql_dynamic = dir_sql_file + "\\" + file_sql_generic
        if not os.path.isfile(file_sql_dynamic):
                temp_msg = "The file " + file_sql_generic + " is not found on server !"
                raise ValueError(temp_msg)

        file_paramsql_output = DIRECTORY2 + "/mariadb_result_" + date + '.txt'
        f_in = open(file_sql_dynamic, "rt")
        f_out = open(file_commands_sql, "wt")
        for line in f_in:
            f_out.write(line.replace('%resultat_file%', file_paramsql_output))
        f_in.close()
        f_out.close()

        params = exec_sql(type_user, file_commands_sql, file_logs_sql, file_output_result, replace_user_app, DIRECTORY, section)

        if os.stat(file_logs_sql).st_size == 0:
            if os.stat(file_output_result).st_size == 0:
                raise ValueError("MariaDB Error : no rows selected")
            else:
                list_lines = []
                with open(file_output_result, "r") as file:
                    for line in file:
                        list_lines.append(line)
                error_sql = False
                if len(list_lines) == 1:
                    print("  ---> Command Executed. {} row selected in file {}".format(len(list_lines), os.path.basename(file_output_result)))
                else:
                    print("  ---> Command Executed. {} rows selected in file {}".format(len(list_lines), os.path.basename(file_output_result)))            
        else:        
            if 'ERROR' in open(file_logs_sql).read() or 'ERREUR' in open(file_logs_sql).read():
                error_sql = True
            else:
                error_sql = False

            if error_sql == True:
                list_lines = []
                with open(file_logs_sql, "r") as file:
                    for line in file:
                        list_lines.append(line)
                if len(list_lines) == 1:
                    temp_msg = "\nMariaDB Error on Commands\n" + list_lines[0]
                else:
                    i = 0
                    while i < len(list_lines):
                        if i == 0:
                            temp_msg = "\nMariaDB Error on Commands\n" + list_lines[i]
                        else:
                            temp_msg = temp_msg + "\n" + list_lines[i]
                        i += 1
                raise ValueError(temp_msg)
            else:
                list_lines = []
                with open(file_output_result, "r") as file:
                    for line in file:
                        list_lines.append(line)
                error_sql = False
                if len(list_lines) == 1:
                    print("  ---> Command Executed. {} row selected in file {}".format(len(list_lines), os.path.basename(file_output_result)))
                else:
                    print("  ---> Command Executed. {} rows selected in file {}".format(len(list_lines), os.path.basename(file_output_result))) 

    elif section == "mysql":
        from nostri import DIRECTORY2

        file_commands_sql = DIRECTORY + "\\mysql_sql_commands_" + date + ".sql"
        file_logs_sql = DIRECTORY + "\\mysql_sql_logs_" + date + ".log"
        file_output_result = DIRECTORY + "\\mysql_result_" + date + ".txt"

        # fichier sql générique
        file_sql_generic = "select1_mysql.sql"
        file_sql_dynamic = dir_sql_file + "\\" + file_sql_generic
        if not os.path.isfile(file_sql_dynamic):
                temp_msg = "The file " + file_sql_generic + " is not found on server !"
                raise ValueError(temp_msg)

        file_paramsql_output = DIRECTORY2 + "/mysql_result_" + date + '.txt'
        f_in = open(file_sql_dynamic, "rt")
        f_out = open(file_commands_sql, "wt")
        for line in f_in:
            f_out.write(line.replace('%resultat_file%', file_paramsql_output))
        f_in.close()
        f_out.close()

        params = exec_sql(type_user, file_commands_sql, file_logs_sql, file_output_result, replace_user_app, DIRECTORY, section)

        if os.stat(file_logs_sql).st_size == 0:
            if os.stat(file_output_result).st_size == 0:
                raise ValueError("MySQL Error : no rows selected")
            else:
                list_lines = []
                with open(file_output_result, "r") as file:
                    for line in file:
                        list_lines.append(line)
                error_sql = False
                if len(list_lines) == 1:
                    print("  ---> Command Executed. {} row selected in file {}".format(len(list_lines), os.path.basename(file_output_result)))
                else:
                    print("  ---> Command Executed. {} rows selected in file {}".format(len(list_lines), os.path.basename(file_output_result)))            
        else:        
            if 'ERROR' in open(file_logs_sql).read() or 'ERREUR' in open(file_logs_sql).read():
                error_sql = True
            else:
                error_sql = False

            if error_sql == True:
                list_lines = []
                with open(file_logs_sql, "r") as file:
                    for line in file:
                        list_lines.append(line)
                if len(list_lines) == 1:
                    temp_msg = "\nMySQL Error on Commands\n" + list_lines[0]
                else:
                    i = 0
                    while i < len(list_lines):
                        if i == 0:
                            temp_msg = "\nMySQL Error on Commands\n" + list_lines[i]
                        else:
                            temp_msg = temp_msg + "\n" + list_lines[i]
                        i += 1
                raise ValueError(temp_msg)
            else:
                list_lines = []
                with open(file_output_result, "r") as file:
                    for line in file:
                        list_lines.append(line)
                error_sql = False
                if len(list_lines) == 1:
                    print("  ---> Command Executed. {} row selected in file {}".format(len(list_lines), os.path.basename(file_output_result)))
                else:
                    print("  ---> Command Executed. {} rows selected in file {}".format(len(list_lines), os.path.basename(file_output_result))) 

    elif section == "oracle":
    
        file_commands_sql = DIRECTORY + "\\ora_sql_commands_" + date + ".sql"
        file_logs_sql = DIRECTORY + "\\ora_sql_logs_" + date + ".log"
        file_output_result = DIRECTORY + "\\ora_result_" + date + ".txt"

        # fichier sql générique
        file_sql_generic = "select1_oracle.sql"
        file_sql_dynamic = dir_sql_file + "\\" + file_sql_generic
        if not os.path.isfile(file_sql_dynamic):
                temp_msg = "The file " + file_sql_generic + " is not found on server !"
                raise ValueError(temp_msg)

        file_paramsql_output = DIRECTORY2 + "/ora_result_" + date + '.txt'
        f_in = open(file_sql_dynamic, "rt")
        f_out = open(file_commands_sql, "wt")
        for line in f_in:
            f_out.write(line.replace('%resultat_file%', file_paramsql_output))
        f_in.close()
        f_out.close()

        params = exec_sql(type_user, file_commands_sql, file_logs_sql, file_output_result, replace_user_app, DIRECTORY, section)
        
        if os.stat(file_output_result).st_size == 0:
            raise ValueError("Oracle Error : no rows selected")
        else:
            if 'ERROR' in open(file_output_result).read() or 'ERREUR' in open(file_output_result).read():
                error_sql = True
            else:
                error_sql = False
            
            if error_sql == True:
                list_lines = []
                with open(file_output_result, "r") as file:
                    for line in file:
                        list_lines.append(line)
                i = 0
                while i < len(list_lines):
                    if i == 0:
                        temp_msg = list_lines[i]
                    else:
                        temp_msg = temp_msg + "\n" + list_lines[i]
                    i += 1
                raise ValueError(temp_msg)
            else:
                num_lines = sum(1 for line in open(file_output_result))
                if num_lines == 1:
                    print("  ---> Command Executed. {} row selected in file {}".format(num_lines, os.path.basename(file_output_result)))
                else:
                    print("  ---> Command Executed. {} rows selected in file {}".format(num_lines, os.path.basename(file_output_result)))

    elif section == "postgresql":

        file_commands_sql = DIRECTORY + "\\psql_sql_commands_" + date + ".sql"
        file_logs_sql = DIRECTORY + "\\psql_sql_logs_" + date + ".log"
        file_output_result = DIRECTORY + "\\psql_result_" + date + ".txt"
        
        # fichier sql générique
        file_sql_generic = "select1_postgresql.sql"
        file_sql_dynamic = dir_sql_file + "\\" + file_sql_generic
        if not os.path.isfile(file_sql_dynamic):
                temp_msg = "The file " + file_sql_generic + " is not found on server !"
                raise ValueError(temp_msg)
        copy2(file_sql_dynamic, file_commands_sql)

        params = exec_sql(type_user, file_commands_sql, file_logs_sql, file_output_result, replace_user_app, DIRECTORY, section)

        if os.stat(file_logs_sql).st_size == 0:
            if os.stat(file_output_result).st_size == 0:
                raise ValueError("PostgreSQL Error : no rows selected")
            else:
                list_lines = []
                with open(file_output_result, "r") as file:
                    for line in file:
                        list_lines.append(line)
                error_sql = False
                if len(list_lines) == 1:
                    print("  ---> Command Executed. {} row selected in file {}".format(len(list_lines), os.path.basename(file_output_result)))
                else:
                    print("  ---> Command Executed. {} rows selected in file {}".format(len(list_lines), os.path.basename(file_output_result)))
        else:
            if 'ERROR' in open(file_logs_sql).read() or 'ERREUR' in open(file_logs_sql).read():
                error_sql = True
            else:
                error_sql = False

            if error_sql == True:
                list_lines = []
                First_Error = False
                with open(file_logs_sql, "r") as file:
                    for line in file:
                        list_lines.append(line)
                i = 0
                while i < len(list_lines):
                    a = list_lines[i].rstrip()
                    b = a.lstrip()
                    if b.lower().startswith("erreur") or b.lower().startswith("error"):
                        indice = i
                        indice2 = indice + 1
                        break
                    i += 1
                a = list_lines[indice].rstrip()
                b = a.lstrip()
                c = list_lines[indice2].rstrip()
                d = c.lstrip()
                if d.lower().startswith("ligne") or d.lower().startswith("line"):
                   temp_msg = list_lines[indice] + list_lines[indice2]
                else:
                    temp_msg = list_lines[indice]
                raise ValueError(temp_msg)

    else:
        temp_msg = ("Wrong name of section type database {} !\n     ---> Values accepted are : mariadb, mysql, oracle or postgresql".format(software))
        raise ValueError(temp_msg)


    move(file_output_result, file_output_csv)

    # formatage du fichier excel
    file_xlsx_output_direname = DIRECTORY
    file_xlsx_output_basename = "Student.xlsx"
    file_xlsx_output = file_xlsx_output_direname + "\\" + file_xlsx_output_basename
    if os.path.isfile(file_xlsx_output):
        os.remove(file_xlsx_output)

    params = format_xlsx_file(file_xlsx_output, file_output_csv) 


    # zip des fichiers csv et xlsx
    os.chdir(DIRECTORY)
    with open(file_list_file_to_compress, "w") as file:
        file.write(file_xlsx_output_basename)
        file.write("\n")
        file.write(file_output_csv_basename)

    no_password = True
    format_compress = "zip" # zip ou gz
    flag_tar_compress = "tar_gz_nopath" # que pour gz
    params = compress_file_by_type(format_compress, file_xlsx_output_basename, file_list_file_to_compress, DIRECTORY, no_password, flag_tar_compress)


    # Après compress des fichiers, les .csv et .xlsx peuvent être supprimés
    if os.path.isfile(file_output_csv):
        os.remove(file_output_csv)
    if os.path.isfile(file_xlsx_output):
        os.remove(file_xlsx_output)


except IOError as err:
    #print(err)
    errno, strerror = err.args
    print("Error on file {}".format(err.filename))
    print("I/O error({}): {}".format(errno, strerror))

except NameError as err:
    print(err)

except ModuleNotFoundError as err:
    print(err)

except ValueError as err:
    print(err)

except AttributeError as err:
    print(err)

except TypeError as err:
    print(err)

except KeyError as err:
    print(err)

except IndexError as err:
    print(err)

except:
   print("Unexpected error : ", sys.exc_info()[0])

finally:
    if os.path.isfile(file_commands_sql):
        os.remove(file_commands_sql)
    if os.path.isfile(file_logs_sql):
        os.remove(file_logs_sql)
    if os.path.isfile(file_list_file_to_compress):
        os.remove(file_list_file_to_compress)
    #if os.path.isfile(file_output_csv):
    #    os.remove(file_output_csv)

    print("")
    print("")
    date_stop = datetime.now().strftime('%H:%M:%S')
    print(" ------------------------------------------- ")
    print("      ... Execution Ended : {}".format(date_stop))
