#from cStringIO import StringIO
import gnupg
import logging
import os
import shutil
from nostri import pgp_public_key, gpg_home_dir

def generate_key(gpg, first_name, last_name, domain, passphrase=None):
    "Generate a key"
    params = {
        'Key-Type': 'DSA',
        'Key-Length': 1024,
        'Subkey-Type': 'ELG-E',
        'Subkey-Length': 2048,
        'Name-Comment': 'A test user',
        'Expire-Date': 0,
    }
    params['Name-Real'] = '%s %s' % (first_name, last_name)
    params['Name-Email'] = ("%s.%s@%s" % (first_name, last_name, domain)).lower()
    if passphrase is None:
        passphrase = ("%s%s" % (first_name[0], last_name)).lower()
    params['Passphrase'] = passphrase
    cmd = gpg.gen_key_input(**params)
    return gpg.gen_key(cmd)

gpg_home = gpg_home_dir
gpg = gnupg.GPG(gnupghome=gpg_home)
"""key = generate_key(gpg, "Barbara", "Brown", "beta.com")
barbara = key.fingerprint

private_keys = gpg.list_keys(True)

for key in private_keys:
    if key['fingerprint'] == barbara:
        barbara_pub_id = key['keyid']
print("Barbara fingerprint : {}".format(barbara))
print("Barbara keyid       : {}".format(barbara_pub_id))


key = generate_key(gpg, "Andrew", "Able", "alpha.com",
                            passphrase="andy")
andrew = key.fingerprint
private_keys = gpg.list_keys(True)
for key in private_keys:
    if key['fingerprint'] == andrew:
        andrew_pub_id = key['keyid']
print("Andrew fingerprint  : {}".format(andrew))
print("Andrew keyid        : {}".format(andrew_pub_id))


data = 'Student.xlsx'
stream = open(data, 'rb')
if os.path.isfile('Student.xlsx.gpg'):
    os.remove('Student.xlsx.gpg')
encrypted = gpg.encrypt_file(stream, barbara,
                                 sign=andrew, passphrase='andy',
                                 output='Student.xlsx.gpg')
#encrypted = gpg.encrypt_file(afile, barbara.split(), always_trust=False, output=savefile)
stream.close()"""

stream = open('Student.xlsx.gpg', 'rb')
verify = gpg.verify_file(stream, None)
print(verify.status)
stream.close()