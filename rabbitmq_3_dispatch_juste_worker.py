import pika
import time
import json

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='task_queue', durable=True)
print(' [*] Waiting for messages. To exit press CTRL+C')



def callback(ch, method, properties, body):
    #print(" [x] Received %r" % body)
    
    # special pour monitor velib
    message = json.loads(body.decode())
    station_number = message['number']
    contract = message['contract_name']
    address = message['address']
    nb_empty = message['number_stations_empty']
    print(" [x] Received : {} - {} - {} - Empty {}".format(station_number, contract, address, nb_empty))

    #time.sleep(body.count(b'.'))
    time.sleep(0.5)
    print(" [x] Done")
    ch.basic_ack(delivery_tag=method.delivery_tag)


channel.basic_qos(prefetch_count=1)

channel.basic_consume(
                        queue='task_queue'
                      , on_message_callback=callback
                     )

channel.start_consuming()