# -*- coding: utf-8 -*-

type_user = "app"
replace_user_app = False
import os
import sys
import subprocess
import xlsxwriter
import zipfile
import gzip
from shutil import copy2
#from psql_config import exec_sql
from db_access import exec_sql
from psql_config import format_xlsx_file
from psql_config import compress_file_xlsx
from psql_config import encrypt_file_xlsx
from psql_config import tar_file_xlsx

from nostri import DIRECTORY, DIRECTORYS, DIRECTORY2
from datetime import datetime
nb_step = 1

date = datetime.now().strftime('%H:%M:%S')
print("")
print("")
print("      ... Execution Started : {}".format(date))
print(" ------------------------------------------- ")
print("")

try:

    os.chdir(DIRECTORY)

    # récupération timestamp
    date = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')

    print("")
    print(" *** Step n. {} : Execution de la commande PSQL sur la base *** ".format(nb_step))
    nb_step += 1
    # fichier sql générique
    file_sql_dynamic = DIRECTORYS + "\\select1_postgresql.sql"
    if not os.path.isfile(file_sql_dynamic):
        temp_msg = "The file " + file_sql_dynamic + " is not found on server !"
        raise ValueError(temp_msg)

    file_cmds_sql = DIRECTORY + "\\psql_create_" + date + ".sql"
    copy2(file_sql_dynamic, file_cmds_sql)
    file_output_sql = DIRECTORY + "\\psql_resultat_" + date + ".txt"
    file_result_sql = DIRECTORY + "\\psql_sql_output_" + date + ".txt"
    file_result_csv = DIRECTORY + "\\student_" + date + ".csv"
    file_list_file_to_compress = DIRECTORY + "\\list_file_to_zip_" + date + ".txt"

    section = 'postgresql'
    params = exec_sql(type_user, file_cmds_sql, file_result_sql, file_output_sql, replace_user_app, DIRECTORY, section)

    if os.stat(file_result_sql).st_size == 0:
        #print("\nAll SQL commands on PostgreSQL are OK\n")
        if os.stat(file_output_sql).st_size == 0:
            raise ValueError("PostgreSQL Error : no rows selected")
        else:
            list_lines = []
            with open(file_output_sql, "r") as file:
                for line in file:
                    list_lines.append(line)
            error_sql = False
            if len(list_lines) == 1:
                print("  ---> Command Executed. {} row selected in file {}".format(len(list_lines), os.path.basename(file_output_sql)))
            else:
                print("  ---> Command Executed. {} rows selected in file {}".format(len(list_lines), os.path.basename(file_output_sql)))
    else:
        if 'ERREUR' in open(file_result_sql).read():
            error_sql = True
        else:
            error_sql = False

    if error_sql == True:
        list_lines = []
        First_Error = False
        with open(file_result_sql, "r") as file:
            for line in file:
                list_lines.append(line)
        i = 0
        while i < len(list_lines):
            a = list_lines[i].rstrip()
            b = a.lstrip()
            if b.lower().startswith("erreur") or b.lower().startswith("error"):
                indice = i
                indice2 = indice + 1
                break
            i += 1
        a = list_lines[indice].rstrip()
        b = a.lstrip()
        c = list_lines[indice2].rstrip()
        d = c.lstrip()
        if d.lower().startswith("ligne") or d.lower().startswith("line"):
            temp_msg = list_lines[indice] + list_lines[indice2]
        else:
            temp_msg = list_lines[indice]
        raise ValueError(temp_msg)
        
    #else:
    #    print("\nAll SQL commands on PostgreSQL are OK\n")

    print("")
    print(" *** Step n. {} : Construction du fichier Excel *** ".format(nb_step))
    nb_step += 1
    copy2(file_output_sql, file_result_csv)

    file_xlsx_output_only = "Student.xlsx"
    file_xlsx_output = DIRECTORY + "\\" + file_xlsx_output_only
    params = format_xlsx_file(file_xlsx_output, file_result_csv)
    
    print("  ---> Excel file {} successfully created".format(file_xlsx_output_only))

    os.chdir(DIRECTORY)
    # on ne passe que le nom du fichier (sans le répertoire) à la fonction zip
    file_result_csv_absolu = os.path.basename(file_result_csv)
    with open(file_list_file_to_compress, "w") as file:
        file.write(file_xlsx_output_only)
        file.write("\n")
        file.write(file_result_csv_absolu)


    print("")
    print(" *** Step n. {} : Compression ZIP des fichiers de sortie *** ".format(nb_step))
    nb_step += 1
    no_password = "True"
    format_compress = "zip"
    params = compress_file_xlsx(format_compress, file_xlsx_output_only, file_list_file_to_compress, DIRECTORY, no_password)
    print("  ---> ZIP file successfully created")

    print("")
    print(" *** Step n. {} : Compression ZIP avec mot de passe des fichiers de sortie *** ".format(nb_step))
    nb_step += 1
    no_password = "False"
    format_compress = "zip"
    params = compress_file_xlsx(format_compress, file_xlsx_output_only, file_list_file_to_compress, DIRECTORY, no_password)
    print("  ---> ZIP file successfully created")

    print("")
    print(" *** Step n. {} : Compression GZIP des fichiers de sortie *** ".format(nb_step))
    nb_step += 1
    no_password = "True"
    format_compress = "gz"
    params = compress_file_xlsx(format_compress, file_xlsx_output_only, file_list_file_to_compress, DIRECTORY, no_password)
    print("  ---> GZIP files successfully created")

    print("")
    print(" *** Step n. {} : Chiffrement GPG des fichiers de sortie *** ".format(nb_step))
    nb_step += 1
    params = encrypt_file_xlsx(file_list_file_to_compress, DIRECTORY)
    print("  ---> GPG files successfully created")

    print("")
    print(" *** Step n. {} : Create TAR container des fichiers de sortie *** ".format(nb_step))
    nb_step += 1
    params = tar_file_xlsx(file_xlsx_output_only, DIRECTORY, file_list_file_to_compress)
    print("  ---> TAR files successfully created")

    print("")
    print(" *** Step n. {} : Formatage de la sortie de la commande SQL *** ".format(nb_step))
    nb_step += 1
    fichier = file_result_csv

    print("")
    print(" ADMISSION  NAME     AGE  COURSE                  DEPARTEMENT  AMOUNT   ")
    print(" ---------  -------  ---  ----------------------  -----------  ---------")

    with open(fichier, "r") as filin:
        line = filin.readline()
        cnt = 1
        while line:
            a = line.strip()
            data = a.split(";")
            i = 1
            for row in data:
                if i == 1:
                    var_1 = str(row).rjust(9)
                    i += 1
                elif i == 2:
                    var_2 = str(row).rjust(7)
                    i += 1
                elif i == 3:
                    var_3 = str(row).rjust(3)
                    i += 1
                elif i == 4:
                    var_4 = str(row).rjust(23)
                    i += 1
                elif i == 5:
                    var_5 = str(row).rjust(12)
                    i += 1
                elif i == 6:
                    val_float = float(row.replace(',', '.'))
                    val_decim = format(val_float, '.2f')
                    var_6 = str(val_decim).rjust(10)
                    i += 1
            print(" {}  {}  {} {} {} {}".format(var_1, var_2, var_3, var_4, var_5, var_6))
            line = filin.readline()
            cnt += 1

except IOError as err:
    #print(err)
    errno, strerror = err.args
    print("Error on file {}".format(err.filename))
    print("I/O error({}): {}".format(errno, strerror))

except NameError as err:
    print(err)

except ModuleNotFoundError as err:
    print(err)

except ValueError as err:
    print(err)

except AttributeError as err:
    print(err)

except TypeError as err:
    print(err)

except KeyError as err:
    print(err)

except:
   print("Unexpected error : ", sys.exc_info()[0])

finally:
    if os.path.isfile(file_cmds_sql):
        os.remove(file_cmds_sql)
    if os.path.isfile(file_result_sql):
        os.remove(file_result_sql)
    if os.path.isfile(file_output_sql):
        os.remove(file_output_sql)
    if os.path.isfile(file_result_csv):
        os.remove(file_result_csv)
    if os.path.isfile(file_list_file_to_compress):
        os.remove(file_list_file_to_compress)
    print("")
    print("")
    date = datetime.now().strftime('%H:%M:%S')
    print(" ------------------------------------------- ")
    print("      ... Execution Ended : {}".format(date))
