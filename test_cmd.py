param = {}
param['host'] = "localhost"
param['port'] = "1421"
param['database'] = "test"

field_user = "scott"
field_passwd = "tiger"
file_exec_sql = "toto.sql"
file_result_sql = "titi.txt"
file_output_sql = "tutu.out"
oracle_home = "C:\\Program Files\\MariaDB 10.4"
wallet = "/@XE_scott"

cmd = ("echo @{} | \"{}\\bin\sqlplus\" {} > {}"
       .format(
                 file_exec_sql
               , oracle_home
               , wallet
               , file_result_sql
              )
      )
print(cmd)