import subcipher, random, string

def RandomToken(length):
   return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(length))

key = subcipher.generate_key()
Token = RandomToken(64)
encrypted = subcipher.encrypt(key, Token)

print(key)
print(Token)
print(encrypted)