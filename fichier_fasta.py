import os, sys

x = len(sys.argv)
if x == 1:
    print("Veuillez passer une variable au script")
    exit()

fichier = sys.argv[1]
try:
    fichier = str(fichier)
except ValueError:
    pass
p = isinstance(fichier,str)
if str(p) == "False":
    print("La variable passée n'est pas un string")
    exit()

t = os.path.isfile(fichier)
if str(t) == "False":
    print("Le fichier {} n'existe pas".format(fichier))
    exit()


def compte_mots_2_lettres(var_seq):
    dict_mot = {}

    i = 0
    while i < len(var_seq):
        if i != 0:
            j = i - 1
            mot = str(var_seq[j]) + str(var_seq[i])
            if len(dict_mot) == 0:
                dict_mot[mot] = 1
            else:
                if mot in dict_mot:
                    value_key = dict_mot[mot]
                    value_key += 1
                    dict_mot[mot] = value_key
                else:
                    dict_mot[mot] = 1
        i += 1

    return dict_mot

def lit_fasta(nom_fichier):
    prot_dict = {}
    with open(nom_fichier, "r") as fasta_file:
        prot_id = ""
        for line in fasta_file:
            if line.startswith(">"):
                prot_id = line[1:].split()[0]
                prot_dict[prot_id] = ""
            else:
                prot_dict[prot_id] += line.strip()
        for id in prot_dict:
            #return(id, prot_dict[id][:30])
            return(id, prot_dict[id])

id_sequence, base_sequence = lit_fasta(fichier)
dict_mot_seq = compte_mots_2_lettres(base_sequence)

print("")
print("Fichier FASTA en cours d'analyse : {}".format(fichier))
print("Séquence aminée                  : {}".format(id_sequence))
print("")
print("Mots de 2 lettres")
for key in dict_mot_seq:
    name_key = key
    value_key = dict_mot_seq[key]
    print("{} : {}".format(name_key, value_key))