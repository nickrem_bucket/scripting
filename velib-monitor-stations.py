import json, os, hashlib, copy, pickle, pika
from kafka import KafkaConsumer
from datetime import datetime

stations = {}
first = True

consumer = KafkaConsumer(  "empty-stations"
                         #, bootstrap_servers='localhost:9095'
                         , bootstrap_servers=['localhost:9095', 'localhost:9096']
                         , group_id="velib-monitor-stations"
                        )

i = 0
for message in consumer:

    message2 = copy.deepcopy(message)
    mdpass = hashlib.md5(str(message2).encode("utf-8")).hexdigest()

    name_file = "d:\\Python\\pickle\\" + str(mdpass)
    with open(name_file, 'wb') as fichier:
        mon_pickler = pickle.Pickler(fichier)
        mon_pickler.dump(message)

    station = json.loads(message.value.decode())

    station_number = station["number"]
    contract = station["contract_name"].capitalize()
    available_bikes = station["available_bikes"]
    address = station["address"]
    nb_empty = station["number_stations_empty"]

   
    date = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')
    print(mdpass)
    if available_bikes == 0:
        print("{} --- The station number {} ({} , {}), just passed empty !!!".format(date, station_number, address, contract))
        print("       Number of Empty Station in {} : {} ".format(contract, nb_empty))
        print("")
    else:
        print("{} --- The station number {} ({} , {}), is ok now".format(date, station_number, address, contract))
        print("       Number of Empty Station in {} : {} ".format(contract, nb_empty))
        print("")

    """toto = json.dumps(station).encode()
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()

    channel.queue_declare(
                            queue='task_queue'
                          , durable=True
                         )

    channel.basic_publish(
                            exchange=''
                          , routing_key='task_queue'
                          , body=toto
                          , properties=pika.BasicProperties
                            (
                               delivery_mode=2,  # make message persistent
                            )
                         )
    #print(" [x] Sent to RabbitMQ.task_queue : %r" % toto)
    print(" [x] Sent to RabbitMQ.task_queue : {} - {} - {} - Empty {}".format(station_number, address, contract, nb_empty))
    connection.close()
    
    name_file = "d:\\Python\\pickle\\" + str(mdpass) + ".in_queue"
    with open(name_file, 'w') as fichier:
        fichier.write("True")"""