import psycopg2
import cipher
import os

software = 'postgresql'
type = 'user'
user = 'postgres_app'

#a = cipher.get_password(software, 'user', 'postgres_app')
#print(a)

file_exec_sql = "test.sql"
file_result_sql = "resultat.txt"
mariadb_home = os.environ['MARIADB_HOME']
section = "mariadb"
param = {}
param['database'] = "test"
param['user_own'] = "scott"
param['user_app'] = "scott2"
param['host'] = "localhost"
param['port'] = "3306"

type_user = "own"
lib_parm_user = "user_own"

cmd = ("\"{}\bin\mysql\" -h {} -P {} -u {} -p{} {} < {} > {} 2>&1"
       .format(
                 mariadb_home
               , param['host']
               , param['port']
               , param['user_app']
               , cipher.get_password(section, 'user', param[lib_parm_user])
               , param['database']
               , file_exec_sql
               , file_result_sql
              )
      )
print(cmd)