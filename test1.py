import database

section = 'postgresql'
db = {}
db['database'] = database.__dict__[section]['database']
db['host'] = database.__dict__[section]['host']
db['port'] = database.__dict__[section]['port']
db['user_own'] = database.__dict__[section]['user_own']
db['user_app'] = database.__dict__[section]['user_app']

print(db)