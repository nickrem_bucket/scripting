class ZDict:
    """Classe enveloppe d'un dictionnaire"""
    def __init__(self):
        """Notre classe n'accepte aucun paramètre"""
        self._dictionnaire = {}
    def __getitem__(self, index):
        """Cette méthode spéciale est appelée quand on fait objet[index]
        Elle redirige vers self._dictionnaire[index]"""
        
        return self._dictionnaire[index]
    def __setitem__(self, index, valeur):
        """Cette méthode est appelée quand on écrit objet[index] = valeur
        On redirige vers self._dictionnaire[index] = valeur"""
        
        self._dictionnaire[index] = valeur
    def __delitem__(self, index):
    
        del self._dictionnaire[index]
        
class InList:
     
    def __init__(self):
        self._list = [1,2,3,4,5,6]
    def __contains__(self, demand):
        return True if demand in self._list else False

class Longueur:

    def __init__(self, mot):
        self._mot = mot
    def __len__(self):
        return len(self._mot)
        
#objet = InList()

#print("8 in objet")
#print(8 in objet)
#print("2 in objet")
#print(2 in objet)

#objet = Longueur("particulier")
#print(len(objet))

objet = ZDict()
print(objet._dictionnaire)
objet._dictionnaire['a'] = 1
print(objet._dictionnaire)
del objet['a']
print(objet._dictionnaire)