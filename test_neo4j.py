from neo4jrestclient.client import GraphDatabase
from neo4jrestclient import client
 
db = GraphDatabase("http://localhost:7474", username="neo4j", password="h6u4%kr")
 
"""# Create some nodes with labels
user = db.labels.create("User")
u1 = db.nodes.create(name="Marco")
user.add(u1)
u2 = db.nodes.create(name="Daniela")
user.add(u2)
 
beer = db.labels.create("Beer")
b1 = db.nodes.create(name="Punk IPA")
b2 = db.nodes.create(name="Hoegaarden Rosee")
# You can associate a label with many nodes in one go
beer.add(b1, b2)

# User-likes->Beer relationships
u1.relationships.create("likes", b1)
u1.relationships.create("likes", b2)
u2.relationships.create("likes", b1)
# Bi-directional relationship?
u1.relationships.create("friends", u2)"""


#q = 'MATCH (u:User)-[r:likes]->(m:Beer) WHERE u.name="Daniela" RETURN u, type(r), m'
#q = 'MATCH (Jamet:Stop {name:"Jamet"}), (Commerce:Stop {name:"Commerce"}), p = shortestPath((Jamet)-[*]-(Commerce)) WHERE NONE (r IN rels(p) WHERE r.type= "bus") RETURN p'
q = 'MATCH (Jamet:Stop) return Jamet'
# "db" as defined above
results = db.query(q, returns=(client.Node, str, client.Node))
for r in results:
    print(r)
    #print("(%s)-[%s]->(%s)" % (r[0]["name"], r[1], r[2]["name"]))
# The output:
# (Marco)-[likes]->(Punk IPA)
# (Marco)-[likes]->(Hoegaarden Rosee)