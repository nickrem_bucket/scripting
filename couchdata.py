import json
import couchdb
import couchdb.mapping as cmap

class User(cmap.Document):
    """  Class used to map a user document inside the '_users' database to a
    Python object.

    For better understanding check https://wiki.apache.org
        /couchdb/Security_Features_Overview

    Args:
        name: Name of the user
        password: password of the user in plain text
        type: (Must be) 'user'
        roles: Roles for the users

    """

    def __init__(self, **values):
        # For user in the _users database id must be org.couchdb.user:<name>
        # Here we're auto-generating it.
        if 'name' in values:
            _id = 'org.couchdb.user:{}'.format(values['name'])
            cmap.Document.__init__(self, id=_id, **values)

    type = cmap.TextField(default='user')
    name = cmap.TextField()
    password = cmap.TextField()
    roles = cmap.ListField(cmap.TextField())

    @cmap.ViewField.define('users')
    def default(doc):
        if doc['name']:
            yield doc['name'], doc

with open('DBdata.json') as data_file:    
    data = json.load(data_file)
    ssl = data.get('ssl')
    user_name = data.get('username')
    user_pass = data.get('password')
    host = data.get('host')
    port = data.get('port')
    data_file.close()
    if ssl is False:
        ssl = 'http://'
    else: 
        ssl = 'https://'

    #couch = couchdb.Server(ssl + str(username) + ':' + str(password) + '@' + str(host) +':' + str(port) + '/')


#couchserver = couchdb.Server("http://localhost:5984")


#couchserver = couchdb.Server("http://admin:nicolas@localhost:5984")
#dbname = "_users"
#db = couchserver[dbname]
#user_params = User(name=user_name, password=user_pass)
#print(user_params)
#user_params.store(db)

#exit()
liste_id = []
liste_all_id = []


couchserver = couchdb.Server("http://scott:tiger@localhost:5984")
dbname = "nicodb"

if dbname in couchserver:
    db = couchserver[dbname]
else:
    db = couchserver.create(dbname)

print(db)
#print("")

for dbname in couchserver:
	print(dbname)

dbname = "_users"
if dbname in couchserver:
    db = couchserver[dbname]
else:
    db = couchserver.create(dbname)
couchdb_selector = {  "selector": {"_id": {"$gt": 0}}
                  , "fields"  : ["_id", "_rev", "name", "password_scheme"]}

for row in db.find(couchdb_selector):
    print(row)

exit()
print("")
#doc_id, doc_rev = db.save({'key': 'value'})
docs = [{'key': 'value1'}, {'key': 'value2'}]
#for (success, doc_id, revision_or_exception) in db.update(docs):
first = 0
for doc in db.update(docs):
    Return_Code = doc[0]
    Id_Doc = doc[1]
    rev_or_excep = doc[2]
    liste_id.append(Id_Doc)
    #print("Return code : {}".format(Return_Code))
    #print("")
    if first == 0:
        del db[Id_Doc]
        first = 1
        test = db.get(Id_Doc)
        print(test)

couchdb_selector = {  "selector": {"_id": {"$gt": 0}}
                  , "fields"  : ["_id", "_rev", "key"]}

for row in db.find(couchdb_selector):
    liste_temp = []
    liste_temp.append(row['_id'])
    liste_temp.append(row['_rev'])
    liste_temp.append(row['key'])
    liste_all_id.append(liste_temp)

#print(liste_id)
print(liste_all_id)

print("")


"""if dbname in couchserver:
    del couchserver[dbname]

for dbname in couchserver:
	print(dbname)"""