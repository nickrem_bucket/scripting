class Personne:
    
    """Classe définissant une personne.
    
    Elle possède comme attributs :
    nom -- le nom de la personne
    prenom -- son prénom
    age -- son âge
    lieu_residence -- son lieu de résidence
    
    Le nom et le prénom doivent être passés au constructeur."""
    
    #def __new__(cls, nom, prenom):
    #    print("Appel de la méthode __new__ de la classe {}".format(cls))
    #    # On laisse le travail à object
    #    return object.__new__(cls, nom, prenom)
    
    #def __init__(self, nom, prenom):
    #    """Constructeur de notre personne."""
    #    print("Appel de la méthode __init__")
    #    self.nom = nom
    #    self.prenom = prenom
    #    self.age = 23
    #    self.lieu_residence = "Lyon"

    def creer_personne(personne, nom, prenom):
        """La fonction qui jouera le rôle de constructeur pour notre classe Personne.
    
        Elle prend en paramètre, outre la personne :
        nom -- son nom
        prenom -- son prenom"""
    
        personne.nom = nom
        personne.prenom = prenom
        personne.age = 21
        personne.lieu_residence = "Lyon"

    def presenter_personne(personne):
        """Fonction présentant la personne.
    
        Elle affiche son prénom et son nom"""
    
        print("{} {}".format(personne.prenom, personne.nom))

    # Dictionnaire des méthodes
    methodes = {
        "__init__": creer_personne,
        "presenter": presenter_personne,
    }

    # Création dynamique de la classe
    Personne = type("Personne", (), methodes)

#Personne = type("Personne", (), {})
#print(Personne)

#john = Personne()
#print(john)
#print(dir(john))

john = Personne("Doe", "John")
print(john.nom)
print(john.prenom)
print(john.age)