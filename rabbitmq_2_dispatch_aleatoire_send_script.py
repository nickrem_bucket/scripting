import subprocess, time

i = 1
while i <= 10:
    if i == 1:
        message = "1st message."
    elif i == 2:
        message = "2nd message.."
    elif i == 3:
        message = "3rd message..."
    else:
        message = str(i) + "th message" + i*"."

    cmd = "python3 rabbitmq_2_dispatch_aleatoire_sender.py " + message
    #print(cmd)
    subprocess.call(cmd, shell=True)

    #time.sleep(2)
    i = i + 1
