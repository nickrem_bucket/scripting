from nostri import DIRECTORY

file_output_sql = DIRECTORY + "\\ora_sql_output_2019-12-10_00-46-16.txt"

if 'ERROR' in open(file_output_sql).read():
    error_sql = True
else:
    if 'ERREUR' in open(file_output_sql).read():
        error_sql = True
    else:
        error_sql = False

if error_sql == True:
    list_lines = []
    First_Error = False
    with open(file_output_sql, "r") as file:
        for line in file:
            list_lines.append(line)
    i = 0
    while i < len(list_lines):
        a = list_lines[i].rstrip()
        b = a.lstrip()
        if b.lower().startswith("erreur") or b.lower().startswith("error"):
            indice = i
            indice2 = indice + 1
            break
        i += 1
    a = list_lines[indice].rstrip()
    b = list_lines[indice2].rstrip()
    temp_msg = a.lstrip() + '\n' + b.lstrip() 
    print(temp_msg)
else:
    print("\nAll SQL Create Synonym commands on Oracle are OK\n")