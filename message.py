"""Module inutile qui affiche des messages :-)."""
import time

from datetime import datetime
DATE = datetime.now().strftime('%Y-%m-%d')


def bonjour(nom):
    """Dit Bonjour."""
    return "Bonjour " + nom


def ciao(nom):
    """Dit Ciao."""
    return "Ciao " + nom


def hello(nom):
    """Dit Hello."""
    return "Hello " + nom