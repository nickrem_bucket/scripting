import mysql.connector as MySQL
import mysql.connector as MariaDB
import keyring
from mysql.connector import Error

try:
    db_name = "demo"
    user_name = "root"
    key_db = "mysql_" + db_name
    passwd = keyring.get_password(key_db, user_name)
    connection = MySQL.connect(  host='localhost'
                               , port='3307'
                               , database=db_name
                               , user=user_name
                               , password=passwd)

    if connection.is_connected():
        db_Info = connection.get_server_info()
        print("Connected to MySQL Server version ", db_Info)
        cursor = connection.cursor()
        cursor.execute("select database();")
        record = cursor.fetchone()
        print("You're connected to database: ", record)

except Error as e:
    print("Error while connecting to MySQL", e)
finally:
    if (connection.is_connected()):
        cursor.close()
        connection.close()
        print("MySQL connection is closed")

print("")
print("")
try:
    db_name = "test"
    user_name = "root"
    key_db = "mariadb_" + db_name
    passwd2 = keyring.get_password(key_db, user_name)
    
    connection = MariaDB.connect(  host='localhost'
                                 , port='3306'
                                 , database=db_name
                                 , user=user_name
                                 , password=passwd2)

    if connection.is_connected():
        db_Info = connection.get_server_info()
        print("Connected to MariaDB Server version ", db_Info)
        cursor = connection.cursor()
        cursor.execute("select database();")
        record = cursor.fetchone()
        print("You're connected to database: ", record)

except Error as e:
    print("Error while connecting to MariaDB", e)
finally:
    if (connection.is_connected()):
        cursor.close()
        connection.close()
        print("MariaDB connection is closed")