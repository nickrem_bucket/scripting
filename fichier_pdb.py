import os, sys

x = len(sys.argv)
if x == 1:
    print("Veuillez passer une variable au script")
    print("Paramètres manquants :")
    print("    - Param 1 : nb lettre (integer)")
    print("    - Param 2 : fichier a traiter (char)")
    exit()
elif x == 2:
    print("")
    n = sys.argv[1]
    try:
        n = int(n)
    except ValueError:
        pass
    p = isinstance(n,int)
    if str(p) == "False":
        print("Le paramètre numéro 1 passé n'est pas un integer")
        exit()
        
    print("Paramètre numéro 2 manquant :")
    print("    - Param 2 : fichier a traiter (char)")
    exit()
elif x == 3:
    n = sys.argv[1]
    fichier = sys.argv[2]

    try:
        n = int(n)
    except ValueError:
        pass
    p = isinstance(n,int)
    if str(p) == "False":
        print("")
        print("La paramètre numéro 1 passé n'est pas un integer")
        exit()

    sys.path.insert(0, 'd:\\Python')
    t = os.path.isfile(fichier)
    if str(t) == "False":
        print("")
        print("Le fichier {} n'existe pas dans le répertoire d:\\Python".format(fichier))
        exit()
else:
    print("")
    print("Trop de Paramètres :")
    print("    - Param 1 : nb lettre (integer)")
    print("    - Param 2 : fichier a traiter (char)")
    exit()

def compte_mots_n_lettres(nb_char, var_seq):
    dict_mot = {}

    i1 = 0
    list_nb1 = [] 
    while i1 < (nb_char - 1):
        list_nb1.append(i1)
        i1 += 1

    i2 = 0
    while i2 < len(var_seq):
        list_nb2 = []

        if i2 not in list_nb1:
            fin = 0
            var = i2
            nb_tour = 1
            while fin == 0:
                list_nb2.append(var)
                var = var - 1
                nb_tour += 1
                if nb_tour == (nb_char + 1) :
                    fin = 1
                else:
                    if var == -1:
                        fin = 1
            list_nb2.sort()

            indice = 0
            while indice < len(list_nb2):
                if indice == 0:
                    mot = var_seq[list_nb2[indice]]
                else:
                    mot = mot + var_seq[list_nb2[indice]]
                indice += 1
            

        
            if len(dict_mot) == 0:
                dict_mot[mot] = 1
            else:
                if mot in dict_mot:
                    value_key = dict_mot[mot]
                    value_key += 1
                    dict_mot[mot] = value_key
                else:
                    dict_mot[mot] = 1
        i2 += 1
    
    return dict_mot

def lit_pdb(nom_fichier):
    pdb_dict = {}
    with open(nom_fichier, "r") as pdb_file:
        for line in pdb_file:
            if line.startswith("ATOM"):
                num_atom = line[1:].split()[1]
                name_atom = line[1:].split()[11]
                resid_atom = line[1:].split()[5]
                coord_x_atom = line[1:].split()[6]
                coord_y_atom = line[1:].split()[7]
                coord_z_atom = line[1:].split()[8]
                list_atom = [resid_atom, coord_x_atom, coord_y_atom, coord_z_atom]
                if name_atom == "C":
                    pdb_dict[num_atom] = list_atom
    return pdb_dict


dict_atom_c = lit_pdb(fichier)


print("")
print("Fichier pdb en cours d'analyse : {}".format(fichier))
print("")

print("Nb total d'atome de Carbone    : {}".format(len(dict_atom_c)))
print("")

first_time_1 = 0
first_time_2 = 0

for key in dict_atom_c:
    if int(dict_atom_c[key][0]) == 1:
        if first_time_1 == 0:
            print("")
            print("Résidu 1 :")
            first_time_1 = 1
        print("     - Numéro Atome : {}".format(key))
        print("           Coord. x : {}".format(dict_atom_c[key][1]))
        print("           Coord. y : {}".format(dict_atom_c[key][2]))
        print("           Coord. z : {}".format(dict_atom_c[key][3]))
    if int(dict_atom_c[key][0]) == 2:
        if first_time_2 == 0:
            print("")
            print("Résidu 2 :")
            first_time_2 = 1
        print("     - Numéro Atome : {}".format(key))
        print("           Coord. x : {}".format(dict_atom_c[key][1]))
        print("           Coord. y : {}".format(dict_atom_c[key][2]))
        print("           Coord. z : {}".format(dict_atom_c[key][3]))
            
exit()