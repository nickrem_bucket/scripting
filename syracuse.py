# ----- script.py ----

import sys

x = len(sys.argv)
if x == 1:
    print("Veuillez passer une variable au script")
    exit()

y = sys.argv[1]
try:
    y = int(y)
except ValueError:
    pass
p = isinstance(y,int)
if str(p) == "False":
    print("La variable passée n'est pas un integer")
    exit()

suite = [y]


fin = 0
var_pivot = int(y)
while fin == 0:
    test_pair = int(var_pivot) % 2
    if test_pair == 0:
        var_calc = int(var_pivot) / 2
    else:
        var_calc = (int(var_pivot) * 3) + 1

    suite.append(int(var_calc))

    if int(var_calc) == 1:
        fin = 1
    else:
        var_pivot = int(var_calc)
			
i = 0
while i < len(suite):
    if i == 0:
        list_disp = str(suite[i])
    else:
        list_disp = list_disp + ", " + str(suite[i])
    i = i + 1
    
print("")
print("Calcul conjecture de Syracuse")
print("")
print("     - Premier terme : {}".format(y))
print("     - Suite         : {}".format(list_disp))