import copy

list = [8, 3, 12.5, 45, 25.5, 52, 1]

old_list = copy.deepcopy(list)
new_list = []


while old_list:
    minimum = old_list[0]
    for x in old_list:
        if x < minimum:
            minimum = x
    new_list.append(minimum)
    old_list.remove(minimum)

print("")
print("Liste en cours : {}".format(list))
print("Liste triée    : {}".format(new_list))

print("")
list2 = copy.deepcopy(list)
list2.sort()
print("Result. sort() : {}".format(list2))