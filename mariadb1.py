import os, sys
import xlsxwriter
from mariadb_config import exec_sql
from mariadb_config import format_xlsx_file
from mariadb_config import compress_file_xlsx
from mariadb_config import encrypt_file_xlsx
from nostri import DIRECTORY, DIRECTORY2, DIRECTORYS
from datetime import datetime


try:

    # récupération timestamp
    date = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')

    # fichier sql générique
    file_sql_dynamic = DIRECTORYS + "\\select1 _mariadb.sql"
    if not os.path.isfile(file_sql_dynamic):
        temp_msg = "The file " + file_sql_dynamic + " is not found on server !"
        raise ValueError(temp_msg)

    # formatage du fichier de commande sql
    file_cmds_sql = DIRECTORY + "\\select_" + date + ".sql"
    file_output_sql = DIRECTORY2 + "/resultat_" + date + '.txt'
    f_in = open(file_sql_dynamic, "rt")
    f_out = open(file_cmds_sql, "wt")
    for line in f_in:
        f_out.write(line.replace('%resultat_file%', file_output_sql))
    f_in.close()
    f_out.close()

    file_result_sql = DIRECTORY + "\\mariadb_sql_select_output_" + date + ".txt"
    file_result_csv = DIRECTORY + "\\student_" + date + ".csv"

    params = exec_sql("own", file_cmds_sql, file_result_sql)

    if os.stat(file_output_sql).st_size == 0:
        raise ValueError("MariaDB Error : no rows selected")
    else:
        if 'ERROR' in open(file_result_sql).read():
            error_sql = True
        else:
            error_sql = False
    
    if error_sql == True:
        list_lines = []
        with open(file_result_sql, "r") as file:
            for line in file:
                list_lines.append(line)
        if len(list_lines) == 1:
            temp_msg = "\nMariaDB Error on Commands\n" + list_lines[0]
        else:
            i = 0
            while i < len(list_lines):
                if i == 0:
                    temp_msg = "\nMariaDB Error on Commands\n" + list_lines[i]
                else:
                    temp_msg = temp_msg + "\n" + list_lines[i]
                i += 1
        raise ValueError(temp_msg)
    else:
        list_lines = []
        with open(file_output_sql, "r") as file:
            for line in file:
                a = line.rstrip()
                b = a.lstrip()
                if not b.lower().startswith("result"):
                    c = b.split(";")
                    list_lines.append(c)
    
    with open(file_result_csv, "w") as file:
        i = 0
        while i < len(list_lines):
            j = 0
            first = True
            while j < len(list_lines[i]):
                if first == True:
                    output = str(list_lines[i][j])
                    first = False
                else:
                    if j == 5:
                        a = float(list_lines[i][j])
                        b = "{0:.2f}".format(round(a,2))
                    else:
                        b = list_lines[i][j]
                    output = output + ';' + str(b)
                j += 1

            line_output = output + "\n"
            file.write(line_output)

            i += 1


    file_xlsx_output = DIRECTORY + "\\Student.xlsx"
    if os.path.isfile(file_xlsx_output):
        os.remove(file_xlsx_output)

    params = format_xlsx_file(file_xlsx_output, file_result_csv)
    
    file_xlsx_zip = file_xlsx_output + ".zip"
    if os.path.isfile(file_xlsx_zip):
        os.remove(file_xlsx_zip)
    
    no_password = "True"
    format_compress = "zip"
    params = compress_file_xlsx(format_compress, file_xlsx_output, file_xlsx_zip, no_password)
    
    file_xlsx_zip_with_password = file_xlsx_output + "pass.zip"
    if os.path.isfile(file_xlsx_zip_with_password):
        os.remove(file_xlsx_zip_with_password)
    
    no_password = "False"
    format_compress = "zip"
    params = compress_file_xlsx(format_compress, file_xlsx_output, file_xlsx_zip_with_password, no_password)

    file_xlsx_gz = file_xlsx_output + ".gz"
    if os.path.isfile(file_xlsx_gz):
        os.remove(file_xlsx_gz)
    no_password = "True"
    format_compress = "gz"
    params = compress_file_xlsx(format_compress, file_xlsx_output, file_xlsx_gz, no_password)

    if os.path.isfile(file_cmds_sql):
        os.remove(file_cmds_sql)
    if os.path.isfile(file_result_sql):
        os.remove(file_result_sql)
    if os.path.isfile(file_output_sql):
        os.remove(file_output_sql)

    file_xlsx_gpg = file_xlsx_output + ".gpg"
    if os.path.isfile(file_xlsx_gpg):
        os.remove(file_xlsx_gpg)
    params = encrypt_file_xlsx(file_xlsx_output, file_xlsx_gpg)


except ValueError as err:
    print(err)
    if os.path.isfile(file_cmds_sql):
        os.remove(file_cmds_sql)
    if os.path.isfile(file_result_sql):
        os.remove(file_result_sql)
    if os.path.isfile(file_output_sql):
        os.remove(file_output_sql)

except AttributeError as err:
    print(err)
    
except NameError as err:
    print(err)

except TypeError as err:
    print(err)

except:
   print("Unexpected error : ", sys.exc_info()[0])

finally:
    print("")
    print("")
    from datetime import datetime
    date = datetime.now().strftime('%H:%M:%S')
    print(" ------------------------------------------- ")
    print("      ... Execution Ended : {}".format(date))
