import pika
import json

connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.exchange_declare(
                           exchange='logs'
                         , exchange_type='fanout')

result = channel.queue_declare(
                                 queue=''
                               , exclusive=True
                              )
                               
queue_name = result.method.queue

channel.queue_bind(
                     exchange='logs'
                   , queue=queue_name
                  )

print(' [*] Waiting for logs. To exit press CTRL+C')

def callback(ch, method, properties, body):
    #print(" [x] %r" % body)
    print("Method: {}".format(method))
    print("Properties : {}".format(properties))
    data = json.loads(body)
    print("ID: {}".format(data['id']))
    print("Name: {}".format(data['name']))
    print("Description: {}".format(data['description']))

channel.basic_consume(
                        queue=queue_name
                      , on_message_callback=callback
                      , auto_ack=True
                     )

channel.start_consuming()