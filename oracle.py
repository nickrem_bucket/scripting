import os, sys, cx_Oracle, subprocess, xlsxwriter, time

LOCATION = r"D:\oracle\product\18.0.0\dbhomeXE\network\admin"

sys.path.insert(0, 'd:\\Python\\params')
import conn_oracle
import nostri
from conn_oracle import var_user2, var_sid
from nostri import DIRECTORY
fichier = "student.csv"
fichier_absolu = DIRECTORY + "\\student.csv"
fichier_excel = "Student.xlsx"
fichier_xlsx = DIRECTORY + "\\Student.xlsx"
nb_step = 1
wallet_user_app = "/@" + var_sid + "_" + var_user2

from datetime import datetime
date = datetime.now().strftime('%H:%M:%S')
print("")
print("")
print("      ... Execution Started : {}".format(date))
print(" ------------------------------------------- ")
print("")

conn = None
try:
    os.environ["TNS_ADMIN"] = LOCATION
    
    print("")
    print(" *** Step n. {} : Oracle Base Open *** ".format(nb_step))
    nb_step += nb_step
    conn = cx_Oracle.connect(wallet_user_app)
    print("         --> Database opened successfully")   

    cur = conn.cursor()

    #idVar = cur.var(cx_Oracle.NUMBER)
    #cur.execute(""" insert into COURSE (NAME) values ('test') returning ID into :id """, id = idVar);
    #id = idVar.getvalue()
    #print(int(id[0]))

    #conn.commit()
    #conn.close()
    #exit()

    #cur.execute(""" update STUDENT set AGE = 19 where ADMISSION = 3424 """);
    #conn.commit()
    #print("Total updated rows:", cur.rowcount)

    print("")
    print(" *** Step n. {} : Select Table STUDENT *** ".format(nb_step))
    nb_step += 1
    cur.execute("""
        select    stu.ADMISSION
                , trim(stu.NAME)
                , stu.AGE
                , trim(cou.NAME)
                , trim(dep.NAME)
                , cou.amount
          from    STUDENT stu
                , COURSE cou
                , DEPARTMENT dep
         where
                (     stu.COURSE     = cou.ID
                 and  stu.DEPARTMENT = dep.ID)
         order by stu.ADMISSION
        """);
    rows = cur.fetchall()

    cur.close()
    print("         --> Query execute successfully")

except cx_Oracle.DatabaseError as e:
    error, = e.args
    print(error.code)
    print(error.message)
    print(error.context)
    print("")

finally:
    if conn is not None:
        print("")
        print(" *** Step n. {} : Oracle Base Close *** ".format(nb_step))
        nb_step += 1
        conn.close()
        print("         --> Database closed successfully")

list_student = []
for row in rows:
    value = row[5] / 100
    print(value)
    infos = [row[0], row[1], row[2], row[3], row[4], row[5]]
    list_student.append(infos)

#print(list_student)


filwri = None
try:

    print("")
    print(" *** Step n. {} : Delete file CSV if exist *** ".format(nb_step))
    nb_step += 1
    t = os.path.isfile(fichier_absolu)
    if str(t) == "True":
        subprocess.call("del {}".format(fichier_absolu), shell=True)
    print("         --> File {} deleted successfully".format(fichier))

    print("")
    print(" *** Step n. {} : Create File CSV *** ".format(nb_step))
    nb_step += 1
    filwri = open(fichier_absolu, "a")

    i = 0
    while i < len(list_student):
        j = 0
        while j < len(list_student[i]):
            if j == 0:
                output = str(list_student[i][j])
            else:
                output = output + ";" + str(list_student[i][j])
            j += 1
    
        output = output + "\n"
        filwri.write(output)
        i += 1
    print("         --> File {} created successfully".format(fichier))
    creatTimeFileCsv = os.path.getmtime(fichier_absolu)
    createdTime = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(creatTimeFileCsv))
    print("             Timestamp Creation File : {}".format(createdTime))

   
except IOError as err:
    errno, strerror = err.args
    print("Error on file {}".format(fichier_absolu))
    print("I/O error({}): {}".format(errno, strerror))
    exit()

except NameError as err:
    print(err)
    exit()

except ValueError as err:
    print(err)
    exit()

except:
   print("Unexpected error : ", sys.exc_info()[0])
   exit()

finally:
    if filwri is not None:
        #print("")
        #print(" *** Step n. {} : Closing File CSV *** ".format(nb_step))
        #nb_step += 1
        filwri.close()
        #print("         --> File {} cclosed successfully".format(fichier))


time.sleep(1)
#workbook = None
try:

    print("")
    print(" *** Step n. {} : Delete file Excel if exist *** ".format(nb_step))
    nb_step += 1
    t = os.path.isfile(fichier_absolu)
    if str(t) == "True":
        subprocess.call("del {}".format(fichier_xlsx), shell=True)
    print("         --> File {} deleted successfully".format(fichier_excel))


    print("")
    print(" *** Step n. {} : Create File Excel *** ".format(nb_step))
    nb_step += 1
    # creation du fichier excel    
    workbook = xlsxwriter.Workbook('Student.xlsx')
    worksheet = workbook.add_worksheet()

    # Format inclus dans le fichier (bold pour les titres, money pour le format des montants)
    bold = workbook.add_format({'bold': True})
    money = workbook.add_format({'num_format': '#,##0'})

    # Ecriture des titres des colonnes
    worksheet.write('A1', 'Admission', bold)
    worksheet.write('B1', 'Name', bold)
    worksheet.write('C1', 'Age', bold)
    worksheet.write('D1', 'Course', bold)
    worksheet.write('E1', 'Department', bold)
    worksheet.write('F1', 'Amount', bold)

    # définition de la première cellule
    row = 1
    col = 0

    # Iteration de la list_student pour l'ecrire sur le tableau excel
    for adm, nam, age, cou, dep, amo in (list_student):
        worksheet.write(row,col, adm)
        worksheet.write(row, col + 1, nam)
        worksheet.write(row, col + 2, age)
        worksheet.write(row, col + 3, cou)
        worksheet.write(row, col + 4, dep)
        worksheet.write(row, col + 5, amo, money)
        row += 1

    # ecriture du total des montants avec une formule
    worksheet.write(row, 4, 'Total',       bold)
    worksheet.write(row, 5, '=SUM(F2:F7)', money)

    workbook.close()
    
    print("         --> File {} created successfully".format(fichier_excel))
    creatTimeFileExcel = os.path.getmtime(fichier_xlsx)
    createdTime = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(creatTimeFileExcel))
    print("             Timestamp Creation File : {}".format(createdTime))
    #modifTimeFileExcel = os.path.getctime(fichier_xlsx)
    #modificTime = time.strftime('%Y%m%d%H%M%S', time.localtime(creatTimeFileExcel))
    
except IOError as err:
    errno, strerror = err.args
    print("Error on file {}".format(fichier_xlsx))
    print("I/O error({}): {}".format(errno, strerror))
    exit()

except NameError as err:
    print(err)
    exit()

except ValueError as err:
    print(err)
    exit()

except:
   print("Unexpected error : ", sys.exc_info()[0])
   exit()

#finally:
#    if workbook is not None:
#        print("")
#        print(" *** Step n. {} : Closing File Excel*** ".format(nb_step))
#        nb_step += 1
#        workbook.close()
#        print("         --> File {} cclosed successfully".format(fichier_excel))

#creatTimeFileExcel = os.path.getmtime(fichier_xlsx)
#createdTime = time.strftime('%Y-%-m-%d %H:%:M:%S', time.localtime(creatTimeFileExcel))
#display = "Timestamp Created file {} : {} ".format(fichier_xlsx, createdTime)
#print(display)
#print("Last modified: %s" % time.ctime(os.path.getmtime(fichier_absolu)))
#print("Created: %s" % time.ctime(os.path.getctime(fichier_absolu)))

print("")
print("")
print(" *** Step n. {} : Format Result Select Query *** ".format(nb_step))
nb_step += 1
print("")
print(" ADMISSION  NAME     AGE  COURSE                  DEPARTEMENT  AMOUNT   ")
print(" ---------  -------  ---  ----------------------  -----------  ---------")

print(list_student[1])
i = 0
while i < len(list_student):
    var_1 = str(list_student[i][0]).rjust(9)
    var_2 = str(list_student[i][1]).rjust(7)
    var_3 = str(list_student[i][2]).rjust(3)
    var_4 = str(list_student[i][3]).rjust(23)
    var_5 = str(list_student[i][4]).rjust(12)
    var_6 = str(list_student[i][5]).rjust(10)
    print(" {}  {}  {} {} {} {}".format(var_1, var_2, var_3, var_4, var_5, var_6))
    i = i + 1
    
print("")
print("")
print("")
from datetime import datetime
date = datetime.now().strftime('%H:%M:%S')
print(" ------------------------------------------- ")
print("      ... Execution Ended : {}".format(date))
