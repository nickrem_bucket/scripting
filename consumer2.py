from confluent_kafka import Consumer, KafkaException
import sys
import os
import getopt
import json
import logging
import importlib
from pprint import pformat
from datetime import datetime


def stats_cb(stats_json_str):
    stats_json = json.loads(stats_json_str)
    print('\nKAFKA Stats: {}\n'.format(pformat(stats_json)))


def print_usage_and_exit(program_name):
    sys.stderr.write('Usage: %s [options..] <bootstrap-brokers> <group> <topic1> <topic2> ..\n' % program_name)
    options = '''
 Options:
  -T <intvl>   Enable client statistics at specified interval (ms)
'''
    sys.stderr.write(options)
    sys.exit(1)


if __name__ == '__main__':
    optlist, argv = getopt.getopt(sys.argv[1:], 'T:')

    nb_params = len(sys.argv)
    if nb_params < 3:
        print("Usage: consumer2.py <Topic Config File...> <Intervalle...>")
        print("")
        print(" Options:")
        print("  <Topic config file>   This file is set to the pythonpath")
        print("")
        print("  <Intervalle>          Enable client statistics at specified interval (ms)")
        print("")
        exit()
    elif nb_params == 3:
        params = sys.argv[1]
        file = params + ".py"
        file_absolu = "d:\\data\\module\\params\\" + file
        if not os.path.isfile(file_absolu):
            print("")
            print("The file {} is not found on the params directory".format(file))
            exit()
        
        try:
            intval = int(sys.argv[2])
        except ValueError:
            #sys.stderr.write("Invalid option value for -T: %s\n" % opt[1])
            sys.stderr.write("Invalid option value for intervalle: %s\n" % sys.argv[2])
            sys.exit(1)
    else:
        print("Usage: consumer2.py <Topic Config File...> <Intervalle...>")
        print("")
        print(" Options:")
        print("  <Topic config file>   This file is set to the pythonpath")
        print("")
        print("  <Intervalle>          Enable client statistics at specified interval (ms)")
        print("")
        exit()

    #if len(argv) < 3:
    #    print_usage_and_exit(sys.argv[0])

    #broker = argv[0]
    #group = argv[1]
    #topics = argv[2:]
    #topics = argv[2:]
    # Consumer configuration
    # See https://github.com/edenhill/librdkafka/blob/master/CONFIGURATION.md
    module = importlib.import_module(params)
    globals().update(module.__dict__)
    conf = {
              'bootstrap.servers': broker
            , 'group.id': groupid
            , 'session.timeout.ms': timeout_ms
            , 'auto.offset.reset': auto_offset_reset
            , 'stats_cb': stats_cb
            , 'statistics.interval.ms': intval
           }
    #conf = {
    #          'bootstrap.servers': "localhost:9095, localhost:9096"              
    #        , 'group.id': "velib-monitor-stations"
    #        , 'session.timeout.ms': 6000
    #        , 'auto.offset.reset': 'earliest'
    #       }

    topics = [topic]

    # Check to see if -T option exists
    """for opt in optlist:
        if opt[0] != '-T':
            continue
        try:
            intval = int(opt[1])
        except ValueError:
            sys.stderr.write("Invalid option value for -T: %s\n" % opt[1])
            sys.exit(1)

        if intval <= 0:
            sys.stderr.write("-T option value needs to be larger than zero: %s\n" % opt[1])
            sys.exit(1)

        conf['stats_cb'] = stats_cb
        conf['statistics.interval.ms'] = int(opt[1])"""
    

    # Create logger for consumer (logs will be emitted when poll() is called)
    logger = logging.getLogger('consumer')
    logger.setLevel(logging.DEBUG)
    handler = logging.StreamHandler()
    handler.setFormatter(logging.Formatter('%(asctime)-15s %(levelname)-8s %(message)s'))
    logger.addHandler(handler)

    # Create Consumer instance
    # Hint: try debug='fetch' to generate some log messages
    c = Consumer(conf, logger=logger)

    def print_assignment(consumer, partitions):
        print('Assignment:', partitions)

    # Subscribe to topics
    c.subscribe(topics, on_assign=print_assignment)

    # Read messages from Kafka, print to stdout
    try:
        while True:
            msg = c.poll(timeout=1.0)
            if msg is None:
                continue
            if msg.error():
                raise KafkaException(msg.error())
            else:
                # Proper message
                date = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')
                sys.stderr.write("")
                sys.stderr.write('\n %% %s %s [%d] at offset %d with key %s:\n' %
                                 (date, msg.topic(), msg.partition(), msg.offset(),
                                  str(msg.key())))
                print(msg.value())

    except KeyboardInterrupt:
        sys.stderr.write('%% Aborted by user\n')

    finally:
        # Close down consumer to commit final offsets.
        c.close()