import logging

log = logging.getLogger()
#log.setLevel('DEBUG')
log.setLevel('INFO')
handler = logging.StreamHandler()
handler.setFormatter(logging.Formatter("%(asctime)s [%(levelname)s] %(name)s: %(message)s"))
log.addHandler(handler)


from cassandra import ConsistencyLevel
from cassandra.cluster import Cluster
from cassandra.query import SimpleStatement
from cassandra.util import OrderedMapSerializedKey

KEYSPACE = "University"

def main():
    cluster = Cluster(['127.0.0.1'])
    session = cluster.connect()

    rows = session.execute("SELECT keyspace_name FROM system_schema.keyspaces")
    if KEYSPACE in [row[0].capitalize() for row in rows]:
        log.info("dropping existing keyspace...")
        session.execute("DROP KEYSPACE " + KEYSPACE)

    log.info("creating keyspace...")
    session.execute("""
        CREATE KEYSPACE %s
        WITH replication = { 'class': 'SimpleStrategy', 'replication_factor': '2' }
        """ % KEYSPACE)

    log.info("setting keyspace...")
    session.set_keyspace(KEYSPACE.lower())

    log.info("creating table...")
    # set<text> <-- collection
    session.execute("""
        CREATE TABLE Teacher (
            id INT,
            Name text,
            Email set<text>,
            PRIMARY KEY (id)
        )
        """)

    cql_Statement= "INSERT INTO Teacher(id, name, email) VALUES (1, 'Guru99', {'abc@hotmail.com', 'xyz^hotmail.com'});"
    session.execute(cql_Statement)
    cql_Statement = "INSERT INTO Teacher(id, name) VALUES (2, 'Max77');"
    session.execute(cql_Statement)
    cql_Statement = "UPDATE Teacher SET email = {'oqp@outlook.com'} WHERE id = 2;"
    session.execute(cql_Statement)
    cql_Statement = "UPDATE Teacher SET email = email + {'lol@yahoo.com'} WHERE id = 2;"
    session.execute(cql_Statement)
    # list<text> <-- list collection
    cql_Statement = "ALTER TABLE Teacher ADD courseNames list<text>;"
    session.execute(cql_Statement)
    cql_Statement = "UPDATE Teacher set courseNames = ['Data Science'] WHERE id = 1;"
    session.execute(cql_Statement)
    cql_Statement = "UPDATE Teacher set courseNames = ['Neural Network'] WHERE id = 2;"
    session.execute(cql_Statement)

    log.info("creating table...")
    # map<text, text> <-- map collection type
    session.execute("""
        CREATE TABLE Course (
            id INT,
            prereq map<text, text>,
            PRIMARY KEY (id)
        )
        """)
    # {'DataScience':'Database' <-- format mapped
    cql_Statement = "INSERT INTO Course(id, prereq) VALUES (1, {'Data Science':'Database', 'Neural Network':'Artificial Intelligence'});"
    session.execute(cql_Statement)

    future = session.execute_async("SELECT * FROM Teacher")
    log.info("SELECT * FROM Teacher :")
    log.info("-----------------------")
    try:
        rows = future.result()
    except Exception:
        log.exeception()
        
    for row in rows:
        log.info(str(row))

    future = session.execute_async("SELECT * FROM Course")
    log.info("SELECT * FROM Course :")
    log.info("----------------------")
    try:
        rows = future.result()
    except Exception:
        log.exeception()

    dict_1 = {}
    for row in rows:
        log.info(str(row))
        
        for key, val in row[1].items():
            dict_1[key] = val
        log.info('dict_1 : ')
        log.info(dict_1)

    cluster.shutdown()

if __name__ == "__main__":
    main()