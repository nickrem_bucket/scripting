import csv
import json

csvFilePath = "d:\\output.csv"
jsonfilePath = "d:\\output.json"

csvfile = open(csvFilePath, 'r')
jsonfile = open(jsonfilePath, 'w')
#reader = csv.DictReader(csvfile)

reader = csv.DictReader( csvfile, fieldnames = ('inspectionDate', 'violationCode', 'violationDescription', 'criticalFlag', 'score', 'grade'))

out = json.dumps( [ row for row in reader ] )

jsonfile.write(out)
csvfile.close()
jsonfile.close()