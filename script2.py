import os, subprocess, time, sys

x = len(sys.argv)
if x == 1:
    print("Veuillez passer une variable au script")
    exit()

param1 = sys.argv[1]
print(param1)

from datetime import datetime
date = datetime.now().strftime('%Y-%m-%d')
log = "script2_" + date + ".log"

fichier = param1 + "\\" + log


filwri = open(fichier, "w")
#filwri = open(fichier, "a")

os.chdir(param1)
output = subprocess.check_output("dir /b /a-d", shell=True, universal_newlines=True)
filwri.write(str(output))
filwri.close()

print("")

with open(fichier, "r") as filin:
    lignes = filin.read()

print(lignes)

