from cryptography.hazmat.backends              import default_backend

from cryptography.hazmat.primitives            import hashes
from cryptography.hazmat.primitives            import serialization

from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.hazmat.primitives.asymmetric import rsa

import os


#Key_Private_File = "/applis/shared/scripts/MODULES/PARAMS/private_key.pem"
Key_Private_File = "/root/private_key.pem"
#Key_Public_File = "/applis/shared/scripts/MODULES/PARAMS/public_key.pem"
Key_Public_File = "/root/public_key.pem"


def generate_keys():

    # generation of the private key
    private_key = rsa.generate_private_key(
                     public_exponent=65537
                   , key_size=2048
                   , backend=default_backend()
                  )

    # get the public key from the private
    public_key = private_key.public_key()

    # Storing the keys
    pem_priv = private_key.private_bytes(
                  encoding=serialization.Encoding.PEM
                , format=serialization.PrivateFormat.PKCS8
                , encryption_algorithm=serialization.NoEncryption()
               )
    pem_pub = public_key.public_bytes(
                 encoding=serialization.Encoding.PEM
               , format=serialization.PublicFormat.SubjectPublicKeyInfo
              )

    with open(Key_Private_File, 'wb') as f:
        f.write(pem_priv)

    with open(Key_Public_File, 'wb') as f:
        f.write(pem_pub)

def encrypted_file(file_to_encrypt, file_encrypted):

    with open(Key_Public_File, "rb") as key_file:
        public_key = serialization.load_pem_public_key(
                        key_file.read()
                      , backend=default_backend()
                     )
    with open(file_to_encrypt, "rb") as f0:
        content = f0.read()

    encrypted = public_key.encrypt(
                   content
                 , padding.OAEP(
                      mgf=padding.MGF1(algorithm=hashes.SHA256())
                    , algorithm=hashes.SHA256()
                    , label=None
                   )
                )

    with open(file_encrypted, "wb") as f1:
        f1.write(encrypted)

def decrypted_file(file_to_decrypt, file_decrypted):

    with open(Key_Private_File, "rb") as key_file:
        private_key = serialization.load_pem_private_key(
                         key_file.read()
                       , password=None
                       , backend=default_backend()
                      )

    with open(file_to_decrypt, "rb") as f0:
        content = f0.read()

    original_message = private_key.decrypt(
                          content
                        , padding.OAEP(
                             mgf=padding.MGF1(algorithm=hashes.SHA256())
                           , algorithm=hashes.SHA256()
                           , label=None
                          )
                       )

    with open(file_decrypted, "wb") as f1:
        f1.write(original_message)

def add_user_to_list(user):

    #file_to_decrypt = "/applis/shared/script/MODULES/PARAMS/list_user_authorized.rsa"
    file_to_decrypt = "/root/list_user_authorized.rsa"
    #file_decrypted = "/applis/shared/scripts/MODULES/PARAMS/list_user_authorized.txt"
    file_decrypted = "/root/list_user_authorized.txt"

    decrypted_file(file_to_decrypt, file_decrypted)

    list_user = []
    with open(file_decrypted, "r") as f0:
        for line in f0:
            list_user.append(line.replace("\n", ""))

    if user in list_user:
        msg = "The user {} is already in the list authorized !".format(user)

        if os.path.isfile(file_decrypted):
            os.remove(file_decrypted)
    else:
        msg = "The user {} was successfully added on the list authorized !".format(user)

        list_user.append(user.replace("\n", ""))

        if os.path.isfile(file_decrypted):
            os.remove(file_decrypted)
        f1 = open(file_decrypted, "w")

        i = 0
        i_last = len(list_user) - 1
        while i < len(list_user):
            if i == i_last:
                lib = list_user[i]
            else:
                lib = list_user[i] + "\n"
            f1.write(lib)
            i += 1

        f1.close()

        if os.path.isfile(file_to_decrypt):
            os.remove(file_to_decrypt)

        encrypted_file(file_decrypted, file_to_decrypt)

        if os.path.isfile(file_decrypted):
            os.remove(file_decrypted)

    return msg

def remove_user_to_list(user):

    #file_to_decrypt = "/applis/shared/script/MODULES/PARAMS/list_user_authorized.rsa"
    file_to_decrypt = "/root/list_user_authorized.rsa"
    #file_decrypted = "/applis/shared/scripts/MODULES/PARAMS/list_user_authorized.txt"
    file_decrypted = "/root/list_user_authorized.txt"

    decrypted_file(file_to_decrypt, file_decrypted)

    list_user = []
    with open(file_decrypted, "r") as f0:
        for line in f0:
            list_user.append(line.replace("\n", ""))

    if user not in list_user:
        msg = "The user {} is not in the list authorized !".format(user)

        if os.path.isfile(file_decrypted):
            os.remove(file_decrypted)
    else:
        msg = "The user {} was successfully removed from the list authorized !".format(user)


        if os.path.isfile(file_decrypted):
            os.remove(file_decrypted)
        f1 = open(file_decrypted, "w")

        i = 0
        i_last = len(list_user) - 1
        while i < len(list_user):
            if list_user[i] != user:
                if i == i_last:
                    lib = list_user[i]
                else:
                    lib = list_user[i] + "\n"
                f1.write(lib)
            i += 1

        f1.close()

        if os.path.isfile(file_to_decrypt):
            os.remove(file_to_decrypt)

        encrypted_file(file_decrypted, file_to_decrypt)

        if os.path.isfile(file_decrypted):
            os.remove(file_decrypted)

    return msg


def validate_user_authorization(user):

    #file_rsa = "/applis/shared/script/MODULES/PARAMS/list_user_authorized.rsa"
    file_rsa = "/root/list_user_authorized.rsa"
    #file_rsa_decrypted = "/applis/shared/scripts/MODULES/PARAMS/list_user_authorized.temp"
    file_rsa_decrypted = "/root/list_user_authorized.temp"

    decrypted_file(file_rsa, file_rsa_decrypted)

    list_user = []
    with open(file_rsa_decrypted, "r") as f:
        for line in f:
            list_user.append(line.replace("\n", ""))

    if os.path.isfile(file_rsa_decrypted):
        os.remove(file_rsa_decrypted)

    if user in list_user:
        result = True
    else:
        result = False

    return result
