import os, sys, xlsxwriter

sys.path.insert(0, 'd:\\Python\\params')
import conn_postgre
import nostri
from conn_postgre import var_database, var_user_own, var_user, var_host, var_port

from nostri import DIRECTORY
fichier = "student.csv"
fichier_absolu = DIRECTORY + "\\student.csv"

fichier_excel = "Student.xlsx"
fichier_xlsx = DIRECTORY + "\\Student.xlsx"
file_result = "student_2019-11-14_16-45-43.csv"

# creation du fichier excel  
workbook = xlsxwriter.Workbook('Student.xlsx')
worksheet = workbook.add_worksheet()

# Format inclus dans le fichier (bold pour les titres, money pour le format des montants)
bold = workbook.add_format({'bold': True})
#money = workbook.add_format({'num_format': '#,##0'})
money = workbook.add_format({'num_format': '#,##0.00€'})

# Ecriture des titres des colonnes
 
cell_format = workbook.add_format({'bold': True})
worksheet.set_column(0, 0, 10, cell_format)
#worksheet.write('A1', 'Admission', bold)
worksheet.write('A1', 'Admission')
worksheet.write('B1', 'Name', bold)
worksheet.write('C1', 'Age', bold)
worksheet.write('D1', 'Course', bold)
worksheet.write('E1', 'Department', bold)
worksheet.write('F1', 'Amount', bold)

# définition de la première cellule
row = 1
col = 0

fichier = DIRECTORY + "\\" + file_result
file_error = fichier

with open(fichier, "r") as filin:
    line = filin.readline()
    cnt = 1
    while line:
        a = line.strip()
        data = a.split(";")
        i = 1
        for value in data:
            if i == 1:
                worksheet.write(row, col, int(value))
                i += 1
            elif i == 2:
                worksheet.write(row, col + 1, value)
                i += 1
            elif i == 3:
                worksheet.write(row, col + 2, int(value))
                i += 1
            elif i == 4:
                worksheet.write(row, col + 3, value)
                i += 1
            elif i == 5:
                worksheet.write(row, col + 4, value)
                i += 1
            elif i == 6:
                val_float = float(value.replace(',', '.'))
                val_decim = format(val_float, '.2f')
                worksheet.write(row, col + 5, float(val_decim), money)
                i += 1
        row += 1
        line = filin.readline()
        cnt += 1
    
# ecriture du total des montants avec une formule
worksheet.write(row, 4, 'Total',       bold)
worksheet.write(row, 5, '=SUM(F2:F7)', money)

workbook.close()