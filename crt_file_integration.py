# coding: latin1

#@(#) ========================================================================
#@(#) SCRIPT : crt_file_integration.py
#@(#) OBJET  : Files integration received from CFT - Main Script
#@(#)
#@(#) AUTEUR : NRE
#@(#) ------------------------------------------------------------------------
#@(#) PARAMETRES :
#@(#)
#@(#) ------------------------------------------------------------------------
#@(#) VERSION DE LIVRAISON : 1.0
#@(#)
#@(#) ------------------------------------------------------------------------
#@(#) DATE        AUTEUR        MODIFICATION
#@(#) 02/04/2020  NRE           Création
#@(#)
#@(#) ========================================================================

import importlib
import os
import sys
import subprocess
from datetime import datetime
from directory_receive import create_workdir_list

date_exec = datetime.now().strftime('%d/%m/%Y')
date_exec2 = datetime.now().strftime('%Y%m%d')
time_exec = datetime.now().strftime('%H:%M:%S')
date = date_exec[6:] + "-" + date_exec[3:5] + "-" + date_exec[0:2] + "_" + time_exec.replace(":", "-")

user_cristal = os.environ['USER_CRISTAL']
file_param_nostri = "/home/{}/Diffusion_listes/PARM_NOSTRI.kst".format(user_cristal)

TRAIT1 = "============================================================================= "
TRAIT2 = "----------------------------------------------------------------------------- "


def redirected(text, path):
        with open(path, "a") as out:
                out.write(text)


#--------------
# Main Program
#--------------

try:

        # Variables recovery of PARM_NOSTRI.kst
        with open(file_param_nostri, "r") as f:
                for line in f:
                        if line.upper().startswith("ARPEPALM="):
                                ARPEPALM = "".join((line.upper().rsplit('=')[1]).splitlines())
                        elif line.upper().startswith("QUEUE_FILEACT="):
                                QUEUE_FILEACT = "".join((line.upper().rsplit('=')[1]).splitlines())
                        elif line.upper().startswith("QUEUE_CFT="):
                                QUEUE_CFT = "".join((line.upper().rsplit('=')[1]).splitlines())
                        elif line.upper().startswith("REP_EXCHANGE="):
                                REP_EXCHANGE = "".join((line.rsplit('=')[1]).splitlines())
                        elif line.upper().startswith("REP_SPHERE="):
                                REP_SPHERE = "".join((line.rsplit('=')[1]).splitlines())
                        elif line.upper().startswith("REP_APPBIN="):
                                REP_APPBIN = "".join((line.rsplit('=')[1]).splitlines())
                        elif line.upper().startswith("REP_NOSTRI="):
                                REP_NOSTRI = "".join((line.rsplit('=')[1]).splitlines())
                        elif line.upper().startswith("REP_LOGS_CRT="):
                                REP_LOGS_CRT = "".join((line.rsplit('=')[1]).splitlines())
                        elif line.upper().startswith("REP_LOGS_CYCLE="):
                                REP_LOGS_CYCLE = "".join((line.rsplit('=')[1]).splitlines())
                        elif line.upper().startswith("HOST="):
                                SERVEUR = "".join((line.rsplit('=')[1]).splitlines())
                        elif line.upper().startswith("TYPE_ENV="):
                                TYPE_ENV = "".join((line.rsplit('=')[1]).splitlines())
                        elif line.upper().startswith("TYPE_SERVEUR="):
                                TYPE_SERVEUR = "".join((line.rsplit('=')[1]).splitlines())


        file_arg_temp = os.path.basename(sys.argv[0])
        tmp_filename, tmp_extension = file_arg_temp.split('.')
        file_log = "{}/{}_{}.log".format(REP_LOGS_CYCLE, tmp_filename, date_exec2)
        file_output = "{}/file_output_integrate_{}.txt".format(REP_NOSTRI, date)
        file_sql = "{}/fic_sql_chaps_{}.sql".format(REP_NOSTRI, date)
        file_result_sql = "{}/fic_result_sql_chaps_{}.res".format(REP_NOSTRI, date)


        # if the log file does not exist --> creation
        if not os.path.isfile(file_log):
                text = TRAIT1 + "\n" \
                       + "   Control and integration of files received from CFT    " +  date_exec + " " + time_exec + " \n" \
                       + TRAIT1 + "\n\n" \
                       + "  Serveur                :  " + SERVEUR.lower() + "\n" \
                       + "  Environnement          :  " + ARPEPALM + " " +  TYPE_SERVEUR + "\n" \
                       + "  Type Environnement     :  " + TYPE_ENV + "\n" \
                       + "  User Cristal           :  " + user_cristal + "\n" \
                       + "\n" \
                       + TRAIT2 + "\n\n"
                redirected(text, file_log)

        # Writing the log on each execution
        time_exec = datetime.now().strftime('%H:%M:%S')
        process_id = os.getpid()
        text = "\n\n" \
             + "     - Execution Start           : " + time_exec + "\n" \
             + "       Process ID                : " + str(process_id) + "\n"
        redirected(text, file_log)

        # list creation of working directories
        do_nothing = create_workdir_list(REP_EXCHANGE, file_output)

        if do_nothing:

                # The modul return "do_nothing", then there is no file to check
                time_exec = datetime.now().strftime('%H:%M:%S')
                text = "       No File to integrate into " + ARPEPALM + "\n\n"
                redirected(text, file_log)

        else:

                list_lines = []
                with open (file_output, "r") as filin:
                        for line in filin:
                                temp = line.replace("\n", "")
                                list_lines.append(temp)

                i = 0
                while i < len(list_lines):
                        # cut lines according to the character ";"
                        temp_sender, temp_file_test, temp_file = list_lines[i].split(";")

                        # instance recovry for crtclicfg_enq
                        module_name = "list_action_file"
                        module = __import__(module_name)
                        instance = getattr(module, temp_sender)

                        if instance == "FILE_EISCDIR":

                                # special case for EISCDIR file (insert table CHAPS_DIRECTORY_FILES)
                                from db_access import modif_chaps_directory

                                cmd = "modif_chaps_directory(temp_file, file_sql, file_result_sql)"
                                text = "       New file to process       : " + os.path.basename(temp_file) + "\n" \
                                     + "       Command                   : \n       " + cmd + "\n\n"
                                redirected(text, file_log)

                                modif_chaps_directory(temp_file, file_sql, file_result_sql)

                                if 'ERROR' in open(file_result_sql).read():
                                        error_sql = True
                                else:
                                        error_sql = False

                                list_lines_result_sql = []
                                with open(file_result_sql, "r") as f:
                                        for line in f:
                                                list_lines_result_sql.append(line)

                                if error_sql == True:
                                        j = 0
                                        while j < len(list_lines_result_sql):
                                                if j == 0:
                                                        text = "\n" + list_lines_result_sql[0]
                                                else:
                                                        text = text + "\n" + list_lines_result_sql[j]
                                                j += 1
                                        text = text + "\n"
                                        raise ValueError(text)
                                else:
                                        text = "      ---> Query Insert into CHAPS_DIRECTORY_FILES successfully executed\n\n"
                                        redirected(text, file_log)
                        else:

                                # the cristal binary (crtclicft_enq or crtclilec_enq) need :
                                #         - parameter FIC (only for crtclilec_enq
                                #         - path and name of the file
                                #         - processing queue

                                if instance == QUEUE_FILEACT:
                                        crt_binary = "crtclilec_enq FIC"
                                else:
                                        crt_binary = "crtclicft_enq"

                                cmd = "{}/{} {} {}".format(REP_APPBIN, crt_binary, temp_file, instance)

                                text = "       New file to process       : " + os.path.basename(temp_file) + "\n" \
                                     + "       Command                   : \n       " + cmd + "\n\n"
                                redirected(text, file_log)

                                subprocess.call(cmd, shell=True)


                        # Writing the file processed into the shared file Nominal1/Nominal2 ($DATADIR/exchange)
                        with open(temp_file_test, 'a') as file:
                                text = os.path.basename(temp_file) + "\n"
                                file.write(text)
                        i += 1




except AttributeError as err:
        mesg = str(err)
        redirected(mesg, file_log)
        print(mesg)

except ImportError as err:
        mesg = str(err)
        redirected(mesg, file_log)
        print(mesg)

except IOError as err:
        mesg = str(err)
        redirected(mesg, file_log)
        print(mesg)

except NameError as err:
        mesg = str(err)
        redirected(mesg, file_log)
        print(mesg)

except SyntaxError as err:
        mesg = str(err)
        redirected(mesg, file_log)
        print(mesg)

except TypeError as err:
        mesg = str(err)
        redirected(mesg, file_log)
        print(mesg)

except ValueError as err:
        mesg = str(err)
        redirected(mesg, file_log)
        print(mesg)

except:
        print("Erreur innatendue : ", sys.exc_info()[0])

finally:
        if os.path.isfile(file_output):
                os.remove(file_output)
        if os.path.isfile(file_result_sql):
                os.remove(file_result_sql)

        # purge logs Cristal (LALOG) "script_execution....log"
        from directory_receive import purge_script_log_bpm
        purge_script_log_bpm(REP_LOGS_CRT, REP_LOGS_CYCLE)

        time_exec = datetime.now().strftime('%H:%M:%S')
        text = "       ... End of execution : " + time_exec + "\n"
        redirected(text, file_log)
