import mysql.connector as mariadb
import keyring, sys

from mariadb_config import connect

conn = None
try:

    params = connect("own", "conn")
    #print(conn)
    #exit()

    conn = mariadb.connect(**params)
    cursor = conn.cursor()
    print("Database opened successfully")


    cursor.execute("drop table if exists STUDENT cascade;")
    cursor.execute("drop table if exists COURSE cascade;")
    cursor.execute("drop table if exists DEPARTMENT cascade;")

    print("")
    print("Tables COURSE/DEPARTMENT/STUDENT droped successfully")

    cursor.execute("""
        create table  COURSE
            (
               ID                       int         not null  auto_increment
             , NAME                     char(50)
             , AMOUNT                   int
             , constraint PK_COURSE     primary key (ID)
            )
        """);
    print("")
    print("Table COURSE created successfully")


    list_course = [('Computer Science', 500042), ('Electrical Engineering', 450000), ('Information Technology', 420000)]
    i = 0
    dict_course = {}
    while i < len(list_course):
        sql_statement = (""" insert into COURSE (NAME, AMOUNT) values ('{}', {}) """).format(list_course[i][0], list_course[i][1])
        cursor.execute(sql_statement)
        sql_statement = (""" select last_insert_id() """)
        cursor.execute(sql_statement)
        id = cursor.fetchone()[0]
        dict_course[list_course[i][0]] = id
        i += 1

    print("{} Inserts on table COURSE successfully".format(len(list_course)))

    cursor.execute("""
        create table  DEPARTMENT
            (
               ID                       int        not null auto_increment
             , NAME                     char(50)
             , constraint PK_DEPARTMENT primary key (ID)
            )
        ;
        """)
    print("")
    print("Table DEPARTMENT created successfully")

    list_department = ["ICT", "Engineering"]
    i = 0
    dict_department = {}
    while i < len(list_department):
        sql_statement = (""" insert into DEPARTMENT (NAME) values ('{}') """).format(list_department[i])
        cursor.execute(sql_statement)
        sql_statement = (""" select last_insert_id() """)
        cursor.execute(sql_statement)
        id = cursor.fetchone()[0]
        dict_department[list_department[i]] = id
        i += 1

    print("{} Inserts on table DEPARTMENT successfully".format(len(list_department)))

    cursor.execute("""
        create table  STUDENT
            (
               ADMISSION               int           not null
             , NAME                    text          not null
             , AGE                     int           not null
             , COURSE                  int
             , DEPARTMENT              int
             , constraint PK_STUDENT   primary key (ADMISSION)
             , constraint FK01_STUDENT foreign key (COURSE)     references COURSE(ID)
             , constraint FK02_STUDENT foreign key (DEPARTMENT) references DEPARTMENT(ID)
            )
        ;
        """)
    print("")
    print("Table STUDENT created successfully")

    list_student = [[3420, "John", 18, "Computer Science", "ICT"],
                    [3419, "Abel", 17, "Computer Science", "ICT"],
                    [3421, "Joel", 17, "Computer Science", "ICT"],
                    [3422, "Anthony", 19, "Electrical Engineering", "Engineering"],
                    [3423, "Alica", 18, "Information Technology", "ICT"],
                    [3424, "Tom", 20, "Information Technology", "ICT"]]

    i = 0
    while i < len(list_student):
        id_adm      = list_student[i][0]
        nam_student = list_student[i][1]
        age_student = list_student[i][2]
        cou_student = dict_course[list_student[i][3]]
        dep_student = dict_department[list_student[i][4]]
        sql_statement = (""" insert into STUDENT (ADMISSION, NAME, AGE, COURSE, DEPARTMENT) values ({}, '{}', {}, {}, {}) """).format(id_adm, nam_student, age_student, cou_student, dep_student);
        cursor.execute(sql_statement)
        i += 1

    print("{} Inserts on table STUDENT successfully".format(len(list_student)))

    cursor.execute(""" show tables """);
    rows = cursor.fetchall()
    for row in rows:
        table_name = row[0]
        sql_statement = (""" grant select, insert, update, delete on table {} to scott2 """).format(table_name)
        cursor.execute(sql_statement)

    print("")
    print("{} Grants on tables COURSE/DEPARTMENT/STUDENT to postgres_app successfully".format(len(rows)))

    conn.commit()
    cursor.close()

except mariadb.DatabaseError as error:
    print(error)
    print("")

finally:
    if conn is not None:
        conn.close()
        print("")
        print("")
        print('Database connection closed.')