import os
import sys
import time

from twisted.internet import defer, reactor

from stompest.config import StompConfig
from stompest.async import Stomp

user = os.getenv('ACTIVEMQ_USER') or 'admin'
password = os.getenv('ACTIVEMQ_PASSWORD') or 'password'
host = os.getenv('ACTIVEMQ_HOST') or 'localhost'
port = int(os.getenv('ACTIVEMQ_PORT') or 61613)
destination = sys.argv[1:2] or ['/topic/event']
destination = destination[0]

messages = 10000
data = b'Hello World from Python'

@defer.inlineCallbacks
def run():
    config = StompConfig('tcp://%s:%d' % (host, port), login=user, passcode=password, version='1.1')
    client = Stomp(config)
    yield client.connect(host='mybroker')

    count = 0
    start = time.time()
    
    for _ in xrange(messages):
        client.send(destination=destination, body=data, headers={'persistent': 'false'})
        count += 1

    diff = time.time() - start
    print('Sent %s frames in %f seconds' % (count, diff))
  
    yield client.disconnect(receipt='bye')

if __name__ == '__main__':
    run()
    reactor.run()