import pika
import sys
import json
from datetime import datetime

connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.exchange_declare(
                           exchange='topic_logs'
                         , exchange_type='topic'
                        )

result = channel.queue_declare(
                                 ''
                               , exclusive=True
                              )
queue_name = result.method.queue

binding_keys = sys.argv[1:]
if not binding_keys:
    sys.stderr.write("Usage: %s [binding_key]...\n" % sys.argv[0])
    sys.exit(1)

for binding_key in binding_keys:
    channel.queue_bind(
                         exchange='topic_logs'
                       , queue=queue_name
                       , routing_key=binding_key
                      )

print(' [*] Waiting for logs. To exit press CTRL+C')


def callback(ch, method, properties, body):
    #print(" [x] %r:%r" % (method.routing_key, body))
    print("")
    print(" [x] Received new message")
    station = json.loads(body.decode())
    station_number = station["number"]
    contract = station["contract_name"].capitalize()
    available_bikes = station["available_bikes"]
    address = station["address"]
    nb_empty = station["number_stations_empty"]
    date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    if available_bikes == 0:
        print("{} --- The station number {} ({} , {}), just passed empty !!!".format(date, station_number, address, contract))
        print("       Number of Empty Station in {} : {} ".format(contract, nb_empty))
        print("")
    else:
        print("{} --- The station number {} ({} , {}), is ok now".format(date, station_number, address, contract))
        print("       Number of Empty Station in {} : {} ".format(contract, nb_empty))
        print("")
    

channel.basic_consume(
                        queue=queue_name
                      , on_message_callback=callback
                      , auto_ack=True
                     )

channel.start_consuming()