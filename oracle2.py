import os, sys, subprocess, time, xlsxwriter, shutil

LOCATION = r"D:\oracle\product\18.0.0\dbhomeXE\network\admin"

sys.path.insert(0, 'd:\\Python\\params')
import nostri
from nostri import DIRECTORY

from datetime import datetime
date_file = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
file_in = "select1.sql"
file_out= "select1_" + date_file + ".sql"
file_result = "resultat_" + date_file + ".txt"

fichier = "student.csv"
fichier_absolu = DIRECTORY + "\\student.csv"
fichier_excel = "Student.xlsx"
fichier_xlsx = DIRECTORY + "\\Student.xlsx"
nb_step = 1

from package.ora_config import config_cmd
param = config_cmd("app")

from datetime import datetime
date = datetime.now().strftime('%H:%M:%S')
print("")
print("")
print("      ... Execution Started : {}".format(date))
print(" ------------------------------------------- ")
print("")

try:

    os.chdir(DIRECTORY)

    print(" *** Step n. {} : Format result file name on sql script *** ".format(nb_step))
    nb_step += 1

    fin = open(file_in, "rt")
        fout = open(file_out, "wt")
    for line in fin:
        fout.write(line.replace('%datefile%', date_file))
    fin.close()
    fout.close()

    print("         --> Format select1.sql successfully")

    print("")
    print(" *** Step n. {} : Execute sql script *** ".format(nb_step))
    nb_step += 1

    cmd = "echo @" + file_out + " | " + param + " > log.txt"
    subprocess.call(cmd, shell=True)

    print("         --> Exec select1.sql successfully")

    print("")
    print(" *** Step n. {} : Rename file resultat.txt to student.csv *** ".format(nb_step))
    nb_step += 1
    
    shutil.copy2(file_result, fichier)
    print("         --> File {} copyed successfully".format(file_result))

    print("")
    print(" *** Step n. {} : Purge temp files *** ".format(nb_step))
    nb_step += 1

    fichier_sql = DIRECTORY + "\\" + file_out
    if os.path.isfile(fichier_sql):
        os.remove(fichier_sql)

    fichier_log = DIRECTORY + "\\log.txt"
    if os.path.isfile(fichier_log):
        os.remove(fichier_log)
    
    fichier_resulttxt = DIRECTORY + "\\" + file_result
    if os.path.isfile(fichier_resulttxt):
        os.remove(fichier_resulttxt)
            
    print("         --> Temp Files deleted successfully")

    print("")
    print(" *** Step n. {} : Delete file Excel if exist *** ".format(nb_step))

    nb_step += 1
    
    if os.path.isfile(fichier_xlsx)
        os.remove(fichier_xlsx)

    print("         --> File {} deleted successfully".format(fichier_excel))

    print("")
    print(" *** Step n. {} : Create File Excel *** ".format(nb_step))
    nb_step += 1

    # creation du fichier excel  
    workbook = xlsxwriter.Workbook('Student.xlsx')
    worksheet = workbook.add_worksheet()

    # Format la ligne entete
    header_format = workbook.add_format()
    header_format.set_bold()
    header_format.set_align('center')
    #header_format.set_align('vcenter')
    header_format.set_text_wrap()
    #header_format.set_font_color('grey')
    header_format.set_border(2)
    
    # Format ligne tableau sans montant
    cell_format = workbook.add_format()
    cell_format.set_align('center')
    cell_format.set_border(1)
    
    # Format ligne tableau avec montant
    cell_amount_format = workbook.add_format()
    cell_amount_format.set_align('right')
    cell_amount_format.set_num_format('#,##0.00')
    cell_amount_format.set_border(1)
    
    # Format ligne Total
    total_format = workbook.add_format()
    total_format.set_bold()
    total_format.set_align('right') 
    
 
    # Ecriture ligne entete
    worksheet.set_column('A:A', 15)
    worksheet.set_column('B:B', 10)
    worksheet.set_column('C:C', 10)
    worksheet.set_column('D:D', 25)
    worksheet.set_column('E:E', 15)
    worksheet.set_column('F:F', 15)
    worksheet.write('A1', 'Admission', header_format)
    worksheet.write('B1', 'Name', header_format)
    worksheet.write('C1', 'Age', header_format)
    worksheet.write('D1', 'Course', header_format)
    worksheet.write('E1', 'Department', header_format)
    worksheet.write('F1', 'Amount (€)', header_format)

    # définition de la première cellule
    row = 1
    col = 0

    fichier_read = DIRECTORY + "\\" + fichier
    file_error = fichier

    with open(fichier_read, "r") as filin:
        line = filin.readline()
        cnt = 1
        while line:
            a = line.strip()
            data = a.split(";")
            i = 1
            for value in data:
                if i == 1:
                    worksheet.write(row, col, int(value), cell_format)
                    i += 1
                elif i == 2:
                    worksheet.write(row, col + 1, value, cell_format)
                    i += 1
                elif i == 3:
                    worksheet.write(row, col + 2, int(value), cell_format)
                    i += 1
                elif i == 4:
                    worksheet.write(row, col + 3, value, cell_format)
                    i += 1
                elif i == 5:
                    worksheet.write(row, col + 4, value, cell_format)
                    i += 1
                elif i == 6:
                    val_float = float(value.replace(',', '.'))
                    val_decim = format(val_float, '.2f')
                    worksheet.write(row, col + 5, float(val_decim), cell_amount_format)
                    i += 1
            row += 1
            line = filin.readline()
            cnt += 1
 

    # ecriture du total des montants avec une formule
    worksheet.write(row, 4, 'Total = ', total_format)
    worksheet.write(row, 5, '=SUM(F2:F7)', cell_amount_format)

    workbook.close()
    
    print("         --> File {} created successfully".format(fichier_excel))
    print("")


    print(" *** Step n. {} : Format Result Select Query *** ".format(nb_step))
    nb_step += 1

    fichier = DIRECTORY + "\\" + fichier
    file_error = fichier

    print("")
    print(" ADMISSION  NAME     AGE  COURSE                  DEPARTEMENT  AMOUNT   ")
    print(" ---------  -------  ---  ----------------------  -----------  ---------")

    with open(fichier, "r") as filin:
        line = filin.readline()
        cnt = 1
        while line:
            a = line.strip()
            data = a.split(";")
            i = 1
            for row in data:
                if i == 1:
                    var_1 = str(row).rjust(9)
                    i += 1
                elif i == 2:
                    var_2 = str(row).rjust(7)
                    i += 1
                elif i == 3:
                    var_3 = str(row).rjust(3)
                    i += 1
                elif i == 4:
                    var_4 = str(row).rjust(23)
                    i += 1
                elif i == 5:
                    var_5 = str(row).rjust(12)
                    i += 1
                elif i == 6:
                    val_float = float(row.replace(',', '.'))
                    val_decim = format(val_float, '.2f')
                    var_6 = str(val_decim).rjust(10)
                    i += 1
            print(" {}  {}  {} {} {} {}".format(var_1, var_2, var_3, var_4, var_5, var_6))
            line = filin.readline()
            cnt += 1

except IOError as err:
    errno, strerror = err.args
    print("Error on file {}".format(file_error))
    print("I/O error({}): {}".format(errno, strerror))
    exit()

except OSError as err:
    errno, strerror = err.args
    print("Error on file {}".format(err.filename))
    print("OS error({}): {}".format(errno, strerror))
    exit()

except NameError as err:
    print(err)
    exit()

except ValueError as err:
    print(err)
    exit()

except:
   print("Unexpected error : ", sys.exc_info()[0])
   exit()


print("")
print("")
print("")
from datetime import datetime
date = datetime.now().strftime('%H:%M:%S')
print(" ------------------------------------------- ")
print("      ... Execution Ended : {}".format(date))