from operator import itemgetter

etudiants = [
    ("Clément", 14, 16),
    ("Charles", 12, 15),
    ("Oriane", 14, 18),
    ("Thomas", 11, 12),
    ("Damien", 12, 15),
]

print("Liste de départ")
print(etudiants)


etudiants = [
    ("Clément", 14, 16),
    ("Charles", 12, 15),
    ("Oriane", 14, 18),
    ("Thomas", 11, 12),
    ("Damien", 12, 15),
]
print("")
print("Tri simple fonction sort")
etudiants.sort()
print(etudiants)

etudiants = [
    ("Clément", 14, 16),
    ("Charles", 12, 15),
    ("Oriane", 14, 18),
    ("Thomas", 11, 12),
    ("Damien", 12, 15),
]
print("")
print("Tri simple fonction sorted(etudiants)")
print(sorted(etudiants))

etudiants = [
    ("Clément", 14, 16),
    ("Charles", 12, 15),
    ("Oriane", 14, 18),
    ("Thomas", 11, 12),
    ("Damien", 12, 15),
]
print("")
print("Tri avec clé lambda sur 3ème colone")
print(sorted(etudiants, key=lambda colonnes: colonnes[2]))


etudiants = [
    ("Clément", 14, 16),
    ("Charles", 12, 15),
    ("Oriane", 14, 18),
    ("Thomas", 11, 12),
    ("Damien", 12, 15),
]
print("")
print("Tri avec l'operateur itemgetter sur la 3ème colonne")
print(sorted(etudiants, key=itemgetter(2)))