import os
from random import randrange
from math import ceil
from time import sleep


continuer_partie = True

print("")
print("Vous vous installez à la table de roulette")
print("")
sleep(1)

nb_mise_init = -1
while nb_mise_init < 0:
    nb_mise_init = input("Quelle est votre mise de départ ? : ")
    print("")
    try:
        nb_mise_init = int(nb_mise_init)
    except ValueError:
        print("Vous n'avez pas saisie de mise initiale")
        nb_mise_init = -1
        continue
    if nb_mise_init <= 0:
        if nb_mise_init == 0:
            Msg = "nulle"
        else:
            Msg = "négative"
        print("Cette mise initiale est {}".format(Msg))
        nb_mise_init = -1
        continue


argent = nb_mise_init

while continuer_partie: # Tant qu'on doit continuer la partie
    # on demande à l'utilisateur de saisir le nombre sur
    # lequel il va miser
    nombre_mise = -1
    while nombre_mise < 0 or nombre_mise > 49:
        nombre_mise = input("Tapez le nombre sur lequel vous voulez miser (entre 0 et 49) : ")
        print("")
        # On convertit le nombre misé
        try:
            nombre_mise = int(nombre_mise)
        except ValueError:
            print("Vous n'avez pas saisi de nombre")
            print("")
            nombre_mise = -1
            continue
        if nombre_mise < 0:
            print("Ce nombre est négatif")
        if nombre_mise > 49:
            print("Ce nombre est supérieur à 49")

    # À présent, on sélectionne la somme à miser sur le nombre
    mise = 0
    while mise <= 0 or mise > argent:
        mise = input("Tapez le montant de votre mise : ")
        print("")
        # On convertit la mise
        try:
            mise = int(mise)
        except ValueError:
            print("Vous n'avez pas saisi de nombre")
            print("")
            mise = -1
            continue
        if mise <= 0:
            print("La mise saisie est négative ou nulle.")
            print("")
        if mise > argent:
            print("Vous ne pouvez miser autant, vous n'avez que", argent, "$")
            print("")

    # Le nombre misé et la mise ont été sélectionnés par
    # l'utilisateur, on fait tourner la roulette
    numero_gagnant = randrange(50)
    print("Les jeux sont faits, rien ne va plus ...")
    sleep(3)
    print("        --> La roulette tourne... ... et s'arrête sur le numéro", numero_gagnant)
    print("")

    # On établit le gain du joueur
    sleep(1)
    if numero_gagnant == nombre_mise:
        print("Félicitations ! Vous obtenez", mise * 3, "$ !")
        print("")
        argent += mise * 3
    elif numero_gagnant % 2 == nombre_mise % 2: # ils sont de la même couleur
        mise = ceil(mise * 0.5)
        print("Vous avez misé sur la bonne couleur. Vous obtenez", mise, "$")
        print("")
        argent += mise
    else:
        print("Désolé l'ami, c'est pas pour cette fois. Vous perdez votre mise.")
        print("")
        argent -= mise

    # On interrompt la partie si le joueur est ruiné
    sleep(1)
    if argent <= 0:
        print("Vous êtes ruiné ! C'est la fin de la partie.")
        print("")
        continuer_partie = False
    else:
        # On affiche l'argent du joueur
        print("Vous avez à présent", argent, "$")
        print("")
        quitter = input("Souhaitez-vous quitter le casino (o/n) ? ")
        print("")
        if quitter == "o" or quitter == "O":
            print("Vous quittez le casino avec vos gains.")
            continuer_partie = False

# On met en pause le système (Windows)
os.system("pause")