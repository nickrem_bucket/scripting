import pymongo

from pymongo import MongoClient, DESCENDING
from pprint import pprint

client = MongoClient('mongodb://localhost:27017/')

with client:
    
    #db = client.new_york
    db = client.testdb

    cars = [ {'name': 'Audi', 'price': 52642},
             {'name': 'Mercedes', 'price': 57127},
             {'name': 'Skoda', 'price': 9000},
             {'name': 'Volvo', 'price': 29000},
             {'name': 'Bentley', 'price': 350000},
             {'name': 'Citroen', 'price': 21000},
             {'name': 'Hummer', 'price': 41400},
             {'name': 'Volkswagen', 'price': 21600} ]
             
    #t = db.restaurants.find()
    t = db.restaurants.find( { "borough" : "Brooklyn",
                               "cuisine" : "Italian",
                               "name":/pizza/i,
                              "address.street" : "5 Avenue"},
                              {"name":1,
                               "grades.score":1} )

    #print(t.next())
    #cars = db.cars.find()
    

    """print(cars.next())
    print(cars.next())
    print(cars.next())
    
    cars.rewind()

    print(cars.next())
    print(cars.next())
    print(cars.next())

    print("")
    print("")
    print(list(cars))"""

    #for car in cars:
    #    print("{} {}".format(car['name'], car['price']))

    #cars = db.cars.find({}, {'_id':1, 'name':1})
    #for car in cars:
    #    print(car)

    #n_cars = db.cars.find().count()
    #print("Il y a {} voitures".format(n_cars))
    
    #expensive_cars = db.cars.find({'price': {'$gt': 50000}})
    
    #for car in expensive_cars:
    #    print(car['name'], car['price'])
    
    #cars = db.cars.find().sort("price", DESCENDING)
    #for car in cars:
    #    print("{} {}".format(car['name'], car['price']))
    
    #agr = [ {'$group': {'_id': 1, 'all': { '$sum': '$price' } } } ]
    #agr = [{ '$match': {'$or': [ { 'name': "Audi" }, { 'name': "Volvo" }] }}, 
    #       { '$group': {'_id': 1, 'sum2cars': { '$sum': "$price" } }}]
    #val = list(db.cars.aggregate(agr))
    #print('La somme des prix est : {}'.format(val[0]['all']))
    #print('La somme des prix des voitures Audi et Volvo est : {}'.format(val[0]['sum2cars']))
    
    # skip des 2 premières ignes, et limite d'affichage à 3
    #cars = db.cars.find().skip(2).limit(3)
    #for car in cars:
    #    print("{}: {}".format(car['name'], car['price']))