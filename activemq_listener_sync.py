import os
import sys
import time

from stompest.config import StompConfig
from stompest.sync import Stomp

user = os.getenv('ACTIVEMQ_USER') or 'admin'
password = os.getenv('ACTIVEMQ_PASSWORD') or 'password'
host = os.getenv('ACTIVEMQ_HOST') or 'localhost'
port = int(os.getenv('ACTIVEMQ_PORT') or 61613)
destination = sys.argv[1:2] or ['/topic/event']
destination = destination[0]

config = StompConfig('tcp://%s:%d' % (host, port), login=user, passcode=password, version='1.1')
client = Stomp(config)

client.connect(host='mybroker')
client.subscribe(destination=destination, headers={'id': 'required-for-STOMP-1.1'})

count = 0
start = time.time()

while (not count) or client.canRead(0):
    client.receiveFrame()
    #print('Got %s' % frame.info())
    count += 1

diff = time.time() - start
print('Received %s frames in %f seconds' % (count, diff))

client.disconnect(receipt='bye')
client.receiveFrame()
client.close()