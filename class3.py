class Protege:
    """Classe possédant une méthode particulière d'accès à ses attributs :
    Si l'attribut n'est pas trouvé, on affiche une alerte et renvoie None"""


    def __init__(self):
        """On crée quelques attributs par défaut"""
        self.a = 1
        self.b = 2
        self.c = 3
    #def __getattr__(self, nom):
    #    """Si Python ne trouve pas l'attribut nommé nom, il appelle
    #    cette méthode. On affiche une alerte"""

    #    print("Alerte ! Il n'y a pas d'attribut {} ici !".format(nom))
    #def __setattr__(self, nom_attr, val_attr):
    #    """Méthode appelée quand on fait objet.nom_attr = val_attr.
    #    On se charge d'enregistrer l'objet"""

    #    object.__setattr__(self, nom_attr, val_attr)
    #    self.enregistrer()

#pro = Protege()
#print(pro.a)
#print(pro.c)
#print(pro.e)
#pro.e = 5
#print(pro.e)

try:
    pro = Protege()
    print(getattr(pro, "a"))
    print(getattr(pro, "c"))
    print(getattr(pro, "e"))
    

except AttributeError as err:
    print(err)

try:
    setattr(pro, "e", 5)
    print(hasattr(pro, "e"))
    print(getattr(pro, "e"))

    delattr(pro, "e")
    print(getattr(pro, "e"))
except AttributeError as err:
    print(err)

print(hasattr(pro, "e"))