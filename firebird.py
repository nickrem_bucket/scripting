import fdb

con = fdb.connect(dsn='D:\data\TEST.FDB', user='scott', password='tiger')

cur = con.cursor()

sql_statement = "select * from languages order by year_released"
cur.execute(sql_statement)
print(cur.fetchall())

print("")
print("")
cur.execute(sql_statement)
for row in cur:
    print("{} has been publicly available since {}.".format(row[0], row[1]))


print("")
print("")
cur.execute(sql_statement)
for row in cur.itermap():
    print("%(name)s has been publicly available since %(year_released)d" % row)

print("")
print("")
cur.execute(sql_statement)
for fieldDesc in cur.description:
    print(fieldDesc[fdb.DESCRIPTION_NAME].ljust(fieldDesc[fdb.DESCRIPTION_DISPLAY_SIZE])) ,
print("\n")# Finish the header with a newline.
print('-' * 78)

print("")
print("")
cur.execute(sql_statement)
fieldIndices = range(len(cur.description))
for row in cur:
    for fieldIndex in fieldIndices:
        fieldValue = str(row[fieldIndex])
        fieldMaxWidth = cur.description[fieldIndex][fdb.DESCRIPTION_DISPLAY_SIZE]

        print(fieldValue.ljust(fieldMaxWidth)) ,

    print("\n") # Finish the row with a newline.


newLanguages = [
    ('Lisp',  1958),
    ('Dylan', 1995),
  ]
"""cur.executemany("insert into languages (name, year_released) values (?, ?)",
    newLanguages
  )

# The changes will not be saved unless the transaction is committed explicitly:
con.commit()"""