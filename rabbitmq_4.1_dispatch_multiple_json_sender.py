import pika
import sys
import json

connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.exchange_declare(
                           exchange='logs'
                         , exchange_type='fanout'
                        )

#message = ' '.join(sys.argv[1:]) or "info: Hello World!"
data = {
          "id":1
        , "name": "Nicolas"
        , "description": "Nicolas envoi un msg json"
        }
message = json.dumps(data)

channel.basic_publish(
                        exchange='logs'
                      , routing_key=''
                      , body=message
                     )

print(" [x] Sent %r" % message)

connection.close()