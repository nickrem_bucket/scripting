# -*- coding: utf-8 -*-

import os
import sys
#import importlib

#x = len(sys.argv)
#if x == 1:
#    print("Veuillez passer une variable au script")
#    exit()
#parameter = sys.argv[1]

#module = importlib.import_module(parameter)
#globals().update(module.__dict__)
#__main__()
type_user = "own"
software = "cubrid"
replace_user_app = False
from datetime import datetime
from db_access import connect
from db_access import exec_sql
from nostri import DIRECTORY
from nostri import DIRECTORYS


try:

    date = datetime.now().strftime('%H:%M:%S')
    print("")
    print("")
    print("      ... Execution Started : {}".format(date))
    print(" ------------------------------------------- ")
    print("")

    if software == 'cubrid':
        from db_create_request import create_request_cubrid
        
        date = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        file_cmds_sql = DIRECTORY + "\\cubrid_create_" + date + ".sql"
        file_result_sql = DIRECTORY + "\\cubrid_sql_output_" + date + ".txt"
        file_output_sql = DIRECTORY + "\\cubrid_resultat_" + date + ".txt"

        create_request = create_request_cubrid(file_cmds_sql)
        #exit()
        replace_user_app = False

        execution = exec_sql(type_user, file_cmds_sql, file_result_sql, file_output_sql, replace_user_app, DIRECTORYS, software)
        #exit()

        if os.stat(file_result_sql).st_size == 0:
            print("\nAll SQL commands on Cubrid are OK\n")
        else:
            if 'ERROR' in open(file_result_sql).read():
                error_sql = True
            else:
                error_sql = False

            if error_sql == True:
                list_lines = []
                First_Error = False
                with open(file_result_sql, "r") as file:
                    for line in file:
                        list_lines.append(line)
                i = 0
                while i < len(list_lines):
                    a = list_lines[i].rstrip()
                    b = a.lstrip()
                    if b.lower().startswith("erreur") or b.lower().startswith("error"):
                        indice = i
                        break
                    i += 1
                indice_prec = indice - 2
                indice_suiv = indice + 1
                a = list_lines[indice_prec].rstrip()
                temp_msg = a.lstrip() + "\n\n"
                a = list_lines[indice].rstrip()
                temp_msg = temp_msg + a.lstrip()
                a = list_lines[indice_suiv].rstrip()
                temp_msg = temp_msg + a.lstrip()
                raise ValueError(temp_msg)
            else:
                print("\nAll SQL commands on Cubrid are OK\n")

    elif software == "firebird":
        from db_create_request import create_request_firebird

        date = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        file_cmds_sql = DIRECTORY + "\\firebird_create_" + date + ".sql"
        file_result_sql = DIRECTORY + "\\firebird_sql_output_" + date + ".txt"
        file_output_sql = DIRECTORY + "\\firebird_resultat_" + date + ".txt"

        create_request = create_request_firebird(file_cmds_sql)

        replace_user_app = False
        execution = exec_sql(type_user, file_cmds_sql, file_result_sql, file_output_sql, replace_user_app, DIRECTORYS, software)
        #exit()

        if os.stat(file_output_sql).st_size == 0:
            print("\nAll SQL commands on Firebird are OK\n")
        else:
            if 'Statement failed' in open(file_output_sql).read():
                error_sql = True
            else:
                error_sql = False

            if error_sql == True:
                list_lines = []
                with open(file_output_sql, "r") as file:
                    for line in file:
                        list_lines.append(line)
                i = 0
                while i < len(list_lines):
                    a = list_lines[i].rstrip()
                    b = a.lstrip()
                    if b.lower().startswith("statement failed"):
                        indice = i
                        indice2 = i + 1
                        indice3 = indice2 + 1
                        indice4 = indice3 + 1
                        break
                    i += 1
                a = list_lines[indice].rstrip()
                temp_msg = a.lstrip()
                a = list_lines[indice2].rstrip()
                temp_msg = temp_msg + "\n" + a.lstrip()
                a = list_lines[indice3].rstrip()
                temp_msg = temp_msg + "\n" + a.lstrip()
                a = list_lines[indice4].rstrip()
                temp_msg = temp_msg + "\n" + a.lstrip()
                raise ValueError(temp_msg)
            else:
                print("\nAll SQL commands on Firebird are OK\n")

    elif software == "mariadb":
        from db_create_request import create_request_mariadb

        date = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        file_cmds_sql = DIRECTORY + "\\mariadb_create_" + date + ".sql"
        file_result_sql = DIRECTORY + "\\mariadb_sql_output_" + date + ".txt"
        file_output_sql = DIRECTORY + "\\mariadb_resultat_" + date + ".txt"

        create_request = create_request_mariadb(file_cmds_sql)
        exit()

        replace_user_app = False
        execution = exec_sql(type_user, file_cmds_sql, file_result_sql, file_output_sql, replace_user_app, DIRECTORYS, software)

        if os.stat(file_result_sql).st_size == 0:
            print("\nAll SQL commands on MariaDB are OK\n")
        else:
            if 'ERROR' in open(file_result_sql).read():
                error_sql = True
            else:
                error_sql = False

            if error_sql == True:
                list_lines = []
                First_Error = False
                with open(file_result_sql, "r") as file:
                    for line in file:
                        list_lines.append(line)
                i = 0
                while i < len(list_lines):
                    a = list_lines[i].rstrip()
                    b = a.lstrip()
                    if b.lower().startswith("erreur") or b.lower().startswith("error"):
                        indice = i
                        break
                    i += 1
                a = list_lines[indice].rstrip()
                temp_msg = a.lstrip()
                raise ValueError(temp_msg)
            else:
                print("\nAll SQL commands on MariaDB are OK\n")

    elif software == "mysql":
        from db_create_request import create_request_mysql

        date = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        file_cmds_sql = DIRECTORY + "\\mysql_create_" + date + ".sql"
        file_result_sql = DIRECTORY + "\\mysql_sql_output_" + date + ".txt"
        file_output_sql = DIRECTORY + "\\mysql_resultat_" + date + ".txt"
        
        create_request = create_request_mysql(file_cmds_sql)

        execution = exec_sql(type_user, file_cmds_sql, file_result_sql, file_output_sql, replace_user_app, DIRECTORYS, software)

        if os.stat(file_result_sql).st_size == 0:
            print("\nAll SQL commands on MySQL are OK\n")
        else:
            if 'ERROR' in open(file_result_sql).read():
                error_sql = True
            else:
                error_sql = False

            if error_sql == True:
                list_lines = []
                First_Error = False
                with open(file_result_sql, "r") as file:
                    for line in file:
                        list_lines.append(line)
                i = 0
                while i < len(list_lines):
                    a = list_lines[i].rstrip()
                    b = a.lstrip()
                    if b.lower().startswith("erreur") or b.lower().startswith("error"):
                        indice = i
                        break
                    i += 1
                a = list_lines[indice].rstrip()
                temp_msg = a.lstrip()
                raise ValueError(temp_msg)
            else:
                print("\nAll SQL commands on MySQL are OK\n")

    elif software == "oracle":
        from db_create_request import create_request_oracle

        date = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        file_cmds_sql = DIRECTORY + "\\ora_create_" + date + ".sql"
        file_result_sql = DIRECTORY + "\\ora_sql_output_" + date + ".txt"
        file_output_sql = DIRECTORY + "\\ora_resultat_" + date + ".txt"

        create_request = create_request_oracle(file_cmds_sql)

        execution = exec_sql(type_user, file_cmds_sql, file_result_sql, file_output_sql, replace_user_app, DIRECTORYS, software)

        if os.stat(file_result_sql).st_size == 0:
            print("\nAll SQL commands on Oracle are OK\n")
        else:
            if 'ERROR' in open(file_result_sql).read():
                error_sql = True
            else:
                if 'ERREUR' in open(file_result_sql).read():
                    error_sql = True
                else:
                    error_sql = False

            if error_sql == True:
                list_lines = []
                First_Error = False
                with open(file_result_sql, "r") as file:
                    for line in file:
                        list_lines.append(line)
                i = 0
                while i < len(list_lines):
                    a = list_lines[i].rstrip()
                    b = a.lstrip()
                    if b.lower().startswith("erreur") or b.lower().startswith("error"):
                        indice = i
                        indice2 = indice + 1
                        break
                    i += 1
                a = list_lines[indice].rstrip()
                b = list_lines[indice2].rstrip()
                temp_msg = a.lstrip() + '\n' + b.lstrip()
                raise ValueError(temp_msg)
            else:
                print("\nAll SQL commands on Oracle are OK\n")

        from db_create_request import create_request_oracle_app

        file_cmds_sql = DIRECTORY + "\\ora_create_" + date + ".sql"
        file_result_sql = DIRECTORY + "\\ora_sql_output_" + date + ".txt"
        file_output_sql = DIRECTORY + "\\ora_resultat_" + date + ".txt"
        if os.path.isfile(file_cmds_sql):
            os.remove(file_cmds_sql)
        if os.path.isfile(file_result_sql):
            os.remove(file_result_sql)
        if os.path.isfile(file_output_sql):
            os.remove(file_output_sql)

        create_request = create_request_oracle_app(file_cmds_sql)
        exit()
        
        replace_user_app = "maj_app_with_own"
        execution = exec_sql('app', file_cmds_sql, file_result_sql, file_output_sql, replace_user_app, DIRECTORYS, software)
        
        if os.stat(file_result_sql).st_size == 0:
            print("\nAll SQL Create Synonym commands on Oracle are OK\n")
        else:
            if 'ERROR' in open(file_result_sql).read():
                error_sql = True
            else:
                if 'ERREUR' in open(file_result_sql).read():
                    error_sql = True
                else:
                    error_sql = False

            if error_sql == True:
                list_lines = []
                First_Error = False
                with open(file_result_sql, "r") as file:
                    for line in file:
                        list_lines.append(line)
                i = 0
                while i < len(list_lines):
                    a = list_lines[i].rstrip()
                    b = a.lstrip()
                    if b.lower().startswith("erreur") or b.lower().startswith("error"):
                        indice = i
                        indice2 = indice + 1
                        break
                    i += 1
                a = list_lines[indice].rstrip()
                b = list_lines[indice2].rstrip()
                temp_msg = a.lstrip() + '\n' + b.lstrip()
                raise ValueError(temp_msg)
            else:
                print("\nAll SQL Create Synonym commands on Oracle are OK\n")
        
    elif software == "postgresql":
        from db_create_request import create_request_postgresql

        date = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        file_cmds_sql = DIRECTORY + "\\psql_create_" + date + ".sql"
        file_result_sql = DIRECTORY + "\\psql_sql_output_" + date + ".txt"
        file_output_sql = DIRECTORY + "\\psql_resultat_" + date + ".txt"
        
        create_request = create_request_postgresql(file_cmds_sql)

        execution = exec_sql(type_user, file_cmds_sql, file_result_sql, file_output_sql, replace_user_app, DIRECTORYS, software)

        if os.stat(file_result_sql).st_size == 0:
            print("\nAll SQL commands on PostgreSQL are OK\n")
        else:
            if 'ERREUR' in open(file_result_sql).read():
                error_sql = True
            else:
                error_sql = False

            if error_sql == True:
                list_lines = []
                First_Error = False
                with open(file_result_sql, "r") as file:
                    for line in file:
                        list_lines.append(line)
                i = 0
                while i < len(list_lines):
                    a = list_lines[i].rstrip()
                    b = a.lstrip()
                    if b.lower().startswith("erreur") or b.lower().startswith("error"):
                        indice = i
                        indice2 = indice + 1
                        break
                    i += 1
                a = list_lines[indice].rstrip()
                b = a.lstrip()
                c = list_lines[indice2].rstrip()
                d = c.lstrip()
                if d.lower().startswith("ligne") or d.lower().startswith("line"):
                    temp_msg = list_lines[indice] + list_lines[indice2]
                else:
                    temp_msg = list_lines[indice]
                raise ValueError(temp_msg)
            else:
                print("\nAll SQL commands on PostgreSQL are OK\n")

    else:
        temp_msg = ("Wrong name of section type database {} !\n     ---> Values accepted are : mariadb, mysql, oracle or postgresql".format(software))
        raise ValueError(temp_msg)


except NameError as err:
    print(err)

except OSError as err:
    print(err)

except ValueError as err:
    print(err)

except TypeError as err:
    print(err)

except KeyError as err:
    print(err)

except Exception as err:
    print(err)

except:
   print("Unexpected error : ", sys.exc_info()[0])

finally:
    if os.path.isfile(file_cmds_sql):
        os.remove(file_cmds_sql)
    if os.path.isfile(file_result_sql):
        os.remove(file_result_sql)
    if os.path.isfile(file_output_sql):
        os.remove(file_output_sql)
    print("")
    print("")
    from datetime import datetime
    date = datetime.now().strftime('%H:%M:%S')
    print(" ------------------------------------------- ")
    print("      ... Execution Ended : {}".format(date))
