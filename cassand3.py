import logging
import csv, json, sys, os

log = logging.getLogger()
#log.setLevel('DEBUG')
log.setLevel('INFO')
handler = logging.StreamHandler()
handler.setFormatter(logging.Formatter("%(asctime)s [%(levelname)s] %(name)s: %(message)s"))
log.addHandler(handler)


from cassandra import ConsistencyLevel
from cassandra.cluster import Cluster
from cassandra.query import SimpleStatement
from cassandra.util import OrderedMapSerializedKey
from datetime import datetime

KEYSPACE = "resto_ny"

def main():
    cluster = Cluster(['127.0.0.1'])
    session = cluster.connect()

    #rows = session.execute("SELECT keyspace_name FROM system_schema.keyspaces")
    #if KEYSPACE in [row[0] for row in rows]:
    #    log.info("dropping existing keyspace...")
    #    session.execute("DROP KEYSPACE " + KEYSPACE)

    #log.info("creating keyspace...")
    #session.execute("""
    #    CREATE KEYSPACE %s
    #    WITH replication = { 'class': 'SimpleStrategy', 'replication_factor': '2' }
    #    """ % KEYSPACE)

    log.info("setting keyspace...")
    session.set_keyspace(KEYSPACE)

    #log.info("creating table...")
    # set<text> <-- collection
    #session.execute("""
    #    CREATE TABLE Restaurant (
    #        id INT,
    #        Name VARCHAR,
    #        borough VARCHAR,
    #        BuildingNum VARCHAR,
    #        Street VARCHAR,
    #        ZipCode INT,
    #        Phone TEXT,
    #        CuisineType VARCHAR,
    #        PRIMARY KEY (id)
    #    )
    #    """)

    #log.info("creating index...")
    #session.execute("""
    #    CREATE INDEX fk_restaurant_cuieine
    #           ON Restaurant ( CuisineType )
    #    """)

    #log.info("creating table...")
    #session.execute("""
    #    CREATE TABLE Inspection (
    #        idRestaurant INT,
    #        InspectionDate DATE,
    #        ViolationCode VARCHAR,
    #        ViolationDescription VARCHAR,
    #        CriticalFlag VARCHAR,
    #        Score INT,
    #        Grade VARCHAR,
    #        PRIMARY KEY (idRestaurant, InspectionDate)
    #    )
    #    """)

    #log.info("creating index...")
    #session.execute("""
    #    CREATE INDEX fk_Inspection_restaurant
    #           ON Inspection ( Grade )
    #    """)

    #log.info("import .csv file...")
    # a exécuter via CQLSH
    #session.execute("""
    #    COPY restaurant (id, name, borough, buildingnum, street,
    #                zipcode, phone, cuisinetype)
    #        FROM 'D:\ddac-5.1.17-bin\restaurants.csv' WITH DELIMITER = ',';
    #    """)
    #session.execute("""
    #    COPY Inspection (idrestaurant, inspectiondate, violationcode,
    #                violationdescription, criticalflag, score, grade)
    #        FROM 'D:\ddac-5.1.17-bin\restaurants_inspections.csv' WITH DELIMITER=',';
    #    """)
    
    #future = session.execute_async("SELECT * FROM Restaurant")
    #future = session.execute_async("SELECT * FROM Restaurant LIMIT 10")
    #future = session.execute_async("SELECT name, borough FROM Restaurant LIMIT 10")
    #future = session.execute_async("SELECT name, borough FROM Restaurant WHERE id = 41569764")
    #future = session.execute_async("SELECT InspectionDate, Grade FROM Inspection WHERE idRestaurant = 41569764")
    #future = session.execute_async("SELECT name, borough FROM Restaurant WHERE cuisineType = 'French'")
    
    # Cette requête tombe en erreur. La précédent sur 'cuisineType' fonctionn car il y a un index dessus
    # pour que cette requête, il faut ajouter ALLOW FILTERING
    # Note : en production, un ALLOW FILTERING peut être très couteux. A voir pour rajouter des indexs
    #future = session.execute_async("SELECT name, borough FROM Restaurant WHERE borough = 'BROOKLYN'")
    #future = session.execute_async("SELECT name, borough FROM Restaurant WHERE borough = 'BROOKLYN' ALLOW FILTERING")
    
    # Même soucis, la présence de 2 critère dans le "where", dont un qui est indexé, empêche Cassandra de prédir la taille du résultat
    #future = session.execute_async("SELECT Grade, Score FROM Inspection WHERE idRestaurant = 41569764 AND Score >=10")
    #future = session.execute_async("SELECT Grade, Score FROM Inspection WHERE idRestaurant = 41569764 AND Score >=10 ALLOW FILTERING")

    # En CQL, l'opérateur != n'existe pas, ni la valeur nulle. Ainsi pour filtrer la valeur nulle, il faut faire un ">''" dans le where
    #future = session.execute_async("SELECT Grade FROM Inspection WHERE Score >30 AND Grade > '' ALLOW FILTERING")
    #log.info("SELECT * FROM Restaurant :")
    #log.info("--------------------------")
    #try:
    #    rows = future.result()
    #except Exception:
    #    log.exeception()

    #dict_1 = {}
    
    # pour le select global, afin de ne pas afficher les 27000 lignes
    #count = 0
    #for row in rows:
    #    count += 1
    #log.info("Nb Enreg : " + str(count))

    # pour le limit 10
    
    #for row in rows:
        #log.info(type(row))
        #a = str(row[0])
        #log.info(row.id)
        #log.info(row.name)
    #    log.info(row)
        
    # La jointure entre Restaurant et Inspection est impossible en NoSQL
    # L'idée est de créer une vue Comprenant Restaurant et Inspection
    # Pour éviter de multiplier les lignes en double, la clé sera un "nouveau type" de docuement JSON représentant la table Restautrat,
    # L'alimentation de la table se fera via un fichier csv aggégant les deux fichiers csv Restaurant et Inspection
    
    #log.info("creating type...")
    #session.execute("""
    #    CREATE TYPE Restaurant (
    #        Name VARCHAR,
    #        Borough VARCHAR,
    #        BuildingNum VARCHAR,
    #        Street VARCHAR,
    #        ZipCode INT,
    #        Phone VARCHAR,
    #        CuisineType VARCHAR
    #    )
    #    """)

    #log.info("creating table aggregate...")
    #session.execute("""
    #    CREATE TABLE InspectionRestaurant (
    #        idRestaurant INT,
    #        InspectionDate DATE,
    #        ViolationCode VARCHAR,
    #        ViolationDescription VARCHAR,
    #        CriticalFlag VARCHAR,
    #        Score INT,
    #        Grade VARCHAR,
    #        Restaurant frozen<Restaurant>,
    #        PRIMARY KEY ( idRestaurant, InspectionDate )
    #    )
    #    """)

    #java -jar JSonFile2Cassandra.jar -host localhost -port 9042 -keyspace resto_ny -columnFamily inspectionrestaurant -file InspectionsRestaurant.json
 
    #log.info("creating index on InspectionRestaurant...")
    #session.execute("""
    #    CREATE INDEX Idx_InspectionRestaurant_grade on InspectionRestaurant (Grade);
    #    """)

    # Les types imbriqués map/set/list ne peuvent être dissociés dans le SELECT. Il faut donc projeter l'ensemble des informations du restaurant
    # Et le nom du restaurant apparait autant de fois qu'il y a d'inspections ...
    #future = session.execute_async("SELECT Restaurant FROM InspectionRestaurant WHERE grade = 'A'")
    #log.info("SELECT * FROM InspectionRestaurant :")
    #log.info("------------------------------------")
    #try:
    #    rows = future.result()
    #except Exception:
    #    log.exeception()

    #for row in rows:
    #    print(row)

    # Jointure inverse, entre les Restaurants et Inspection
    #log.info("creating type Inspection...")
    #session.execute("""
    #        CREATE TYPE Inspection (
    #                ViolationCode VARCHAR,
    #                ViolationDescription VARCHAR,
    #                CriticalFlag VARCHAR,
    #                Score INT,
    #                Grade VARCHAR
    #        )
    #    """)
    
    # Création de la table de jointure
    #log.info("creating table RestaurantInspections...")
    #session.execute("""
    #        CREATE TABLE RestaurantInspections (
    #                id INT,
    #                Name VARCHAR,
    #                borough VARCHAR,
    #                BuildingNum VARCHAR,
    #                Street VARCHAR,
    #                ZipCode INT,
    #                Phone VARCHAR,
    #                CuisineType VARCHAR,
    #                Inspections map<text, frozen<Inspection>>,
    #                PRIMARY KEY (id)
    #        )
    #    """)
        
    # Chargement via la classe java
      #    COPY restaurant (id, name, borough, buildingnum, street,
    #                zipcode, phone, cuisinetype)
    #        FROM 'D:\ddac-5.1.17-bin\restaurants.csv' WITH DELIMITER = ',';
    #    """)
 
    #future = session.execute_async("SELECT * FROM Restaurant WHERE id = 41553467")
    #log.info("SELECT * FROM Restaurant :")
    #log.info("------------------------------------")
    #try:
    #    rows = future.result()
    #except Exception:
    #    log.exeception()

    #for row in rows:
    #    print(row.id)
    #    print(row.borough)

    future = session.execute_async("SELECT id, borough, buildingnum, cuisinetype, name, phone, street, zipcode from restaurant")
    log.info("SELECT * FROM Restaurant :")
    log.info("------------------------------------")
    try:
        rows1 = future.result()
    except Exception:
        log.exeception()

    dict_rest = {}
    for row in rows1:
        dict_rest["id"] = row.id
        esc_name = row.name
        esc_name2 = esc_name.replace("\'", "%")
        dict_rest["name"] = esc_name2
        esc_name = str(row.borough)
        esc_name2 = esc_name.replace("\'", "%")
        dict_rest["borough"] = esc_name2
        esc_name = str(row.buildingnum)
        esc_name2 = esc_name.replace("\'", "%")
        dict_rest["buildingnum"] = esc_name2
        esc_name = str(row.street)
        esc_name2 = esc_name.replace("\'", "%")
        dict_rest["street"] = esc_name2
        dict_rest["zipcode"] = row.zipcode
        dict_rest["phone"] = row.phone
        dict_rest["cuisinetype"] = row.cuisinetype

        cql_Statement = "SELECT inspectiondate, violationcode, violationdescription, criticalflag, score, grade FROM inspection WHERE idrestaurant = {}".format(row.id)
        future2 = session.execute_async(cql_Statement)
        rows2 = future2.result()
        dict_a = {}
        
        if not rows2:
            print(dict_rest["id"])
            print("empty")
        else:
            for row in rows2:
                dict_b = {}
                #a = row.inspectiondate
                dict_b["inspectiondate"] = str(row.inspectiondate)
                dict_b["violationCode"] = row.violationcode
                esc_name = str(row.violationdescription)
                esc_name2 = esc_name.replace("\'", "%")
                if dict_rest['id'] == 41722730:
                    print(str(esc_name))
                    print(str(esc_name2))
                dict_b["violationDescription"] = esc_name2
                dict_b["criticalFlag"] = row.criticalflag
                dict_b["score"] = row.score
                dict_b["grade"] = row.grade
                #dict_a[str(a)] = dict_b

            fileout = "d:\\output_nico.json"
            #output = str(dict_rest) + "{\'inspections\' :{" + str(dict_a) + "}"
            output = str(dict_rest) + "{\'inspections\' :{" + str(dict_b) + "}"
            output2 = output.replace("}{", ", ")
            output3 = output2.replace("{{", "{")
            output4 = output3.replace("\'", "\"")
            if dict_rest['id'] == 41722730:
                print(output4)
            output5 = output4.replace("%", "\'")
            output6 = output5.replace("\"grade\": None", "\"grade\": \"\"")
            output7 = output6.replace("\"score\": None", "\"score\": \"\"")
            output8 = output7.replace("\"violationCode\": None", "\"violationCode\": \"\"")
            output9 = output8.replace("\"violationDescription\": \"None\"", "\"violationDescription\": \"\"")
            output10 = output9.replace("\"buildingnum\": None", "\"buildingnum\": \"\"")
            output11 = output10.replace("\"phone\": None", "\"phone\": \"\"")
            output12 = output11.replace("\x1a", "\'")
            with open(fileout, 'a') as out:
                out.write(str(output12))
                out.write("\n")
    


    """future = session.execute_async("SELECT inspectiondate, violationcode, violationdescription, criticalflag, score, grade FROM inspection WHERE idrestaurant = 41553467")
    log.info("SELECT * FROM Restaurant :")
    log.info("------------------------------------")
    try:
        rows = future.result()
    except Exception:
        log.exeception()

    dict_a = {}
    for row in rows:
        dict_b = {}
        a = row.inspectiondate
        dict_b['violationCode'] = row.violationcode
        dict_b['violationDecsription'] = row.violationdescription
        dict_b['criticalFlag'] = row.criticalflag
        dict_b['score'] = row.score
        dict_b['grade'] = row.grade
        dict_a[str(a)] = dict_b        
    #print(dict_a['2011-01-05'])
    print(dict_a)
    #cql_Statement = "INSERT INTO RestaurantInspections (id, inspections) values (%s, %s)"
    #session.execute(cql_Statement, (41553467, dict_a))
    
    fileout = "d:\\output_nico.json"
    output = str(dict_rest) + str(dict_a)
    output2 = output.replace("}{", ",")
    output3 = output2.replace("\'", "\"")
    with open(fileout, 'w') as out:
        out.write(str(output3))"""
    
    cluster.shutdown()

if __name__ == "__main__":
    main()