import sys
from package.fonctions import table

x = len(sys.argv)
if x == 1:
    print("Veuillez passer une variable au script")
    exit()

y = sys.argv[1]
try:
    y = int(y)
except ValueError:
    pass
p = isinstance(y,int)
if str(p) == "False":
    print("La variable passée n'est pas un integer")
    exit()

#table(5) # Appel de la fonction table
table(y)

# Ou ...
#import package.fonctions
#package.fonctions.table(5)  # Appel de la fonction table