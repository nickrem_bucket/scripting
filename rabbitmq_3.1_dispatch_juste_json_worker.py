import pika
import time
import json

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='task_queue', durable=True)
print(' [*] Waiting for messages. To exit press CTRL+C')


def callback(ch, method, properties, body):
    #print(" [x] Received %r" % body)
    #time.sleep(body.count(b'.'))
    #time.sleep(10)
    #print(" [x] Done")
    print("")
    print(" [x] Received new message")
    #print("CH: {}".format(ch))
    #data = json.loads(body)
    data = json.loads(body.decode())
    print("ID: {}".format(data['id']))
    
    fichier = "testrabbit.json"
    filwri = open(fichier, "w")
    filwri.write(data['description'])
    filwri.close()
    #print(data)
    #print("ID: {}".format(data['id']))
    #print("Date-Time: {}".format(data['datetime']))
    #print("Description: {}".format(data['description']))
    #time.sleep(body.count(b'.'))
    time.sleep(5)
    print(" [x] Done")
    ch.basic_ack(delivery_tag=method.delivery_tag)


channel.basic_qos(prefetch_count=1)

channel.basic_consume(
                        queue='task_queue'
                      , on_message_callback=callback
                     )

channel.start_consuming()