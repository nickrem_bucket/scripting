import sys

x = len(sys.argv)
if x == 1:
    print("Veuillez passer une variable au script")
    exit()

y = sys.argv[1]
try:
    y = str(y)
except ValueError:
    pass
p = isinstance(y,str)
if str(p) == "False":
    print("La variable passée n'est pas un string")
    exit()

i = 0
dico_a = {}
dico_b = {}
while i < len(y):
    if i not in (0, 1):
        j = i - 1
        k = j - 1
        var = str(y[k]) + str(y[j]) + str(y[i])
        if len(dico_a) == 0:
            dico_b[var] = 1
        else:
            if var in dico_a:
                list_cles = list(dico_a.keys())
                list_values = list(dico_a.values())

                ## ind_pivot correspond a la position de var dans la liste des cles et values
                ind_pivot = 0
                
                indice = 0
                while indice < len(list_cles):
                    if list_cles[indice] == var:
                        ind_pivot = indice
                    indice += 1 

                t = 0
                while t < len(list_cles):
                    name_key = list_cles[t]
                    if name_key == var:
                        valu_key = list_values[ind_pivot]
                        valu_key += 1
                    else:
                        valu_key = list_values[t]
                    dico_b[name_key] = valu_key
                    t += 1
            else:
                ## reprise du dico_a dans dico_b
                for key in dico_a:
                    name_key = key
                    valu_key = dico_a[key]
                    dico_b[name_key] = valu_key
                ## ajout de la nouvelle entree dans dico_b
                dico_b[var] = 1

        ## réassignation de dico_a et reinit de dico_b
        dico_a = dico_b.copy()
        dico_b = {}

            
    i += 1

print("")
print("Mots de 3 lettres")
for key in dico_a:
    name_key = key
    valu_key = dico_a[key]
    print("{} : {}".format(name_key, valu_key))