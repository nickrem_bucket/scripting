import datetime
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography import x509
from cryptography.x509.oid import NameOID
from cryptography.hazmat.primitives import hashes

# generation de la clé privée rsa
key = rsa.generate_private_key(
      public_exponent=65537
    , key_size=2048
    , backend=default_backend()
)

# écriture de la key sur le disque dur
with open("d:\Python\ssl_key\key.pem", "wb") as f:
	f.write(key.private_bytes(
			  encoding=serialization.Encoding.PEM
			, format=serialization.PrivateFormat.TraditionalOpenSSL
			, encryption_algorithm=serialization.BestAvailableEncryption(b"passphrase")
			, )
	)

# detail du certificat. Poiur un certificat self-signed
# le sujet et l'émetteur sont les même
subject = issuer = x509.Name([
	  x509.NameAttribute(NameOID.COUNTRY_NAME, u"FR")
    , x509.NameAttribute(NameOID.STATE_OR_PROVINCE_NAME, u"Ile-de-France")
	, x509.NameAttribute(NameOID.LOCALITY_NAME, u"Paris")
	, x509.NameAttribute(NameOID.ORGANIZATION_NAME, u"NR Corp")
	, x509.NameAttribute(NameOID.COMMON_NAME, u"nrcorp.fr")
    , ]
)

# génération
cert = x509.CertificateBuilder().subject_name(
    subject
).issuer_name(
    issuer
).public_key(
    key.public_key()
).serial_number(
    x509.random_serial_number()
).not_valid_before(
    datetime.datetime.utcnow()
).not_valid_after(
    # Our certificate will be valid for 10 days
    datetime.datetime.utcnow() + datetime.timedelta(days=10)
).add_extension(
    x509.SubjectAlternativeName([x509.DNSName(u"localhost")]),
    critical=False,
# Signature avec la clé privée
).sign(key, hashes.SHA256(), default_backend())
# Ecriture sur le disque.
with open("d:\Python\ssl_key\certificate.pem", "wb") as f:
    f.write(cert.public_bytes(serialization.Encoding.PEM))