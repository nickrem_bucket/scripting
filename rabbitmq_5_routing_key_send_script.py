import subprocess, time, copy

time.sleep(2)
cmd1 = "python3 rabbitmq_5_routing_key_direct_sender.py error \"Error Not Known. Systeme Crash\" "
cmd2 = "python3 rabbitmq_5_routing_key_direct_sender.py warning \"Warning. Something was wrong\" "
cmd3 = "python3 rabbitmq_5_routing_key_direct_sender.py info \"Info : New instance was set\" "

liste1 = [1, 4, 7, 10, 13, 16]
liste2 = [2, 5, 8, 11, 14, 17]
liste3 = [3, 6, 9, 12, 15, 18]

i = 1
while i <= 18:
    if i in liste1:
        cmd = copy.deepcopy(cmd1)
    elif i in liste2:
        cmd = copy.deepcopy(cmd2)
    else:
        cmd = copy.deepcopy(cmd3)

    subprocess.call(cmd, shell=True)
    time.sleep(1)
    i += 1
