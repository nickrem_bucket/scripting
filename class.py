class Personne: # Définition de notre classe Personne
    """Classe définissant une personne caractérisée par :
    - son nom
    - son prénom
    - son âge
    - son lieu de résidence"""

    
    def __init__(self, nom, prenom): # Notre méthode constructeur
        """Pour l'instant, on ne va définir qu'un seul attribut"""
        #self.nom = "Dupont"
        self.nom = nom
        #self.prenom = "bernard"
        self.prenom = prenom
        self.age = 33
        self.lieu_residence = "Paris"

class Compteur:
    """Cette classe possède un attribut de classe qui s'incrémente à chaque
    fois que l'on crée un objet de ce type"""

    
    objets_crees = 0 # Le compteur vaut 0 au départ
    def __init__(self):
        """À chaque fois qu'on crée un objet, on incrémente le compteur"""
        Compteur.objets_crees += 1
    def combien(cls):
        """Méthode de classe affichant combien d'objets ont été créés"""
        print("Jusqu'à présent, {} objets ont été créés.".format(cls.objets_crees))
    combien = classmethod(combien)

class Test:
    """Une classe de test tout simplement"""
    def afficher():
        """Fonction chargée d'afficher quelque chose"""
        print("On affiche la même chose.")
        print("peu importe les données de l'objet ou de la classe.")
    afficher = staticmethod(afficher)
    
class TableauNoir:
    """Classe définissant une surface sur laquelle on peut écrire,
    que l'on peut lire et effacer, par jeu de méthodes. L'attribut modifié
    est 'surface'"""

    
    def __init__(self):
        """Par défaut, notre surface est vide"""
        self.surface = ""
    def ecrire(self, message_a_ecrire):
        """Méthode permettant d'écrire sur la surface du tableau.
        Si la surface n'est pas vide, on saute une ligne avant de rajouter
        le message à écrire"""

        
        if self.surface != "":
            self.surface += "\n"
        self.surface += message_a_ecrire
    def lire(self):
        """Cette méthode se charge d'afficher, grâce à print,
        la surface du tableau"""

        
        print(self.surface)
    def effacer(self):
        """Cette méthode permet d'effacer la surface du tableau"""
        self.surface = ""

#bernard = Personne()
#bernard = Personne("Durant", "Robert")
#print(bernard)

#print(bernard.nom)
#print(bernard.prenom)
#print(bernard.age)
#print(bernard.lieu_residence)

#bernard.lieu_residence = "Berlin"
#print(bernard.lieu_residence)

#t = Compteur.objets_crees
#print(t)

#a = Compteur() # création premier objet
#t = Compteur.objets_crees # Valeur du compteur après création
#print(t)

#b = Compteur()
#t = Compteur.objets_crees
#print(t)"""

#tab = TableauNoir()
#print(tab.surface)
#tab.ecrire("C'est la fin de l'année !")
#tab.surface
#print(tab.surface)   #--> print prend en compte le "\n" et l'interprete avec un saut de ligne
#tab.ecrire("Joyeux Noel :")
#tab.surface
#print(tab.surface)  # --> sans le "\n" dans la méthode, il n'y aurai pas de saut de ligne"""

#tab = TableauNoir()
#TableauNoir.ecrire(tab, "essai")
#print(tab.surface)

#tab = TableauNoir()
#tab.lire()
#tab.ecrire("Salut tout le monde.")
#tab.ecrire("La forme ?")
#tab.lire()
#tab.effacer()
#tab.lire()

#Compteur.combien() # methode de classe pour trouver le nombre d'objets créés
#a = Compteur()     # création d'un objet
#Compteur.combien() # Compteur.combien() et a.combien() sont équivalent, puisque a est un objet de la classe Compteur
#a.combien()
#b = Compteur()
#Compteur.combien()
#b.combien()

a = Test()
a.afficher()