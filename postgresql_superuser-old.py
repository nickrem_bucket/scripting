import psycopg2

from psql_config import connect


conn = None
try:

    # read psql param pour le user own
    params = connect("own")
    
    conn = psycopg2.connect(**params)
    print("Database opened successfully")

    cur = conn.cursor()
    cur.execute("drop table if exists COURSE cascade;")
    cur.execute("drop table if exists DEPARTMENT cascade;")
    cur.execute("drop table if exists STUDENT cascade;")
    cur.execute("drop sequence if exists seq_course_id;")
    cur.execute("drop sequence if exists seq_department_id;")
    cur.execute("drop function if exists function_course_id;")
    cur.execute("drop function if exists function_department_id;")
    print("")
    print("Tables COURSE/DEPARTMENT/STUDENT droped successfully")


    #cur.execute("""
    #    create table  COURSE
    #        (
    #           ID_COURSE            serial        not null
    #         , NAME_COURSE          char(50)
    #         , constraint PK_COURSE primary key (ID_COURSE)
    #        )
    #    ;
    #    """)
    cur.execute("""
        create table  COURSE
            (
               ID                       int         not null
             , NAME                     char(50)
             , AMOUNT                   int
             , constraint PK_COURSE     primary key (ID)
            )
        """);
    print("")
    print("Table COURSE created successfully")

    cur.execute(""" create sequence seq_course_id """)
    print("Sequence to course.ID created successfully")
    
    cur.execute("""
        create or replace function function_course_id()
        returns "trigger" as
        $body$
            begin
                new.id:=nextval('seq_course_id');
                return new;
            end;
        $body$
        
        language 'plpgsql' volatile;
        """)
    print("Function to course.ID created successfully")
    
    cur.execute("""
        create trigger trigger_course_id
            before insert on COURSE
            for each row
                execute procedure function_course_id()
        """);
    print("Trigger to course.ID created successfully")
    
    list_course = [('Computer Science', 500042), ('Electrical Engineering', 450000), ('Information Technology', 420000)]
    i = 0
    dict_course = {}
    while i < len(list_course):
        sql_statement = (""" insert into COURSE (NAME, AMOUNT) values ('{}', {}) returning id """).format(list_course[i][0], list_course[i][1])
        cur.execute(sql_statement)
        id = cur.fetchone()[0]
        dict_course[list_course[i][0]] = id
        i += 1
    print("{} Inserts on table COURSE successfully".format(len(list_course)))
    
    cur.execute("""
        create table  DEPARTMENT
            (
               ID                       int        not null
             , NAME                     char(50)
             , constraint PK_DEPARTMENT primary key (ID)
            )
        ;
        """)
    print("")
    print("Table DEPARTMENT created successfully")

    cur.execute(""" create sequence seq_department_id """)
    print("Sequence to department.ID created successfully")
    
    cur.execute("""
        create or replace function function_department_id()
        returns "trigger" as
        $body$
            begin
                new.id:=nextval('seq_department_id');
                return new;
            end;
        $body$
        
        language 'plpgsql' volatile;
        """)
    print("Function to department.ID created successfully")
    
    cur.execute("""
        create trigger trigger_department_id
            before insert on DEPARTMENT
            for each row
                execute procedure function_department_id()
        """);
    print("Trigger to department.ID created successfully")

    list_department = ["ICT", "Engineering"]
    i = 0
    dict_department = {}
    while i < len(list_department):
        sql_statement = (""" insert into DEPARTMENT (NAME) values ('{}') returning id """).format(list_department[i])
        cur.execute(sql_statement)
        id = cur.fetchone()[0]
        dict_department[list_department[i]] = id
        i += 1

    print("{} Inserts on table DEPARTMENT successfully".format(len(list_department)))
    
    cur.execute("""
        create table  STUDENT
            (
               ADMISSION               int           not null
             , NAME                    text          not null
             , AGE                     int           not null
             , COURSE                  int
             , DEPARTMENT              int
             , constraint PK_STUDENT   primary key (ADMISSION)
             , constraint FK01_STUDENT foreign key (COURSE)     references COURSE(ID)
             , constraint FK02_STUDENT foreign key (DEPARTMENT) references DEPARTMENT(ID)
            )
        ;
        """)
    print("")
    print("Table STUDENT created successfully")

    list_student = [[3420, "John", 18, "Computer Science", "ICT"],
                    [3419, "Abel", 17, "Computer Science", "ICT"],
                    [3421, "Joel", 17, "Computer Science", "ICT"],
                    [3422, "Anthony", 19, "Electrical Engineering", "Engineering"],
                    [3423, "Alice", 18, "Information Technology", "ICT"],
                    [3424, "Tom", 20, "Information Technology", "ICT"]]

    i = 0
    while i < len(list_student):
        id_adm      = list_student[i][0]
        nam_student = list_student[i][1]
        age_student = list_student[i][2]
        cou_student = dict_course[list_student[i][3]]
        dep_student = dict_department[list_student[i][4]]
        sql_statement = (""" insert into STUDENT (ADMISSION, NAME, AGE, COURSE, DEPARTMENT) values ({}, '{}', {}, {}, {}) """).format(id_adm, nam_student, age_student, cou_student, dep_student);
        cur.execute(sql_statement)
        i += 1

    print("{} Inserts on table STUDENT successfully".format(len(list_student)))

    cur.execute(""" select TABLE_NAME from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA = 'public' """);
    rows = cur.fetchall()
    for row in rows:
        sql_statement = (""" grant select, insert, update, delete on table {} to postgres_app """).format(row[0])
        cur.execute(sql_statement)
    
    #cur.execute("grant select, insert, update, delete on table public.COURSE to postgres_app");
    #cur.execute("grant select, insert, update, delete on table public.DEPARTMENT to postgres_app");
    #cur.execute("grant select, insert, update, delete on table public.STUDENT to postgres_app");
    print("")
    print("{} Grants on tables COURSE/DEPARTMENT/STUDENT to postgres_app successfully".format(len(rows)))

    cur.execute(""" select RELNAME from PG_CLASS where RELKIND = 'S' """);
    rows = cur.fetchall()
    for row in rows:
        sql_statement = (""" grant select on sequence {} to postgres_app """).format(row[0])
        cur.execute(sql_statement)
    print("")
    print("{} Grants on SEQUENCE COURSE/DEPARTMENT to postgres_app successfully".format(len(rows)))

    conn.commit()
    cur.close()

except psycopg2.DatabaseError as error:
    print(error)
    print("")

finally:
    if conn is not None:
        conn.close()
        print("")
        print("")
        print('Database connection closed.')

