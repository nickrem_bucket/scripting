import json
import couchdb
import couchdb.mapping as cmap
import getpass
import sys


def updateDoc(self, id, json):
    doc = self.db.get(id)
    #doc["firstName"] = json["firstName"]
    #doc = self.db.save(doc)
    #print doc['_id'], doc['_rev']
    #print "Saved"
    
class User(cmap.Document):
    """  Class used to map a user document inside the '_users' database to a
    Python object.

    For better understanding check https://wiki.apache.org
        /couchdb/Security_Features_Overview

    Args:
        name: Name of the user
        password: password of the user in plain text
        type: (Must be) 'user'
        roles: Roles for the users

    """

    def __init__(self, **values):
        # For user in the _users database id must be org.couchdb.user:<name>
        # Here we're auto-generating it.
        if 'name' in values:
            _id = 'org.couchdb.user:{}'.format(values['name'])
            cmap.Document.__init__(self, id=_id, **values)

    type = cmap.TextField(default='user')
    name = cmap.TextField()
    password = cmap.TextField()
    roles = cmap.ListField(cmap.TextField())

    @cmap.ViewField.define('users')
    def default(doc):
        if doc['name']:
            yield doc['name'], doc

with open('DBdata.json') as data_file:    
    data = json.load(data_file)
    ssl = data.get('ssl')
    user_name = data.get('username')
    user_pass = data.get('password')
    host = data.get('host')
    port = data.get('port')
    data_file.close()
    if ssl is False:
        ssl = 'http://'
    else: 
        ssl = 'https://'

print("")
print(" *** Scripts Admin for add regular user ***")
print("")
admin_user = input("User Administrator : ")
admin_pass = getpass.getpass("Password : ")

print("")
print("Fonction (Add/Remove/Changepassword)")


address = "http://" + str(admin_user) + ":" + str(admin_pass) + "@" + str(host) + ":" + str(port)

try:

    couchserver = couchdb.Server(address)
    dbname = "_users"


    if dbname in couchserver:
        db = couchserver[dbname]
        print("Database \"_users\" opened successfully")
    else:
        db = couchserver.create(dbname)
        print("Database \"_users\" created and opened successfully")

    user_pass = "orange"
    user_params = User(name=user_name, password=user_pass)
    print(user_params)


    rows = get_db().view('_all_docs', include_docs=True)
    docs = []
    for row in rows:
        print(row)
     

    #user_params.store(db)

except NameError as err:
    print(err)
    exit()

except ValueError as err:
    print(err)
    exit()

except:
   print("Unexpected error : ", sys.exc_info()[0])
   exit()
