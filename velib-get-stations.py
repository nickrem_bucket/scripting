import json
import time
import urllib.request
import copy
from datetime import datetime

from kafka import KafkaProducer

API_KEY = "7e19ba325ea45f307dc34008f3f97be1ff769274" # FIXME
url = "https://api.jcdecaux.com/vls/v1/stations?apiKey={}".format(API_KEY)

producer = KafkaProducer(
                        #   bootstrap_servers="localhost:9095"
                           bootstrap_servers=['localhost:9095', 'localhost:9096']
                        )

first_message = True

list_column = ['number', 'contract_name', 'address', 'available_bikes']
list_stations_ref = []
new_dict = {}

cpt = 0
cpt_empty = 0
cpt_not_empty =0

dict_nb_empty_tot = {}
dict_cpt_contract = {}

while True:
    response = urllib.request.urlopen(url)
    stations = json.loads(response.read().decode())

    len_stations = len(stations)
    #print(len_stations)
    # première éxécution, sauvegarde de la liste comme référence
    # + création dictionnaire pour avoir le nombre de station vide par ville
    if first_message == True:
        i = 0
        while i < len(stations):            
            for key in stations[i].keys():
                if key in list_column:
                    new_dict[key] = copy.deepcopy(stations[i][key])
            dict_temp = copy.deepcopy(new_dict)
            list_stations_ref.append(dict_temp)
            i += 1
        i = 0
        while i < len(list_stations_ref):
            cpt_contract = "cpt_" + str(list_stations_ref[i]['contract_name']) + "_" + str(list_stations_ref[i]['number'])
            if cpt_contract not in dict_cpt_contract:
                dict_cpt_contract[cpt_contract] = 0
            if list_stations_ref[i]['available_bikes'] == 0:
                #print(list_stations_ref[i]['contract_name'])
                if list_stations_ref[i]['contract_name'] not in dict_nb_empty_tot:
                    dict_nb_empty_tot[list_stations_ref[i]['contract_name']] = 1
                else:
                    dict_nb_empty_tot[list_stations_ref[i]['contract_name']] += 1
            i += 1

        first_message = False

        #print(dict_cpt_contract)
        #print(list_stations_ref)
        #print(dict_nb_empty_tot)
        #exit()

    else:
        for station in stations:
        
            # récupération des information de la station
            station_number = station['number']
            station_contract = station['contract_name']
            station_available_bikes = station['available_bikes']
            station_address = station['address']

            # Vérification si la station (number, contract) n'existe pas, pour l'ajouter à la liste référence
            i = 0
            in_list = False
            while i < len(list_stations_ref):
                if (list_stations_ref[i]['number'] == station_number) and (list_stations_ref[i]['contract_name'] == station_contract):
                    in_list = True
                i += 1
            if in_list == False:
                print("new entry in list_stations_ref")
                new_dict = {}
                new_dict['number'] = station_number
                new_dict['contract_name'] = station_contract
                new_dict['address'] = station_address
                new_dict['available_bikes']= station_address
                dict_temp = copy.deepcopy(new_dict)
                list_stations_ref.append(dict_temp)
                if station_contract in dict_nb_empty_tot:
                    dict_nb_empty_tot[station_contract] = dict_nb_empty_tot[station_contract] + 1
                else:
                    dict_nb_empty_tot[station_contract] = 1

            # récupération de l'indice de la liste de référence correspondance à la station en cours
            i = 0
            while i < len(list_stations_ref):
                station_ref_number = list_stations_ref[i]['number']
                station_ref_contract = list_stations_ref[i]['contract_name']
                station_ref_available_bikes = list_stations_ref[i]['available_bikes']
                if (station_ref_number == station_number) and (station_ref_contract == station_contract):
                    break
                i += 1

            # La station était vide
            if station_ref_available_bikes == 0:

                # la station n'est plus vide, envoi d'un message à "empty-stations"
                if station_available_bikes > 0:
                    list_stations_ref[i]['available_bikes'] = station_available_bikes

                    if station_contract in dict_nb_empty_tot:
                        dict_nb_empty_tot[station_contract] -= 1
                    
                    station['number_stations_empty'] = dict_nb_empty_tot[station_contract]

                    from dict_vi_pay import dict_ville_pays
                    if not station_contract in dict_ville_pays:
                        routing_key = station_contract
                    else:
                        ville = station_contract.lower()
                        routage = dict_ville_pays[ville]
                        #print(routage)
                        routing_key = "country." + routage

                    #var = str(station["contract_name"]) + ";" + str(station['number'])
                    #var = str(station["contract_name"])
                    date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    if station_contract == "lyon":
                        producer.send(
                                       "empty-stations-lyon"
                                      , json.dumps(station).encode()
                                     )
                    else:
                        producer.send(
                                        "empty-stations"
                                      , json.dumps(station).encode()
                                     )
                    #producer.send(
                    #                "empty-stations"
                    #              , json.dumps(station).encode()
                    #              , key=str(var).encode()
                    #             )

                    print(" [x] Message \"not empty\" Sent :")
                    print("     {} --- Station : {} ({} {})".format(date, station_number, station_address, station_contract))
                    print("            Routing Key : {}".format(routing_key))
                    print("            Nb Empty Station in {} : {}".format(station_contract, dict_nb_empty_tot[station_contract]))
                    print("")

            # la station n'était pas vide
            elif station_ref_available_bikes == 1:

                # la station est de nouveau vide, envoi d'un message à "empty-stations"
                if station_available_bikes == 0:
                    list_stations_ref[i]['available_bikes'] = 0

                    if station_contract in dict_nb_empty_tot:
                        dict_nb_empty_tot[station_contract] += 1
                    else:
                        dict_nb_empty_tot[station_contract] = 1
                        

                    station['number_stations_empty'] = dict_nb_empty_tot[station_contract]

                    from dict_vi_pay import dict_ville_pays
                    if not station_contract in dict_ville_pays:
                        routing_key = station_contract
                    else:
                        ville = station_contract.lower()
                        routage = dict_ville_pays[ville]
                        #print(routage)
                        routing_key = "country." + routage

                    #var = str(station["contract_name"]) + ";" + str(station['number'])
                    #var = str(station["contract_name"])
                    date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    if station_contract == "lyon":
                        producer.send(
                                        "empty-stations-lyon"
                                      , json.dumps(station).encode()
                                     )
                    else:
                        producer.send(
                                        "empty-stations"
                                      , json.dumps(station).encode()
                                     )
                    #producer.send(
                    #                "empty-stations"
                    #              , json.dumps(station).encode()
                    #              , key=str(var).encode()
                    #             )

                    print(" [x] Message \"Empty\" Sent :")
                    print("     {} --- Station : {} ({} {})".format(date, station_number, station_address, station_contract))
                    print("            Routing Key : {}".format(routing_key))
                    print("            Nb Empty Station in {} : {}".format(station_contract, dict_nb_empty_tot[station_contract]))
                    print("")

    print("Produced {} station records".format(len(stations)))
    time.sleep(1)

