import sys, os
 
# Main definition - constants
menu_actions  = {}  
 
# --- MENUS FUNCTIONS
 
# Main menu
def main_menu():
    os.system('cls')
    
    print("   ******** Welcome,\n")
    print("   Please choose the menu you want to start:")
    print("       1. Menu 1")
    print("       2. Menu 2")
    print("")
    print("       q. Quit")
    choice = input(" >>  ")
    exec_menu(choice)
 
    return
 
# Execute menu
def exec_menu(choice):
    os.system('cls')
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            menu_actions[ch]()
        except KeyError:
            print("Invalid selection, please try again.")
            print("")
            menu_actions['main_menu']()
    return
 
# Menu 1
def menu1():
    print("   Hello Menu 1 !")
    print("")
    print("       r. Back")
    print("       0. Quit")
    choice = input(" >>  ")
    exec_menu(choice)
    return
 
# Menu 2
def menu2():
    print("   Hello Menu 2 !")
    print("")
    print("       r. Back")
    print("       0. Quit")
    choice = input(" >>  ")
    exec_menu(choice)
    return
 
# Back to main menu
def back():
    menu_actions['main_menu']()
 
# Exit program
def exit():
    sys.exit()
 
 
# --- Menu definition
menu_actions = {
    'main_menu': main_menu,
    '1': menu1,
    '2': menu2,
    'r': back,
    'q': exit,
}
 
 
# --- Main Program
if __name__ == "__main__":
    main_menu()