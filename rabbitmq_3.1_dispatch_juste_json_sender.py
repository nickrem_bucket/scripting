import pika
import sys
import json
from datetime import datetime

connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.queue_declare(
                        queue='task_queue'
                      , durable=True
                     )

#message = ' '.join(sys.argv[1:]) or "Hello World!"
#x = sys.argv[1]
#x = int(x)
#date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
#if x == 1 :
#    lib_desc = "1st message sended"
#elif x == 2:
#    lib_desc = "2nd message sended"
#elif x == 3:
#    lib_desc = "3rd message sended"
#else:
#    lib_desc = str(x) + "th message sended"
#data = {
#          "id":x
#        , "datetime": date 
#        , "description": lib_desc
#        }
#message = json.dumps(data)
with open('donnees_swift', "r") as filin:
    lignes = filin.read()

data = {
          "id":1
        , "description": lignes
        }
#message = json.dumps(lignes).encode()
message = json.dumps(data).encode()
channel.basic_publish(
                        exchange=''
                      , routing_key='task_queue'
                      , body=message
                      , properties=pika.BasicProperties(
                              delivery_mode=2,  # make message persistent
                                                       )
                     )

print(" [x] Sent %r" % message)

connection.close()