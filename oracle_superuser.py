import os, sys, cx_Oracle, platform

name_platform = platform.system().upper()
if name_platform == "WINDOWS":
    location = r"D:\oracle\product\18.0.0\dbhomeXE\network\admin"
    sys.path.insert(0, 'd:\\Python\\params')
else:
    location = r"/apps/oracle/network/admin"
    sys.path.insert(0, '/home/nicolas')

from package.ora_config import config


conn = None
try:

    # read psql param pour le user own
    param = config("own")
    
    os.environ["TNS_ADMIN"] = location
    #dsn_tns = cx_Oracle.makedsn(var_host, var_port, service_name=var_sid) 
    #conn = cx_Oracle.connect(var_user, var_password, dsn_tns)
    conn = cx_Oracle.connect(param)
    print("Database opened successfully")

    cur = conn.cursor()
    cur.callproc("DELOBJECT", ["COURSE","TABLE"])
    cur.callproc("DELOBJECT", ["SEQ_COURSE_ID","SEQUENCE"])
    cur.callproc("DELOBJECT", ["TRIGG_COURSE_ID","TRIGGER"])
    cur.callproc("DELOBJECT", ["DEPARTMENT","TABLE"])
    cur.callproc("DELOBJECT", ["SEQ_DEPARTMENT_ID","SEQUENCE"])
    cur.callproc("DELOBJECT", ["TRIGG_DEPARTMENT_ID","TRIGGER"]) 
    cur.callproc("DELOBJECT", ["STUDENT","TABLE"])
    print("")
    print("Tables COURSE/DEPARTMENT/STUDENT droped successfully")

    cur.execute("""
        create table COURSE
            (
               ID                       int           not null
             , NAME                     varchar2(50)
             , AMOUNT                   number(10)
             , constraint PK_COURSE     primary key (ID)
            )
        """);
    print("")
    print("Table COURSE created successfully")
    
    cur.execute("""
        create sequence SEQ_COURSE_ID
            start with 1
            increment by 1
            cache 100
        """);
    print("Sequence SEQ_COURSE to table COURSE created successfully")
    
    cur.execute("""
        create or replace trigger TRIGG_COURSE_ID
            before insert on COURSE
            for each row
                begin
                    select SEQ_COURSE_ID.nextval
                      into :new.ID
                      from dual;
                end;
        """);
    print("Trigger TRIGG_COURSE to table COURSE created successfully")

    list_course = [('Computer Science', 500042), ('Electrical Engineering', 450000), ('Information Technology', 420000)]
    
    i = 0
    idVar = cur.var(cx_Oracle.NUMBER)
    dict_course = {}
    while i < len(list_course):
        sql_statement = (""" insert into COURSE (NAME, AMOUNT) values ('{}', {}) returning ID into :id """).format(list_course[i][0], list_course[i][1]);
        cur.execute(sql_statement, id = idVar)
        id = idVar.getvalue()
        dict_course[list_course[i][0]] = int(id[0])
        i += 1
    print("{} Inserts on table COURSE successfully".format(len(list_course)))
    
    cur.execute("""
        create table DEPARTMENT
            (
               ID                           int           not null
             , NAME                         varchar2(50)
             , constraint PK_DEPARTMENT     primary key (ID)
            )
        """);
    print("")
    print("Table DEPARTMENT created successfully")
    
    cur.execute("""
        create sequence SEQ_DEPARTMENT_ID
            start with 1
            increment by 1
            cache 100
    """);
    print("Sequence SEQ_DEPARTMENT to table DEPARTMENT created successfully")
    
    cur.execute("""
        create or replace trigger TRIGG_DEPARTMENT_ID
            before insert on DEPARTMENT
            for each row
                begin
                    select  SEQ_DEPARTMENT_ID.nextval
                      into  :new.ID
                      from  dual;
                end;
        """);
    print("Trigger TIGG_DEPARTMENT to table DEPARTMENT created successfully")

    list_department = ["ICT", "Engineering"]
    i = 0
    idVar = cur.var(cx_Oracle.NUMBER)
    dict_department = {}
    while i < len(list_department):
        sql_statement = (""" insert into DEPARTMENT (NAME) values ('{}') returning ID into :id """).format(list_department[i]);
        cur.execute(sql_statement, id = idVar)
        id = idVar.getvalue()
        dict_department[list_department[i]] = int(id[0])
        i += 1
    print("{} Inserts on table DEPARTMENT successfully".format(len(list_department)))

    cur.execute("""
        create table STUDENT
            (
               ADMISSION                int             not null
             , NAME                     varchar2(20)    not null
             , AGE                      int             not null
             , COURSE                   int
             , DEPARTMENT               int
             , constraint PK_STUDENT    primary key (ADMISSION)
             , constraint FK01_STUDENT  foreign key (COURSE)     references COURSE (ID)
             , constraint FK02_STUDENT  foreign key (DEPARTMENT) references DEPARTMENT (ID)
            )
        """);
    print("")
    print("Table STUDENT created successfully")

    list_student = [[3420, "John", 18, "Computer Science", "ICT"],
                    [3419, "Abel", 17, "Computer Science", "ICT"],
                    [3421, "Joel", 17, "Computer Science", "ICT"],
                    [3422, "Anthony", 19, "Electrical Engineering", "Engineering"],
                    [3423, "Alice", 18, "Information Technology", "ICT"],
                    [3424, "Tom", 20, "Information Technology", "ICT"]]

    i = 0
    while i < len(list_student):
        id_adm      = list_student[i][0]
        nam_student = list_student[i][1]
        age_student = list_student[i][2]
        cou_student = dict_course[list_student[i][3]]
        dep_student = dict_department[list_student[i][4]]
        sql_statement = (""" insert into STUDENT (ADMISSION, NAME, AGE, COURSE, DEPARTMENT) values ({}, '{}', {}, {}, {}) """).format(id_adm, nam_student, age_student, cou_student, dep_student);
        cur.execute(sql_statement)
        i += 1

    print("{} Insert on table STUDENT successfully".format(len(list_student)))

    cur.execute(""" select TABLE_NAME from USER_TABLES """);
    rows = cur.fetchall()
    for row in rows:
        sql_statement = (""" grant select, insert, update, delete on {} to scott2 """).format(row[0])
        cur.execute(sql_statement)
        
    print("")
    print("{} Grants on tables COURSE/DEPARTMENT/STUDENT to scott2 successfully".format(len(rows)))

    cur.execute(""" select SEQUENCE_NAME from USER_SEQUENCES """);
    rows = cur.fetchall()
    for row in rows:
        sql_statement = (""" grant select on {} to scott2 """).format(row[0])
        cur.execute(sql_statement)
    print("")
    print("{} Grants on sequence COURSE/DEPARTMENT to scott2 successfully".format(len(rows)))
    
    conn.commit()
    cur.close()

except cx_Oracle.DatabaseError as e:
    error, = e.args
    print(error.code)
    print(error.message)
    print(error.context)
    print("")

finally:
    if conn is not None:
        conn.close()

conn = None
try:

    # read psql param pour le user app
    param = config("app")
    
    os.environ["TNS_ADMIN"] = location
    #dsn_tns = cx_Oracle.makedsn(var_host, var_port, service_name=var_sid) 
    #conn = cx_Oracle.connect(var_user2, var_password2, dsn_tns)
    conn = cx_Oracle.connect(param)
    print("Database opened successfully")

    cur = conn.cursor()

    cur.callproc("DELOBJECT", ["COURSE","SYNONYM"])
    cur.callproc("DELOBJECT", ["DEPARTMENT","SYNONYM"])
    cur.callproc("DELOBJECT", ["STUDENT","SYNONYM"])
    cur.callproc("DELOBJECT", ["SEQ_COURSE_ID", "SYNONYM"])
    cur.callproc("DELOBJECT", ["SEQ_DEPARTMENT_ID", "SYNONYM"])
    print("")
    print("Synonyms scott2 COURSE/DEPARTMENT/STUDENT droped successfully")
    
    cur.execute(""" create synonym COURSE     for scott.COURSE """);
    cur.execute(""" create synonym DEPARTMENT for scott.DEPARTMENT """);
    cur.execute(""" create synonym STUDENT    for scott.STUDENT """);
    print("")
    print("Create synonyms COURSE/DEPARTMENT/STUDENT to scott2 successfully")

    cur.execute(""" create synonym SEQ_COURSE_ID     for scott.SEQ_COURSE_ID """);
    cur.execute(""" create synonym SEQ_DEPARTMENT_ID for scott.SEQ_DEPARTMENT_ID """);
    print("")
    print("Create synonyms SEQ_COURSE_ID/SEQ_DEPARTMENT_ID to scott2 successfully")

    conn.commit()
    cur.close()

except cx_Oracle.DatabaseError as e:
    error, = e.args
    print(error.code)
    print(error.message)
    print(error.context)
    print("")

finally:
    if conn is not None:
        conn.close()

