from cryptography.fernet import Fernet

text = 'nicolas'
nico = text.encode()

nico2 = nico.decode()


key = Fernet.generate_key() #this is your "password"
cipher_suite = Fernet(key)
encoded_text = cipher_suite.encrypt(nico)
decoded_text = cipher_suite.decrypt(encoded_text)

last_step = encoded_text.decode()

b = last_step.encode()
decoded_text = cipher_suite.decrypt(b)

#print(encoded_text)
print(b)
print(decoded_text)