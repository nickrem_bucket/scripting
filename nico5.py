#import json

#with open('donnees_swift', "r") as filin:
#    lignes = filin.read()

#data = {
#          "id":1
#        , "description": lignes
#        }
#message1 = json.dumps(data)
#message2 = json.dumps(data).encode()

#print(message1)
#print(message2)

import os
"""data = "toto.xlsx.gpg"
print(data)

filename, file_extention = os.path.splitext(data)
print(filename)
print(file_extention)"""

import gnupg, sys
from nostri import pgp_public_key, gpg_home_dir
gpg_home = gpg_home_dir
gpg = gnupg.GPG(gnupghome=gpg_home)
rkeys = pgp_public_key
passphrase_gpg = "Azerty*6789*"
signer_fingerprint = rkeys
data = "Student.xlsx.gpg"
data_nogpg = "Student.xlsx"
savefile = data
with open(data, 'rb') as f:
    verify = gpg.verify(data)
print(verify.status)
print(verify.stderr)
exit()
try:
    """if os.path.isfile(data):
        name_file, name_extention = os.path.splitext(data)                
        with open(data, 'rb') as f_gpg:
            cmd_gpg = gpg.decrypt_file(f_gpg, passphrase=passphrase_gpg, output=name_file)
        if cmd_gpg.ok == False:
            temp_msg = str(f_gpg) + "\n" + cmd_gpg.status + "\n" + cmd_gpg.stderr
            raise ValueError(temp_msg)"""
    if os.path.isfile(data_nogpg):
        afile = open(data_nogpg, 'rb')
        cmd_gpg = gpg.encrypt_file(afile, rkeys.split(), always_trust=False, sign=signer_fingerprint, passphrase=passphrase_gpg, output=savefile)
        afile.close()
        if cmd_gpg.ok == False:
            temp_msg = str(afile) + "\n" + cmd_gpg.status + "\n" + cmd_gpg.stderr
            raise ValueError(temp_msg)

except ValueError as err:
    print(err)

except NameError as err:
    print(err)

except TypeError as err:
    print(err)

except:
   print("Unexpected error : ", sys.exc_info()[0])
   
finally:
    print("Fin")