import sys

x = len(sys.argv)
if x == 1:
    print("Veuillez passer une variable au script")
    exit()

y = sys.argv[1]
try:
    y = str(y)
except ValueError:
    pass
p = isinstance(y,str)
if str(p) == "False":
    print("La variable passée n'est pas un string")
    exit()

dict_mot = {}

i = 0
while i < len(y):
    if i not in (0, 1):
        j = i - 1
        k = j - 1
        mot = str(y[k]) + str(y[j]) + str(y[i])
        if len(dict_mot) == 0:
            dict_mot[mot] = 1
        else:
            if mot in dict_mot:
                value_key = dict_mot[mot]
                value_key += 1
                dict_mot[mot] = value_key
            else:
                dict_mot[mot] = 1
    i += 1

print("")
print("Mots de 3 lettres")
for key in dict_mot:
    name_key = key
    value_key = dict_mot[key]
    print("{} : {}".format(name_key, value_key))