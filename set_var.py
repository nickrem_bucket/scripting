def __main__():
    import inspect, site
    scripts_authorize = "postgresql.py;postgresql2.py;mariadb.py;mariadb2.py;mysql.py;mysql2.py;oracle.py;oracle2.py;postgresql_superuser.py;mariadb_superuser.py;mysql_superuser.py;oracle_superuser.py;pass_keyring.py"
    
    list_scripts = scripts_authorize.split(';')
    name_caller = inspect.stack()[1][1]
    
    if name_caller in list_scripts:
        site.addsitedir('D:\data\module\params')
        site.addsitedir('D:\data\module\packages')