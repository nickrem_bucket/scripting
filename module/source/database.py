cubrid = dict(
          database = "testdb"
        , host = "localhost"
        , port = "33000"
        , user_own = "scott"
        , user_app = "scott2"
)
firebird = dict(
          database = "D:\\data\\TEST.FDB"
        , host = "localhost"
        , port = "3050"
        , user_own = "scott"
        , user_ap = "scott2"
)
mariadb = dict(
		  database = "test"
		, host = "localhost"
		, port = "3306"
		, user_own = "scott"
		, user_app = "scott2"
)
mysql = dict(
		  database = "demo"
		, host = "localhost"
		, port = "3307"
		, user_own = "scott"
		, user_app = "scott2"
)
oracle = dict(
		  database = "XE"
		, host = "localhost"
		, port = "1521"
		, user_own = "scott"
		, user_app = "scott2"
)
postgresql = dict(
		  database = "postgres"
		, host = "localhost"
		, port = "5432"
		, user_own = "scott"
		, user_app = "scott2"
)