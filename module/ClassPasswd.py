# -*- coding: latin-1 -*-

import os
import base64
import traceback
import pickle
from getpass import getpass
from pbkdf2 import PBKDF2
from Crypto.Cipher import AES

class SecurePasswd(object):

        # Random String
        SEED = 'mkhgts465wef4fwtdd'

        # Fichier Key password
        KP_FILE = '/home/nicolas/.local/share/store_passwd/key_file_app.kp'

        # Fichier Salt DabaBase
        SDB_FILE = '/home/nicolas/.local/share/store_passwd/salt_file_app.sdb'

        PASSPHRASE_SIZE = 64            # 512-bit passphrase
        KEY_SIZE = 32                   # 256-bit key
        BLOCK_SIZE = 16                 # 16-bit blocks
        IV_SIZE = 16                    # 128-bits to initialize
        SALT_SIZE = 8                   # 64-bits of salt

        def __init__(self):
                try:
                        with open(self.KP_FILE) as f:
                                self.kp = f.read()
                        if len(self.kp) == 0:
                                raise IOError
                        else:
                                # decodage du fichier kp base64
                                self.kp = base64.b64decode(self.kp)

                except IOError:
                        with open(self.KP_FILE, 'w') as f:

                                # generation du kp aleatoire
                                self.kp = os.urandom(self.PASSPHRASE_SIZE)
                                f.write(str(base64.b64encode(self.kp)))

                                try:
                                        # Si le kp doit etre regenere, les anciennes donnees du fichier SDB
                                        # ne peuvent plus etre utilisees et donc doivent etre supprimees
                                        if os.path.exists(self.SDB_FILE):
                                                os.remove(self.SDB_FILE)
                                except:
                                        print(traceback.format_exc())
                                        print("Possible erreur sur les permissions du SDB_FILE {}".format(self.SDB_FILE))

                # Chargement ou creation du SDB_FILE
                try:
                        with open(self.SDB_FILE, "r") as f:
                                self.sdb = pickle.load(f)

                        # le sdb est un dictionnaire à couple key/value
                        if self.sdb == {}:
                                raise IOError
                except (IOError, EOFError):
                        self.sdb = {}
                        with open(self.SDB_FILE, 'w') as f:
                                pickle.dump(self.sdb, f)

        def getSaltForPname(self, pname):
                # Generation du Salt a partir de la cle hash avec son propre Salt
                return PBKDF2(pname, self.SEED).read(self.SALT_SIZE)


        # Chiffrement du mot de passe
        def encrypt(self, pname, p):

                # Initialisation aleatoire de l'initVector (IV) et du Salt
                initVector = os.urandom(self.IV_SIZE)
                salt = self.getSaltForPname(pname)

                # Preparation de la key utilisee pour chiffrer/dechiffrer
                k = PBKDF2(self.kp, salt).read(self.KEY_SIZE)

                # Creation du cipher utilise pour le chiffrement
                cipher = AES.new(k, AES.MODE_CBC, initVector)

                # Padding et chiffrement
                self.sdb[pname] = initVector + cipher.encrypt(p + ' '*(self.BLOCK_SIZE - (len(p) % self.BLOCK_SIZE)))

                # Sauvegarde dans le SDB
                with open(self.SDB_FILE, 'w') as f:
                        pickle.dump(self.sdb, f)

        # Dechiffrement du mot de passe
        def decrypt(self, pname):

                # Recuperation du Salt du parametre
                self.sdb[pname]
                salt = self.getSaltForPname(pname)

                # Recreation de la key a l'identique
                key = PBKDF2(self.kp, salt).read(self.KEY_SIZE)

                # Recuperation de l'IV (a partir du Salt) concatene dans
                # le data stocke dans le fichier SDB_FILE
                initVector = self.sdb[pname][:self.IV_SIZE]

                # Recuperation du data a dechiffrer
                encryptedData = self.sdb[pname][self.IV_SIZE:]

                # Recreation du cipher
                cipher = AES.new(key, AES.MODE_CBC, initVector)

                # Dechiffrement et de-padding
                return cipher.decrypt(encryptedData).decode()
