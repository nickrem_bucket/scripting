# coding: latin1

#@(#) ========================================================================
#@(#) SCRIPT : Encrypt-Decrypt_file.py
#@(#) OBJET  : Chiffrement/Déchiffrement AES/HMAC d'un fichbier
#@(#)
#@(#) AUTEUR : NRE
#@(#) ------------------------------------------------------------------------
#@(#) PARAMETRES :
#@(#)     - {1} : paramètre d'exécution (-e / -d)
#@(#)     - {2} : répertoire de travail
#@(#)     - {3} : fichier à chiffrer/déchiffrer
#@(#)     - {4} : fichier chiffrer/déchiffrer
#@(#)     - {5} : Clé de chiffrement
#@(#)     - {6} : Passphrase de renfort de la clé de chiffrement
#@(#)
#@(#) ------------------------------------------------------------------------
#@(#) VERSION DE LIVRAISON : 1.0
#@(#)
#@(#) ------------------------------------------------------------------------
#@(#) DATE        AUTEUR        MODIFICATION
#@(#) 20/03/2020  NRE           Création
#@(#)
#@(#) ========================================================================

import argparse
import os
import sys

from Crypto.Cipher import AES
from Crypto.Hash import HMAC
from Crypto.Protocol.KDF import PBKDF2

parser = argparse.ArgumentParser()
parser.add_argument("dir", help="Working directory ( ex: /applis )")
parser.add_argument("filein", help="File to encrypt/decrypt")
parser.add_argument("fileout", help="Result file encrypted/decrypted")
parser.add_argument("key", help="Key to encrypt")
parser.add_argument("salt", help="Passphrase to enforce the key encrypt")
parser.add_argument("-e", "--encrypt", action="store_true")
parser.add_argument("-d", "--decrypt", action="store_true")
args = parser.parse_args()

MyKey = args.key
MySalt = args.salt
filein_abs = args.dir + "/" + args.filein
fileout_abs = args.dir + "/" + args.fileout

if os.path.isfile(fileout_abs):
        os.remove(fileout_abs)

# input file
try:
        inputfile = open(filein_abs, "rb")
except IOError:
        sys.exit("Could not open the input file")

# output file
try:
        output = open(fileout_abs, "wb")
except IOError:
        sys.exit("Could not create the output file")

# make 256bits keys for encryption and mac
kdf = PBKDF2(MyKey, MySalt, 64, 1000)
key = kdf[:32]
key_mac = kdf[32:]

# create HMAC
mac = HMAC.new(key_mac) # default is MD5


if args.encrypt:
        # encryption
        iv = os.urandom(16)
        cipher = AES.new(key, AES.MODE_CFB, iv)

        encrypted = cipher.encrypt(inputfile.read())
        mac.update(iv + encrypted)

        # output
        output.write(mac.hexdigest().encode())
        output.write(iv)
        output.write(encrypted)

else:
        # decryption
        data = inputfile.read()
        # check for MAC first
        verify = data[0:32]
        mac.update(data[32:])

        if mac.hexdigest().encode() != verify:
                sys.exit("message was modified, aborting decryption")

        iv = data[32:48]
        cipher = AES.new(key, AES.MODE_CFB, iv)

        decrypted = cipher.decrypt(data[48:])

        #output
        output.write(decrypted)

inputfile.close()
output.close()
