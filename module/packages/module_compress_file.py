def compress_file_by_type(format_compress, file_name_compress, list_files_to_compress, directory, no_password, flag_tar_compress):
    """Compression zip ou gzip d'une liste de fichier"""
    import os
    import glob
    import gzip
    import keyring
    import shutil
    import subprocess
    import tarfile
    import zipfile


    os.chdir(directory)
    
    with open(list_files_to_compress, "r") as file:
        list_file = file.read().splitlines()
    
    if format_compress == "gz":
        # gzip ne permet pas de faire une archive multi-fichier. il faut utiliser tar
        num_lines = sum(1 for line in open(file_output_result))
        if num_lines == 1:
            i = 0
            while i < len(list_files_to_compress):
                file_compress = list_file[i] + ".gz"
                if os.path.isfile(file_compress):
                    os.remove(file_compress)
                with open(list_file[i], 'rb') as f_in, gzip.open(file_compress, 'wb') as f_out:
                    shutil.copyfileobj(f_in, f_out)
                i += 1
        else:
            # création tar ou tar.gz
            file_tar = file_name_compress + ".tar"
            file_compress = file_tar + ".gz"
            if os.path.isfile(file_tar):
                os.remove(file_tar)
            if os.path.isfile(file_compress):
                os.remove(file_compress)

            if flag_tar_compress == "tar_nopath":
                t1 = tarfile.open(file_tar, 'w')
                i = 0
                while i < len(list_files_to_compress):
                    t1.add(list_files_to_compress[i])
                    i += 1
                t1.close()

            elif flag_tar_compress == "tar_path":
                t2 = tarfile.open(file_tar, 'w')
                for f in glob.glob(os.path.join(directory, "*")):
                    i = 0
                    while i < len(list_files_to_compress):
                        if os.path.basename(f) == list_files_to_compress[i]:
                            #print(f,)
                            t2.add(f, arcname=None, recursive=False)
                        i += 1
                t2.close()

            elif flag_tar_compress == "tar_gz_nopath":
                tz1 = tarfile.open(file_compress, 'w:gz')
                i = 0
                while i < len(list_files_to_compress):
                    tz1.add(list_files_to_compress[i])
                    i += 1
                tz1.close()

            elif flag_tar_compress == "tar_gz_path":
                tz2 = tarfile.open(file_compress, 'w:gz')
                for f in glob.glob(os.path.join(directory, "*")):
                    i = 0
                    while i < len(list_files_to_compress):
                        if os.path.basename(f) == list_files_to_compress[i]:
                            tz2.add(f, arcname=None, recursive=False)
                        i += 1
                tz2.close()

            else:
                temp_msg = "ERROR : flag_tar_compress {} not accepted !!!\n".format(flag_tar_compress)
                temp_msg = temp_msg + "---> Values accepted : tar_path, tar_nopath, tar_gz_path, tar_get_nopath"
                raise ValueError(temp_msg)

    elif format_compress == "zip":
        file_compress = file_name_compress + ".zip"
        if os.path.isfile(file_compress):
            os.remove(file_compress)

        if no_password == True:
            zip = zipfile.ZipFile(file_compress, "w", zipfile.ZIP_DEFLATED)
            i = 0
            while i < len(list_file):
                zip.write(list_file[i])
                i += 1
        else:
            pass_zip_file = keyring.get_password("zip-file", "zip_passwd")
            i = 0
            while i < len(list_file):
                if i == 0:
                    cmd_arch = "-r " + list_file[i]
                else:
                    cmd_arch = cmd_arch + " " + list_file[i]
                i += 1
            cmd = "zip -e -P " + pass_zip_file + " " + file_compress + " " + cmd_arch
            #print(cmd)
            subprocess.call(cmd, shell=True)

    else:
        temp_msg = ("ERROR : Compress format {} not accepted !!!\n   ---> Compress format accepted are zip or gz".format(format_compress))
        raise ValueError(temp_msg)