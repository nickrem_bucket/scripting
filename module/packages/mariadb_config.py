import os, json, subprocess
import xlsxwriter
import zipfile
import gzip, shutil
import keyring
import gnupg
#import mysql.connector as mariadb
from pprint import pprint

from configparser import ConfigParser

from nostri import DIRECTORY, DIRECTORYS, fic_param_mariadb, pgp_public_key, gpg_home_dir

#new_directory = DIRECTORYS + "\\params"
new_directory = DIRECTORYS
 
def connect(type_user, nom_connect, filename=fic_param_mariadb):
    os.chdir(new_directory)

    t = os.path.isfile(fic_param_mariadb)
    if str(t) == "True":

        with open(filename) as data_file:    
            data = json.load(data_file)
            database = data.get("database")
            if type_user == "own":
                username = data.get('user_own')
            else:
                username = data.get('user_app')
            host = data.get('host')
            port = data.get('port')
            data_file.close()


        keyring.get_keyring()
        key_database = "mariadb_" + database
        password = keyring.get_password(key_database, username)

        db = {}
        db['host'] = host
        db['database'] = database
        db['user'] = username
        db['password'] = password
        db['port'] = port
        #db = database=var_database, user=var_user_own, password=var_password_own, host=var_host, port=var_port
    else:
        raise Exception('Params File {} not found in {}'.format(fic_param_mariadb, new_directory))
    
    return db

def cmd_line(type_user, filename=fic_param_mariadb):
    os.chdir(new_directory)

    t = os.path.isfile(fic_param_mariadb)
    if str(t) == "True":

        with open(filename) as data_file:    
            data = json.load(data_file)
            database = data.get("database")
            if type_user == "own":
                username = data.get('user_own')
            else:
                username = data.get('user_app')
            host = data.get('host')
            port = data.get('port')
            data_file.close()

        keyring.get_keyring()
        key_database = "mariadb_" + database
        password = keyring.get_password(key_database, username)

        #os.environ["PGPASSWORD"] = password
        cmd = "mysql -h " + host + " -P " + port + " -u " + username + " -p" + password + " " + database
        print(cmf)
    else:
        raise Exception('Params File {} not found in {}'.format(fic_param_mariadb, new_directory))
    
    return cmd


def exec_sql(type_user, file_exec_sql, file_result_sql, file_output_sql, filename=fic_param_mariadb):
    os.chdir(new_directory)

    t = os.path.isfile(fic_param_mariadb)
    if str(t) == "True":

        with open(filename) as data_file:    
            data = json.load(data_file)
            database = data.get("database")
            if type_user == "own":
                username = data.get('user_own')
            else:
                username = data.get('user_app')
            host = data.get('host')
            port = data.get('port')
            data_file.close()

        keyring.get_keyring()
        key_database = "mariadb_" + database
        password = keyring.get_password(key_database, username)

        cmd = "mysql -h " + host + " -P " + port + " -u " + username + " -p" + password + " " + database + " < " + file_exec_sql + " > " + file_result_sql + " 2>&1 "
        subprocess.call(cmd, shell=True)
    else:
        raise Exception('Params File {} not found in {}'.format(fic_param_mariadb, new_directory))


def format_xlsx_file(file_xlsx_output, file_result_csv):

    # creation du fichier excel  
    workbook = xlsxwriter.Workbook(file_xlsx_output)
    worksheet = workbook.add_worksheet()

    # Format la ligne entete
    header_format = workbook.add_format()
    header_format.set_bold()
    header_format.set_align('center')
    #header_format.set_align('vcenter')
    header_format.set_text_wrap()
    #header_format.set_font_color('grey')
    header_format.set_border(2)
    
    # Format ligne tableau sans montant
    cell_format = workbook.add_format()
    cell_format.set_align('center')
    cell_format.set_border(1)
    
    # Format ligne tableau avec montant
    cell_amount_format = workbook.add_format()
    cell_amount_format.set_align('right')
    cell_amount_format.set_num_format('#,##0.00')
    cell_amount_format.set_border(1)
    
    # Format ligne Total
    total_format = workbook.add_format()
    total_format.set_bold()
    total_format.set_align('right') 

    # Ecriture ligne entete    
    worksheet.set_column('A:A', 15)
    worksheet.set_column('B:B', 10)
    worksheet.set_column('C:C', 10)
    worksheet.set_column('D:D', 25)
    worksheet.set_column('E:E', 15)
    worksheet.set_column('F:F', 15)
    worksheet.write('A1', 'Admission', header_format)
    worksheet.write('B1', 'Name', header_format)
    worksheet.write('C1', 'Age', header_format)
    worksheet.write('D1', 'Course', header_format)
    worksheet.write('E1', 'Department', header_format)
    worksheet.write('F1', 'Amount', header_format)

    # définition de la première cellule
    row = 1
    col = 0

    with open(file_result_csv, "r") as filin:
        line = filin.readline()
        cnt = 1
        while line:
            a = line.strip()
            data = a.split(";")
            i = 1
            for value in data:
                if i == 1:
                    worksheet.write(row, col, int(value), cell_format)
                    i += 1
                elif i == 2:
                    worksheet.write(row, col + 1, value, cell_format)
                    i += 1
                elif i == 3:
                    worksheet.write(row, col + 2, int(value), cell_format)
                    i += 1
                elif i == 4:
                    worksheet.write(row, col + 3, value, cell_format)
                    i += 1
                elif i == 5:
                    worksheet.write(row, col + 4, value, cell_format)
                    i += 1
                elif i == 6:
                    val_float = float(value.replace(',', '.'))
                    val_decim = format(val_float, '.2f')
                    worksheet.write(row, col + 5, float(val_decim), cell_amount_format)
                    i += 1
            row += 1
            line = filin.readline()
            cnt += 1
    
    # ecriture du total des montants avec une formule
    worksheet.write(row, 4, 'Total = ', total_format)
    worksheet.write(row, 5, '=SUM(F2:F7)', cell_amount_format)

    workbook.close()

def compress_file_xlsx(format_compress, file_xlsx_output, file_xlsx_compress, no_password):

    if format_compress == "gz":
        with open(file_xlsx_output, 'rb') as f_in, gzip.open(file_xlsx_compress, 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)
    
    elif format_compress == "zip":
        if no_password == "True":
            zip = zipfile.ZipFile(file_xlsx_compress, "w", zipfile.ZIP_DEFLATED)
            zip.write(file_xlsx_output, os.path.basename(file_xlsx_output))
        else:
            pass_zip_file = keyring.get_password("zip-file", "zip_passwd")
            cmd = "zip -e -P " + pass_zip_file + " " + file_xlsx_compress + " " + file_xlsx_output + " > log.txt"
            subprocess.call(cmd, shell=True)
    else:
        temp_msg = ("ERROR : Compress format {} not accepted !!!\n   ---> Compress format accepted are zip or gz".format(format_compress))
        raise ValueError(temp_msg)

def encrypt_file_xlsx(file_xlsx_output, file_xlsx_gpg, pgpkey=pgp_public_key, homedir=gpg_home_dir):

    gpg_home = homedir
    gpg = gnupg.GPG(gnupghome=gpg_home)

    # méthode pour lister les clés
    #public_keys = gpg.list_keys()
    #private_keys = gpg.list_keys(True)
    #print("public keys : ")
    #pprint(public_keys)
    #print("private keys : ")
    #pprint(private_keys)

    # Méthode pour chiffrer un fichier
    data = file_xlsx_output
    rkeys = pgpkey

    savefile = data + ".gpg"
    afile = open(data, "rb")

    encrypted_ascii_data = gpg.encrypt_file(afile, rkeys.split(), always_trust=True, output=savefile)

    afile.close()

def config2(filename='database.ini', section='mariadbql'):

    os.chdir(new_directory)
    
    # create a parser
    parser = ConfigParser()
    # read config file
    parser.read(filename)
 
    # get section, default to mariadbql
    db = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            db[param[0]] = param[1]
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))
 
    return db

