def get_param_database(fic_json_or_ini):
    """Récupère les paramètres de connexion à la base"""
    import json, keyring, os
    from configparser import ConfigParser
    from nostri import DIRECTORYS, fic_param_json, fic_param_ini

    if fic_json_or_ini == "json":
        filename = fic_param_json
    else:
        filename = fic_param_ini
    section = 'postgresql'

    os.chdir(DIRECTORYS)

    t = os.path.isfile(filename)
    
    if t == True:
        db = {}
        if fic_json_or_ini == "json":
            with open(filename, 'r') as data_file:
                data = json.load(data_file)
                db['database'] = data[section]['database']
                db['user_own'] = data[section]['user_own']
                db['user_app'] = data[section]['user_app']
                db['host'] = data[section]['host']
                db['port'] = data[section]['port']
                data_file.close()
        else:
            parser = ConfigParser()
            parser.read(filename)
            if parser.has_section(section):
                print(1)
                params = parser.items(section)
                for param in params:
                    db[param[0]] = param[1]

        key_database = "postgres_" + db['database']
        password = keyring.get_password(key_database, db['user_own'])
        db['passwd_own'] = password
        password = keyring.get_password(key_database, db['user_app'])
        db['passwd_app'] = password

        #print(db)
        #exit()

    else:
        raise ValueError('Params File {} not found in {}'.format(filename, DIRECTORYS))

    return db

def connect(type_user, directory):
    """Renvoi les paramètres de connection à la base"""
    import os
    from nostri import fic_param_json, fic_param_ini
    os.chdir(directory)

    fic_json_or_ini = "json"
    param = get_param_database(fic_json_or_ini)
    
    db = {}
    db['host'] = param['host']
    db['database'] = param['database']
    if type_user == "own":
        db['user'] = param['user_own']
        db['password'] = param['passwd_own']
        #db = database=param['database'], user=param['user_own'], password=param['passwd_own'], host=param['host'], port=param['port']
    else:
        db['user'] = param['user_app']
        db['password'] = param['passwd_app']
        #db = database=param['database'], user=param['user_app'], password=param['passwd_app'], host=param['host'], port=param['port']
    
    return db

def exec_sql(type_user, file_exec_sql, file_result_sql, file_output_sql, replace_user_app, directory):
    """Execute la ligne de commande psql avec les paramètres des fichiers de sortie"""
    import os, subprocess
    from shutil import move

    os.chdir(directory)

    fic_json_or_ini = "json"
    param = get_param_database(fic_json_or_ini)
    

    # Dans le cas ou c'est le user own qui appelle la fonction
    # le user_app n'est pas renseigné dans le fichier des commandes
    # remplacement avec le nom récupérer dans le fichier des paramètres
    if type_user == "own" and replace_user_app == True:
        file_exec_sql_bis = file_exec_sql + ".bis"
        f_in = open(file_exec_sql, "rt")
        f_out = open(file_exec_sql_bis, "wt")
        for line in f_in:
            f_out.write(line.replace('%user_app%', param['user_app']))
        f_in.close()
        f_out.close()
        move(file_exec_sql_bis, file_exec_sql)

    # Set de la variable d'environnement nécessaire à la connexion via psql
    if type_user == "own":
        os.environ["PGPASSWORD"] = param['passwd_own']
        cmd = "psql -d " + param['database'] + " -h " + param['host'] + " -p " + param['port'] + " -U " + param['user_own'] + " -t -A -o " + file_output_sql + " < " + file_exec_sql +  " 2> " + file_result_sql
    else:
        os.environ["PGPASSWORD"] = param['passwd_app']
        cmd = "psql -d " + param['database'] + " -h " + param['host'] + " -p " + param['port'] + " -U " + param['user_app'] + " -t -A -o " + file_output_sql + " < " + file_exec_sql +  " 2> " + file_result_sql

    # Exécution de la commande psql
    subprocess.call(cmd, shell=True)
    
    #return cmd

def format_xlsx_file(file_xlsx_output, file_result_csv):
    """Construit un fichier Excel à partir d'un fichier CSV"""
    import os, xlsxwriter
    
    if os.path.isfile(file_xlsx_output):
        os.remove(file_xlsx_output)

    if os.path.isfile(file_result_csv):
        
        # creation du fichier excel  
        workbook = xlsxwriter.Workbook(file_xlsx_output)
        worksheet = workbook.add_worksheet()

        # Format la ligne entete
        header_format = workbook.add_format()
        header_format.set_bold()
        header_format.set_align('center')
        #header_format.set_align('vcenter')
        header_format.set_text_wrap()
        #header_format.set_font_color('grey')
        header_format.set_border(2)
    
        # Format ligne tableau sans montant
        cell_format = workbook.add_format()
        cell_format.set_align('center')
        cell_format.set_border(1)
    
        # Format ligne tableau avec montant
        cell_amount_format = workbook.add_format()
        cell_amount_format.set_align('right')
        cell_amount_format.set_num_format('#,##0.00')
        cell_amount_format.set_border(1)
    
        # Format ligne Total
        total_format = workbook.add_format()
        total_format.set_bold()
        total_format.set_align('right') 

        # Ecriture ligne entete    
        worksheet.set_column('A:A', 15)
        worksheet.set_column('B:B', 10)
        worksheet.set_column('C:C', 10)
        worksheet.set_column('D:D', 25)
        worksheet.set_column('E:E', 15)
        worksheet.set_column('F:F', 15)
        worksheet.write('A1', 'Admission', header_format)
        worksheet.write('B1', 'Name', header_format)
        worksheet.write('C1', 'Age', header_format)
        worksheet.write('D1', 'Course', header_format)
        worksheet.write('E1', 'Department', header_format)
        worksheet.write('F1', 'Amount', header_format)

        # définition de la première cellule
        row = 1
        col = 0

        with open(file_result_csv, "r") as filin:
            line = filin.readline()
            cnt = 1
            while line:
                a = line.strip()
                data = a.split(";")
                i = 1
                for value in data:
                    if i == 1:
                        worksheet.write(row, col, int(value), cell_format)
                        i += 1
                    elif i == 2:
                        worksheet.write(row, col + 1, value, cell_format)
                        i += 1
                    elif i == 3:
                        worksheet.write(row, col + 2, int(value), cell_format)
                        i += 1
                    elif i == 4:
                        worksheet.write(row, col + 3, value, cell_format)
                        i += 1
                    elif i == 5:
                        worksheet.write(row, col + 4, value, cell_format)
                        i += 1
                    elif i == 6:
                        val_float = float(value.replace(',', '.'))
                        val_decim = format(val_float, '.2f')
                        worksheet.write(row, col + 5, float(val_decim), cell_amount_format)
                        i += 1
                row += 1
                line = filin.readline()
                cnt += 1
    
        # ecriture du total des montants avec une formule
        worksheet.write(row, 4, 'Total = ', total_format)
        worksheet.write(row, 5, '=SUM(F2:F7)', cell_amount_format)

        workbook.close()

    else:
        raise ValueError('Csv File {} not found'.format(os.path.basename(file_result_csv), os.path.dirname(file_result_csv)))

def compress_file_xlsx(format_compress, file_xlsx_output_only, list_files_to_compress, directory, no_password):
    """Compression zip ou gzip d'une liste de fichier"""
    import os, gzip, keyring, shutil, subprocess, zipfile

    os.chdir(directory)
    
    with open(list_files_to_compress, "r") as file:
        list_file = file.read().splitlines()
    
    if format_compress == "gz":
        # gzip ne permet pas de faire une archive multi-fichier. il faut utiliser tar
        i = 0
        while i < len(list_file):
            file_compress = list_file[i] + ".gz"
            if os.path.isfile(file_compress):
                os.remove(file_compress)
            with open(list_file[i], 'rb') as f_in, gzip.open(file_compress, 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)
            i += 1
    
    elif format_compress == "zip":
        file_xlsx_compress = file_xlsx_output_only + ".zip"
        if os.path.isfile(file_xlsx_compress):
            os.remove(file_xlsx_compress)
        if no_password == "True":
            zip = zipfile.ZipFile(file_xlsx_compress, "w", zipfile.ZIP_DEFLATED)
            i = 0
            while i < len(list_file):
                zip.write(list_file[i])
                i += 1
        else:
            pass_zip_file = keyring.get_password("zip-file", "zip_passwd")
            i = 0
            while i < len(list_file):
                if i == 0:
                    cmd_arch = "-r " + list_file[i]
                else:
                    cmd_arch = cmd_arch + " " + list_file[i]
                i += 1
            #cmd = "zip -e -P " + pass_zip_file + " " + file_xlsx_compress + " " + file_xlsx_output + " > log.txt"
            cmd = "zip -e -P " + pass_zip_file + " " + file_xlsx_compress + " " + cmd_arch + " > log.txt"
            #print(cmd)
            subprocess.call(cmd, shell=True)
    else:
        temp_msg = ("ERROR : Compress format {} not accepted !!!\n   ---> Compress format accepted are zip or gz".format(format_compress))
        raise ValueError(temp_msg)

def encrypt_decrypt_file_xlsx(list_file_to_compress, directory, encrypt_or_decrypt):
    """Chiffreement GPG d'une liste de fichiers"""
    import gnupg, os, keyring
    from nostri import pgp_public_key, gpg_home_dir
    
    os.chdir(directory)

    with open(list_file_to_compress, "r") as file:
        list_file = file.read().splitlines()

    gpg_home = gpg_home_dir
    gpg = gnupg.GPG(gnupghome=gpg_home)
    rkeys = pgp_public_key
    gkey = keyring.get_password("gpg-key", "gpg-passwd")

    # méthode pour lister les clés
    #public_keys = gpg.list_keys()
    #private_keys = gpg.list_keys(True)
    #print("public keys : ")
    #pprint(public_keys)
    #print("private keys : ")
    #pprint(private_keys)

    # Méthode pour chiffrer un fichier
    if encrypt_or_decrypt == "encrypt":
        i = 0
        while i < len(list_file):
            data = list_file[i]
            savefile = data + ".gpg"
            afile = open(data, 'rb')
            cmd_gpg = gpg.encrypt_file(afile, rkeys.split(), always_trust=False, sign=rkeys, passphrase=gkey, output=savefile)
            afile.close()
            if cmd_gpg.ok == False:
                temp_msg = str(afile) + "\n" + cmd_gpg.status + "\n" + cmd_gpg.stderr
                raise ValueError(temp_msg)
            i += 1
    elif encrypt_or_decrypt == "decrypt":
        
        i = 0
        while i < len(list_file):
            data = list_file[i]
            name_file, name_extention = os.path.splitext(data)
            fgpg = open(data, 'rb')
            cmd_gpg = gpg.decrypt_file(fgpg, passphrase=gkey, output=name_file)
            fgpg.close()
            if cmd_gpg.ok == False:
                temp_msg = str(fgpg) + "\n" + cmd_gpg.status + "\n" + cmd_gpg.stderr
                raise ValueError(temp_msg)
            i += 1
    else:
        temp_msg = ("ERROR : Third parameter {} not accepted !!!\n   ---> Values accepted are encryt or decrypt".format(encrypt_or_decrypt))
        raise ValueError(temp_msg)
    

def tar_file_xlsx(file_xlsx_output_only, directory_tar, list_files_to_compress):
    """Mise en container tar d'une liste de fichiers"""
    import os, glob, tarfile
    from io import BytesIO
    
    #import info
    os.chdir(directory_tar)
    file_tar = file_xlsx_output_only + ".tar"
    file_tar_with_path = file_xlsx_output_only + ".with_path.tar"
    file_tar_gz = file_tar + ".gz"
    file_tar_with_path_gz = file_tar_with_path + ".gz"
    
    if os.path.isfile(file_tar):
        os.remove(file_tar)
    if os.path.isfile(file_tar_gz):
        os.remove(file_tar_gz)
    if os.path.isfile(file_tar_with_path):
        os.remove(file_tar_with_path)
    if os.path.isfile(file_tar_with_path_gz):
        os.remove(file_tar_with_path_gz)

    with open(list_files_to_compress, "r") as file:
        list_file = file.read().splitlines()

    # fichier tar simple, sans le path des fichiers
    t1 = tarfile.open(file_tar, 'w')
    i = 0
    while i < len(list_file):
        t1.add(list_file[i])
        i += 1
    t1.close()
    
    # fichier tar simple, avec le path des fichiers
    """for root, dirs, _ in os.walk(directory_tar):
        print(root)
        print(directory_tar)
        for d in dirs:
            for _, _, files in os.walk(os.path.join(root,d)):
                for f in files:
                    print(os.path.join(root, d, f))"""

    t2 = tarfile.open(file_tar_with_path, 'w')
    for f in glob.glob(os.path.join(directory_tar, "*")):
        i = 0
        while i < len(list_file):
            if os.path.basename(f) == list_file[i]:
                #print(f,)
                t2.add(f, arcname=None, recursive=False)
            i += 1
    t2.close()
    
    # fichier tar gzip, sans le path des fichiers
    tz1 = tarfile.open(file_tar_gz, 'w:gz')
    i = 0
    while i < len(list_file):
        tz1.add(list_file[i])
        i += 1
    tz1.close()

    # fichier tar gzip, avec le path des fichiers
    tz2 = tarfile.open(file_tar_with_path_gz, 'w:gz')
    for f in glob.glob(os.path.join(directory_tar, "*")):
        i = 0
        while i < len(list_file):
            if os.path.basename(f) == list_file[i]:
                tz2.add(f, arcname=None, recursive=False)
            i += 1
    tz2.close()
   
