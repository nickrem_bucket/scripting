def create_request_cubrid(file_cmds_sql):
    import os
    from nostri import dir_sql_file
    from nostri import DIRECTORY
    from nostri import fic_create_sql_cubrid
    from db_access import get_param_database
    from cipher import get_usr_or_passwd

    param = get_param_database('ini', 'cubrid')
    label_software = 'cubrid_testdb'
    name_user_app = get_usr_or_passwd(label_software, 'user_app')
    var_grantee = name_user_app


    file_sql_request = dir_sql_file + "\\" + fic_create_sql_cubrid
    f_in = open(file_sql_request, 'rt')
    f_out = open(file_cmds_sql, 'wt')
    for line in f_in:
        f_out.write(line.replace('%user_app%', var_grantee))
    f_in.close()
    f_out.close()

    with open(file_cmds_sql, "a") as file:
        file.write("\n")
        file.write("\n")
        list_course = [('Computer Science', 500042), ('Electrical Engineering', 450000), ('Information Technology', 420000)]
        i = 0
        while i < len(list_course):
            sql_statement = ("insert into COURSE (NAME, AMOUNT) values ('{}', {});\n").format(list_course[i][0], list_course[i][1])
            file.write(sql_statement)
            i += 1
        file.write("\n")

        list_department = ["ICT", "Engineering"]
        i = 0
        while i < len(list_department):
            sql_statement = ("insert into DEPARTMENT (NAME) values ('{}');\n").format(list_department[i])
            file.write(sql_statement)
            i += 1
        file.write("\n")

        list_student = [[3420, "John", 18, "Computer Science", "ICT"],
                        [3419, "Abel", 17, "Computer Science", "ICT"],
                        [3421, "Joel", 17, "Computer Science", "ICT"],
                        [3422, "Anthony", 19, "Electrical Engineering", "Engineering"],
                        [3423, "Alice", 18, "Information Technology", "ICT"],
                        [3424, "Tom", 20, "Information Technology", "ICT"]]
        i = 0
        while i < len(list_student):
            id_adm      = list_student[i][0]
            nam_student = list_student[i][1]
            age_student = list_student[i][2]
            cou_student = list_student[i][3]
            dep_student = list_student[i][4]
            sel_cou_student = ("(select ID from COURSE where NAME = '{}')").format(cou_student)
            sel_dep_student = ("(select ID from DEPARTMENT where NAME = '{}')").format(dep_student)
            sql_statement = ("insert into STUDENT (ADMISSION, NAME, AGE, COURSE, DEPARTMENT)  values ({}, '{}', {}, {}, {});\n").format(id_adm, nam_student, age_student, sel_cou_student, sel_dep_student);
            file.write(sql_statement)
            i += 1
        file.write("\n")

def create_request_firebird(file_cmds_sql):
    import os
    from nostri import dir_sql_file
    from nostri import DIRECTORY
    from nostri import fic_create_sql_firebird
    from db_access import get_param_database
    from cipher import get_usr_or_passwd

    param = get_param_database('ini', 'firebird')
    label_software = 'firebird' + "_" + os.path.basename(param['database'])
    name_user_app = get_usr_or_passwd(label_software, 'user_app')
    var_grantee = name_user_app

    file_sql_request = dir_sql_file + "\\" + fic_create_sql_firebird
    f_in = open(file_sql_request, 'rt')
    f_out = open(file_cmds_sql, 'wt')
    for line in f_in:
        f_out.write(line.replace('%user_app%', var_grantee))
    f_in.close()
    f_out.close()

    with open(file_cmds_sql, "a") as file:
        file.write("\n")
        file.write("\n")
        list_course = [('Computer Science', 500042), ('Electrical Engineering', 450000), ('Information Technology', 420000)]
        i = 0
        while i < len(list_course):
            sql_statement = ("insert into COURSE (NAME, AMOUNT) values ('{}', {});\n").format(list_course[i][0], list_course[i][1])
            file.write(sql_statement)
            i += 1
        file.write("\n")

        list_department = ["ICT", "Engineering"]
        i = 0
        while i < len(list_department):
            sql_statement = ("insert into DEPARTMENT (NAME) values ('{}');\n").format(list_department[i])
            file.write(sql_statement)
            i += 1
        file.write("\n")

        list_student = [[3420, "John", 18, "Computer Science", "ICT"],
                        [3419, "Abel", 17, "Computer Science", "ICT"],
                        [3421, "Joel", 17, "Computer Science", "ICT"],
                        [3422, "Anthony", 19, "Electrical Engineering", "Engineering"],
                        [3423, "Alice", 18, "Information Technology", "ICT"],
                        [3424, "Tom", 20, "Information Technology", "ICT"]]
        i = 0
        while i < len(list_student):
            id_adm      = list_student[i][0]
            nam_student = list_student[i][1]
            age_student = list_student[i][2]
            cou_student = list_student[i][3]
            dep_student = list_student[i][4]
            sel_cou_student = ("(select ID from COURSE where NAME = '{}')").format(cou_student)
            sel_dep_student = ("(select ID from DEPARTMENT where NAME = '{}')").format(dep_student)
            sql_statement = ("insert into STUDENT (ADMISSION, NAME, AGE, COURSE, DEPARTMENT)  values ({}, '{}', {}, {}, {});\n").format(id_adm, nam_student, age_student, sel_cou_student, sel_dep_student);
            file.write(sql_statement)
            i += 1
        file.write("\n")

def create_request_mariadb(file_cmds_sql):
    from nostri import dir_sql_file
    from nostri import DIRECTORY
    from nostri import fic_create_sql_mariadb
    from db_access import get_param_database
    from cipher import get_usr_or_passwd

    param = get_param_database('json', 'mariadb')
    label_software = 'mariadb' + "_" + param['database']
    name_user_app = get_usr_or_passwd(label_software, 'user_app')
    var_grantee = name_user_app

    file_sql_request = dir_sql_file + "\\" + fic_create_sql_mariadb
    f_in = open(file_sql_request, 'rt')
    f_out = open(file_cmds_sql, 'wt')
    for line in f_in:
        f_out.write(line.replace('%user_app%', var_grantee))
    f_in.close()
    f_out.close()

    with open(file_cmds_sql, "a") as file:
        file.write("\n")
        file.write("\n")
        list_course = [('Computer Science', 500042), ('Electrical Engineering', 450000), ('Information Technology', 420000)]
        i = 0
        while i < len(list_course):
            sql_statement = ("insert into COURSE (NAME, AMOUNT) values ('{}', {});\n").format(list_course[i][0], list_course[i][1])
            file.write(sql_statement)
            i += 1
        file.write("\n")

        list_department = ["ICT", "Engineering"]
        i = 0
        while i < len(list_department):
            sql_statement = ("insert into DEPARTMENT (NAME) values ('{}');\n").format(list_department[i])
            file.write(sql_statement)
            i += 1
        file.write("\n")

        list_student = [[3420, "John", 18, "Computer Science", "ICT"],
                        [3419, "Abel", 17, "Computer Science", "ICT"],
                        [3421, "Joel", 17, "Computer Science", "ICT"],
                        [3422, "Anthony", 19, "Electrical Engineering", "Engineering"],
                        [3423, "Alice", 18, "Information Technology", "ICT"],
                        [3424, "Tom", 20, "Information Technology", "ICT"]]
        i = 0
        while i < len(list_student):
            id_adm      = list_student[i][0]
            nam_student = list_student[i][1]
            age_student = list_student[i][2]
            cou_student = list_student[i][3]
            dep_student = list_student[i][4]
            sel_cou_student = ("(select ID from COURSE where NAME = '{}')").format(cou_student)
            sel_dep_student = ("(select ID from DEPARTMENT where NAME = '{}')").format(dep_student)
            sql_statement = ("insert into STUDENT (ADMISSION, NAME, AGE, COURSE, DEPARTMENT)  values ({}, '{}', {}, {}, {});\n").format(id_adm, nam_student, age_student, sel_cou_student, sel_dep_student);
            file.write(sql_statement)
            i += 1
        file.write("\n")


def create_request_mysql(file_cmds_sql):
    from nostri import dir_sql_file
    from nostri import DIRECTORY
    from nostri import fic_create_sql_mysql
    from db_access import get_param_database
    from cipher import get_usr_or_passwd

    param = get_param_database('json', 'mysql')
    label_software = 'mysql' + "_" + param['database']
    name_user_app = get_usr_or_passwd(label_software, 'user_app')
    var_grantee = "\'" + name_user_app + "\'@\'" + param['host'] + "\'"

    file_sql_request = dir_sql_file + "\\" + fic_create_sql_mysql
    f_in = open(file_sql_request, 'rt')
    f_out = open(file_cmds_sql, 'wt')
    for line in f_in:
        f_out.write(line.replace('%user_app%', var_grantee))
    f_in.close()
    f_out.close()

    with open(file_cmds_sql, "a") as file:
        file.write("\n")
        file.write("\n")
        list_course = [('Computer Science', 500042), ('Electrical Engineering', 450000), ('Information Technology', 420000)]
        i = 0
        while i < len(list_course):
            sql_statement = ("insert into COURSE (NAME, AMOUNT) values ('{}', {});\n").format(list_course[i][0], list_course[i][1])
            file.write(sql_statement)
            i += 1
        file.write("\n")

        list_department = ["ICT", "Engineering"]
        i = 0
        while i < len(list_department):
            sql_statement = ("insert into DEPARTMENT (NAME) values ('{}');\n").format(list_department[i])
            file.write(sql_statement)
            i += 1
        file.write("\n")

        list_student = [[3420, "John", 18, "Computer Science", "ICT"],
                        [3419, "Abel", 17, "Computer Science", "ICT"],
                        [3421, "Joel", 17, "Computer Science", "ICT"],
                        [3422, "Anthony", 19, "Electrical Engineering", "Engineering"],
                        [3423, "Alice", 18, "Information Technology", "ICT"],
                        [3424, "Tom", 20, "Information Technology", "ICT"]]
        i = 0
        while i < len(list_student):
            id_adm      = list_student[i][0]
            nam_student = list_student[i][1]
            age_student = list_student[i][2]
            cou_student = list_student[i][3]
            dep_student = list_student[i][4]
            sel_cou_student = ("(select ID from COURSE where NAME = '{}')").format(cou_student)
            sel_dep_student = ("(select ID from DEPARTMENT where NAME = '{}')").format(dep_student)
            sql_statement = ("insert into STUDENT (ADMISSION, NAME, AGE, COURSE, DEPARTMENT)  values ({}, '{}', {}, {}, {});\n").format(id_adm, nam_student, age_student, sel_cou_student, sel_dep_student);
            file.write(sql_statement)
            i += 1
        file.write("\n")


def create_request_oracle(file_cmds_sql):
    from nostri import dir_sql_file
    from nostri import DIRECTORY
    from nostri import fic_create_sql_oracle
    from db_access import get_param_database
    from cipher import get_usr_or_passwd

    param = get_param_database('json', 'oracle')
    label_software = 'oracle' + "_" + param['database']
    name_user_app = get_usr_or_passwd(label_software, 'user_app')
    var_grantee = name_user_app

    file_sql_request = dir_sql_file + "\\" + fic_create_sql_oracle
    f_in = open(file_sql_request, 'rt')
    f_out = open(file_cmds_sql, 'wt')
    for line in f_in:
        f_out.write(line.replace('%user_app%', var_grantee))
    f_in.close()
    f_out.close()

    with open(file_cmds_sql, "a") as file:
        file.write("\n")
        file.write("\n")
        list_course = [('Computer Science', 500042), ('Electrical Engineering', 450000), ('Information Technology', 420000)]
        i = 0
        while i < len(list_course):
            sql_statement = ("insert into COURSE (NAME, AMOUNT) values ('{}', {});\n").format(list_course[i][0], list_course[i][1])
            file.write(sql_statement)
            i += 1
        file.write("\n")

        list_department = ["ICT", "Engineering"]
        i = 0
        while i < len(list_department):
            sql_statement = ("insert into DEPARTMENT (NAME) values ('{}');\n").format(list_department[i])
            file.write(sql_statement)
            i += 1
        file.write("\n")

        list_student = [[3420, "John", 18, "Computer Science", "ICT"],
                        [3419, "Abel", 17, "Computer Science", "ICT"],
                        [3421, "Joel", 17, "Computer Science", "ICT"],
                        [3422, "Anthony", 19, "Electrical Engineering", "Engineering"],
                        [3423, "Alice", 18, "Information Technology", "ICT"],
                        [3424, "Tom", 20, "Information Technology", "ICT"]]
        i = 0
        while i < len(list_student):
            id_adm      = list_student[i][0]
            nam_student = list_student[i][1]
            age_student = list_student[i][2]
            cou_student = list_student[i][3]
            dep_student = list_student[i][4]
            sel_cou_student = ("(select ID from COURSE where NAME = '{}')").format(cou_student)
            sel_dep_student = ("(select ID from DEPARTMENT where NAME = '{}')").format(dep_student)
            sql_statement = ("insert into STUDENT (ADMISSION, NAME, AGE, COURSE, DEPARTMENT)  values ({}, '{}', {}, {}, {});\n").format(id_adm, nam_student, age_student, sel_cou_student, sel_dep_student);
            file.write(sql_statement)
            i += 1
        file.write("\n")


def create_request_oracle_app(file_cmds_sql):
    from nostri import dir_sql_file
    from nostri import DIRECTORY
    from nostri import fic_create_syn_oracle
    from db_access import get_param_database
    from cipher import get_usr_or_passwd

    param = get_param_database('json', 'oracle')
    label_software = 'oracle' + "_" + param['database']
    name_user_app = get_usr_or_passwd(label_software, 'user_own')
    var_grantee = name_user_app

    file_sql_request = dir_sql_file + "\\" + fic_create_syn_oracle
    f_in = open(file_sql_request, 'rt')
    f_out = open(file_cmds_sql, 'wt')
    for line in f_in:
        f_out.write(line.replace('%user_own%', var_grantee))
    f_in.close()
    f_out.close()


def create_request_postgresql(file_cmds_sql):
    from nostri import dir_sql_file
    from nostri import DIRECTORY
    from nostri import fic_create_sql_postgresql
    from db_access import get_param_database
    from cipher import get_usr_or_passwd

    param = get_param_database('json', 'oracle')
    label_software = 'oracle' + "_" + param['database']
    name_user_app = get_usr_or_passwd(label_software, 'user_app')
    var_grantee = name_user_app

    file_sql_request = dir_sql_file + "\\" + fic_create_sql_postgresql
    f_in = open(file_sql_request, 'rt')
    f_out = open(file_cmds_sql, 'wt')
    for line in f_in:
        f_out.write(line.replace('%user_app%', var_grantee))
    f_in.close()
    f_out.close()

    with open(file_cmds_sql, "a") as file:
        file.write("\n")
        file.write("\n")
        list_course = [('Computer Science', 500042), ('Electrical Engineering', 450000), ('Information Technology', 420000)]
        i = 0
        while i < len(list_course):
            sql_statement = ("insert into COURSE (NAME, AMOUNT) values ('{}', {});\n").format(list_course[i][0], list_course[i][1])
            file.write(sql_statement)
            i += 1
        file.write("\n")

        list_department = ["ICT", "Engineering"]
        i = 0
        while i < len(list_department):
            sql_statement = ("insert into DEPARTMENT (NAME) values ('{}');\n").format(list_department[i])
            file.write(sql_statement)
            i += 1
        file.write("\n")

        list_student = [[3420, "John", 18, "Computer Science", "ICT"],
                        [3419, "Abel", 17, "Computer Science", "ICT"],
                        [3421, "Joel", 17, "Computer Science", "ICT"],
                        [3422, "Anthony", 19, "Electrical Engineering", "Engineering"],
                        [3423, "Alice", 18, "Information Technology", "ICT"],
                        [3424, "Tom", 20, "Information Technology", "ICT"]]
        i = 0
        while i < len(list_student):
            id_adm      = list_student[i][0]
            nam_student = list_student[i][1]
            age_student = list_student[i][2]
            cou_student = list_student[i][3]
            dep_student = list_student[i][4]
            sel_cou_student = ("(select ID from COURSE where NAME = '{}')").format(cou_student)
            sel_dep_student = ("(select ID from DEPARTMENT where NAME = '{}')").format(dep_student)
            sql_statement = ("insert into STUDENT (ADMISSION, NAME, AGE, COURSE, DEPARTMENT)  values ({}, '{}', {}, {}, {});\n").format(id_adm, nam_student, age_student, sel_cou_student, sel_dep_student);
            file.write(sql_statement)
            i += 1
        file.write("\n")

