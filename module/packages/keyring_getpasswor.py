def get_password():
	import keyring
	
	label = 'oracle_XE'
	type = 'wallet_own'
	value = keyring.get_password('oracle_XE', 'wallet_own')
	
	return value