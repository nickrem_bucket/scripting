def format_xlsx_file(file_xlsx_output, file_result_csv):
    """Construit un fichier Excel à partir d'un fichier CSV"""
    import os, xlsxwriter
    
    if os.path.isfile(file_xlsx_output):
        os.remove(file_xlsx_output)

    if os.path.isfile(file_result_csv):
        
        # creation du fichier excel  
        workbook = xlsxwriter.Workbook(file_xlsx_output)
        worksheet = workbook.add_worksheet()

        # Format la ligne entete
        header_format = workbook.add_format()
        header_format.set_bold()
        header_format.set_align('center')
        #header_format.set_align('vcenter')
        header_format.set_text_wrap()
        #header_format.set_font_color('grey')
        header_format.set_border(2)
    
        # Format ligne tableau sans montant
        cell_format = workbook.add_format()
        cell_format.set_align('center')
        cell_format.set_border(1)
    
        # Format ligne tableau avec montant
        cell_amount_format = workbook.add_format()
        cell_amount_format.set_align('right')
        cell_amount_format.set_num_format('#,##0.00')
        cell_amount_format.set_border(1)
    
        # Format ligne Total
        total_format = workbook.add_format()
        total_format.set_bold()
        total_format.set_align('right') 

        # Ecriture ligne entete    
        worksheet.set_column('A:A', 15)
        worksheet.set_column('B:B', 10)
        worksheet.set_column('C:C', 10)
        worksheet.set_column('D:D', 25)
        worksheet.set_column('E:E', 15)
        worksheet.set_column('F:F', 15)
        worksheet.write('A1', 'Admission', header_format)
        worksheet.write('B1', 'Name', header_format)
        worksheet.write('C1', 'Age', header_format)
        worksheet.write('D1', 'Course', header_format)
        worksheet.write('E1', 'Department', header_format)
        worksheet.write('F1', 'Amount', header_format)

        # définition de la première cellule
        row = 1
        col = 0

        with open(file_result_csv, "r") as filin:
            line = filin.readline()
            cnt = 1
            while line:
                a = line.strip()
                data = a.split(";")
                i = 1
                for value in data:
                    if i == 1:
                        worksheet.write(row, col, int(value), cell_format)
                        i += 1
                    elif i == 2:
                        worksheet.write(row, col + 1, value, cell_format)
                        i += 1
                    elif i == 3:
                        worksheet.write(row, col + 2, int(value), cell_format)
                        i += 1
                    elif i == 4:
                        worksheet.write(row, col + 3, value, cell_format)
                        i += 1
                    elif i == 5:
                        worksheet.write(row, col + 4, value, cell_format)
                        i += 1
                    elif i == 6:
                        val_float = float(value.replace(',', '.'))
                        val_decim = format(val_float, '.2f')
                        worksheet.write(row, col + 5, float(val_decim), cell_amount_format)
                        i += 1
                row += 1
                line = filin.readline()
                cnt += 1
    
        # ecriture du total des montants avec une formule
        worksheet.write(row, 4, 'Total = ', total_format)
        worksheet.write(row, 5, '=SUM(F2:F7)', cell_amount_format)

        workbook.close()

    else:
        raise ValueError('Csv File {} not found'.format(os.path.basename(file_result_csv), os.path.dirname(file_result_csv)))