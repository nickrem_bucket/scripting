def __main__():
    var = True
    return var

def get_param_database(fic_json_or_xml_or_ini, section):
    # Récupèration des paramètres de connexion à la base
    import keyring
    import os
    import transpositionDecrypt
    from nostri import DIRECTORYS
    from nostri import fic_param_json, fic_param_ini, fic_param_xml, fic_param_yaml

    os.chdir(DIRECTORYS)

    #fic_json_or_xml_or_ini = "yaml"
    if fic_json_or_xml_or_ini == "json":
        filename = "database.json"
        outputFilename = "database.decrypt.json"
    elif fic_json_or_xml_or_ini == "ini":
        filename = "database.ini"
        outputFilename = "database.decrypt.ini"
    elif fic_json_or_xml_or_ini == "xml":
        filename = "database.xml"
        outputFilename = "database.decrypt.xml"
    elif fic_json_or_xml_or_ini == "py":
        filename = "database.py"
        outputFilename = "database.decrypt.py"
    else:
        filename = "database.yaml"
        outputFilename = "database.decrypt.yaml"

    t = os.path.isfile(filename)
    
    if t == True:
        myKey = 50
        with open(filename, "r") as fileObj, open(outputFilename, "w") as outputFileObj:
            content = fileObj.read()
            outputFileObj.write(transpositionDecrypt.decryptMessage(myKey, content))

        db = {}
        if fic_json_or_xml_or_ini == "json":
            import json
            #with open(filename, 'r') as json_file:
            with open(outputFilename, "r") as json_file: 
                data = json.load(json_file)
                db['database'] = data[section]['database']
                db['user_own'] = data[section]['user_own']
                db['user_app'] = data[section]['user_app']
                db['host'] = data[section]['host']
                db['port'] = data[section]['port']
                json_file.close()
            
        elif fic_json_or_xml_or_ini == "xml":
            import xml.etree.ElementTree as ET
            #tree = ET.parse(filename)
            tree = ET.parse(outputFilename)
            root = tree.getroot()
            pattern = section + "/"
            obj = tree.findall(pattern)
            for item in obj:
                tag = item.tag
                val = item.text
                db[str(tag)] = str(val)

        elif fic_json_or_xml_or_ini == "ini":
            from configparser import ConfigParser
            parser = ConfigParser()
            #parser.read(filename)
            parser.read(outputFilename)
            if parser.has_section(section):
                params = parser.items(section)
                for param in params:
                    db[param[0]] = param[1]

        elif fic_json_or_xml_or_ini == "py":
            import database
            db['database'] = database.__dict__[section]['database']
            db['host'] = database.__dict__[section]['host']
            db['port'] = database.__dict__[section]['port']
            db['user_own'] = database.__dict__[section]['user_own']
            db['user_app'] = database.__dict__[section]['user_app']

        else:
            import yaml
            in_data_label = False
            #with open(filename, 'r') as yml_file:
            with open(outputFilename, 'r') as yml_file:
                data = yaml.load(yml_file, Loader=yaml.FullLoader)
                for label in data:
                    if label == section:
                        in_data_label = True
            if in_data_label == True:
                db = data[section]
            else:
                temp_msg = "The software {} is not found on the yaml file parameter".format(software)
                raise ValueError(temp_msg)

        if os.path.isfile(outputFilename):
            os.remove(outputFilename)

        print(db)
        exit()

    else:
        raise ValueError('Params File {} not found in {}'.format(filename, DIRECTORYS))

    return db

def get_script_author():
    # Recuperation :
    #    - liste des scripts autorisés pour exécuter du sql avec le user own
    #    - liste des scripts autorisés pour exécuter du sql avec le user app
    from nostri import DIRECTORYS
    from nostri import fic_param_scripts

    fic_scripts_author = DIRECTORYS + "\\" + fic_param_scripts
    list_scripts_superuser = []
    list_scripts_normaluser = []

    with open(fic_scripts_author, "r") as file:
        list_file = file.read().splitlines()

    i = 0
    while i < len(list_file):
        type, name = list_file[i].split('=')
        if type.lower() == "superuser":
            list_scripts_superuser.append(name)
        elif type.lower() == "normaluser":
            list_scripts_normaluser.append(name)
        i += 1

    return list_scripts_superuser, list_scripts_normaluser
                

def connect(type_user, section):
    # Renvoi les paramètres de connection à la base
    import cipher
    import inspect
    import os
    from nostri import sections_database
    #from nostri import DIRECTORYS
    #directory = DIRECTORYS
    list_sections = sections_database.split(';')
    name_caller = inspect.stack()[1][1]

    list_superuser, list_normaluser = get_script_author()

    #os.chdir(directory)

    if type_user == "own":
        if section in list_sections:
            name_script = section + "_superuser.py"
            if name_caller not in list_superuser:
                temp_msg = "You don't have the rights to execute some sql requests with the user own of database"
                raise ValueError(temp_msg)
        else:
            temp_msg = ("Wrong name of section type database {} !\n     ---> Values accepted are : mariadb, mysql, oracle or postgresql".format(section))
            raise ValueError(temp_msg)
    elif type_user == "app":
        if name_caller not in list_normaluser:
            temp_msg = "You don't have the rights to execute some sql requests with the user app of the database"
            raise ValueError(temp_msg)
    else:
        temp_msg = "Wrong name of the user type for database access !!!"
        raise ValueError(temp_msg)

    fic_json_or_xml_or_ini = "xml"
    param = get_param_database(fic_json_or_xml_or_ini, section)
    
    db = {}
    if section == "oracle":
        if type_user == "own":
            label_parm_user = "wallet_own"
        else:
            label_parm_user = "wallet_app"
        label_section = software + "_" + param['database']
        db['wallet'] = "/@" + get_usr_or_passwd(label_section, label_parm_user)
    elif section == "cubrid" or section == "firebird" or section == "mariadb" or section == "mysql" or section == "postgresql":
        db['host'] = param['host']
        db['database'] = param['database']
        label_section = software + "_" + param['database']
        if type_user == "own":
            db['user'] = param['user_own']
            db['password'] = cipher.get_usr_or_passwd(label_section, 'passwd_own')
        else:
            db['user'] = param['user_app']
            db['password'] = cipher.get_usr_or_passwd(label_section, 'passwd_app')
    else:
        temp_msg = ("Wrong name of section type database {} !\n     ---> Values accepted are : mariadb, mysql, oracle or postgresql".format(section))
        raise ValueError(temp_msg)

    return db

def get_user_app(section, type, user):
    # Recuperer le user applicatif en fonction du logiciel
    import inspect
    import os
    import sys
    from cipher import get_usr_or_passwd
    from nostri import sections_database
    list_sections = sections_database.split(';')
    name_caller = inspect.stack()[1][1]

    list_superuser, list_normaluser = get_script_author()

    if name_caller not in list_normaluser:
        temp_msg = "You don't have the rights to execute some sql requests with the user app of the database"
        raise ValueError(temp_msg)
    else:
        fic_json_or_xml_or_ini = "json"
        param = get_param_database(fic_json_or_xml_or_ini, section)
        label_section = software + "_" + param['database']
        name_user_app = get_usr_or_passwd(label_section, 'user_app')
     
    return name_user_app, param['host']
    
    
    
def exec_sql(type_user, file_exec_sql, file_result_sql, file_output_sql, replace_user_app, directory, section):
    # Execute la ligne de commande psql avec les paramètres des fichiers de sortie
    import cipher
    import inspect
    import os
    import subprocess
    import sys
    from shutil import move

    from nostri import sections_database
    from nostri import DIRECTORYS
    list_sections = sections_database.split(';')
    name_caller = inspect.stack()[1][1]

    list_superuser, list_normaluser = get_script_author()
    
    os.chdir(directory)
    if type_user == "own":
        if section in list_sections:
            if name_caller not in list_superuser:
                temp_msg = "You don't have the rights to execute some sql requests with the user own of the database"
                raise ValueError(temp_msg)
        else:
            temp_msg = ("Wrong name of section type database {} !\n     ---> Values accepted are : mariadb, mysql, oracle or postgresql".format(section))
            raise ValueError(temp_msg)
    elif type_user == "app":
        if not replace_user_app == "maj_app_with_own":
            if name_caller not in list_normaluser:
                temp_msg = "You don't have the rights to execute some sql requests with the user app of the database"
                raise ValueError(temp_msg)
    else:
        temp_msg = "Wrong name of the user type for database access !!!"
        raise ValueError(temp_msg)

    fic_json_or_xml_or_ini = "json"
    param = get_param_database(fic_json_or_xml_or_ini, section)


    if section == "cubrid":
        cubrid_home = os.environ['CUBRID']
        if type_user == "own":
            label_parm_user = "user_own"
            label_parm_passwd = "passwd_own"
        else:
            label_parm_user = "user_app"
            label_parm_passwd = "passwd_app"
        label_section = section + "_" + param['database']

        user_param = cipher.get_usr_or_passwd(label_section, label_parm_user)
        passwd_param = cipher.get_usr_or_passwd(label_section, label_parm_passwd)

        cmd = ("\"{}\\bin\\csql\" --user={} --password={} {}@{} --input={} --output={} -N -t > {} 2>&1"
               .format(  cubrid_home
                       , user_param
                       , passwd_param
                       , param['database']
                       , param['host']
                       , file_exec_sql
                       , file_output_sql
                       , file_result_sql
                      )
              )

        #print(cmd)
        #exit()

        # Exécution de la commande mysql
        subprocess.call(cmd, shell=True)

    elif section == "firebird":
        firebird_home = os.environ['FIREBIRD_HOME']
        if type_user == "own":
            label_parm_user = "user_own"
            label_parm_passwd = "passwd_own"
        else:
            label_parm_user = "user_app"
            label_parm_passwd = "passwd_app"
        label_section = section + "_" + os.path.basename(param['database'])

        user_param = cipher.get_usr_or_passwd(label_section, label_parm_user)
        passwd_param = cipher.get_usr_or_passwd(label_section, label_parm_passwd)

        cmd = ("\"{}\\isql\" -user {} -password {} \"{}/{}:{}\" -i {} > {} 2>&1"
               .format(  firebird_home
                       , user_param
                       , passwd_param
                       , param['host']
                       , param['port']
                       , param['database']
                       , file_exec_sql
                       , file_output_sql )
              )

        #print(cmd)
        #exit()

        # Exécution de la commande mysql
        subprocess.call(cmd, shell=True)

    elif section == "mariadb":
        mariadb_home = os.environ['MARIADB_HOME']
        if type_user == "own":
            label_parm_user = "user_own"
            label_parm_passwd = "passwd_own"
        else:
            label_parm_user = "user_app"
            label_parm_passwd = "passwd_app"
        label_section = section + "_" + param['database']

        user_param = cipher.get_usr_or_passwd(label_section, label_parm_user)
        passwd_param = cipher.get_usr_or_passwd(label_section, label_parm_passwd)

        from datetime import datetime
        date_file = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        file_user_config = DIRECTORYS + "\\mariadb_" + user_param + "_" + date_file + "_script.cnf"
        with open(file_user_config, 'w') as file:
            file.write("[client]\n")
            line_statement = "user = {}\n".format(user_param)
            file.write(line_statement)
            line_statement = "password = {}\n".format(passwd_param)
            file.write(line_statement)

        cmd = ("\"{}\\bin\\mysql\" --defaults-extra-file={} -h {} -P {} {} < {} > {} 2>&1"
               .format(  mariadb_home
                       , file_user_config
                       , param['host']
                       , param['port']
                       , param['database']
                       , file_exec_sql
                       , file_result_sql )
              )

        #print(cmd)
        #exit()

        # Exécution de la commande mysql
        subprocess.call(cmd, shell=True)

        if os.path.isfile(file_user_config):
            os.remove(file_user_config)


    elif section == "mysql":
        mysql_home = os.environ['MYSQL_HOME']
        if type_user == "own":
            label_parm_user = "user_own"
            label_parm_passwd = "passwd_own"
        else:
            label_parm_user = "user_app"
            label_parm_passwd = "passwd_app"
        label_section = section + "_" + param['database']

        user_param = cipher.get_usr_or_passwd(label_section, label_parm_user)
        passwd_param = cipher.get_usr_or_passwd(label_section, label_parm_passwd)

        from datetime import datetime
        date_file = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        file_user_config = DIRECTORYS + "\\mysql_" + user_param + "_" + date_file + "_script.cnf"
        with open(file_user_config, 'w') as file:
            file.write("[client]\n")
            line_statement = "user = {}\n".format(user_param)
            file.write(line_statement)
            line_statement = "password = {}\n".format(passwd_param)
            file.write(line_statement)

        cmd = ("\"{}\\bin\\mysql\" --defaults-extra-file={} -h {} -P {} {} < {} > {} 2>&1"
               .format(  mysql_home
                       , file_user_config
                       , param['host']
                       , param['port']
                       , param['database']
                       , file_exec_sql
                       , file_result_sql )
              )
        
        #print(cmd)
        #exit()

        # Exécution de la commande mysql
        subprocess.call(cmd, shell=True)

        if os.path.isfile(file_user_config):
            os.remove(file_user_config)
        

    elif section == "oracle":
        oracle_home = os.environ['ORACLE_HOME']
        if type_user == "own":
            label_parm_user = "wallet_own"
        else:
            label_parm_user = "wallet_app"
        label_section = section + "_" + param['database']
        LOCATION = r"D:\oracle\product\18.0.0\dbhomeXE\network\admin"
        os.environ['TNS_ADMIN'] = LOCATION
        cmd = ("echo @{} | \"{}\\bin\sqlplus\" {} > {}"
               .format(  file_exec_sql
                       , oracle_home
                       , "/@{}".format(cipher.get_usr_or_passwd(label_section, label_parm_user))
                       , file_result_sql )
              )

        #print(cmd)
        #exit()

        # Exécution de la commande sqlplus
        subprocess.call(cmd, shell=True)


    elif section == "postgresql":
        postgresql_home = os.environ['POSTGRESQL_HOME']
        
        if type_user == "own":
            label_parm_user = "user_own"
            label_parm_passwd = "passwd_own"
        else:
            label_parm_user = "user_app"
            label_parm_passwd = "passwd_app"
        label_section = section + "_" + param['database']
        os.environ["PGPASSWORD"] = cipher.get_usr_or_passwd(label_section, label_parm_passwd)
        cmd = ("\"{}\\bin\psql\" -d {} -h {} -p {} -U {} -t -A -o {} < {} 2> {}"
               .format(  postgresql_home
                       , param['database']
                       , param['host']
                       , param['port']
                       , cipher.get_usr_or_passwd(label_section, label_parm_user)
                       , file_output_sql
                       , file_exec_sql
                       , file_result_sql )
              )

        #print(cmd)
        #exit()

        # Exécution de la commande psql
        subprocess.call(cmd, shell=True)

    else:
        temp_msg = ("Wrong name of section type database {} !\n     ---> Values accepted are : mariadb, mysql, oracle or postgresql".format(section))
        raise ValueError(temp_msg)

    #exit()
    

def format_xlsx_file(file_xlsx_output, file_result_csv):
    """Construit un fichier Excel à partir d'un fichier CSV"""
    import os, xlsxwriter
    
    if os.path.isfile(file_xlsx_output):
        os.remove(file_xlsx_output)

    if os.path.isfile(file_result_csv):
        
        # creation du fichier excel  
        workbook = xlsxwriter.Workbook(file_xlsx_output)
        worksheet = workbook.add_worksheet()

        # Format la ligne entete
        header_format = workbook.add_format()
        header_format.set_bold()
        header_format.set_align('center')
        #header_format.set_align('vcenter')
        header_format.set_text_wrap()
        #header_format.set_font_color('grey')
        header_format.set_border(2)
    
        # Format ligne tableau sans montant
        cell_format = workbook.add_format()
        cell_format.set_align('center')
        cell_format.set_border(1)
    
        # Format ligne tableau avec montant
        cell_amount_format = workbook.add_format()
        cell_amount_format.set_align('right')
        cell_amount_format.set_num_format('#,##0.00')
        cell_amount_format.set_border(1)
    
        # Format ligne Total
        total_format = workbook.add_format()
        total_format.set_bold()
        total_format.set_align('right') 

        # Ecriture ligne entete    
        worksheet.set_column('A:A', 15)
        worksheet.set_column('B:B', 10)
        worksheet.set_column('C:C', 10)
        worksheet.set_column('D:D', 25)
        worksheet.set_column('E:E', 15)
        worksheet.set_column('F:F', 15)
        worksheet.write('A1', 'Admission', header_format)
        worksheet.write('B1', 'Name', header_format)
        worksheet.write('C1', 'Age', header_format)
        worksheet.write('D1', 'Course', header_format)
        worksheet.write('E1', 'Department', header_format)
        worksheet.write('F1', 'Amount', header_format)

        # définition de la première cellule
        row = 1
        col = 0

        with open(file_result_csv, "r") as filin:
            line = filin.readline()
            cnt = 1
            while line:
                a = line.strip()
                data = a.split(";")
                i = 1
                for value in data:
                    if i == 1:
                        worksheet.write(row, col, int(value), cell_format)
                        i += 1
                    elif i == 2:
                        worksheet.write(row, col + 1, value, cell_format)
                        i += 1
                    elif i == 3:
                        worksheet.write(row, col + 2, int(value), cell_format)
                        i += 1
                    elif i == 4:
                        worksheet.write(row, col + 3, value, cell_format)
                        i += 1
                    elif i == 5:
                        worksheet.write(row, col + 4, value, cell_format)
                        i += 1
                    elif i == 6:
                        val_float = float(value.replace(',', '.'))
                        val_decim = format(val_float, '.2f')
                        worksheet.write(row, col + 5, float(val_decim), cell_amount_format)
                        i += 1
                row += 1
                line = filin.readline()
                cnt += 1
    
        # ecriture du total des montants avec une formule
        worksheet.write(row, 4, 'Total = ', total_format)
        worksheet.write(row, 5, '=SUM(F2:F7)', cell_amount_format)

        workbook.close()

    else:
        raise ValueError('Csv File {} not found'.format(os.path.basename(file_result_csv), os.path.dirname(file_result_csv)))

def compress_file_xlsx(format_compress, file_xlsx_output_only, list_files_to_compress, directory, no_password):
    # Compression zip ou gzip d'une liste de fichier
    import os, gzip, keyring, shutil, subprocess, zipfile

    os.chdir(directory)
    
    with open(list_files_to_compress, "r") as file:
        list_file = file.read().splitlines()
    
    if format_compress == "gz":
        # gzip ne permet pas de faire une archive multi-fichier. il faut utiliser tar
        i = 0
        while i < len(list_file):
            file_compress = list_file[i] + ".gz"
            if os.path.isfile(file_compress):
                os.remove(file_compress)
            with open(list_file[i], 'rb') as f_in, gzip.open(file_compress, 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)
            i += 1
    
    elif format_compress == "zip":
        file_xlsx_compress = file_xlsx_output_only + ".zip"
        if os.path.isfile(file_xlsx_compress):
            os.remove(file_xlsx_compress)
        if no_password == "True":
            zip = zipfile.ZipFile(file_xlsx_compress, "w", zipfile.ZIP_DEFLATED)
            i = 0
            while i < len(list_file):
                zip.write(list_file[i])
                i += 1
        else:
            pass_zip_file = keyring.get_password("zip-file", "zip_passwd")
            i = 0
            while i < len(list_file):
                if i == 0:
                    cmd_arch = "-r " + list_file[i]
                else:
                    cmd_arch = cmd_arch + " " + list_file[i]
                i += 1
            #cmd = "zip -e -P " + pass_zip_file + " " + file_xlsx_compress + " " + file_xlsx_output + " > log.txt"
            cmd = "zip -e -P " + pass_zip_file + " " + file_xlsx_compress + " " + cmd_arch + " > log.txt"
            #print(cmd)
            subprocess.call(cmd, shell=True)
    else:
        temp_msg = ("ERROR : Compress format {} not accepted !!!\n   ---> Compress format accepted are zip or gz".format(format_compress))
        raise ValueError(temp_msg)

def encrypt_file_xlsx(list_file_to_compress, directory):
    # Chiffreement GPG d'une liste de fichiers
    import gnupg, os
    from nostri import pgp_public_key, gpg_home_dir
    
    os.chdir(directory)

    with open(list_file_to_compress, "r") as file:
        list_file = file.read().splitlines()

    gpg_home = gpg_home_dir
    gpg = gnupg.GPG(gnupghome=gpg_home)
    rkeys = pgp_public_key

    # méthode pour lister les clés
    #public_keys = gpg.list_keys()
    #private_keys = gpg.list_keys(True)
    #print("public keys : ")
    #pprint(public_keys)
    #print("private keys : ")
    #pprint(private_keys)

    # Méthode pour chiffrer un fichier
    i = 0
    while i < len(list_file):
        data = list_file[i]
        savefile = data + ".gpg"
        afile = open(data, 'rb')
        encrypted_ascii_data = gpg.encrypt_file(afile, rkeys.split(), always_trust=True, output=savefile)
        afile.close()
        i += 1

def tar_file_xlsx(file_xlsx_output_only, directory_tar, list_files_to_compress):
    # Mise en container tar d'une liste de fichiers"""
    import os, glob, tarfile
    from io import BytesIO
    
    #import info
    os.chdir(directory_tar)
    file_tar = file_xlsx_output_only + ".tar"
    file_tar_with_path = file_xlsx_output_only + ".with_path.tar"
    file_tar_gz = file_tar + ".gz"
    file_tar_with_path_gz = file_tar_with_path + ".gz"
    
    if os.path.isfile(file_tar):
        os.remove(file_tar)
    if os.path.isfile(file_tar_gz):
        os.remove(file_tar_gz)
    if os.path.isfile(file_tar_with_path):
        os.remove(file_tar_with_path)
    if os.path.isfile(file_tar_with_path_gz):
        os.remove(file_tar_with_path_gz)

    with open(list_files_to_compress, "r") as file:
        list_file = file.read().splitlines()

    # fichier tar simple, sans le path des fichiers
    t1 = tarfile.open(file_tar, 'w')
    i = 0
    while i < len(list_file):
        t1.add(list_file[i])
        i += 1
    t1.close()
    
    # fichier tar simple, avec le path des fichiers
    """for root, dirs, _ in os.walk(directory_tar):
        print(root)
        print(directory_tar)
        for d in dirs:
            for _, _, files in os.walk(os.path.join(root,d)):
                for f in files:
                    print(os.path.join(root, d, f))"""

    t2 = tarfile.open(file_tar_with_path, 'w')
    for f in glob.glob(os.path.join(directory_tar, "*")):
        i = 0
        while i < len(list_file):
            if os.path.basename(f) == list_file[i]:
                #print(f,)
                t2.add(f, arcname=None, recursive=False)
            i += 1
    t2.close()
    
    # fichier tar gzip, sans le path des fichiers
    tz1 = tarfile.open(file_tar_gz, 'w:gz')
    i = 0
    while i < len(list_file):
        tz1.add(list_file[i])
        i += 1
    tz1.close()

    # fichier tar gzip, avec le path des fichiers
    tz2 = tarfile.open(file_tar_with_path_gz, 'w:gz')
    for f in glob.glob(os.path.join(directory_tar, "*")):
        i = 0
        while i < len(list_file):
            if os.path.basename(f) == list_file[i]:
                tz2.add(f, arcname=None, recursive=False)
            i += 1
    tz2.close()
   
