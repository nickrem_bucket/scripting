def crypt_password(plain_text):
    """
    password = clé à crypter
    """
    from cryptography.fernet import Fernet
    
    test = plain_text.encode()
    
    key = Fernet.generate_key()
    cipher_suite = Fernet(key)
    encoded_text = cipher_suite.encrypt(test)
    encoded_text_bis = encoded_text.decode()
    key_bis = key.decode()

    return encoded_text_bis, key_bis

def decrypt_password(value, key):
    """
    value = clé à décrypter
    key = clé Fermet de décryptage
    """
    from cryptography.fernet import Fernet
    
    text1 = value.encode()
    text2 = key.encode()

    cipher_suite = Fernet(text2)
    decoded_text = cipher_suite.decrypt(text1)
    decoded_decode = decoded_text.decode()
    
    return decoded_decode

def set_pass_keyring(label_software, label_type, value, key):
    """
    software = type de logiciel utilisé (mariadb, mysql,
               postgresql, oracle ...)
    type = type de la donnée cryptée
    value_type = valeur du type la donnée
    password = valeur cryptée du type de donnée
    key = clé Fermet utiliser par cipher
    """
    import keyring
    
    label = label_type
    keyring.set_password(label_software, label, value)
    label = label_type + '_key'
    keyring.set_password(label_software, label, key)
    
def deroule_actions(label_software, label_type, plain_text):
    """
    label_software = type de logiciel utilisé (mariadb, mysql,
                     postgresql, oracle ...) + database
    label_type     = type de donnée à crypter (user, walllet, ou passwd)
    plain_text     = valeur a crypter/sauvegarder
    """
    import keyring

    value_encrypt, value_key = crypt_password(plain_text)
    print(value_encrypt)
    print(value_key)

    set_pass_keyring(label_software, label_type, value_encrypt, value_key)

    label = label_type
    value_keyring = keyring.get_password(label_software, label)
    label = label_type + '_key'
    key_keyring = keyring.get_password(label_software, label)    

    value_decoded = decrypt_password(value_keyring, key_keyring)

    return value_decoded    

def get_usr_or_passwd(label_software, label_type):
    """
    software = type de logiciel utilisé (mariadb, mysql,
               postgresql, oracle ...)
    type = type de la donnée cryptée
    value_type = valeur du type la donnée
    """
    import keyring
    
    label = label_type
    value_keyring = keyring.get_password(label_software, label)
    label = label_type + '_key'
    key_keyring = keyring.get_password(label_software, label)
    
    password_decoded = decrypt_password(value_keyring, key_keyring)
    
    return password_decoded
