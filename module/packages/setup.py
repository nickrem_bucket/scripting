from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
ext_modules = [
    Extension("keyring_getpasswor",  ["keyring_getpasswor.py"]),
]

setup(
    name = 'keyring_getpasswor',
    cmdclass = {'build_ext': build_ext},
    ext_modules = ext_modules
)