call DELOBJECT('COURSE', 'TABLE');
call DELOBJECT('SEQ_COURSE_ID', 'SEQUENCE');
call DELOBJECT('TRIGG_COURSE_ID', 'TRIGGER');
call DELOBJECT('DEPARTMENT', 'TABLE');
call DELOBJECT('SEQ_DEPARTMENT_ID', 'SEQUENCE');
call DELOBJECT('TRIGG_DEPARTMENT_ID', 'TRIGGER');
call DELOBJECT('STUDENT', 'TABLE');
purge RECYCLEBIN;

create table  COURSE
    (
       ID                       int         not null
     , NAME                     char(50)
     , AMOUNT                   int
     , constraint PK_COURSE     primary key (ID)
    );
/
create sequence SEQ_COURSE_ID
    start with 1
    increment by 1
    cache 100;
/
create trigger TRIGG_COURSE_ID
    before insert on COURSE
    for each row
        begin
            select SEQ_COURSE_ID.nextval
              into :new.ID
              from dual;
        end;
/
create table  DEPARTMENT
    (
       ID                       int         not null
     , NAME                     char(50)
     , constraint PK_DEPARTMENT primary key (ID)
    );
/
create sequence SEQ_DEPARTMENT_ID
    start with 1
    increment by 1
    cache 100;
/
create or replace trigger TRIGG_DEPARTMENT_ID
    before insert on DEPARTMENT
    for each row
        begin
            select SEQ_DEPARTMENT_ID.nextval
              into :new.ID
              from dual;
        end;
/
create table  STUDENT
    (
       ADMISSION               int           not null
     , NAME                    varchar2(20)  not null
     , AGE                     int           not null
     , COURSE                  int
     , DEPARTMENT              int
     , constraint PK_STUDENT   primary key (ADMISSION)
     , constraint FK01_STUDENT foreign key (COURSE)     references COURSE(ID)
     , constraint FK02_STUDENT foreign key (DEPARTMENT) references DEPARTMENT(ID)
    );
/
grant select, insert, update, delete on COURSE to %user_app%;
grant select, insert, update, delete on DEPARTMENT to %user_app%;
grant select, insert, update, delete on STUDENT to %user_app%;
/
grant select on SEQ_COURSE_ID to %user_app%;
grant select on SEQ_DEPARTMENT_ID to %user_app%;