execute DELOBJECT('COURSE', 'SYNONYM');)
execute DELOBJECT('DEPARTMENT', 'SYNONYM');)
execute DELOBJECT('STUDENT', 'SYNONYM');)
execute DELOBJECT('SEQ_COURSE_ID', 'SYNONYM');)
execute DELOBJECT('SEQ_DEPARTMENT_ID', 'SYNONYM');)
/
create synonym COURSE for %user_own%.COURSE;
create synonym DEPARTMENT for %user_own%.DEPARTMENT;
create synonym STUDENT for %user_own%.STUDENT;
create synonym SEQ_COURSE_ID for %user_own%.SEG_COURSE_ID;
create synonym SEQ_DEPARTMENT_ID for %user_own%.SEQ_DEPARTMENT_ID;