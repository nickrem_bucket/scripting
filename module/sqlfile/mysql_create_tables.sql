drop table if exists STUDENT cascade;
drop table if exists COURSE cascade;
drop table if exists DEPARTMENT cascade;

create table  COURSE
    (
       ID                       int    not null    auto_increment
     , NAME                     char(50)
     , AMOUNT                   int
     , constraint PK_COURSE     primary key(ID)
    );

create table DEPARTMENT
    (
       ID                       int    not null    auto_increment
     , NAME                     char(50)
     , AMOUNT                   int
     , constraint PK_DEPARTMENT primary key(ID)
    );

create table  STUDENT
    (
       ADMISSION               int           not null
     , NAME                    text          not null
     , AGE                     int           not null
     , COURSE                  int
     , DEPARTMENT              int
     , constraint PK_STUDENT   primary key (ADMISSION)
     , constraint FK01_STUDENT foreign key (COURSE)     references COURSE(ID)
     , constraint FK02_STUDENT foreign key (DEPARTMENT) references DEPARTMENT(ID)
    );

grant select, insert, update, delete on table COURSE to %user_app%;
grant select, insert, update, delete on table DEPARTMENT to %user_app%;
grant select, insert, update, delete on table STUDENT to %user_app%;