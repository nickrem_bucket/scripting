select concat(
               trim(stu.ADMISSION)
             , ';'
             , trim(stu.NAME)
             , ';'
             , trim(stu.AGE)
             , ';'
             , trim(cou.NAME)
             , ';'
             , trim(dep.NAME)
             , ';'
             , trim(cou.AMOUNT/100)
             ) as RESULT
       into OUTFILE '%resultat_file%'
       from    STUDENT stu
             , COURSE cou
             , DEPARTMENT dep
      where  stu.COURSE = cou.ID
        and  stu.DEPARTMENT = dep.ID
      order by stu.ADMISSION;
