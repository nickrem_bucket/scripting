# -*- coding: latin-1 -*-

from ClassPasswd import SecurePasswd
from getpass import getpass

pname = raw_input("Libellé du mot de passe: ")

p1 = getpass("Entrer une valeur pour {}: ".format(pname))

p2 = getpass("Pour vérification, Ré-entrer la valeur pour {}: ".format(pname))

while p1 != p2:
        print('Les valeurs NE correspondent PAS')
        p1 = getpass("Entrer une valeur pour {}: ".format(pname))
        p2 = getpass("Pour vérification, Ré-entrer la valeur pour {}: ".format(pname))

new_class = SecurePasswd()

# Chiffrement et padding
new_class.encrypt(pname, p1)
print('Chiffrement Completé!')
