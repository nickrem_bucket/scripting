#!/bin/ksh
#
# Creation : O. VUILLEMARD,         le 07/09/98
# Modification: S.VELMOUROUGANE, Calcul du NB de Jours adapte pour AIX , le 31/08/2011
# Modification: F.JIERRY, Correction Bug lie au find $(print $rep) sous AIX, erreur "...ksh: no space", le 06/01/2012
# Modification: F.JIERRY, Ajout de la fonctionnalite tsm pour la mise sur bande, le 06/01/2012
# Modification: F.JIERRY, Modification pour Linux suite a un probleme xargs, le 14/03/2013
# Modification: F.JIERRY, Suppression de la recursivite du find + compression via gzip au lieu de compress, le 06/05/2014
# Modification: F.JIERRY, Ajout d'une verification supplementaire sur la syntaxe des lignes du fichier cfg, le 07/05/2014
# Modification: F.JIERRY, Ajout des options "cs" et "ss" pour la compression et la suppression securisee, le 07/05/2014
# Modification: F.JIERRY, Ajout des couleurs, le 09/05/2014
# Modification: F.JIERRY, Modifications du find + xargs, en find + exec car problÃ¨me avec les fichiers Ã  caractÃ¨res spÃ©ciaux, le 20/06/2014
# Modification: F.JIERRY, Correction bug lsof sur AIX 5.3 TL12, le 17/07/2014
# Modification: F.JIERRY, Correction bug args too long suite a l'ajout de la non recursivite, le 18/07/2014
# Modification: F.JIERRY, Correction de l'option ad car completement bugge, et renommage en rv et rd pour rotation version et rotation date,
#                         Remplacement de la commande echo par print pour une meilleur lisibilite et portabilite, le 28/11/2014
# Modification: F.JIERRY, Ajout de verifications supplementaires sur le type d operation a utiliser, log plus verbeuse en cas d'erreurs,
#                         Ne prend plus en compte les lignes vides ou contenant uniquement des espaces et des tabulations, le 02/12/2014
#
# Script permettant quotidiennement de :
# 1) compresser les fichiers de plus de n jours
# 2) supprimer les fichiers de plus de n jours
# 3) rotation des fichiers trop volumineux
# 4) mettre sur TSM les fichiers de plus de n jours
#
# Principe d'utilisation du script :
# Le script s'execute sans parametres, il va simplement lire un fichier de configuration contenant la liste des actions a realiser.
# Chaque ligne du fichier de configuration sera traitee l'une apres l'autre pour realiser au quotidien, toutes les taches de maintenance.
# La structure du fichier de configuration est decrite ci-dessous.
#
# Structure du fichier de Configuration :
# Liste des champs attendus pour chaque ligne du fichier :
# <t>   <f>     <r>     [opt1]     [opt2]
# Ou <t> represente le "Type" parmi les options suivantes :
#       "rv"    pour une rotation de log avec un numero de version et compression (log.1.gz, log.2.gz, etc.)
#       "rd"    pour une rotation de log avec une date et compression (log.20141127.gz, log.20141128.gz, etc.)
#       "c"     pour compression, via la commande gzip
ilisationcs"    pour compression securisee, toujours via la commande gzip mais on verifie que le fichier a compresser n'est pas en cours d'ut#       "s"     pour suppression
#       "ss"    pour suppression securisee, on verifie que le fichier a supprimer n'est pas en cours d'utilisation
#       "t"     pour mise sur bande TSM, via la commande dsmc
# Ou <f> represente le "Fichier ou Repertoire" a traiter ou le "Fichier" pour la rotation
# Ou <r> represente le "Nombre de Jours de Retention" pour suppression/compression/tsm ou "Nombre de versions" pour la rotation
# Ou [opt1] represente la "Taille maximale" en octet du fichier pour la rotation rv ou la "classe de retention TSM" pour la mise sur bande TSM
# Ou [opt2] represente l' "Option possible" pour la commande "find", ou l'option possible quand la mise sur bande TSM est demandee
#
# Exemple de fichier de Configuration :
#       # PURGES
#       #
#       ss      /apps/cristal2/log                      7
#       s       /apps/cristal2/output/LOCAL             5
#       s       /apps/cristal2/output/LEGAL             1
#       s       /apps/cristal2/output                   30
#       s       /apps/cristal2/output/crtarch           1
#       s       /apps/cristal2/input                    30
#       s       /apps/cristal2/output/MICROFICHES_BNPAFR        7
#       s       /apps/cristal2/output/MICROFICHES_BDEBFR        7
#       s       /apps/cristal2/output/COMPTABILITE      7
#       s       /apps/cristal2/output/temp              7
#       ss      /apps/exploit2/log                      30
#       ss      /apps/oracle/adm/diag/rdbms/arp2frr0/ARP2FRR0/trace/*trm        30
#       ss      /apps/oracle/adm/diag/rdbms/arp2frr0/ARP2FRR0/trace/*trc        30
#       #
#       # COMPRESSIONS
#       #
#       c       /apps/cristal2/output/LEGAL             1
#       c       /apps/cristal2/output/LOCAL             2
#       c       /apps/cristal2/output/crtarch           7
#       cs      /apps/cristal2/log/log                  3
#       c       /apps/cristal2/output                   7
#       c       /apps/cristal2/input                    7
#       cs      /apps/exploit2/log                      7
#       cs      /apps/exploit2/log/mouchard*            1
#       cs      /apps/cristal2/output/COMPTABILITE/input        3
#       cs      /apps/cristal2/output/COMPTABILITE/trace        3
#       #
#       # ROTATION
#       #
#       rv      /apps/cristal2/output/COMPTABILITE/log/LOG_compta       5       5242880
#       rd      /apps/cristal2/output/COMPTABILITE/log/LOG_business       7
#       rv      /apps/webservers7/fnfprd01web/LogsIHS/error_log_sec     5       50000000
#       #
#       # TSM
#       #
#       t     /apps/cristal2/log/log                  3       ARCH0030
#       t     /apps/cristal2/input                    7       ARCH0030          -deletefiles
#-------------------------------------------------------------------------------
# VARIABLES
export LOG=$(dirname $0)/../logs/$(basename $0 .ksh)_$(date +%Y%m%d_%HH%M).log
export CFG=$(dirname $0)/$(basename $0 .ksh).cfg
export LSOF="/usr/sbin/lsof"
# COULEURS
export NORMAL=$(print "\033[0;39m")
export BRILLANT=$(print "\033[01;01m")
export RED=$(print "\033[1;31m")
export GREEN=$(print "\033[1;32m")
export MAGENTA=$(print "\033[1;35m")
export CYAN=$(print "\033[1;36m")
export JAUNE=$(print "\033[1;33m")
# USAGE
export usage="${NORMAL}${BRILLANT}Syntaxe${NORMAL}

\t${BRILLANT}$(basename $0)${NORMAL} [${BRILLANT}-h${NORMAL}|${BRILLANT}-help${NORMAL}|${BRILLANT}help${NORMAL}]

${BRILLANT}Options${NORMAL}

\t${BRILLANT}-h${NORMAL} : affiche un equivalent du man${NORMAL}\n"
# Man de la commande
export MAN_CMD="${NORMAL}${BRILLANT}$(basename $0) Script - Man Page Style ;)${NORMAL}

${BRILLANT}But${NORMAL}

\tRealiser des operations de maintenance MeO

${usage}
${BRILLANT}Description${NORMAL}

\tLe script $(basename $0) s'execute sans parametres. Il va lire un fichier de configuration nomme $(basename $0 .ksh).cfg situe dans le meme repertoire
\tque le script lui-meme. Chaque ligne du fichier de configuration represente une tache de maintenance a realiser sur le serveur.
\tVoici le detail de la structure du fichier de configuration attendu par le script :

\tListe des champs attendus pour chaque ligne du fichier :
\t  ${BRILLANT}<t>     <f>     <r>     [opt1]     [opt2]${NORMAL}
\t  Ou ${BRILLANT}<t>${NORMAL} represente le \"Type\" parmi les options suivantes :
\t   \"${BRILLANT}rv${NORMAL}\" pour ${BRILLANT}r${NORMAL}otation de log avec un numero de ${BRILLANT}v${NORMAL}ersion et compression (log.1.gz, log.2.gz, etc.)
\t   \"${BRILLANT}rd${NORMAL}\" pour ${BRILLANT}r${NORMAL}otation de log avec une ${BRILLANT}d${NORMAL}ate et compression (log.20141127.gz, log.20141128.gz, etc.)
\t   \"${BRILLANT}c${NORMAL}\"  pour ${BRILLANT}c${NORMAL}ompression. Realise via la commande gzip
\t   \"${BRILLANT}cs${NORMAL}\" pour ${BRILLANT}c${NORMAL}ompression ${BRILLANT}s${NORMAL}ecurisee. Realise via la commande gzip mais on s'assure que le fichier a compresser n'est pas en cours d'utilisation
\t   \"${BRILLANT}s${NORMAL}\"  pour ${BRILLANT}s${NORMAL}uppression.
\t   \"${BRILLANT}ss${NORMAL}\" pour ${BRILLANT}s${NORMAL}uppression ${BRILLANT}s${NORMAL}ecurisee. On s'assure que le fichier a supprimer n'est pas en cours d'utilisation
\t   \"${BRILLANT}t${NORMAL}\"  pour mise sur bande ${BRILLANT}t${NORMAL}sm. On realise la mise sur bande via la commande dsmc
\t  Ou ${BRILLANT}<f>${NORMAL} represente le \"Fichier ou Repertoire\" a traiter ou le \"Fichier\" pour la rotation
\t  Ou ${BRILLANT}<r>${NORMAL} represente le \"Nombre de Jours de Retention\" pour suppression/compression/tsm ou \"Nombre de versions\" pour la rotation
\t  Ou ${BRILLANT}[opt1]${NORMAL} represente la \"Taille maximale\" en octet du fichier pour rotation par version ou la \"classe de retention TSM\" pour la mise sur bande TSM
\t  Ou ${BRILLANT}[opt2]${NORMAL} represente l' \"Option possible\" pour la commande \"find\", ou l'option possible quand la mise sur bande TSM est demandee

${BRILLANT}Exemples${NORMAL}

\tLignes pouvant apparaitre dans le fichier de configuration \"$(basename $0 .ksh).cfg\" :
\t  ${BRILLANT}#
\t  # PURGES
\t  #
\t  ss\t/apps/cristal2/log\t\t\t\t7
\t  s\t/apps/cristal2/output/LOCAL\t\t\t5
\t  s\t/apps/cristal2/output/LEGAL\t\t\t1
\t  s\t/apps/cristal2/output\t\t\t\t30
\t  s\t/apps/cristal2/output/crtarch\t\t\t1
\t  s\t/apps/cristal2/input\t\t\t\t30
\t  s\t/apps/cristal2/output/MICROFICHES_BNPAFR\t7
\t  s\t/apps/cristal2/output/MICROFICHES_BDEBFR\t7
\t  s\t/apps/cristal2/output/COMPTABILITE\t\t7
\t  s\t/apps/cristal2/output/temp\t\t\t7
\t  ss\t/apps/exploit2/log\t\t\t\t30
\t  ss\t/apps/oracle/adm/diag/rdbms/arp2frr0/ARP2FRR0/trace/*trm\t30
\t  ss\t/apps/oracle/adm/diag/rdbms/arp2frr0/ARP2FRR0/trace/*trc\t30
\t  ss\t/apps/sys/admin/$(basename $0 .ksh)*.log\t\t\t15
\t  #
\t  # COMPRESSIONS
\t  #
\t  c\t/apps/cristal2/output/LEGAL\t\t\t1
\t  c\t/apps/cristal2/output/LOCAL\t\t\t2
\t  c\t/apps/cristal2/output/crtarch\t\t\t7
\t  cs\t/apps/cristal2/log/log\t\t\t\t3
\t  c\t/apps/cristal2/output\t\t\t\t7
\t  c\t/apps/cristal2/input\t\t\t\t7
\t  cs\t/apps/exploit2/log\t\t\t\t7
\t  cs\t/apps/exploit2/log/mouchard*\t\t\t1
\t  cs\t/apps/cristal2/output/COMPTABILITE/input\t3
\t  cs\t/apps/cristal2/output/COMPTABILITE/trace\t3
\t  #
\t  # ROTATION
\t  #
\t  rv\t/apps/cristal2/output/COMPTABILITE/log/LOG_compta\t5\t5242880
\t  rd\t/apps/cristal2/output/COMPTABILITE/log/LOG_business\t7
\t  rv\t/apps/webservers7/fnfprd01web/LogsIHS/error_log_sec\t5\t50000000
\t  #
\t  # TSM
\t  #
\t  t\t/apps/cristal2/log/log\t\t3\tARCH0030
\t  t\t/apps/cristal2/input\t\t7\tARCH0030\t-deletefiles
${NORMAL}"

# Fonction verifiant si un fichier est utilise par un processus
#       - Passer en argument le nom du fichier
#       - Retourne 1 si le fichier est utilise, 0 sinon
Verif_utilisation_fichier() {

        export CR=0
        export FICHIER=$1
        export PROCESSUS=$(${LSOF} ${FICHIER} 2> /dev/null | grep -v -E "^Value of |^COMMAND |In while loop:" | awk '{print $2}' | sort -n | uniq)

        if [ "${PROCESSUS}" != "" ]
        then
                print "${RED}# ATTENTION : Processus present(s) sur le fichier :\t${FICHIER}${NORMAL}"
                print "${RED}# Liste :"
                ${LSOF} ${FICHIER} 2> /dev/null
                print "${RED}\n# Detail des processus :"

                for i in $(print ${PROCESSUS})
                do
                        ps -fT $i
                done
                CR=1

                print "${RED}# Le fichier \"${FICHIER}\" est utilise par des processus, il n'a donc pas ete traite !${NORMAL}"
        fi

        return ${CR}
}

# Gestion des options
while [ $# != 0 ]
do
        case $1 in
                -h|-help|help ) # option1
                        print -u2 "${MAN_CMD}"
                        exit 0
                ;;
                -* ) # option inconnue
                        print -u2 "${RED}($1) option inconnue${NORMAL}"
                        print -u2 "$usage"
                        exit 1
                ;;
                * ) # le parametre en fin de commande
                        print -u2 "$usage"
                        exit 1
                ;;
        esac
done


# Redirection de la LOG
exec 1> $LOG 2>&1

# Gestion des commandes suivants l'OS
# Commande FIND : gestion de la non recursivite
export OS=`uname`
case ${OS} in
        "AIX")          export non_rec=" -type d ! -name \"*\" -prune -o"
                        ;;
        "Linux")        export non_rec=" -mindepth 1 -maxdepth 1"
                        ;;
        *)              print "${RED}ATTENTION : OS \"$OS\" non supporte${NORMAL}"
                        exit 1
        ;;
esac

# Verifier la presence du fichier de configuration
if [ ! -f "$CFG" ]
then
        print "${RED}ATTENTION : le fichier de configuration \"$CFG\" n'existe pas${NORMAL}"
        exit 2
fi

##################################################################################
# On parcourt le fichier de config $CFG
grep -vE "^#|^[         ]*$" $CFG | while read LINE
do
        export type=$(print "$LINE" | awk '{print $1}')
        export string=$(print "$LINE" | awk '{print $2}')
        export nbjours=$(print "$LINE" | awk '{print $3}')
        export opt1=$(print "$LINE" | awk '{print $4}')
        export opt2=$(print "$LINE" | awk '{print $5}')



        print "${JAUNE}#######################################################################"
        print "# Traitement de la ligne :"
        print "${CYAN}$LINE${JAUNE}"
        print "#######################################################################${NORMAL}"
        ##################################################################################
        # Verifications de la ligne de traitement
        # Est-ce que la variable nbjours est bien numerique
        expr $nbjours + 0 > /dev/null 2>&1
        CR_nbjours=$?
        case $type in
                rd|c|cs|s|ss)
                        if [ -z "${string}" ] || [ -z "${nbjours}" ] || [ ! ${CR_nbjours} -lt 2 ]
                        then
                                print "${RED}ERREUR - Traitement du type <$type>"
                                print "Verifiez la ligne du fichier $CFG affichee ci-dessus"
                                print "Il faut qu'elle soit au format suivant :${NORMAL}"
                                print "${BRILLANT}$type${NORMAL} <${BRILLANT}Fichiers${NORMAL}> <${BRILLANT}Retention_sur_disque_en_jours${NORMAL}>${NORMAL}"
                                print "${RED}On passe a la ligne suivante...${NORMAL}\n"
                                export FLAG_VERIF=KO
                                export CR_G=3
                        else
                                export FLAG_VERIF=OK
                        fi
                ;;
                rv)
                        # Est-ce que la variable opt1 est bien numerique (represente la taille max en octets)
                        expr $opt1 + 0 > /dev/null 2>&1
                        CR_opt1=$?
                        if [ -z "${string}" ] || [ -z "$nbjours" ] || [ -z "$opt1" ] || [ ! ${CR_nbjours} -lt 2 ] || [ ! ${CR_opt1} -lt 2 ]
                        then
                                print "${RED}ERREUR - Traitement du type <$type>"
                                print "Verifiez la ligne du fichier $CFG affichee ci-dessus"
                                print "Il faut qu'elle soit au format suivant :${NORMAL}"
                                print "${BRILLANT}$type${NORMAL} <${BRILLANT}Fichiers${NORMAL}> <${BRILLANT}Retention_sur_disque_en_jours${NORMAL}> <${BRILLANT}Taille_max_en_octets>${NORMAL}"
                                print "${RED}On passe a la ligne suivante...${NORMAL}\n"
                                export FLAG_VERIF=KO
                                export CR_G=3
                        else
                                export FLAG_VERIF=OK
                        fi
                ;;
                t)
                        if [ -z "$string" ] || [ -z "$nbjours" ] || [ -z "$opt1" ] || [ ! ${CR_nbjours} -lt 2 ]
                        then
                                print "${RED}ERREUR - Traitement du type <$type>"
                                print "Verifiez la ligne du fichier $CFG affichee ci-dessus"
                                print "Il faut qu'elle soit au format suivant :${NORMAL}"
                                print "${BRILLANT}$type${NORMAL} <${BRILLANT}Fichiers${NORMAL}> <${BRILLANT}Retention_sur_disque_en_jours${NORMAL}> <${BRILLANT}Classe_d'archivage_TSM${NORMAL}> [-delefiles]"
                                print "${RED}On passe a la ligne suivante...${NORMAL}\n"
                                export FLAG_VERIF=KO
                                export CR_G=3
                        else
                                export FLAG_VERIF=OK
                        fi
                ;;
                *)
                        print "${RED}ERREUR - Traitement du type <$type> Inconnu."
                        print "Verifiez la ligne du fichier $CFG affichee ci-dessus"
                        print "${NORMAL}Le type de traitement pris en charge est parmi les suivants :"
                        print "\t   \"${BRILLANT}rv${NORMAL}\" pour ${BRILLANT}r${NORMAL}otation de log avec un numero de ${BRILLANT}v${NORMAL}ersion et compression (log.1.gz, log.2.gz, etc.)
\t   \"${BRILLANT}rd${NORMAL}\" pour ${BRILLANT}r${NORMAL}otation de log avec une ${BRILLANT}d${NORMAL}ate et compression (log.20141127.gz, log.20141128.gz, etc.)
\t   \"${BRILLANT}c${NORMAL}\"  pour ${BRILLANT}c${NORMAL}ompression. Realise via la commande gzip
\t   \"${BRILLANT}cs${NORMAL}\" pour ${BRILLANT}c${NORMAL}ompression ${BRILLANT}s${NORMAL}ecurisee. Realise via la commande gzip mais on s'assure que le fichier a compresser n'est pas en cours d'utilisation
\t   \"${BRILLANT}s${NORMAL}\"  pour ${BRILLANT}s${NORMAL}uppression.
\t   \"${BRILLANT}ss${NORMAL}\" pour ${BRILLANT}s${NORMAL}uppression ${BRILLANT}s${NORMAL}ecurisee. On s'assure que le fichier a supprimer n'est pas en cours d'utilisation
\t   \"${BRILLANT}t${NORMAL}\"  pour mise sur bande ${BRILLANT}t${NORMAL}sm. On realise la mise sur bande via la commande dsmc"
                        print "${RED}On passe a la ligne suivante...${NORMAL}\n"
                        export FLAG_VERIF=KO
                        export CR_G=3
                ;;
        esac

        if [ $FLAG_VERIF == "OK" ]
        then
                ##################################################################################
                # Affection des variables pour la recherche via la commande find
                export djour=$(date +%Y%m%d)

                if [ $nbjours -ge 0 ]
                then
                        if [ $type != "rv" ] && [ $type != "rd" ]
                        then
                                export mtime="-mtime +${nbjours}"
                        else
                                export mtime=""
                        fi
                else
                        export mtime=""
                fi

                export rep=""
                export pattern=""
                export nbfiles=""

                if [ -d "$string" ]
                then
                        # Cas particulier d'un repertoire
                        rep=$string
                        pattern="*"
                else
                        # Sinon on recupere le repertoire et les fichiers a traiter
                        rep=$(dirname "$string")
                        pattern=$(basename "$string")
                fi

                cmd_find="find ${rep}${non_rec} -type f -name \"${pattern}\" ${mtime}"
                nbfiles=$(print "$cmd_find -print" | ksh | wc -l | awk '{print $1}')

                ##################################################################################
                # Traitement selon le type
                case $type in
                        rv)
                                print "${BRILLANT}Rotation avec version du repertoire : ${rep}"
                                print "Fichiers a traiter : ${pattern}"
                                print "Taille maximale = ${opt1} Octets"
                                print "Retention = ${nbjours} Versions"
                                print "Option(s) = ${opt2}"
                                print "Nombre de fichiers a traiter : ${nbfiles}${NORMAL}"
                                print "$cmd_find ${opt2} -print" | ksh

                                # Recherche des fichiers concernes
                                files=$(print "$cmd_find ${opt2} -print" | ksh)
                                filesfound=$(for f in $files
                                do
                                        size=$(ls -al $f | awk '{print $5}')
                                        if [ $size -ge ${opt1} ]
                                        then
                                                print $f
                                        fi
                                done)

                                # Rotation des Fichiers
                                for file in $filesfound
                                do
                                        print "\nTraitement du fichier $file"
                                        # Rotation des Versions
                                        typeset -i ver=$nbjours
                                        typeset -i prever
                                        while [ $ver -gt 1 ]
                                        do
                                                prever=${ver}-1
                                                mv -f "${file}.${prever}.gz" "${file}.${ver}.gz" > /dev/null 2>&1
                                                ver=${prever}
                                        done
                                        # Recopie/compression et remise a zero du Fichier
                                        gzip "${file}" -c > "${file}.${ver}.gz" && print |>| "${file}"
                                        CR=$?
                                        if [ $CR -ne 0 ]
                                        then
                                                print "${RED}Probleme lors de la recopie du fichier $file${NORMAL}"
                                                CR_G=$CR
                                        fi

                                        ls -ltr ${file}*
                                done
                                ;;

                        rd)
                                print "${BRILLANT}Rotation avec date du repertoire : ${rep}"
                                print "Fichiers a traiter : ${pattern}"
                                print "Retention = ${nbjours} Jours"
                                print "Option(s) = ${opt2}"
                                print "Nombre de fichiers a traiter : ${nbfiles}${NORMAL}"
                                print "$cmd_find ${opt2} -print" | ksh

                                # Recherche des fichiers concernes
                                filesfound=$(print "$cmd_find ${opt2} -print" | ksh)

                                #  des Fichiers
                                for file in $filesfound
                                do
                                        print "\nTraitement du fichier $file"
                                        # Recopie/compression et remise a zero du Fichier
                                        if [ ! -e "${file}.${djour}.gz" ]
                                        then
                                                gzip "${file}" -c > "${file}.${djour}.gz" && print |>| "${file}"
                                                CR=$?
                                                if [ $CR -ne 0 ]
                                                then
                                                        print "${RED}Probleme lors de la recopie du fichier $file vers ${file}.${djour}.gz${NORMAL}"
                                                        CR_G=$CR
                                                fi
                                        else
                                                print "${RED}Attention : le fichier $file.${djour}.gz est deja present, on ne l ecrase pas${NORMAL}"
                                        fi

                                        # Suppression des fichiers de plus de ${nbjours} Jours
                                        export mtime="-mtime +${nbjours}"
                                        cmd_find="find ${rep}${non_rec} -type f -name \"${pattern}.[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9].gz\" ${mtime}"
                                        print "$cmd_find -exec rm -f {} \;" | ksh

                                        ls -ltr ${file}*
                                done
                                ;;

                        t)
                                print "${BRILLANT}Mise sur bande TSM dans le repertoire : ${rep}"
                                print "Fichiers a traiter : ${pattern}"
                                print "Retention (sur disque) = ${nbjours} Jours"
                                print "Nombre de fichiers a traiter : ${nbfiles}"
                                print "Liste des fichiers a sauvegarder"
                                export filelist=${LOG}_Liste_TSM
                                print "$cmd_find -print" | ksh | tee ${filelist}
                                CR=$?
                                print "Classe de retention TSM (sur bande) = ${opt1}"
                                print "Option(s) = ${opt2}${NORMAL}"

                                if [ ${CR} -eq 0 ]
                                then
                                        CL_ARCH=$(dsmc q mgmt | grep ${opt1} | grep -v grep | wc -l | awk '{print $1}')
                                        if [ ${CL_ARCH} -ne "0" ]
                                        then
                                                dsmc ar -filelist="${filelist}" -archmc=${opt1} -desc="Svg ${rep}" -subdir=no ${opt2}
                                                CR=$?
                                                if [ $CR -ne "0" ]
                                                then
                                                        CR_G=$?
                                                fi
                                        else
                                                print "La classe de retention TSM : ${opt1} n existe pas !"
                                                CR_G=4
                                        fi
                                else
                                        print "Probleme lors de la recherche du find, CR = ${CR}"
                                        CR_G=${CR}
                                fi
                                ;;

                        c)
                                print "${BRILLANT}Compression dans le repertoire : ${rep}"
                                print "Fichiers a traiter : ${pattern}"
                                print "Retention = ${nbjours} Jours"
                                print "Option(s) = ${opt1}"
                                print "Nombre de fichiers a traiter : ${nbfiles}${NORMAL}"

                                if [ ${nbfiles} -gt 0 ]
                                then
                                        print "$cmd_find ${opt1} -exec ls -ltr {} \;" | ksh
                                        print "$cmd_find ${opt1} -exec gzip {} \;" | ksh
                                fi
                                ;;

                        cs)
                                print "${BRILLANT}Compression Securisee dans le repertoire : ${rep}"
                                print "Fichiers a traiter : ${pattern}"
                                print "Retention = ${nbjours} Jours"
                                print "Option(s) = ${opt1}"
                                print "Nombre de fichiers a traiter : ${nbfiles}${NORMAL}"

                                if [ ${nbfiles} -gt 0 ]
                                then
                                        print "$cmd_find ${opt1} -exec ls -ltr {} \;" | ksh
                                        print "$cmd_find ${opt1} -print" | ksh | while read LINE
                                        do
                                                Verif_utilisation_fichier $LINE
                                                CR=$?

                                                if [ $CR -eq 0 ]
                                                then
                                                        gzip "$LINE"
                                                fi
                                        done
                                fi
                                ;;

                        s)
                                print "${BRILLANT}Suppression dans le repertoire : ${rep}"
                                print "Fichiers a traiter : ${pattern}"
                                print "Retention = ${nbjours} Jours"
                                print "Option(s) = ${opt1}"
                                print "Nombre de fichiers a traiter : ${nbfiles}${NORMAL}"

                                if [ ${nbfiles} -gt 0 ]
                                then
                                        print "$cmd_find ${opt1} -exec ls -ltr {} \;" | ksh
                                        print "$cmd_find ${opt1} -exec rm -f {} \;" | ksh
                                fi
                                ;;

                        ss)
                                print "${BRILLANT}Suppression Securisee dans le repertoire : ${rep}"
                                print "Fichiers a traiter : ${pattern}"
                                print "Retention = ${nbjours} Jours"
                                print "Option(s) = ${opt1}"
                                print "Nombre de fichiers a traiter : ${nbfiles}${NORMAL}"

                                if [ ${nbfiles} -gt 0 ]
                                then
                                        print "$cmd_find ${opt1} -exec ls -ltr {} \;" | ksh
                                        print "$cmd_find ${opt1} -print" | ksh | while read LINE
                                        do
                                                Verif_utilisation_fichier $LINE
                                                CR=$?

                                                if [ $CR -eq 0 ]
                                                then
                                                        rm -f "$LINE"
                                                fi
                                        done
                                fi
                                ;;

                        *)
                                print "${RED}Traitement <$type> Inconnu."
                                continue;;
                esac

                print "${NORMAL}"
        fi
done

exit ${CR_G}
