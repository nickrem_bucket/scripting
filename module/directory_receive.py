# coding: latin1

#@(#) ========================================================================
#@(#) SCRIPT : directory_receive.py
#@(#) OBJET  : Files integration received from CFT - Module for Directories
#@(#)
#@(#) AUTEUR : NRE
#@(#) ------------------------------------------------------------------------
#@(#) PARAMETRES :
#@(#)
#@(#) ------------------------------------------------------------------------
#@(#) VERSION DE LIVRAISON : 1.0
#@(#)
#@(#) ------------------------------------------------------------------------
#@(#) DATE        AUTEUR        MODIFICATION
#@(#) 06/04/2020  NRE           Création
#@(#)
#@(#) ========================================================================

import os

def __main__():
    var = True
    return var

def create_workdir_list(REP_EXCHANGE, file_output):

        from datetime import datetime
        date_exec = datetime.now().strftime('%d/%m/%Y')
        date_exec2 = datetime.now().strftime('%Y%m%d')
        time_exec = datetime.now().strftime('%H:%M:%S')
        date = date_exec[6:] + "-" + date_exec[3:5] + "-" + date_exec[0:2] + "_" + time_exec.replace(":", "-")

        from list_dir_cft_reception import param

        list_dirs = []
        dirs = param.split(';')
        for dir in dirs:
                if os.path.isdir(dir):
                        list_dirs.append(dir)

        f_out = open(file_output, "a")

        if len(list_dirs) == 0:
                #do_nothing = True
                A=0
        else:

                i = 0
                while i < len(list_dirs):
                        #do_nothing = False

                        # Number of files in the directory list_dirs[i]
                        if len(os.listdir(list_dirs[i])) == 0:
                                # Si 0 fichier --> rien à faire
                                #do_nothing = True
                                A=0

                        else:
                                temp_split = list_dirs[i].split("in/")
                                which_sender = temp_split[1].lower()
                                file_test_file = "{}/file_{}_integrate_{}.txt".format(REP_EXCHANGE, which_sender, date_exec2)

                                list_files_datej = []
                                for f in os.listdir(list_dirs[i]):
                                        f_abs = "{}/{}".format(list_dirs[i], f)
                                        statinfo = os.stat(f_abs)
                                        mtime = statinfo.st_mtime
                                        f_date = datetime.fromtimestamp(mtime).strftime('%d/%m/%Y')
                                        if f_date == date_exec:
                                                if os.path.isfile(file_test_file):
                                                        if f not in open(file_test_file).read():
                                                                line_to_write = "{};{};{}".format(which_sender, file_test_file, f_abs)
                                                                f_out.write(line_to_write)
                                                                f_out.write("\n")
                                                        else:
                                                                #do_nothing = True
                                                                A=0
                                                else:
                                                        line_to_write = "{};{};{}".format(which_sender, file_test_file, f_abs)
                                                        f_out.write(line_to_write)
                                                        f_out.write("\n")
                                        else:
                                                #do_nothing = True
                                                A=0

                        i += 1

        f_out.close()

        if os.path.isfile(file_output):
                if os.stat(file_output).st_size == 0:
                        os.remove(file_output)
                        do_nothing = True
                else:
                        do_nothing = False
        else:
                do_nothing = False

        return do_nothing


def purge_script_log_bpm(dir_log, dir_log_cycle):

        import datetime
        import inspect
        import glob

        date_exec2 = datetime.datetime.now().strftime('%Y%m%d')
        time_exec2 = datetime.datetime.now().strftime('%H%M%S')
        file_log = "{}/purge_script_log_bpm_{}.log".format(dir_log_cycle, date_exec2)
        #name_caller = os.path.basename(inspect.stack()[1][1])
        name_caller = "crt_file_integration.sh"
        pattern_log = "script_execution_*_{}.log".format(name_caller)

        time_minus_30 = datetime.datetime.now() - datetime.timedelta(minutes=30)
        time_str = str(time_minus_30)
        time_str_format = time_str[0:4] + time_str[5:7] + time_str[8:10] + time_str[11:13] + time_str[14:16] + time_str[17:19]

        os.chdir(dir_log)

        if not os.path.isfile(file_log):
                text1 = ("    --> Purge des fichiers script_execution...crt_file_integration.sh.log a {} :\n"
                        ).format(time_exec2)
        else:
                text1 = ("\n\n\n" \
                         "    --> Purge des fichiers script_execution...crt_file_integration.sh.log a {} :\n"
                        ).format(time_exec2)

        f_log = open(file_log, "a")
        f_log.write(text1)


        list_files_sorted = sorted(glob.glob(pattern_log), key=os.path.getmtime)

        #for file in glob.glob(pattern_log):
        for file in list_files_sorted:
                #f_abs = "{}/{}".format(dir_log, file)
                #statinfo = os.stat(f_abs)
                statinfo = os.stat(file)
                mtime = statinfo.st_mtime
                f_date = datetime.datetime.fromtimestamp(mtime).strftime('%Y%m%d%H%M%S')
                if f_date < time_str_format:
                        text2 = ""
                        text2 = (" - {}\n").format(file)
                        f_log.write(text2)
                        os.remove(file)

        f_log.write("\n")
        f_log.close()

def file_copy(filename, type_depot):

        from shutil import copy2

        list_type_depot_compta = []
        i = 1
        while i < 18:
                if i in [ 1, 2, 3, 4, 5, 6, 7, 8, 9 ]:
                        lib = "compta0" + str(i)
                else:
                        lib = "compta" + str(i)
                list_type_depot_compta.append(lib)
                i += 1

        type_depot_bis = type_depot.lower() + ";"
        user_cristal = os.environ['USER_CRISTAL']
        file_param_nostri = "/home/{}/Diffusion_listes/PARM_NOSTRI.kst".format(user_cristal)
        file_param_icaf = "/home/{}/Diffusion_listes/param_compta_icaf.lst".format(user_cristal)

        if type_depot.lower() in list_type_depot_compta:
                file_temp = file_param_icaf
        else:
                file_temp = file_param_nostri

        with open(file_param_nostri, "r") as f:
                for line in f:
                        if line.upper().startswith("REP_EXCHANGE="):
                                REP_EXCHANGE = "".join((line.rsplit('=')[1]).splitlines())
                        elif line.upper().startswith("REP_OUTILAR="):
                                REP_OUTILAR = "".join((line.rsplit('=')[1]).splitlines())
                        elif line.upper().startswith("TYPE_SERVEUR="):
                                TYPE_SERVEUR = "".join((line.upper().rsplit('=')[1]).splitlines())

        fic_depot_dir = REP_OUTILAR + "/list_depot_fic_exchange"
        with open(fic_depot_dir, "r") as f:
                for line in f:
                        if line.lower().startswith(type_depot_bis):
                                REP_WORK = "".join((line.rsplit(';')[1]).splitlines())


        REP_WORK_BIS = REP_WORK.upper() + "="
        with open(file_temp, "r") as f:
                for line in f:
                        if line.upper().startswith(REP_WORK_BIS):
                                REP_OUT = "".join((line.rsplit('=')[1]).splitlines())

        if TYPE_SERVEUR == "NOMINAL1":
                fic_exchange = REP_EXCHANGE + "/fic_exchange_" + type_depot.lower() + "_nominal1"
        else:
                fic_exchange = REP_EXCHANGE + "/fic_exchange_" + type_depot.lower() + "_nominal2"

        f_in = REP_OUT + "/" + filename
        f_out = REP_EXCHANGE + "/" + filename

        copy2(f_in, f_out)

        with open(fic_exchange, "w") as f:
                f.write(filename)

