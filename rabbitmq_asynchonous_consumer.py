# -*- coding: utf-8 -*-
# pylint: disable=C0111,C0103,R0205

import functools
import logging
import time
import pika
import sys
import importlib
import os

LOG_FORMAT = ('%(levelname) -10s %(asctime)s %(name) -30s %(funcName) '
              '-35s %(lineno) -5d: %(message)s')
LOGGER = logging.getLogger(__name__)

nb_params = len(sys.argv)
if nb_params == 1:
    print("Please set a variable to this script")
    exit()
elif nb_params == 2:
    params = sys.argv[1]
    file = params + ".py"
    file_absolu = "d:\\data\\module\\params\\" + file
    if not os.path.isfile(file_absolu):
        print("")
        print("The file {} is not found on the params directory".format(file))
        exit()
else:
    print("")
    print("There is too many variables set to this script")
    print("    --> Please set only one variable !")
    exit()


class RabbitmqConsumer(object):
    """
    Définition de la classe du consumer qui gérera des interactions inattendues
    avec RabbitMQ, tels que les fermetures de canaux et de connexion.
    Si RabbitMQ ferme la connexion, la classe s'arrête et indiquera q'une demande
    de reconnexion est nécessaire (autorisations, délais d'attente du socket).
    Si le canal est fermé, la classe indiquera un problème avec des commandes qui
    ont été émises.
    """
    #from rabbitmq_config import name_exchange, name_exchange_type, name_queue, value_routing_key
    #import importlib
    module = importlib.import_module(params)
    locals().update(module.__dict__)
    EXCHANGE = name_exchange
    EXCHANGE_TYPE = name_exchange_type
    QUEUE = name_queue
    ROUTING_KEY = value_routing_key

    def __init__(self, amqp_url):
        """
        Création d'une nouvelle instance de la classe, consumer en passant, dans le AMQP,
        pour se reconnecter à RabbitMQ, le paramètre :paramstr.
        amqp_url = l'URL AMQP avec laquelle se reconnteter
        """
        module = importlib.import_module(params)
        locals().update(module.__dict__)
        self.should_reconnect = False
        self.was_consuming = False

        self._connection = None
        self._channel = None
        self._closing = False
        self._consumer_tag = None
        self._url = amqp_url
        self._consuming = False
        
        # la valeur du prefetch_count est a augmenter pour un débit
        # plus élevé du consumer
        #from rabbitmq_config import value_prefetch_count
        self._prefetch_count = value_prefetch_count

    def connect(self):
        """
        Cette méthode se connecte à RabbitMQ et retourne le descripteur de
        connexion. Lorsque la connexion est établie, la méthode _on_connection_open
        est invoqué par pika.SelectConnextion
        """
        label = "Connecting to {}".format(self._url)
        LOGGER.info(label)
        return pika.SelectConnection(
                                       parameters=pika.URLParameters(self._url)
                                     , on_open_callback=self.on_connection_open
                                     , on_open_error_callback=self.on_connection_open_error
                                     , on_close_callback=self.on_connection_closed
                                    )

    def close_connection(self):
        self._consuming = False
        if self._connection.is_closing or self._connection.is_closed:
            label = "Connection is closing or already closed"
            LOGGER.info(label)
        else:
            label = "Closing connection"
            LOGGER.info(label)
            self._connection.close()

    def on_connection_open(self, _unused_connection):
        """
        Cette méthode est appelée par PIKA une fois la connexion établie à
        RabbitMQ. Elle passe le handle à l'objet de connexion
        """
        label = "Connection opened"
        LOGGER.info(label)
        self.open_channel()

    def on_connection_open_error(self, _unused_connection, err):
        """
        Cette méthode est appelée par PIKA si la connexion à RabbitmQ ne peut
        pas être établie.
        """
        label = "Connection open failed: {}".format(err)
        LOGGER.error(label)
        self.reconnect()

    def on_connection_closed(self, _unused_connection, reason):
        """
        Cette méthode est appelée par PIKA lorsque la connexion est fermée de
        manière innatendue. Ainsi, la demande de reconnexion est lancée.
        """
        self._channel = None
        if self._closing:
            self._connection.ioloop.stop()
        else:
            label = "Connection closed, reconnect necessary: {}".format(reason)
            LOGGER.warning(label)
            self.reconnect()

    def reconnect(self):
        """
        Cette méthode est invoquée si la connexion ne peut pas être "ouverte" ou
        est "fermée". On indique que la reconnexion est nécessaire, et la boucle
        ioloop est arrêtée.
        """
        self.should_reconnect = True
        self.stop()

    def open_channel(self):
        """
        Ouverture d'un nouveau canal avec RabbitMQ en transférant le RPC
        Channel.Open.Commander. Si RabbitMQ répond que le canal est ouver, le
        rappel on_channel_open est appelé par PIKA
        """
        label = "Creating a new channel"
        LOGGER.info(label)
        self._connection.channel(on_open_callback=self.on_channel_open)

    def on_channel_open(self, channel):
        """
        Cette méthode est appelée par PIKA lorsque le canal a été ouvert.
        L'objet du canal est appelé en paramètre pour l'utiliser.
        Comme la canal est ouvert, il faut déclarer que l'EXCHANGE est en
        utilisation.
        """
        label = "Channel opened"
        LOGGER.info(label)
        self._channel = channel
        self.add_on_channel_close_callback()
        self.setup_exchange(self.EXCHANGE)

    def add_on_channel_close_callback(self):
        """
        Cette méthode dit à PIKA d'appeler la méthode on_channel_closed
        si RabbitMQ ferme le canal inopinément.
        """
        label = "Adding channel close callback"
        LOGGER.info(label)
        self._channel.add_on_close_callback(self.on_channel_closed)

    def on_channel_closed(self, channel, reason):
        """
        Cette méthode est appelée par PIKA lorsque le canal est fermé
        inopinément. Les canaux sont fermés si la tentative viole le protocole,
        comme re-déclarer un EXCHANGE ou une QUEUE d'attente avec différents paramètres.
        Dans ce cas, la connexion est fermée pour stoper l'objet.
        """
        label = "Channel {} was closed: {}".format(channel, reason)
        LOGGER.warning(label)
        self.close_connection()

    def setup_exchange(self, exchange_name):
        """
        Cette méthode configure l'EXCHANGE sur RabbitMQ en appelant la RPC commande
        exchange.declare. Une fois terminée, la méthode on_exchange_declareok est
        invoqué par PIKA
        """
        label = "Declarting exchange: {}".format(exchange_name)
        LOGGER.info(label)

        cb = functools.partial(
                                 self.on_exchange_declareok
                               , userdata=exchange_name
                              )
        self._channel.exchange_declare(
                                         exchange=exchange_name
                                       , exchange_type=self.EXCHANGE_TYPE
                                       , callback=cb
                                      )

    def on_exchange_declareok(self, _unused_frame, userdata):
        """
        Cette méthode est appelée par PIKA lorsque RabbitMQ a terminé la RPC commande
        exchange.declare
        """
        label = "Exchange declared: {}".format(userdata)
        LOGGER.info(label)
        self.setup_queue(self.QUEUE)

    def setup_queue(self, queue_name):
        """
        Cette méthode configure la QUEUE d'attente sur RabbitMQ via la RPC commande
        queue.declare. Une fois terminée, la méthode on_queue_declareok est invoquée par PIKA.
        """
        label = "Declaring queue {}".format(queue_name)
        LOGGER.info(label)
        cb = functools.partial(
                                 self.on_queue_declareok
                               , userdata=queue_name
                              )
        self._channel.queue_declare(
                                      queue=queue_name
                                    , callback=cb
                                   )

    def on_queue_declareok(self, _unused_frame, userdata):
        """
        Cette méthode est invoquée par PIKA lors de l'appel à la RPC commande queue.declare,
        dans setup_queue qui est terminée.
        Dans cetre méthode, il faut lier la QUEUE d'attente à l'EXCHANGE avec la ROUTING_KEY
        via l'appel de la RPC commande queue_bind.
        Lorsque cette commande est terminée, la méthode on_bindok est invoqué par PIKA
        """
        queue_name = userdata
        label = "Binding {} to {} with {}".format(self.EXCHANGE, queue_name, self.ROUTING_KEY)
        LOGGER.info(label)
        cb = functools.partial(
                                 self.on_bindok
                               , userdata=queue_name
                              )
        self._channel.queue_bind(
                                   queue_name
                                 , self.EXCHANGE
                                 , routing_key=self.ROUTING_KEY
                                 , callback=cb
                                )

    def on_bindok(self, _unused_frame, userdata):
        """
        Cetté méthode est appelée par PIKA, lorsque la méthode queue.bind est
        terminée. Après, il faut définir le nombre de pré-lecture pour le canal.
        """
        label = "Queue bound: {}".format(userdata)
        LOGGER.info(label)
        self.set_qos()

    def set_qos(self):
        """
        Cette méthode configure uniquement la pré-lecture du consumer.
        Si prefetch_count est à 1, alors il y a aura lecture d'un message à la fois.
        Le consumer doit avoir traiter ce message avant que RabbitMQ n'en délivre un autre.
        Le paramètre prefetch_count peut être plus grand, pour améliorer les perfs
        """
        self._channel.basic_qos(
                                  prefetch_count=self._prefetch_count
                                , callback=self.on_basic_qos_ok
                               )

    def on_basic_qos_ok(self, _unused_frame):
        """
        Cette méthode est appelée par PIKA lorsque la méthode basic.qos est
        terminée. Après, il faut commencer à consommer des messages en invoquant
        start_consuming(), méthode qui exécute les commandes RPC nécessaires
        pour lancer le processus.
        """
        label = "QOS set to: {}".format(self._prefetch_count)
        LOGGER.info(label)
        self.start_consuming()

    def start_consuming(self):
        """
        Cette méthode configure le consumer en appelant d'abbord add_on_cancel_callback
        pour que l'object soit notifié, si RabbitMQ annule le consumer.
        Il émet ensuite la commande RPC basic.consume qui renvoie la balise consumer utilisée
        pour idéntifier, de manière unique, le consumer avec RabbitMQ.
        La méthode on_message est transmise, tant que PIKA sera invoqué lorsqu'un message est
        entièrement reçu.
        """
        label = "Issuing consumer related RPC commands"
        LOGGER.info(label)
        self.add_on_cancel_callback()
        self._consumer_tag = self._channel.basic_consume(
                                                           self.QUEUE
                                                         , self.on_message
                                                        )
        self.was_consuming = True
        self._consuming = True

    def add_on_cancel_callback(self):
        """
        Cette méthode ajoute un rappel qui sera exécuté si RabbitMQ annule le consumer, selon
        certaines raisons. Si RabbitMQ annule le consumer, on_consumer.cancelled est invoquée
        par PIKA
        """
        label = "Adding consumer cancellation callback"
        LOGGER.info(label)
        self._channel.add_on_cancel_callback(self.on_consumer_cancelled)

    def on_consumer_cancelled(self, method_frame):
        """
        Cette méthode est appelée par PIKA lorsque RabbitMQ envoi une commande basic.cancel,
        pour que le consumer consomme des messages
        """
        label = "Consumer was cancelled remotely, shutting down: {}".format(method_frame)
        LOGGER.info(label)
        if self._channel:
            self._channel.close()

    def on_message(self, _unused_channel, basic_deliver, properties, body):
        """
        Cette méthode est invoquée par PIKA lorsqu'un message est remis par RabbitMQ.
        Le canal est passé par commoditié. La commande basic_deliver passée porte
        l'EXCHANGE, l'étiquette de livraison, et un flag remis à nouveau pour le message.
        La propriété transmise est une instance de BasicPropertiers avec les caractéristiques
        du message et le corps du message qui a été envoyé
        """
        label = "Received message # {} from {}: {}".format(basic_deliver.delivery_tag, properties.app_id, body)
        LOGGER.info(label)
        self.acknowledge_message(basic_deliver.delivery_tag)

    def acknowledge_message(self, delivery_tag):
        """
        Cette méthode accuse réception du message de RabbitMQ, en envoyant un message basic.ack.
        """
        label = "Acknowledging message {}".format(delivery_tag)
        LOGGER.info(label)
        self._channel.basic_ack(delivery_tag)

    def stop_consuming(self):
        """
        Cette méthode dit à RabbitMQ d'arrêter le consumer, en envoyant une commande RPC basic.cancel 
        """
        if self._channel:
            label = "Sending a Basic.Cancel RPC command to RabbitMQ"
            LOGGER.info(label)
            cb = functools.partial(
                                     self.on_cancelok
                                   , userdata=self._consumer_tag
                                  )
            self._channel.basic_cancel(
                                         self._consumer_tag
                                       , cb
                                      )

    def on_cancelok(self, _unused_frame, userdata):
        """
        Cette méthode est invoquée par PIKA lorsque RabbitMQ acquitte l'annulation d'un
        consumer. A ce stade, le canal doit être fermé.
        Ceci inviquera la méthode on_channel_closed, une fois que le canal est fermé.
        Ce qui fermera à son tour la connexion.
        """
        self._consuming = False
        label = "RabbitMQ acknowledged the cancellation of the consumer: {}".format(userdata)
        LOGGER.info(label)
        self.close_channel()

    def close_channel(self):
        """
        Cette méthode est appelée pour fermer le canal avec RabbitMQ proprement, en
        émettant la commande channel.close RPC
        """
        label = "Closing the channel"
        LOGGER.info(label)
        self._channel.close()

    def run(self):
        """
        Cette méthode exécute le consumer, en se connectant à RabbitMQ, puis lance
        la boucle ioloop, pour permettre à SelectConnection de fonctionner.
        """
        self._connection = self.connect()
        self._connection.ioloop.start()

    def stop(self):
        """
        Cette méthode ferme proprement la connexion à RabbitMQ, en arrêtant le consumer.
        Lorsque RabbitMQ confirme l'annulation, on_cancelok est invoqué par PIKA, qui ferme
        ensuuite le canal et la connexion. Ioloop est redémarrée, car cette méthode est appelée.
        Lorsque un CTRL-C est effectuée, une exception KeyboardInterrupt est déclenchée. Cette
        exception arrête ioloop qui est en cours d'exécution.
        """
        if not self._closing:
            self._closing = True
            label = "Stopping"
            LOGGER.info(label)
            if self._consuming:
                self.stop_consuming()
                self._connection.ioloop.start()
            else:
                self._connection.ioloop.stop()
            label = "Stopped"
            LOGGER.info(label)


class ReconnectingRabbitmqConsumer(object):
    """
    Cette classe est un consumer qui se reconnecte si le message imbriqué RabbitmqConsumer
    indique qu'une reconnexion est nécessaire.
    """

    def __init__(self, amqp_url):
        self._reconnect_delay = 0
        self._amqp_url = amqp_url
        self._consumer = RabbitmqConsumer(self._amqp_url)

    def run(self):
        while True:
            try:
                self._consumer.run()
            except KeyboardInterrupt:
                self._consumer.stop()
                break
            self._maybe_reconnect()

    def _maybe_reconnect(self):
        if self._consumer.should_reconnect:
            self._consumer.stop()
            reconnect_delay = self._get_reconnect_delay()
            label = "Reconnecting after {} seconds".format(reconnect_delay)
            LOGGER.info(label)
            time.sleep(reconnect_delay)
            self._consumer = RabbitmqConsumer(self._amqp_url)

    def _get_reconnect_delay(self):
        if self._consumer.was_consuming:
            self._reconnect_delay = 0
        else:
            self._reconnect_delay += 1
        if self._reconnect_delay > 30:
            self._reconnect_delay = 30
        return self._reconnect_delay


def main():

    module = importlib.import_module(params)
    globals().update(module.__dict__)
    #from rabbitmq_config import logger_level_consumer
    #from rabbitmq_config import port_rabbitmq, address_rabbitmq
    #from rabbitmq_config import user_login, passwd_login
    if logger_level_consumer.upper() == "DEBUG":
        logging.basicConfig(level=logging.DEBUG, format=LOG_FORMAT)
    elif logger_level_consumer.upper() == "INFO":
        logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)
    elif logger_level_consumer.upper() == "WARN":
        logging.basicConfig(level=logging.WARN, format=LOG_FORMAT)
    elif logger_level_consumer.upper() == "ERROR":
        logging.basicConfig(level=logging.ERROR, format=LOG_FORMAT)
    elif logger_level_consumer.upper() == "CRITICAL":
        logging.basicConfig(level=logging.CRITICAL, format=LOG_FORMAT)
    else:
        logging.basicConfig(level=logging.NOTSET, format=LOG_FORMAT)

    #amqp_url = 'amqp://guest:guest@localhost:5672/%2F'
    amqp_url = 'amqp://' + user_login + ':' + passwd_login + '@' + address_rabbitmq + ':' + port_rabbitmq + '/%2F'
    print(amqp_url)

    consumer = ReconnectingRabbitmqConsumer(amqp_url)
    consumer.run()


if __name__ == '__main__':
    main()