class Personne:
    """Classe représentant une personne"""
    def __init__(self, nom):
        """Constructeur de notre classe"""
        self.nom = nom
        self.prenom = "James"
    def __str__(self):
        """Méthode appelée lors d'une conversion de l'objet en chaîne"""
        return "{0} {1}".format(self.prenom, self.nom)

class AgentSpecial(Personne):
    """Classe définissant un agent spécial.
    Elle hérite de la classe Personne"""
    
    def __init__(self, nom, matricule):
        """Un agent se définit par son nom et son matricule"""
        self.nom = nom
        self.matricule = matricule
    def __str__(self):
        """Méthode appelée lors d'une conversion de l'objet en chaîne"""
        return "Agent {0}, matricule {1}".format(self.nom, self.matricule)

class AgentSpecial2(Personne):
    """Classe définissant un agent spécial.
    Elle hérite de la classe Personne"""
    
    def __init__(self, nom, matricule):
        """Un agent se définit par son nom et son matricule"""
        """Un agent se définit par son nom et son matricule"""
        # On appelle explicitement le constructeur de Personne :
        Personne.__init__(self, nom)
        self.matricule = matricule
    def __str__(self):
        """Méthode appelée lors d'une conversion de l'objet en chaîne"""
        return "Agent {0}, matricule {1}".format(self.nom, self.matricule)

agent = AgentSpecial("Bond", "007")
print(agent.nom)

#print(agent.prenom)
#Traceback (most recent call last):
#  File "<stdin>", line 1, in <module>
#AttributeError: 'AgentSpecial' object has no attribute 'prenom'

# --> Pour avoir le nom il faut déclarer l'appel à la classe mère, dans la méthode __init__ (constructeur) de la classe fille
#      modif faites dans AgentSpecial2
print("")
print("")
agent = AgentSpecial2("Bond", "007")
print(agent.nom)
print(agent.prenom)
print(agent)

print("")
print("AgentSpecial2 hérite-t-elle de Personne?") 
print(issubclass(AgentSpecial2, Personne))
print("AgentSpecial2 est-elle un objet?")
print(issubclass(AgentSpecial2, object))
print("Personne est-elle un objet?")
print(issubclass(Personne, object))
print("Personne hérite-t-elle de AgentSpecial2?")
print(issubclass(Personne, AgentSpecial2))

print("")
print("L'objet créé agent est-il une instance de AgentSpecial?")
print(isinstance(agent, AgentSpecial))
print("L'objet créé agent est-il une instance de AgentSecial2?")
print(isinstance(agent, AgentSpecial2))
print("L'objet créé agent est une instance héritée de Personne?")
print(isinstance(agent, Personne))