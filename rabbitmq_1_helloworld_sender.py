import pika

# Script de test, envoi du message "Hello World", à la queue Hello
connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='hello')

channel.basic_publish(
                        exchange=''
                      , routing_key='hello'
                      , body='Hello World!'
                     )

print(" [x] Sent 'Hello World!'")

connection.close()