import os, sys

x = len(sys.argv)
if x == 1:
    print("")
    print("Paramètres manquants :")
    print("    - Param 1 : nb lettre (integer)")
    print("    - Param 2 : fichier a traiter (char)")
    exit()
elif x == 2:
    print("")
    n = sys.argv[1]
    try:
        n = int(n)
    except ValueError:
        pass
    p = isinstance(n,int)
    if str(p) == "False":
        print("Le paramètre numéro 1 passé n'est pas un integer")
        exit()
        
    print("Paramètre numéro 2 manquant :")
    print("    - Param 2 : fichier a traiter (char)")
    exit()
elif x == 3:
    n = sys.argv[1]
    fichier = sys.argv[2]

    try:
        n = int(n)
    except ValueError:
        pass
    p = isinstance(n,int)
    if str(p) == "False":
        print("")
        print("La paramètre numéro 1 passé n'est pas un integer")
        exit()

    sys.path.insert(0, 'd:\\Python')
    t = os.path.isfile(fichier)
    if str(t) == "False":
        print("")
        print("Le fichier {} n'existe pas dans le répertoire d:\\Python".format(fichier))
        exit()
else:
    print("")
    print("Trop de Paramètres :")
    print("    - Param 1 : nb lettre (integer)")
    print("    - Param 2 : fichier a traiter (char)")
    exit()


def compte_mots_n_lettres(nb_char, var_seq):
    dict_mot = {}

    # constitution de la liste des indices à lire en fonction du paramètre nb lettre
    # exemple : si n = 4 , alors la liste est [0,1,2]
    i1 = 0
    list_nb1 = [] 
    while i1 < (nb_char - 1):
        list_nb1.append(i1)
        i1 += 1

    # constitution de la liste des indices en cours sur la séquence en fonction du positionnement
    # exemple : si n = 4 et i2 = 0, rien à lire
    #           si n = 4 et i2 = 1, rien à lire
    #           si n = 4 et i2 = 2, rien à lire
    #           si n = 4 et i2 = 3, liste des indices est [0,1,2,3)
    #           si n = 4 et i2 = 4, liste des incides est (1,2,3,4)
    #           si n = 4 et i2 = 5, liste des indices est (2,3,4,5)
    #           ... etc ...
    i2 = 0
    while i2 < len(var_seq):
        list_nb2 = []

        if i2 not in list_nb1:
            fin = 0
            var = i2
            nb_tour = 1
            while fin == 0:
                list_nb2.append(var)
                var = var - 1
                nb_tour += 1
                #si on dépasse le nb de lettre paramètre, il faut arrêter la lecture
                if nb_tour == (nb_char + 1) :
                    fin = 1
                else:
                    # si l'indice lu est 0, il faut arrêter la lecture
                    if var == -1:
                        fin = 1
            
            # reeordnner la liste finale 
            list_nb2.sort()

            # constitution de la chaine de n lettres à lire
            indice = 0
            while indice < len(list_nb2):
                if indice == 0:
                    mot = var_seq[list_nb2[indice]]
                else:
                    mot = mot + var_seq[list_nb2[indice]]
                indice += 1

            # constitution du dictionnaire
            if len(dict_mot) == 0:
                dict_mot[mot] = 1
            else:
                if mot in dict_mot:
                    value_key = dict_mot[mot]
                    value_key += 1
                    dict_mot[mot] = value_key
                else:
                    dict_mot[mot] = 1
        i2 += 1
    
    return dict_mot

def lit_fasta(nom_fichier):
    prot_dict = {}
    with open(nom_fichier, "r") as fasta_file:
        prot_id = ""
        for line in fasta_file:
            if line.startswith(">"):
                prot_id = line[1:].split()[0] + " " + line[1:].split()[1] + " " + line[1:].split()[2]
                prot_dict[prot_id] = ""
            else:
                prot_dict[prot_id] += line.strip()
        for id in prot_dict:
            #return(id, prot_dict[id][:30])
            return(id, prot_dict[id])


id_sequence, base_sequence = lit_fasta(fichier)
dict_mot_seq = compte_mots_n_lettres(n, base_sequence)

print("")
print("Fichier FASTA en cours d'analyse : {}".format(fichier))
print("Séquence aminée                  : {}".format(id_sequence))
print("Nb Lettres parametre             : {}".format(n))
print("")
print("Mots de {} lettres".format(n))
for key in dict_mot_seq:
    name_key = key
    value_key = dict_mot_seq[key]
    print("{} : {}".format(name_key, value_key))