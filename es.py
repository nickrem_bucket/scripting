from elasticsearch import Elasticsearch 

# Connexion au cluster elastic (addresse http)
es = Elasticsearch([{'host':'localhost','port':9200}])

# - index    = table
# - doc_type = mapping de la table
# - 3eme option du es.get = clé de recherche
"""res = es.get(index='movies2', doc_type='movie', id=3)
print(res)"""

    #D:\Python>python3 es.py
    #{	  '_index': 'movies2'
    #    , '_type': 'movie'
    #    , '_id': '3'
    #    , '_version': 1
    #    , '_seq_no': 2
    #    , '_primary_term': 1
    #    , 'found': True
    #    , '_source':	{	  'fields':	{	  'directors': ['Denis Villeneuve']
    #                                        , 'release_date': '2013-08-30T00:00:00Z'
    #                                        , 'rating': 8.2
    #                                        , 'genres': ['Crime', 'Drama', 'Thriller']
    #                                        , 'image_url': 'http://ia.media-imdb.com/images/M/MV5BMTg0NTIzMjQ1NV5BMl5BanBnXkFtZTcwNDc3MzM5OQ@@._V1_SX400_.jpg'
    #                                        , 'plot': "When Keller Dover's daughter and her friend go missing, he takes matters into his own hands as the police pursue multiple leads and the pressure mounts. But just how far will this desperate father go to protect his family?"
    #                                        , 'title': 'Prisoners'
    #                                        , 'rank': 3
    #                                        , 'running_time_secs': 9180
    #                                        , 'actors': ['Hugh Jackman', 'Jake Gyllenhaal', 'Viola Davis']
    #                                        , 'year': 2013
    #                                    }
    #                        , 'id': 'tt1392214'
    #                , 'type': 'add'
    #            }
    #}"""

"""res = es.get(index='movies2',doc_type='movie',id=3)
title = res['_source']['fields']['title']
year = res['_source']['fields']['year']
print("Title : {}".format(title))
print("   - Year      : {}".format(year))
i = 0
while i < len(res['_source']['fields']['directors']):
    director = res['_source']['fields']['directors'][i]
    if i == 0:
        if len(res['_source']['fields']['directors']) > 1:
            print("   - Directors : {}".format(director))
        else:
            print("   - Director  : {}".format(director))
    else:
        print("                 {}".format(director))
    i += 1
j = 0
while j < len(res['_source']['fields']['actors']):
    actor = res['_source']['fields']['actors'][j]
    if j == 0:
        if len(res['_source']['fields']['actors']) > 1:
            print("   - Actors    : {}".format(actor))
        else:
            print("   - Actor     : {}".format(actor))
    else:
            print("                 {}".format(actor))
    j += 1

print("")"""

    #D:\Python>python3 es.py
    #Title : Prisoners
    #    - Year      : 2013
    #    - Director  : Denis Villeneuve
    #    - Actors    : Hugh Jackman
    #                  Jake Gyllenhaal
    #                  Viola Davis


# - index    = table
# - doc_type = mapping de la table
# - body     = requete a exécuter
"""res = es.search(index='movies2', body=
        {
         'query': {
                   'match':{ "fields.title":"Star Wars" }
                  }
        }
)
l = len(res['hits']['hits'])
print("Nb enreg. : {}".format(l))
print("")
i = 0
while i < len(res['hits']['hits']):
    title = res['hits']['hits'][i]['_source']['fields']['title']
    year = res['hits']['hits'][i]['_source']['fields']['year']
    print("Title : {}".format(title))
    print("   - Year      : {}".format(year))
    j = 0
    while j < len(res['hits']['hits'][i]['_source']['fields']['directors']):
        director = res['hits']['hits'][i]['_source']['fields']['directors'][j]
        if j == 0:
            if len(res['hits']['hits'][i]['_source']['fields']['directors']) > 1:
                print("   - Directors : {}".format(director))
            else:
                print("   - Director  : {}".format(director))
        else:
            print("                 {}".format(director))
        j += 1
    k = 0
    while k < len(res['hits']['hits'][i]['_source']['fields']['actors']):
        actor = res['hits']['hits'][i]['_source']['fields']['actors'][k]
        if k == 0:
            if len(res['hits']['hits'][i]['_source']['fields']['actors']) > 1:
                print("   - Actors    : {}".format(actor))
            else:
                print("   - Actor     : {}".format(actor))
        else:
            print("                 {}".format(actor))
        k += 1
    i += 1
    print("")"""

    #D:\Python>python3 es.py
    #Nb enreg. : 10
    #
    #Title : Star Wars
    #    - Year      : 1977
    #    - Director  : George Lucas
    #    - Actors    : Mark Hamill
    #                  Harrison Ford
    #                  Carrie Fisher
    #
    #Title : Star Wars: Episode VII
    #   - Year      : 2015
    #   - Director  : J.J. Abrams
    #   - Actors    : Mark Hamill
    #                 Harrison Ford
    #                 Carrie Fisher
    #
    #Title : Bride Wars
    #   - Year      : 2009
    #   - Director  : Gary Winick
    #   - Actors    : Kate Hudson
    #                 Anne Hathaway
    #                 Candice Bergen
    #
    #Title : Star Wars: Episode I - The Phantom Menace
    #   - Year      : 1999
    #   - Director  : George Lucas
    #   - Actors    : Ewan McGregor
    #                 Liam Neeson
    #                 Natalie Portman
    #
    #Title : Star Wars: Episode III - Revenge of the Sith
    #   - Year      : 2005
    #   - Director  : George Lucas
    #   - Actors    : Hayden Christensen
    #                 Natalie Portman
    #                 Ewan McGregor
    #
    #Title : Star Wars: Episode V - The Empire Strikes Back
    #   - Year      : 1980
    #   - Director  : Irvin Kershner
    #   - Actors    : Mark Hamill
    #                 Harrison Ford
    #                 Carrie Fisher
    #
    #Title : Star Wars: Episode VI - Return of the Jedi
    #   - Year      : 1983
    #   - Director  : Richard Marquand
    #   - Actors    : Mark Hamill
    #                 Harrison Ford
    #                 Carrie Fisher
    #
    #Title : Star Wars: Episode II - Attack of the Clones
    #   - Year      : 2002
    #   - Director  : George Lucas
    #   - Actors    : Hayden Christensen
    #                 Natalie Portman
    #                 Ewan McGregor
    #
    #Title : Hollywood Sex Wars
    #   - Year      : 2011
    #   - Director  : Paul Sapiano
    #   - Actors    : Mario Diaz
    #                 Eli Jane
    #                 Jenae Altschwager
    #
    #Title : Star Trek
    #   - Year      : 2009
    #   - Director  : J.J. Abrams
    #   - Actors    : Chris Pine
    #                 Zachary Quinto
    #                 Simon Pegg

# - index    = table
# - doc_type = mapping de la table
# - body     = requete a exécuter
"""res = es.search(index='movies2', body=
        {
         "query": {
                   "bool": {
                            "should": [
                                         { "match": { "fields.title": "Star Wars" }}
                                       , { "match": { "fields.directors": "George Lucas" }}
                                      ]
                           }
                   }
        }
        )

#print(res)
l = len(res['hits']['hits'])
print("Nb enreg. : {}".format(l))
print("")
i = 0
while i < len(res['hits']['hits']):
    title = res['hits']['hits'][i]['_source']['fields']['title']
    year = res['hits']['hits'][i]['_source']['fields']['year']
    print("Title : {}".format(title))
    print("   - Year      : {}".format(year))
    j = 0
    while j < len(res['hits']['hits'][i]['_source']['fields']['directors']):
        director = res['hits']['hits'][i]['_source']['fields']['directors'][j]
        if j == 0:
            if len(res['hits']['hits'][i]['_source']['fields']['directors']) > 1:
                print("   - Directors : {}".format(director))
            else:
                print("   - Director  : {}".format(director))
        else:
            print("                 {}".format(director))
        j += 1
    k = 0
    while k < len(res['hits']['hits'][i]['_source']['fields']['actors']):
        actor = res['hits']['hits'][i]['_source']['fields']['actors'][k]
        if k == 0:
            if len(res['hits']['hits'][i]['_source']['fields']['actors']) > 1:
                print("   - Actors    : {}".format(actor))
            else:
                print("   - Actor     : {}".format(actor))
        else:
            print("                 {}".format(actor))
        k += 1
    i += 1
    print("")"""

    #D:\Python>python3 es.py
    #Nb enreg. : 10
    #
    #Title : Star Wars
    #   - Year      : 1977
    #   - Director  : George Lucas
    #   - Actors    : Mark Hamill
    #                 Harrison Ford
    #                 Carrie Fisher
    #
    #Title : Star Wars: Episode I - The Phantom Menace
    #   - Year      : 1999
    #   - Director  : George Lucas
    #   - Actors    : Ewan McGregor
    #                 Liam Neeson
    #                 Natalie Portman
    #
    #Title : Star Wars: Episode III - Revenge of the Sith
    #   - Year      : 2005
    #   - Director  : George Lucas
    #   - Actors    : Hayden Christensen
    #                 Natalie Portman
    #                 Ewan McGregor
    #
    #Title : Star Wars: Episode II - Attack of the Clones
    #   - Year      : 2002
    #   - Director  : George Lucas
    #   - Actors    : Hayden Christensen
    #                 Natalie Portman
    #                 Ewan McGregor
    #
    #Title : American Graffiti
    #   - Year      : 1973
    #   - Director  : George Lucas
    #   - Actors    : Richard Dreyfuss
    #                 Ron Howard
    #                 Paul Le Mat
    #
    #Title : THX 1138
    #   - Year      : 1971
    #   - Director  : George Lucas
    #   - Actors    : Robert Duvall
    #                 Donald Pleasence
    #                 Don Pedro Colley
    #
    #Title : Star Wars: Episode VII
    #   - Year      : 2015
    #   - Director  : J.J. Abrams
    #   - Actors    : Mark Hamill
    #                 Harrison Ford
    #                 Carrie Fisher
    #
    #Title : Bride Wars
    #   - Year      : 2009
    #   - Director  : Gary Winick
    #   - Actors    : Kate Hudson
    #                 Anne Hathaway
    #                 Candice Bergen
    #
    #Title : Star Wars: Episode V - The Empire Strikes Back
    #   - Year      : 1980
    #   - Director  : Irvin Kershner
    #   - Actors    : Mark Hamill
    #                 Harrison Ford
    #                 Carrie Fisher
    #
    #Title : Star Wars: Episode VI - Return of the Jedi
    #   - Year      : 1983
    #   - Director  : Richard Marquand
    #   - Actors    : Mark Hamill
    #                 Harrison Ford
    #                 Carrie Fisher
    
    # La requête est booléenne pour laquelle le document devrait (should) avoir le titre "Star Wars" et le réalisateur "George Lucas"
    # La subtilité vient du fait que le filtre conditionnel produisant ainsi un score et triant le résultat sur celui-ci.
    # pour être plus restrictif et obliger le nom du director, il faut effectuer une requête de type "must".

# - index    = table
# - doc_type = mapping de la table
# - body     = requete a exécuter
"""res = es.search(index='movies2', body=
        {
         "query": {
                   "bool": {
                              "should": { "match": { "fields.title": "Star Wars" }}
                            , "must" :  { "match": { "fields.directors": "George Lucas" }}
                           }
                  }
        }
)

#print(res)
l = len(res['hits']['hits'])
print("Nb enreg. : {}".format(l))
print("")
i = 0
while i < len(res['hits']['hits']):
    title = res['hits']['hits'][i]['_source']['fields']['title']
    year = res['hits']['hits'][i]['_source']['fields']['year']
    print("Title : {}".format(title))
    print("   - Year      : {}".format(year))
    j = 0
    while j < len(res['hits']['hits'][i]['_source']['fields']['directors']):
        director = res['hits']['hits'][i]['_source']['fields']['directors'][j]
        if j == 0:
            if len(res['hits']['hits'][i]['_source']['fields']['directors']) > 1:
                print("   - Directors : {}".format(director))
            else:
                print("   - Director  : {}".format(director))
        else:
            print("                 {}".format(director))
        j += 1
    k = 0
    while k < len(res['hits']['hits'][i]['_source']['fields']['actors']):
        actor = res['hits']['hits'][i]['_source']['fields']['actors'][k]
        if k == 0:
            if len(res['hits']['hits'][i]['_source']['fields']['actors']) > 1:
                print("   - Actors    : {}".format(actor))
            else:
                print("   - Actor     : {}".format(actor))
        else:
            print("                 {}".format(actor))
        k += 1
    i += 1
    print("")"""

    #D:\Python>python3 es.py
    #Nb enreg. : 10
    #
    #Title : Star Wars
    #   - Year      : 1977
    #   - Director  : George Lucas
    #   - Actors    : Mark Hamill
    #                Harrison Ford
    #                Carrie Fisher
    #
    #Title : Star Wars: Episode I - The Phantom Menace
    #   - Year      : 1999
    #   - Director  : George Lucas
    #   - Actors    : Ewan McGregor
    #                 Liam Neeson
    #                 Natalie Portman
    #
    #Title : Star Wars: Episode III - Revenge of the Sith
    #   - Year      : 2005
    #   - Director  : George Lucas
    #   - Actors    : Hayden Christensen
    #                 Natalie Portman
    #                 Ewan McGregor
    #
    #Title : Star Wars: Episode II - Attack of the Clones
    #   - Year      : 2002
    #   - Director  : George Lucas
    #   - Actors    : Hayden Christensen
    #                 Natalie Portman
    #                 Ewan McGregor
    #
    #Title : American Graffiti
    #   - Year      : 1973
    #   - Director  : George Lucas
    #   - Actors    : Richard Dreyfuss
    #                 Ron Howard
    #                 Paul Le Mat
    #
    #Title : THX 1138
    #   - Year      : 1971
    #   - Director  : George Lucas
    #   - Actors    : Robert Duvall
    #                 Donald Pleasence
    #                 Don Pedro Colley
    #
    #Title : Mad Max Beyond Thunderdome
    #   - Year      : 1985
    #   - Directors : George Miller
    #                 George Ogilvie
    #   - Actors    : Mel Gibson
    #                 Tina Turner
    #                 Bruce Spence
    #
    #Title : 21 & Over
    #   - Year      : 2013
    #   - Directors : Jon Lucas
    #                 Scott Moore
    #   - Actors    : Miles Teller
    #                 Justin Chon
    #                 Jonathan Keltz
    #
    #Title : The Monuments Men
    #   - Year      : 2013
    #   - Director  : George Clooney
    #   - Actors    : George Clooney
    #                 Cate Blanchett
    #                 Matt Damon
    #
    #Title : The Ides of March
    #   - Year      : 2011
    #   - Director  : George Clooney
    #   - Actors    : Paul Giamatti
    #                 George Clooney
    #                 Philip Seymour Hoffman

    # Là encore, d’autres films vont apparaître (filtre sur le titre), avec un score beaucoup plus faible.
    # Il est également possible de rechercher des séquences de mots (ou phrases) dans un texte,
    # pour forcer l’ordre de "Star" puis "Wars" avec la clé "match_phrase"

# - index    = table
# - doc_type = mapping de la table
# - body     = requete a exécuter
"""res = es.search(index='movies2',body=
        { "query":
                   {
                     "bool": {
                              "should": [
                                           { "match_phrase": { "fields.title": "Star Wars" }}
                                         , { "match": { "fields.directors": "George Lucas" }}
                                        ]
                             }
                   }
        }
)

#print(res)
l = len(res['hits']['hits'])
print("Nb enreg. : {}".format(l))
print("")
i = 0
while i < len(res['hits']['hits']):
    title = res['hits']['hits'][i]['_source']['fields']['title']
    year = res['hits']['hits'][i]['_source']['fields']['year']
    print("Title : {}".format(title))
    print("   - Year      : {}".format(year))
    j = 0
    while j < len(res['hits']['hits'][i]['_source']['fields']['directors']):
        director = res['hits']['hits'][i]['_source']['fields']['directors'][j]
        if j == 0:
            if len(res['hits']['hits'][i]['_source']['fields']['directors']) > 1:
                print("   - Directors : {}".format(director))
            else:
                print("   - Director  : {}".format(director))
        else:
            print("                 {}".format(director))
        j += 1
    k = 0
    while k < len(res['hits']['hits'][i]['_source']['fields']['actors']):
        actor = res['hits']['hits'][i]['_source']['fields']['actors'][k]
        if k == 0:
            if len(res['hits']['hits'][i]['_source']['fields']['actors']) > 1:
                print("   - Actors    : {}".format(actor))
            else:
                print("   - Actor     : {}".format(actor))
        else:
            print("                 {}".format(actor))
        k += 1
    i += 1
    print("")"""

    #D:\Python>python3 es.py
    #Nb enreg. : 10
    #
    #Title : Star Wars
    #   - Year      : 1977
    #   - Director  : George Lucas
    #   - Actors    : Mark Hamill
    #                 Harrison Ford
    #                 Carrie Fisher
    #
    #Title : Star Wars: Episode I - The Phantom Menace
    #   - Year      : 1999
    #   - Director  : George Lucas
    #   - Actors    : Ewan McGregor
    #                 Liam Neeson
    #                 Natalie Portman
    #
    #Title : Star Wars: Episode III - Revenge of the Sith
    #   - Year      : 2005
    #   - Director  : George Lucas
    #   - Actors    : Hayden Christensen
    #                 Natalie Portman
    #                 Ewan McGregor
    #
    #Title : Star Wars: Episode II - Attack of the Clones
    #   - Year      : 2002
    #   - Director  : George Lucas
    #   - Actors    : Hayden Christensen
    #                 Natalie Portman
    #                 Ewan McGregor
    #
    #Title : American Graffiti
    #   - Year      : 1973
    #   - Director  : George Lucas
    #   - Actors    : Richard Dreyfuss
    #                 Ron Howard
    #                 Paul Le Mat
    #
    #Title : THX 1138
    #   - Year      : 1971
    #   - Director  : George Lucas
    #   - Actors    : Robert Duvall
    #                 Donald Pleasence
    #                 Don Pedro Colley
    #
    #Title : Star Wars: Episode VII
    #   - Year      : 2015
    #   - Director  : J.J. Abrams
    #   - Actors    : Mark Hamill
    #                 Harrison Ford
    #                 Carrie Fisher
    #
    #Title : Star Wars: Episode V - The Empire Strikes Back
    #   - Year      : 1980
    #   - Director  : Irvin Kershner
    #   - Actors    : Mark Hamill
    #                 Harrison Ford
    #                 Carrie Fisher
    #
    #Title : Star Wars: Episode VI - Return of the Jedi
    #   - Year      : 1983
    #   - Director  : Richard Marquand
    #   - Actors    : Mark Hamill
    #                 Harrison Ford
    #                 Carrie Fisher
    #
    #Title : Mad Max Beyond Thunderdome
    #   - Year      : 1985
    #   - Directors : George Miller
    #                 George Ogilvie
    #   - Actors    : Mel Gibson
    #                 Tina Turner
    #                 Bruce Spence

# Ecriture avec l'acteur Harrison Ford, et interdiction du mot Nazis, et de Indiana Jones 5, pas encore sorti
"""res = es.search(index='movies2',body=
        {
         "query": {
                   "bool": {
                            "should": [
                                         { "match_phrase": { "fields.title": "Star Wars" }}
                                       , { "match": { "fields.directors": "George Lucas" }}
                                       , { "match": { "fields.actors": "Harrison Ford" }}
                                      ],
                            "must_not" : [
                                            { "match" : {"fields.plot":"Nazis"}}
                                          , { "match" : {"fields.title": "Indiana Jones 5"}}
                                         ]
                           }
                  }
        }
    )

#print(res)
l = len(res['hits']['hits'])
print("Nb enreg. : {}".format(l))
print("")
i = 0
while i < len(res['hits']['hits']):
    title = res['hits']['hits'][i]['_source']['fields']['title']
    year = res['hits']['hits'][i]['_source']['fields']['year']
    print("Title : {}".format(title))
    print("   - Year      : {}".format(year))
    j = 0
    while j < len(res['hits']['hits'][i]['_source']['fields']['directors']):
        director = res['hits']['hits'][i]['_source']['fields']['directors'][j]
        if j == 0:
            if len(res['hits']['hits'][i]['_source']['fields']['directors']) > 1:
                print("   - Directors : {}".format(director))
            else:
                print("   - Director  : {}".format(director))
        else:
            print("                 {}".format(director))
        j += 1
    k = 0
    while k < len(res['hits']['hits'][i]['_source']['fields']['actors']):
        actor = res['hits']['hits'][i]['_source']['fields']['actors'][k]
        if k == 0:
            if len(res['hits']['hits'][i]['_source']['fields']['actors']) > 1:
                print("   - Actors    : {}".format(actor))
            else:
                print("   - Actor     : {}".format(actor))
        else:
            print("                 {}".format(actor))
        k += 1
    i += 1
    print("")"""

    #D:\Python>python3 es.py
    #Nb enreg. : 10
    #
    #Title : Star Wars
    #   - Year      : 1977
    #   - Director  : George Lucas
    #   - Actors    : Mark Hamill
    #                 Harrison Ford
    #                 Carrie Fisher
    #
    #Title : Star Wars: Episode VII
    #   - Year      : 2015
    #   - Director  : J.J. Abrams
    #   - Actors    : Mark Hamill
    #                 Harrison Ford
    #                 Carrie Fisher
    #
    #Title : Star Wars: Episode I - The Phantom Menace
    #   - Year      : 1999
    #   - Director  : George Lucas
    #   - Actors    : Ewan McGregor
    #                 Liam Neeson
    #                 Natalie Portman
    #
    #Title : Star Wars: Episode III - Revenge of the Sith
    #   - Year      : 2005
    #   - Director  : George Lucas
    #   - Actors    : Hayden Christensen
    #                 Natalie Portman
    #                 Ewan McGregor
    #
    #Title : Star Wars: Episode II - Attack of the Clones
    #   - Year      : 2002
    #   - Director  : George Lucas
    #   - Actors    : Hayden Christensen
    #                 Natalie Portman
    #                 Ewan McGregor
    #
    #Title : Star Wars: Episode V - The Empire Strikes Back
    #   - Year      : 1980
    #   - Director  : Irvin Kershner
    #   - Actors    : Mark Hamill
    #                 Harrison Ford
    #                 Carrie Fisher
    #
    #Title : Star Wars: Episode VI - Return of the Jedi
    #   - Year      : 1983
    #   - Director  : Richard Marquand
    #   - Actors    : Mark Hamill
    #                 Harrison Ford
    #                 Carrie Fisher
    #
    #Title : American Graffiti
    #   - Year      : 1973
    #   - Director  : George Lucas
    #   - Actors    : Richard Dreyfuss
    #                 Ron Howard
    #                 Paul Le Mat
    #
    #Title : THX 1138
    #   - Year      : 1971
    #   - Director  : George Lucas
    #   - Actors    : Robert Duvall
    #                 Donald Pleasence
    #                 Don Pedro Colley
    #
    #Title : Ender's Game
    #   - Year      : 2013
    #   - Director  : Gavin Hood
    #   - Actors    : Harrison Ford
    #                 Asa Butterfield
    #                 Hailee Steinfeld

# Requête avec range, filtre sur les valeurs numériques
# Films de James Cameron, dont le rang est inférieur à 1000
"""res = es.search(index='movies2',body=
        {
         "_source": [ "fields.rank", "fields.directors", "fields.title" ],
         "query": {
                   "bool": {
                            "should": [
                                         { "match": { "fields.directors": "James Cameron" }}
                                       , { "range": { "fields.rank": {"lt":1000} }}
                                      ]
                           }
                  }
        }
)

#print(res)
l = len(res['hits']['hits'])
print("Nb enreg. : {}".format(l))
print("")
i = 0
while i < len(res['hits']['hits']):
    title = res['hits']['hits'][i]['_source']['fields']['title']
    rank = res['hits']['hits'][i]['_source']['fields']['rank']
    print("Title : {}".format(title))
    print("   - Rank      : {}".format(rank))
    j = 0
    while j < len(res['hits']['hits'][i]['_source']['fields']['directors']):
        director = res['hits']['hits'][i]['_source']['fields']['directors'][j]
        if j == 0:
            if len(res['hits']['hits'][i]['_source']['fields']['directors']) > 1:
                print("   - Directors : {}".format(director))
            else:
                print("   - Director  : {}".format(director))
        else:
            print("                 {}".format(director))
        j += 1
    i += 1
    print("")"""

    #D:\Python>python3 es.py
    #Nb enreg. : 10
    #
    #Title : Titanic
    #   - Rank      : 111
    #   - Director  : James Cameron
    #
    #Title : Avatar
    #   - Rank      : 283
    #   - Director  : James Cameron
    # 
    #Title : Terminator 2: Judgment Day
    #   - Rank      : 420
    #   - Director  : James Cameron
    #
    #Title : Aliens
    #   - Rank      : 552
    #   - Director  : James Cameron
    #
    #Title : The Terminator
    #   - Rank      : 678
    #   - Director  : James Cameron
    #
    #Title : Avatar 2
    #   - Rank      : 909
    #   - Director  : James Cameron
    #
    #Title : True Lies
    #   - Rank      : 2067
    #   - Director  : James Cameron
    #
    #Title : The Abyss
    #   - Rank      : 2470
    #   - Director  : James Cameron
    #
    #Title : We Bought a Zoo
    #   - Rank      : 567
    #   - Director  : Cameron Crowe
    #
    #Title : Almost Famous
    #   - Rank      : 599
    #   - Director  : Cameron Crowe
    
# Utilisation de la clé must/ et match_phrase pour tomber sur les valeurs demandées
"""res = es.search(index='movies2',body=
        {
         "_source": [ "fields.rank", "fields.directors", "fields.title" ],
         "query": {
                   "bool": {
                            "must": [
                                         { "match_phrase": { "fields.directors": "James Cameron" }}
                                       , { "range": { "fields.rank": {"lt":1000} }}
                                      ]
                           }
                  }
        }
)

#print(res)
l = len(res['hits']['hits'])
print("Nb enreg. : {}".format(l))
print("")
i = 0
while i < len(res['hits']['hits']):
    title = res['hits']['hits'][i]['_source']['fields']['title']
    rank = res['hits']['hits'][i]['_source']['fields']['rank']
    print("Title : {}".format(title))
    print("   - Rank      : {}".format(rank))
    j = 0
    while j < len(res['hits']['hits'][i]['_source']['fields']['directors']):
        director = res['hits']['hits'][i]['_source']['fields']['directors'][j]
        if j == 0:
            if len(res['hits']['hits'][i]['_source']['fields']['directors']) > 1:
                print("   - Directors : {}".format(director))
            else:
                print("   - Director  : {}".format(director))
        else:
            print("                 {}".format(director))
        j += 1
    i += 1
    print("")"""

    #D:\Python>python3 es.py
    #Nb enreg. : 6
    #
    #Title : Titanic
    #   - Rank      : 111
    #   - Director  : James Cameron
    #
    #Title : Avatar
    #   - Rank      : 283
    #   - Director  : James Cameron
    #
    #Title : Terminator 2: Judgment Day
    #   - Rank      : 420
    #   - Director  : James Cameron
    #
    #Title : Aliens
    #   - Rank      : 552
    #   - Director  : James Cameron
    #
    #Title : The Terminator
    #   - Rank      : 678
    #   - Director  : James Cameron
    #
    #Title : Avatar 2
    #   - Rank      : 909
    #   - Director  : James Cameron

"""res = es.search(index='movies2',body=
        {
         "_source": [ "fields.rating", "fields.directors", "fields.title", "fields.genres" ],
         "query": {
                   "bool": {
                            "must": [
                                       { "match_phrase": { "fields.directors": "James Cameron" }}
                                     , { "range": { "fields.rating": {"gte":5} }}
                                    ],
                            "must_not": [
                                           {"match":{"fields.genres":"Action"}}
                                         , {"match":{"fields.genres":"Drama"}} 
                                        ]
                           }
                  }
        }
)
print(res)"""

    #D:\Python>python3 es.py
    #{  'took': 6
    # , 'timed_out': False
    # , '_shards': { 'total': 1, 'successful': 1, 'skipped': 0, 'failed': 0 }
    # , 'hits': {  'total': {'value': 0, 'relation': 'eq'}
    #            , 'max_score': None
    #            , 'hits': []
    #           }
    #}
    # --> pas de résultat, donc James Cameron réalise des films d'Action, ou des Dramas

# Ecriture avec un filtre sur la date de release (le format date est automatiquement reconnu)
'''res = es.search(index='movies2',body=
        {
         "_source": [ "fields.rating", "fields.directors", "fields.title", "fields.genres", "fields.release_date" ],
         "query": {
                   "bool": {
                              "must": { "match_phrase": { "fields.directors": "J.J. Abrams" }}
                            , "filter": {"range": {"fields.release_date": { "from": "2010-01-01", "to": "2015-12-31"}}}
                           }
                  }
        }
)
print(res)'''

    #D:\Python>python3 es.py
    #{  'took': 2
    # , 'timed_out': False
    # , '_shards': {'total': 1, 'successful': 1, 'skipped': 0, 'failed': 0}
    # , 'hits': {  'total': {'value': 3, 'relation': 'eq'}
    #            , 'max_score': 14.205162
    #            , 'hits': [ {  '_index': 'movies2'
    #                         , '_type': 'movie'
    #                         , '_id': '16'
    #                         , '_score': 14.205162
    #                         , '_source': { 'fields': {  'release_date': '2013-05-02T00:00:00Z'
    #                                                   , 'genres': ['Action', 'Adventure', 'Sci-Fi']
    #                                                   , 'directors': ['J.J. Abrams']
    #                                                   , 'rating': 7.9
    #                                                   , 'title': 'Star Trek Into Darkness' } }
    #                        }
    #                        ,
    #                        {  '_index': 'movies2'
    #                         , '_type': 'movie'
    #                         , '_id': '168'
    #                         , '_score': 14.205162
    #                         , '_source': { 'fields': {  'release_date': '2015-01-01T00:00:00Z'
    #                                                   , 'genres': ['Action', 'Adventure', 'Fantasy', 'Sci-Fi']
    #                                                   , 'directors': ['J.J. Abrams']
    #                                                   , 'title': 'Star Wars: Episode VII' } }
    #                        }
    #                        ,
    #                        {  '_index': 'movies2'
    #                         , '_type': 'movie'
    #                         , '_id': '557'
    #                         , '_score': 14.205162
    #                         , '_source': { 'fields': {  'release_date': '2011-06-09T00:00:00Z'
    #                                                   , 'genres': ['Mystery', 'Sci-Fi', 'Thriller']
    #                                                   , 'directors': ['J.J. Abrams']
    #                                                   , 'rating': 7.1
    #                                                   , 'title': 'Super 8' } }
    #                        } ]
    #            }
    #}

# Aggrégat simple
"""res = es.search(index='movies2',body=
        {
         "aggs": {
                   "nb_par_annees": {
                                     "terms": { "field": "fields.year" }
                                    }
                 }
        }
)
print(res)"""

    #{
    #    ...,
    #    'aggregations': {
    #        'nb_par_annees': {
    #              'doc_count_error_upper_bound': 0
    #            , 'sum_other_doc_count': 2192
    #            , 'buckets': [
    #                  {'key': 2013, 'doc_count': 448}
    #                , {'key': 2012, 'doc_count': 404}
    #                , {'key': 2011, 'doc_count': 308}
    #                , {'key': 2009, 'doc_count': 253}
    #                , {'key': 2010, 'doc_count': 249}
    #                , {'key': 2008, 'doc_count': 207}
    #                , {'key': 2006, 'doc_count': 204}
    #                , {'key': 2007, 'doc_count': 200}
    #                , {'key': 2005, 'doc_count': 170}
    #                , {'key': 2014, 'doc_count': 152}
    #              ]
    #)))}

# Calcul de moyennne
"""res = es.search(index='movies2',body=
        {
         "aggs": {
                   "note_moyenne": {
                                     "avg": { "field": "fields.rating" }
                                    }
                 }
        }
)
print(res)"""

    #{
    #    ...,
    #    'aggregations': {
    #      'note_moyenne': {
    #        'value': 6.387107691895831
    #}}}

# Combinaison d'aggrégats
"""res = es.search(index='movies2',body=
        {
           "query": {
                     "bool": {
                              "must": { "match_phrase": { "fields.directors": "George Lucas"} }
                             }
                    }
         , "aggs": {
                      "note_moyenne": { "avg": { "field": "fields.rating" }}
                    , "rang_moyen":   { "avg": { "field": "fields.rank" }}
                   }
        }
)
print(res)"""

    #{
    #    ...,
    #    'aggregations': {
    #        'note_moyenne': {
    #          'value': 7.299999872843425
    #                      }
    #      , 'rang_moyen': {
    #          'value': 1029.1666666666667
    #}}}

# Calcul par année, le note moyenne des films --> aggrégat dans un aggrégat
"""res = es.search(index='movies2',body=
        {
         "aggs": {
                  "group_year": {
                                   "terms": { "field": "fields.year" }
                                 , "aggs": {
                                            "note_moyenne": { "avg": { "field": "fields.rating"}}
                                           }
                                }
                 }
        }
)
print(res)"""

    #{
    #    ...,
    #    'aggregations': {
    #        'group_year': {
    #              'doc_count_error_upper_bound': 0
    #            , 'sum_other_doc_count': 2192
    #            , 'buckets': [
    #                    {  'key': 2013, 'doc_count': 448, 'note_moyenne': {'value': 5.962700002789497}}
    #                  , {  'key': 2012, 'doc_count': 404, 'note_moyenne': {'value': 5.961786593160322}}
    #                  , {  'key': 2011, 'doc_count': 308, 'note_moyenne': {'value': 6.114285714440531}}
    #                  , {  'key': 2009, 'doc_count': 253, 'note_moyenne': {'value': 6.268774692248921}}
    #                  , {  'key': 2010, 'doc_count': 249, 'note_moyenne': {'value': 6.239759046868627}}
    #                  , {  'key': 2008, 'doc_count': 207, 'note_moyenne': {'value': 6.230917865527425}}
    #                  , {  'key': 2006, 'doc_count': 204, 'note_moyenne': {'value': 6.31617646708208}}
    #                  , {  'key': 2007, 'doc_count': 200, 'note_moyenne': {'value': 6.419499988555908}}
    #                  , {  'key': 2005, 'doc_count': 170, 'note_moyenne': {'value': 6.289999998317045}}
    #                  , {  'key': 2014, 'doc_count': 152, 'note_moyenne': {'value': 4.860000133514404}}
    #               ]}}}
    
# Tru de résultats
"""res = es.search(index='movies2',body=
        {
         "size": 0,
         "aggs": {
                  "group_year": {
                                   "terms": {
                                               "field": "fields.year"
                                             , "order": { "note_moyenne" : "asc" }
                                             , "size": 10000
                                            }
                                 , "aggs": {
                                            "note_moyenne": { "avg": { "field": "fields.rating"}}
                                           }
                                }
                 }
        }
)
print(res)"""

    #{
    #    ...,
    #    'aggregations': {
    #        'group_year': {
    #              'doc_count_error_upper_bound': 0
    #            , 'sum_other_doc_count': 0
    #            , 'buckets': [
    #                    {'key': 2014, 'doc_count': 152, 'note_moyenne': {'value': 4.860000133514404}}
    #                  , {'key': 2012, 'doc_count': 404, 'note_moyenne': {'value': 5.961786593160322}}
    #                  , {'key': 2013, 'doc_count': 448, 'note_moyenne': {'value': 5.962700002789497}}
    #                  , {'key': 2011, 'doc_count': 308, 'note_moyenne': {'value': 6.114285714440531}}
    #                  , {'key': 2003, 'doc_count': 133, 'note_moyenne': {'value': 6.227067664153594}}
    #                  , {'key': 2008, 'doc_count': 207, 'note_moyenne': {'value': 6.230917865527425}}
    #                  , {'key': 2010, 'doc_count': 249, 'note_moyenne': {'value': 6.239759046868627}}
    #                  , {'key': 2009, 'doc_count': 253, 'note_moyenne': {'value': 6.268774692248921}}
    #                  , {'key': 2005, 'doc_count': 170, 'note_moyenne': {'value': 6.289999998317045}}
    #                  , {'key': 2006, 'doc_count': 204, 'note_moyenne': {'value': 6.31617646708208}}
    #                  , {'key': 2001, 'doc_count': 127, 'note_moyenne': {'value': 6.363779541075699}}
    #                  , {'key': 2000, 'doc_count': 106, 'note_moyenne': {'value': 6.36886794837016}}
    #                  , {'key': 1996, 'doc_count': 82, 'note_moyenne': {'value': 6.369512191632899}}
    #     ...

# Aggrégats par plages de valeurs
"""res = es.search(index='movies2',body=
        {
         "size": 0,
         "aggs": {
                  "group_range": {
                                   "range": {
                                               "field": "fields.rating"
                                             , "ranges": [
                                                            { "to" : 1.9 }
                                                          , { "from": 2, "to": 3.9 }
                                                          , { "from": 4, "to": 5.9 }
                                                          , { "from": 6, "to": 7.9 }
                                                          , { "from": 8 }
                                                         ]                                                          
                                             #, "size": 10000
                                            }
                                }
                 }
        }
)
print(res)"""

    #{
    #    ...,
    #    'aggregations': {
    #        'group_range': {
    #              'buckets': [
    #                    {'key': '*-1.9', 'to': 1.9, 'doc_count': 8}
    #                  , {'key': '2.0-3.9', 'from': 2.0, 'to': 3.9, 'doc_count': 106}
    #                  , {'key': '4.0-5.9', 'from': 4.0, 'to': 5.9, 'doc_count': 1182}
    #                  , {'key': '6.0-7.9', 'from': 6.0, 'to': 7.9, 'doc_count': 2751}
    #                  , {'key': '8.0-*', 'from': 8.0, 'doc_count': 299
    #}]}}}

# Aggrégats de type raw (données telle quelle, complète)
res = es.search(index='movies2',body=
        {
         "aggs": {
                  "group_actors": {
                                   "terms": { "field": "fields.actors.raw" }
                                   }
                 }
        }
)
print(res)