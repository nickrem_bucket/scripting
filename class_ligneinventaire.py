class LigneInventaire:

    """Classe représentant une ligne d'un inventaire de vente.

    Attributs attendus par le constructeur :
        produit -- le nom du produit
        prix -- le prix unitaire du produit
        quantite -- la quantité vendue du produit.

    """

    def __init__(self, produit, prix, quantite):
        self.produit = produit
        self.prix = prix
        self.quantite = quantite

    def __repr__(self):
        return "<Ligne d'inventaire {} ({}X{})>".format(
                self.produit, self.prix, self.quantite)

# Création de l'inventaire
inventaire = [
    LigneInventaire("pomme rouge", 1.2, 19),
    LigneInventaire("orange", 1.4, 24),
    LigneInventaire("banane", 0.9, 21),
    LigneInventaire("poire", 1.2, 24),
]

print("")
print("Liste de départ")
print(inventaire)

from operator import attrgetter

print("")
print("Tri sur prix et quantité")
print(sorted(inventaire, key=attrgetter("prix", "quantite")))

print("")
print("Tri sur prix croissant et quantité décroissante en deux étapes :")
print("     - 1ère étape : tri décroissant de quantité")
print("     - 2ème étape : tri croissant du prix sur le résultat de la 1ère")
inventaire.sort(key=attrgetter("quantite"), reverse=True)
print(sorted(inventaire, key=attrgetter("prix")))