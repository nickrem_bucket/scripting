import yaml

file = "d:\\data\\module\\params\\database.yaml"

with open(file, 'r') as ymlfile:
	cfg = yaml.load(ymlfile)

software = "mariadb"
in_section = False
for section in cfg:
    if section == software:
        in_section = True

db = {}
if in_section == True:
    #print(cfg[software])
    db = cfg[software]
    print(db)

#print(cfg['mysql']['database'])
#print(cfg['postgresql']['database'])